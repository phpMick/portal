<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_user', function(Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('customer_id');
            $table->unique(['user_id', 'customer_id']);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign(('customer_id'))->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_user');
    }
}
