<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agreements', function($table)
        {
            $table->dropForeign('agreements_customer_id_foreign');
        });

        Schema::table('agreements', function($table)
        {
            $table->renameColumn('customer_id','user_group_id');
        });

       Schema::table('agreements', function($table)
        {
            $table->bigInteger('user_group_id')->unsigned()->change();
            $table->foreign('user_group_id')->references('id')->on('user_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agreements', function ($table) {
            $table->dropForeign('agreements_user_group_id_foreign');
        });

        Schema::table('agreements', function ($table) {
            $table->renameColumn('user_group_id', 'customer_id');

        });
        Schema::table('agreements', function ($table) {
            $table->foreign('customer_id')->references('id')->on('customers');

        });
    }
}
