<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerIdToTenants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aad_tenants', function($table)
        {
            $table->unsignedInteger('customer_id')->nullable();
        });

        Schema::table('aad_tenants', function(Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customers');
        });


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agreements', function ($table) {
            $table->dropColumn('customer_id');
        });
    }
}
