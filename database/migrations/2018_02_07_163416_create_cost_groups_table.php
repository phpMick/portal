<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostGroupsTable extends Migration {

	public function up()
	{
		Schema::create('cost_groups', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('agreement')->nullable();
			$table->string('subscription')->nullable();
			$table->string('resource_group')->nullable();
			$table->string('resource')->nullable();
			$table->string('tag_key')->nullable();
			$table->string('tag_value')->nullable();

		});
	}

	public function down()
	{
		Schema::drop('cost_groups');
	}
}