<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostGroupUserGroupTable extends Migration {

	public function up()
	{
		Schema::create('cost_group_user_group', function(Blueprint $table) {
			$table->bigInteger('cost_group_id')->unsigned();
			$table->bigInteger('user_group_id')->unsigned();
		});




	}

	public function down()
	{
		Schema::drop('user_group_cost_group');
	}
}