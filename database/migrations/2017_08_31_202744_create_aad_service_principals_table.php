<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAadServicePrincipalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aad_service_principals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->string('app_identifier');
            $table->string('object_guid');
            $table->tinyInteger('status')->default(0);
            $table->text('permissions')->nullable();

            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('aad_tenants');
            $table->unique(['tenant_id', 'app_identifier']); // only one service principal per tenant per application
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aad_service_principals');
    }
}
