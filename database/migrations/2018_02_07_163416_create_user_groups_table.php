<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserGroupsTable extends Migration {

	public function up()
	{
		Schema::create('user_groups', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
            $table->integer('type');
            $table->nestedSet();
            $table->string('properties')->nullable();
            $table->string('parent_name')->nullable();

		});
	}

	public function down()
	{
		Schema::drop('user_groups');
	}
}