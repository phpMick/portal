<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetsTable extends Migration {

	public function up()
	{
		Schema::create('budgets', function(Blueprint $table) {
			$table->bigInteger('cost_group_id')->unsigned();
			$table->decimal('amount');
			$table->date('start_date');
			$table->date('end_date');
            $table->text('details')->nullable();
		});

	}

	public function down()
	{
		Schema::drop('budgets');
	}
}