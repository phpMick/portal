<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('agreement_id')->nullable();
            $table->string('guid');
            $table->unsignedInteger('aad_tenant_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('name')->nullable();
            $table->string('partner_name')->nullable();
            $table->string('azure_name')->nullable();
            $table->string('offer_code')->nullable();
            $table->timestamps();

            $table->unique(['agreement_id', 'guid']); // allow a subscription to exist in multiple agreements (as they can be moved)
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('agreement_id')->references('id')->on('agreements');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_subscriptions');
    }
}
