<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeysAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budgets', function(Blueprint $table) {
            $table->foreign('cost_group_id')->references('id')->on('cost_groups');
        });

        Schema::table('cost_group_user_group', function(Blueprint $table) {
            $table->foreign('cost_group_id')->references('id')->on('cost_groups');
            $table->foreign('user_group_id')->references('id')->on('user_groups');
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('cost_group_user_group', function(Blueprint $table) {
            $table->dropForeign('user_group_cost_group_cost_group_id_foreign');
            $table->dropForeign('user_group_cost_group_user_group_id_foreign');
        });


        Schema::table('budgets', function(Blueprint $table) {
            $table->dropForeign('budgets_cost_group_id_foreign');
        });
    }
}
