<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureSubscriptionUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_subscription_usages', function (Blueprint $table) {
            $prec = config('app.decimal_high.precision');
            $scale = config('app.decimal_high.scale');

            $table->bigIncrements('id');
            $table->unsignedInteger('subscription_id');
            $table->date('usage_date');
            $table->char('usage_month', 7);
            $table->unsignedInteger('service_id');
            $table->decimal('total_cost', $prec, $scale)->nullable();
            $table->timestamps();

            $table->unique(['subscription_id', 'usage_date', 'service_id'], 'azure_subscription_usage_combined_unique');
            $table->foreign('subscription_id')->references('id')->on('azure_subscriptions');
            $table->foreign('service_id')->references('id')->on('azure_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_subscription_usages');
    }
}
