<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserGroupToMargins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('agreement_margins', function($table)
        {
            $table->bigInteger('user_group_id')->default('0')->unsigned();
        });

        Schema::table('agreement_margins', function(Blueprint $table) {
            $table->foreign('user_group_id')->references('id')->on('user_groups');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agreement_margins', function(Blueprint $table) {
            $table->dropForeign('agreement_margins_user_group_id_foreign');
        });


        Schema::table('agreement_margins', function ($table) {
            $table->dropColumn('user_group_id');
        });


    }
}
