<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementMarginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_margins', function (Blueprint $table) {
            $prec = config('app.decimal_low.precision');
            $scale = config('app.decimal_low.scale');

            $table->increments('id');
            $table->unsignedInteger('agreement_id');
            $table->decimal('margin', $prec, $scale)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('agreement_id')->references('id')->on('agreements');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_margins');
    }
}
