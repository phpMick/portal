<?php

use Illuminate\Database\Seeder;


//Models
use App\Models\User;
use App\Models\UserGroup;
use App\Models\Agreement;

class AgreementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //just make a few under the master group
        $masterGroup = UserGroup::where('name' ,'=', UserGroup::MASTER_NAME)->first();


        for($i = 0 ; $i <10; $i++){
            $agreement = factory(Agreement::class)->create(['user_group_id' => $masterGroup->id]);

        }
    }

    //php artisan db:seed --class=AgreementsSeeder

}


