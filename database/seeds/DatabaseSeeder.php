<?php

use Illuminate\Database\Seeder;

//Models
use App\Models\User;
use App\Models\UserGroup;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call('UsersSeeder');
        $this->call('AgreementsSeeder');

    }
}
