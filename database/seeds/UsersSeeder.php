<?php

use Illuminate\Database\Seeder;


//Models
use App\Models\User;
use App\Models\UserGroup;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //create Master
        $userGroup = factory(UserGroup::class)->create([
            'name' => UserGroup::MASTER_NAME,
            'type' => UserGroup::MASTER_TYPE
        ]);

        //create me and Chris
        $chris = new User();
        $chris->name = 'Chris (Admin)';
        $chris->email = 'chris@cspeer.me';
        $chris->password = bcrypt('password');
        $chris->user_group_id = $userGroup->id;
        $chris->role = User::ADMIN;
        $chris->save();

        $mick = new User();
        $mick->name = 'Mick (Admin)';
        $mick->email = 'mick@yahoo.com';
        $mick->password = bcrypt('password');
        $mick->user_group_id = $userGroup->id;
        $mick->role = User::ADMIN;
        $mick->save();

        $masterUserGroup = UserGroup::getMasterGroup();

        //Make the Bytes group and attach to master
        $masterUserGroup->children()->create([
            'name' => 'Bytes',
            'type' => UserGroup::PARTNER_TYPE
        ]);


        $bytesUserGroup = UserGroup::where('name','=','Bytes')->first();

        //make Bytes Admin
        $bytes_admin = factory(User::class)->create([
            'name' => 'Bytes Admin',
            'email' => 'bytes_admin@yahoo.com',
            'user_group_id'=>$bytesUserGroup->id,
            'role'=>User::ADMIN
        ]);


        //add a customer
        $customer = factory(UserGroup::class,'customer')->make();

        $bytesUserGroup->appendNode($customer);

        //add a customer admin and a user
        $customer_admin = factory(User::class)->create([
            'name' => 'Customer Admin',
            'email' => 'customer_admin@yahoo.com',
            'user_group_id'=>$customer->id,
            'role'=>User::ADMIN

        ]);

        $customer_user = factory(User::class)->create([
            'name' => 'Customer User',
            'email' => 'customer_user@yahoo.com',
            'user_group_id'=>$customer->id,
            'role'=>User::USER
        ]);

    }

}


