<?php

// Models

use App\Models\User;
use App\Models\UserGroup;
use App\Models\Customer;
use App\Models\AgreementFactory;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */


/**
 * Customer User
 *
 */
$factory->define(User::class, function(Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'password' => bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

//default user group
$factory->define(UserGroup::class, function(\Faker\Generator $faker) {
    return  [
        'name' =>  $faker->company
    ];
});

//customer user group
$factory->define(UserGroup::class, function(\Faker\Generator $faker) {
    return  [
        'name' =>  $faker->company,
         'type' => UserGroup::CUSTOMER_TYPE,

        //'properties' => json_encode(['code'=>strtoupper($faker->unique()->bothify('??#####'))])
        'properties' => ['code'=>strtoupper($faker->unique()->bothify('??#####'))]

    ];
},'customer');


$factory->define(\App\Models\Agreement::class, function(\Faker\Generator $faker) {
    return [
        'user_group_id' => factory(UserGroup::class,'customer')->create()->id,
        'name' => $faker->word,
        'type' => $faker->randomElement([
            AgreementFactory::TYPE_AZURE_PAYG,
            AgreementFactory::TYPE_AZURE_EA,
            AgreementFactory::TYPE_AZURE_CSP,
            AgreementFactory::TYPE_AZURE_SCE
        ])
    ];
});

$factory->define(\App\Models\Microsoft\AadTenant::class, function(\Faker\Generator $faker) {
    return [
        'guid' => $faker->word,
        'user_group_id' => factory(\App\Models\UserGroup::class)->create(['type' => UserGroup::CUSTOMER_TYPE])->id
    ];
});


$factory->define(\App\Models\Microsoft\AadUser::class, function(\Faker\Generator $faker) {
    return [
        'user_id' => factory(\App\Models\User::class)->create()->id,
        'tenant_id' => factory(\App\Models\Microsoft\AadTenant::class)->create()->id
    ];
});





