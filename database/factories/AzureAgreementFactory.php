<?php

use Faker\Generator as Faker;
use App\Models\Agreement;


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Agreement::class, function (Faker $faker) {
    static $type;

    if(empty($type)) {
        $type = Agreement::TYPE_ALL[$faker->numberBetween(0, count(Agreement::TYPE_ALL)-1)];
    }

    return [
        'type' => $type,
        'status' => Agreement::STATUS_NEW,
        'name' => $faker->sentence(5),
        'identifier' => (string) $faker->numberBetween(100000, 9999999),
        'margin' => $type==Agreement::TYPE_CSP ? $faker->numberBetween(0,200)/10 : NULL,
        'customer_id' => function() {
            return factory(\App\Models\Customer::class)->create()->id;
        },
    ];
});

/*$factory->state(Agreement::class, 'sce-invalid-secret', [
    'type' => Agreement::TYPE_SCE,
    'access_key' => env('AZURE_TESTEA_KEY'),
    'access_secret' => 'testing123',
]);

$factory->state(Agreement::class, 'sce-invalid-key', [
    'type' => Agreement::TYPE_SCE,
    'access_key' => '123456',
    'access_secret' => env('AZURE_TESTEA_SECRET'),
]);

$factory->state(Agreement::class, 'sce-valid', [
    'type' => Agreement::TYPE_SCE,
    'access_key' => env('AZURE_TESTEA_KEY'),
    'access_secret' => env('AZURE_TESTEA_SECRET'),
]);*/
