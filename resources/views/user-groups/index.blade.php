@extends('layouts/main')

@section('title', 'List User Groups')


@section('include-css')
    <link  href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
@endsection

@section('content')



<div class="container">

    <div class="row">
        <div class="col-xs-12">
            <h1>User Groups
            @can('create', \App\Models\UserGroup::class)
                <a href="{{ route('user-groups.create') }}" class="btn btn-primary pull-right">Add User Group</a>
            @endcan
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <table class="table table-bordered table-striped" id ="user-groups">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Parent</th>
                        <th>Operations</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($userGroups as $userGroup)
                            <tr>
                                <td>{{ $userGroup->code }}</td>
                                <td>{{ $userGroup->name }}</td>
                                <td>{{ $userGroup->getTypeName() }}</td>
                                <td>{{ $userGroup->getParentName() }}</td>
                                <td>
                                @if($userGroup->name != \App\Models\UserGroup::MASTER_NAME && $userGroup->name != 'Bytes' )
                                    <a href="{{ route('user-groups.show', $userGroup->id) }}" class="btn btn-default pull-left" style="margin-right:4px">View Details</a>
                                    @if($canEdit)
                                            <a href="{{ route('user-groups.edit', $userGroup->id) }}" class="btn btn-info pull-left" style="margin-right:4px">Edit</a>
                                            <a class="btn btn-danger" onclick="destroy({{$userGroup->id}})">Delete</a>
                                    @endif
                                    </td>
                                @endif
                            </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div> {{--row--}}



</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/user-groups-common.js') }}"></script>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#user-groups').DataTable();
    } );

</script>
@endsection