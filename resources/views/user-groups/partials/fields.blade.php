



{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$userGroup->name) }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>


{{--Type--}}
<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
    <label for="type" class="col-md-4 control-label">Type</label>

    <div class="col-md-6">

        <select class="form-control" name="type">
            @foreach($userGroupTypes as $userGroupType)
                <option value="{{$userGroupType['id']}}"  @if(old('type',$userGroup->type) == "{$userGroupType['id']}") selected = 'selected'  @endif> {{$userGroupType['description']}}</option>
            @endforeach
        </select>


        @if ($errors->has('type'))
            <span class="help-block"><strong>{{ $errors->first('type') }}</strong></span>
        @endif
    </div>
</div>


{{--Code--}}
<div class="form-group{{ $errors->has('properties.code') ? ' has-error' : '' }}">
    <label for="code" class="col-md-4 control-label">Customer ID/Reference</label>

    <div class="col-md-6">
        <input id="code" type="text" class="form-control" name="properties[code]" value="{{ old('properties.code',$userGroup->properties['code']) }}">

        @if ($errors->has('details.code'))
            <span class="help-block"><strong>{{ $errors->first('properties.code') }}</strong></span>
        @endif
    </div>
</div>

