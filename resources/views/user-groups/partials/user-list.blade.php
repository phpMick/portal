<div class="panel panel-default">
    <div class="panel-heading">Users</div>
    <div class="panel-body">


        <table class="table table-bordered table-striped" id ="users">
            <thead>
            <tr>
                <th>Name</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->roleName() }}</td>
                </tr>
            @endforeach

            </tbody>

        </table>


    </div> {{--panel body--}}
</div>
