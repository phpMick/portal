@extends('layouts/main')

@section('title', 'Edit User Group')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User Group</div>
                    <div class="panel-body">
                        @include('partials/forms/errors')
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user-groups.update',[$userGroup->id]) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}


                            @include('user-groups/partials/fields')
                          
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update User Group
                                    </button>

                                    <a class="btn btn-link" href="{{ route('user-groups.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection