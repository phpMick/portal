@extends('layouts/main')

@section('title', 'User Group Administration')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-xs-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">User Group Administration</h1>
                    </div>
                    <div class="panel-body">

                        @if($aadLinked)
                            <a href="{{ route('login.aad.start')}}" class="btn btn-primary disabled">Allow Microsoft Login</a>
                        @else
                            <a href="{{ route('login.aad.start')}}" class="btn btn-primary">Allow Microsoft Login</a>
                        @endif

                    </div>

                    <div class="panel-footer">

                    </div>

                </div>



            </div>
        </div>

    </div>

@endsection