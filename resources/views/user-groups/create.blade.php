@extends('layouts/main')

@section('title', 'Add a User Group')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add a User Group</div>
                    <div class="panel-body">
                        @include('partials/forms/errors')
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user-groups.store') }}">
                            {{ csrf_field() }}


                            @include('user-groups/partials/fields')

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add User Group
                                    </button>

                                    <a class="btn btn-link" href="{{ route('user-groups.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection