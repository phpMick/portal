@extends('layouts/main')

@section('title', 'User Group')

@section('include-css')
    <link  href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">User Group</div>
                    <div class="panel-body">

                        {{--Name--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Name</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$userGroup->name}}</span>
                            </div>
                        </div>

                        {{--Type--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Type</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$userGroupType}}</span>
                            </div>
                        </div>

                        {{--Code--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Customer ID/Reference</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$userGroup->code}}</span>
                            </div>
                        </div>




                    </div>
                    <div class="panel-footer">

                        @can('update',$userGroup)
                            <a class="btn btn-primary mr1" href="{{ route('user-groups.edit', [$userGroup]) }}">Edit User Group</a>
                        @endcan

                        @can('destroy',$userGroup)
                            <a class="btn btn-danger" onclick="destroy({{$userGroup->id}})">Delete User Group</a>
                        @endcan


                    </div>
                </div> {{--panel--}}


                @include('user-groups/partials/user-list')

                <div>
                    <a class="btn btn-default" a href="{{ route('user-groups.index') }}"><span class="icon-return"></span>Back to User Groups list</a>
                </div>


            </div>
        </div> {{--container--}}
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/user-groups-common.js') }}"></script>
@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );

    </script>
@endsection