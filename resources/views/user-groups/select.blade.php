@extends('layouts/main')

@section('title', 'Select User Group')

@section('include-css')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-xs-8">
                <h1>Select a User Group</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-10">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('user-groups.select') }}">
                    {{ csrf_field() }}
                    <div id="app"> {{--The Vue application--}}
                        <bss-tree
                                options-string="{{$userGroups}}"
                                title="{{$title}}"
                                initial-id="{{$user->user_group_id}}">
                        </bss-tree>
                    </div> {{--app--}}

                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary pull-right">
                                Select User Group
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>




@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#user-groups').DataTable();
        });

    </script>
@endsection