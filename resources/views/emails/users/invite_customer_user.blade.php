@component('mail::message')
# Hello

Someone has added you as a user to our site. Please click the appropriate button to accept or reject the invitation.

@component('mail::button', ['url' => '', 'color'=>'green'])
Accept Invite
@endcomponent

@component('mail::button', ['url' => '', 'color'=>'red'])
    Reject Invite
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
