@component('mail::message')
# Hello {{$user->name}}

Someone has added you as a user to our site. Please click the appropriate button to accept or reject the invitation.


@component('mail::button', ['url' => route('new-user-reset',['token ' => $token,'id' => $user->id]), 'color'=>'green'])

    Accept Invite
@endcomponent

@component('mail::button', ['url' => '', 'color'=>'red'])
    Reject Invite
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
