<div class="container">

    {{--Message Bar--}}

    @if(Session::has('success'))
        <div class="alert alert-success  alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             {{ Session::get('message') }}.
        </div>
    @endif


    @if(Session::has('info'))
        <div class="alert alert-info alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             {{ Session::get('message') }}.
        </div>
    @endif


    @if(Session::has('warning'))
        <div class="alert alert-warning alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}.
        </div>
    @endif


    @if(Session::has('danger'))
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}.
        </div>
    @endif

</div>