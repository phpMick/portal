<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration Functions <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{ route('user-groups.index') }}">User Groups</a></li>
        <li><a href="{{ route('user-groups.select-list') }}">Select a User Group</a></li>
        <li><a href="{{ route('users.index') }}">Users</a></li>
        <li><a href="{{ route('user-groups.admin') }}">Administration</a></li>
    </ul>
</li>