<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Selected User Groups:

        @if (session()->has('selectedUserGroup'))
            {{ count(session()->get('selectedUserGroup')) }}
        @else
            All
        @endif
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu customer-select" role="menu">
        @if(session()->has('selectedUserGroup') AND session()->get('selectedUserGroup'))
            <li>Filtered to:</li>
            <li class="customer-link"><a href="{{ route('user-groups.show', [$selectedGroupId]) }}">{{$selectedGroupName}}</a></li>
        @endif
        <li><a href="{{ route('user-groups.select-none') }}">Show all User Groups</a></li>
        <li><a href="{{ route('user-groups.select-list') }}">Choose User Group</a></li>
    </ul>
</li>