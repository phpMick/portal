<li><a href="{{ route('home') }}">Dashboard</a></li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cloud Services <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/agreements">Agreements</a></li>
    </ul>
</li>

@can('seeAdminMenuOptions')
    @include('partials/nav/main-menu-admin')
@endcan


