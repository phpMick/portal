{{--Agreement Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Agreement Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old( 'name',$agreement->name) }}" placeholder="Give this agreement a name" required>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>


{{--Start Date--}}
<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
    <label for="start_date" class="col-md-4 control-label">Start Date</label>

    <div class="col-md-6 date"  id="start-datepicker" >

        <input type="text" id="start_date"  data-date-format='yyyy-mm-dd' data-provide="datepicker"  data-date-autoclose="true" class="form-control" name="start_date" value="{{ old( 'start_date',$agreement->start_date)}}" placeholder="Give this agreement a start date">

        @if ($errors->has('start_date'))
            <span class="help-block"><strong>{{ $errors->first('start_date') }}</strong></span>
        @endif
    </div>
</div>


{{--End Date--}}
<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
    <label for="end_date" class="col-md-4 control-label">End Date</label>

    <div class="col-md-6 date" id="end-datepicker">

        <input id="end_date" type="text"  data-date-format='yyyy-mm-dd'  data-date-autoclose="true" data-provide="datepicker" class="form-control" name="end_date" value="{{ old( 'end_date',$agreement->end_date)}}" placeholder="Give this agreement an end date">

        @if ($errors->has('end_date'))
            <span class="help-block"><strong>{{ $errors->first('end_date') }}</strong></span>
        @endif
    </div>
</div>


{{--Currency--}}
<div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
    <label for="currency" class="col-md-4 control-label">Currency</label>

    <div class="col-md-6">


        <select class="form-control" name="currency">
            <option value=""  @if(old('currency',$agreement->currency) == "") selected = 'selected' @endif   ></option>
            <option value="GBP"  @if(old('currency',$agreement->currency) == "GBP") selected = 'selected' @endif >GBP</option>

        </select>


        @if ($errors->has('currency'))
            <span class="help-block"><strong>{{ $errors->first('currency') }}</strong></span>
        @endif
    </div>
</div>