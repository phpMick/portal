


<div class="form-group{{ $errors->has('customer-id') ? ' has-error' : '' }}">
    <label for="customer" class="col-md-4 control-label">Selected Customer</label>

    <div class="col-md-6 statictext">
        <span>{{ $userGroupName }}</span>
        @if ($errors->has('customer'))
            <span class="help-block"><strong>{{ $errors->first('customer-id') }}</strong></span>
        @endif
    </div>
    <div class="col-md-2">
        <a href="{{ route('user-groups.select-list') }}" class="btn btn-default">Choose...</a>
    </div>
</div>
{{--
<input type="hidden" name="customer_id" id="customer_id" value="{{ old('customer_id') ?: key(session('selected_customers')) }}" />--}}
