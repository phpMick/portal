<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Agreements</h1>
    </div>
    <div class="panel-body">

        @if($agreements->count()==0)
            <p>No agreements have been added yet!</p>
        @else
            <table class="table cell-border table-striped text-middle" id="agreements">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Type</th>
                    <th>Number</th>
                    <th>Number of Active Subscriptions</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($agreements as $agreement)
                    <tr>
                        <td>{{ $agreement->name }}</td>
                        <td>{{ $agreement->getTypeText() ?: '?' }}</td>
                        <td>{{ $agreement->identifier ?: '-' }}</td>
                        <td>{{ $agreement->subscriptions()->count() }}</td>
                        <td>{{ $agreement->getStatusText() }}</td>
                        <td>
                            <div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions&nbsp;<span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                @foreach($agreement->getActionList() as $title=>$link)

                                    @if($title=='Delete')
                                            <li><a href="#" onclick="destroy({{$agreement->id}})" >Delete</a></li>
                                        @else
                                        <li{!! $link==FALSE ? ' class="disabled"' : '' !!}><a href="{!! $link ?: '' !!}">{{ $title }}</a></li>
                                    @endif


                                @endforeach
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>

    @can('create', 'App\Models\Agreement')
    <div class="panel-footer">
        <a href="{{ route('agreements.make') }}" class="btn btn-primary">Add Agreement</a>
    </div>
    @endcan
</div>

