


{{--This gets saved to the identifier field.--}}

<div class="form-group{{ $errors->has('details.enrollment_number') ? ' has-error' : '' }}">
    <label for="enrollment_number" class="col-md-4 control-label">Enrolment Number</label>

    <div class="col-md-6">
        <input id="enrollment_number" type="text" class="form-control" name="details[enrollment_number]" value="{{ old( 'details.enrollment_number',$agreement->details['enrollment_number'])}}" placeholder="Enter the enrolment number">

        @if ($errors->has("details.enrollment_number"))
            <span class="help-block"><strong>{{ $errors->first("details.enrollment_number") }}</strong></span>
        @endif
    </div>
</div>




<div class="form-group{{ $errors->has('details.access_key') ? ' has-error' : '' }}">
    <label for="access_key" class="col-md-4 control-label">EA Billing API Access Key</label>

    <div class="col-md-6">
        <input id="access_key" type="text" class="form-control" name="details[access_key]" value="{{ old( 'details.access_key',$agreement->details['access_key'])}}" placeholder="Enter the access key">

        @if ($errors->has('details.access_key'))
            <span class="help-block"><strong>{{ $errors->first('details.access_key') }}</strong></span>
        @endif
    </div>
</div>




