<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Azure Subscriptions</h1>
    </div>
    <div class="panel-body">
        @if($subscriptions->count()==0)
            @if($showCustomerInfo)
                <p>No customer subscriptions defined yet!</p>
            @else
                <p>No Azure subscriptions have been added yet!</p>
            @endif
        @else
            <table class="table table-bordered table-striped text-middle">
                <tr>
                    @if($showCustomerInfo)
                        <th>Customer Name</th>
                    @endif
                    <th>Agreement</th>
                    <th>Subscription ID</th>
                    <th>Subscription Name</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                @foreach($subscriptions as $subscription)
                    <tr>
                        @if($showCustomerInfo)
                            <td>{{ $subscription->customer_name }}</td>
                        @endif
                        <td>
                        @if($subscription->agreement)
                            <a href="{{ route('agreements.show', $subscription->agreement) }}">{{ $subscription->agreement->getIdentifierText() ?: $subscription->agreement->name }}</a>
                        @else
                            -
                        @endif
                        </td>
                        <td>{{ $subscription->guid }}</td>
                        <td>{{ $subscription->getName($showCustomerInfo) }}</td>
                        <td class="bg-{{ $subscription->getStatusClass() ?: 'default' }}">{{ $subscription->getStatusText() }}</td>
                        <td>
                            <div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions&nbsp;<span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    @foreach($subscription->getActionList() as $title=>$link)
                                        <li{!! $link==FALSE ? ' class="disabled"' : '' !!}><a href="{!! $link ?: '' !!}">{{ $title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        @endif
    </div>
{{--
    @can('create', 'App\CloudServices\Azure\Models\Subscription')
        <div class="panel-footer">
            <a href="{{ route('azure.subscription.create') }}" class="btn btn-primary">Add Subscription</a>
        </div>
    @endcan
--}}
</div>
