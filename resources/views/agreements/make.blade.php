@extends('layouts/main')

@section('title', 'Add an Agreement')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add an Agreement</div>
                    <div class="panel-body">
                        @include('/partials/forms/errors')
                        <form id = "agreements-form" class="form-horizontal" role="form" method="GET" action="{{ route('agreements.create') }}">
                            {{ csrf_field() }}



                            @if($partnerUser)
                                @include('agreements/partials/selectedCustomer')
                            @else
                                <input type="hidden" name="customer_id" id="customer_id" value="{{Auth::user()->customer_id}}">
                            @endif

                            {{--providers--}}

                            <div class="form-group">
                                <label for="provider" class="col-md-4 control-label">Service Provider</label>
                                <div class="col-md-6">
                                    <select id="provider"  class="form-control" required>

                                        <option value = "" selected="true" disabled="disabled">Please select ...</option>

                                        @foreach($providers as $provider)
                                            <option value="{{$provider['provider']}}">{{$provider['description']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            {{--Outer loop through providers--}}
                            @foreach($providersAndAgreements as $provider)

                                {{--Now a select for each provider--}}

                                <div hidden class="form-group" id="{{$provider['provider']}}">
                                    <label for="{{$provider['provider']}}" class="col-md-4 control-label">Agreement Type</label>
                                    <div class="col-md-6">
                                        <select  id="{{$provider['provider']}}Select" class="form-control">

                                            <option value = "" selected="true" disabled="disabled">Please select ...</option>
                                            {{--option for each agreement type for this provider--}}

                                            @foreach($provider['types'] as $type)
                                                <option value="{{$type['id']}}">{{$type['description']}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>


                                {{--valdiation errors?--}}


                            @endforeach

                            <input type="hidden" name="provider" id="hiddenProvider">
                            <input type="hidden" name="type" id="hiddenType">

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create Agreement
                                    </button>

                                    <a class="btn btn-link" href="{{ back() }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection

@section('script')
    <script>

        /**
         * When a provider is selected, show its agreement type
         *
         */
        $('#provider').change(function(){
            hideAllAgreementSelects();
            showSelectedAgreementSelect(this.value);

        });

        /**
         * Show the select and make required for the selected provider.
         *
         */
        function showSelectedAgreementSelect(selectedProvider){
            $('#' + selectedProvider).show();
            $('#' + selectedProvider + "Select").attr('required','required');

        }

        /**
         * Hide the selects and remove required for the unselected providers.
         *
         */
        function hideAllAgreementSelects(){
            @foreach($providersAndAgreements as $provider)
                $('#' + '{{$provider['provider']}}').hide();
                $('#' + '{{$provider['provider']}}' + "Select").removeAttr('required');
            @endforeach
        }


        /**
         * Set the form to GET to /agreements/create/{provider}/{type}
         *
         */
        $('#agreements-form').submit(function()
        {

            //get the selected provider
            var provider = $('#provider').find(":selected").val();

            //get the selected agreement
            var type = $('#' + provider + 'Select' ).find(":selected").val();


            //set the hidden fields so that they are in the GET
            $('#hiddenType').val(type);
            $('#hiddenProvider').val(provider);


        });


    </script>
@endsection