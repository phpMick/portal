@extends('layouts/main')

@section('title', 'Add an Agreement')

@section('include-css')
<link  href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endsection

@section('content')


    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add an Agreement</div>
                    <div class="panel-body">
                        @include('/partials/forms/errors')
                        <form id = "agreements-form" class="form-horizontal" role="form" method="POST" action="{{ route('agreements.store') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="customer_id" id="hiddenProvider" value="{{$customer_id}}">
                            <input type="hidden" name="type" id="hiddenType" value = "{{$type}}">


                            @include('agreements/partials/common_fields')

                            {{--The agreement type specific partial--}}
                            @if(View::exists($partial))
                                @include($partial)
                            @endif


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create Agreement
                                    </button>

                                    <a class="btn btn-link" href="{{ route('agreements.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection

@section('script')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script>


        $('#start-datepicker input,#end-datepicker input').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

    </script>

@endsection