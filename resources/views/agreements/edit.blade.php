@extends('layouts.main')

@section('title', 'Edit Agreement')

@section('include-css')
    <link  href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('agreements.update', [$agreement]) }}">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Agreement</div>
                        <div class="panel-body">
                            @include('partials/forms/errors')
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}


                                    <div class="form-group">
                                        <label for="customer" class="col-md-4 control-label">User Group</label>

                                        <div class="col-md-6 statictext">
                                            <span>{{ $agreement->user_group->name }}</span>
                                        </div>
                                    </div>


                            <input type="hidden" name="customer_id" id="hiddenCustomer" value="{{$agreement->customer_id}}">



                                <div class="form-group">
                                    <label for="type" class="col-md-4 control-label">Agreement Type</label>
                                    <div class="col-md-6 statictext">
                                        <span>{{ $agreement->getTypeTextLong() }}</span>
                                    </div>
                                </div>

                               <input type="hidden" name="type" id="hiddenType" value="{{$agreement->type}}">


                            @include('agreements/partials/common_fields')


                            {{--The agreement type specific partial--}}
                            @if(View::exists($partial))
                                @include($partial)
                            @endif


                        </div> {{--panel body--}}

                        <div class="panel-footer">

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Update Agreement
                                    </button>




                                   {{-- @can('storeMargins',$agreement)
                                    @if(\App\Models\AgreementFactory::hasMargins($agreement->type))
                                        <a class="btn btn-primary" href="{{ route('agreements.margins',[$agreement]) }}">
                                            Edit Margins
                                        </a>
                                    @endif
                                    @endcan--}}


                                    <a class="btn btn-link" href="{{ route('agreements.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> {{--panel--}}
                </form>

            </div> {{--col--}}

        </div> {{--row--}}
    </div> {{--container--}}
@endsection

@section('script')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

@endsection