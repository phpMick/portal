@extends('layouts/main')

@section('title', 'Agreements')

@section('include-css')
    <link  href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('agreements/partials/agreement_table')
            </div>
        </div>
    </div>

@endsection



@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/agreements-common.js') }}"></script>

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#agreements').DataTable();
        } );

    </script>
@endsection