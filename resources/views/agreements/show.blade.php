@extends('layouts/main')

@section('title', 'Azure Agreement')

@section('content')

    <div class="container">
        {{--Agreed to leave these field like they are for now--}}
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Agreement</div>
                        <div class="panel-body">


                            {{--agreement type--}}
                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>Agreement Type</strong>
                                </div>
                                <div class="col-md-6">
                                    <span>{{ $agreement->getTypeTextLong() }}</span>
                                </div>
                            </div>


                            {{--agreement name--}}
                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>Agreement Name</strong>
                                </div>
                                <div class="col-md-6">
                                    <span>{{ $agreement->name }}</span>
                                </div>
                            </div>

                             {{--identifier--}}
                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>Agreement Identifier</strong>
                                </div>
                                <div class="col-md-6">
                                    <span>{{ $agreement->getTypeText() . $agreement->identifier }}</span>
                                </div>
                            </div>

                            {{--country--}}
                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>Country of Purchase</strong>
                                </div>
                                <div class="col-md-6">
                                    <span>{{ $agreement->purchase_region }}</span>
                                </div>
                            </div>


                        @if($agreement->isEA())
                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>Enrolment Number</strong>
                                </div>
                                <div class="col-md-6">
                                    <span>{{ $agreement->access_key }}</span>
                                </div>
                            </div>

                            <div class="row mt1">
                                <div class="col-md-4 text-right">
                                    <strong>EA Billing API Access Key</strong>
                                </div>
                                <div class="col-md-6">
                                    <span><em>hidden</em></span>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-primary mr1" href="{{ route('agreements.edit', [$agreement]) }}">Edit Agreement</a>

                        {{--DELETE	    /photos/{photo}	        destroy	photos.destroy--}}

                        <a class="btn btn-danger" onclick="destroy({{$agreement->id}})">Delete Agreement</a>

                    </div>
                </div> {{--panel--}}

                @can('viewMargins', $agreement)
                    @if(\App\Models\AgreementFactory::hasMargins($agreement->type))
                        @include('agreements/margins/partials/table')
                    @endif
                @endcan('viewMargins')

                <div>
                    <a class="btn btn-default" a href="{{ route('agreements.index') }}"><span class="icon-return"></span>Back to Agreement list</a>
                </div>



            </div>
        </div> {{--container--}}
    </div>
@endsection

@section('script')

    <script src="{{ asset('js/agreements-common.js') }}"></script>

@endsection