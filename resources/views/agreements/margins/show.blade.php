@extends('layouts.main')

@section('title', 'Agreement Margins')

@section('include-css')
    <link  href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="container">
        <div class="col-md-8 col-md-offset-2">

            @include('agreements/margins/partials/table')


            <div>
                <a class="btn btn-default" a href="{{ route('agreements.index') }}"><span class="icon-return"></span>Back to Agreement list</a>
            </div>

        </div> {{--cols--}}
    </div> {{--container--}}


@endsection


@section('script')

@endsection
