<div class="panel panel-default">
    <div class="panel-heading">Margins</div>
    <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Margin</th>
            </tr>
            </thead>
            <tbody>
            @foreach($margins as $margin)
                <tr>
                    <td>{{ $margin->start_date }}</td>
                    <td>{{ $margin->end_date }}</td>
                    <td>{{ $margin->margin }}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div> {{--panel body--}}


    @can('storeMargins', $agreement)
        <div class="panel-footer">
            <a href="{{ route('agreements.margins.edit',$agreement) }}" class="btn btn-primary">Edit Margins</a>
        </div>
    @endcan


</div> {{--panel--}}