@extends('layouts.main')

@section('title', 'Agreement Margins')

@section('include-css')
    <link  href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endsection

@section('content')



    <div class="container">
        <div class="col-md-8 col-md-offset-2">
        <form  id="margins-form" class="form-horizontal" role="form" method="POST" action="{{ route('agreements.margins.store',[$agreement] ) }}">
            {{ csrf_field() }}
            <input type="hidden" name="agreement_id" id="hidden-agreement-id" value="{{$agreement->id}}" />

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Agreement Margins</h1>
            </div>
            <div class="panel-body">


                {{--@include('partials/forms/errors')--}}

                @if(count($errors))
                    <div class="panel panel-danger">
                        <div class="panel-heading">There were errors</div>
                        <div class="panel-body">
                            <ul>
                                {{$errors['message']}}
                            </ul>
                        </div>
                    </div>
                @endif



                    <table class="table table-bordered table-striped text-middle" id="margins-table">
                        <tr>
                            {{--<th></th>--}}
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Margin % </th>
                            <th>Action</th>
                        </tr>

                        @if(count($errors)) {{--old input failed validation--}}

                            @foreach($margins as $margin)
                                <tr>

                                    @if(($errors['rowId'] == $margin['rowId']) && ($errors['element'] == 'start_date') )
                                        <td class="table-error"><input type="text" name="start_date[]" value="{{ $margin['start_date'] }}" data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker" data-date-autoclose="true" class="form-control"></td>
                                    @else
                                        <td><input type="text" name="start_date[]" value="{{ $margin['start_date'] }}" data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker" data-date-autoclose="true" class="form-control"></td>
                                    @endif


                                    @if(($errors['rowId'] == $margin['rowId']) && ($errors['element'] == 'end_date') )
                                        <td class="table-error"><input type="text" name="end_date[]" value="{{ $margin['end_date'] }}"  data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker" data-date-autoclose="true" class="form-control"></td>
                                    @else
                                        <td><input type="text" name="end_date[]" value="{{ $margin['end_date'] }}" data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker"  data-date-autoclose="true" class="form-control"></td>
                                    @endif


                                    @if(($errors['rowId'] == $margin['rowId']) && ($errors['element'] == 'margin') )
                                        <td class="table-error"><input type="number" min="0" name="margin[]" value="{{ $margin['margin'] }}"  class="form-control"></td>
                                    @else
                                        <td><input type="number" min="0" name="margin[]" value="{{ $margin['margin'] }}"  class="form-control"></td>
                                    @endif


                                    <td><button type = 'button' class='btn btn-danger' onClick="$(this).closest('tr').remove();">Delete</button></td>

                                </tr>


                            @endforeach



                        @else {{--first page load--}}



                            @foreach($margins as $margin)
                                <tr>
                                    {{--<td><input type="hidden" name="id" value="{{ $margin->id }}"></td>--}}
                                    <td><input type="text" name="start_date[]" value="{{ $margin->start_date }}" data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker" data-date-autoclose="true" class="form-control"></td>
                                    <td><input type="text" name="end_date[]" value="{{ $margin->end_date }}" data-date-orientation='bottom' data-date-format='yyyy-mm-dd' data-provide="datepicker" data-date-autoclose="true" class="form-control"></td>
                                    <td><input type="number" min="0" name="margin[]" value="{{ $margin->margin }}"  class="form-control"></td>
                                    <td><button type = 'button' class='btn btn-danger' onClick="$(this).closest('tr').remove();">Delete</button></td>
                                </tr>
                            @endforeach

                        @endif



                    </table>


            </div> {{--panel body--}}

            <div class="panel-footer">
                <button type ="button" class="btn btn-primary" id="add-row" onclick="btnAddRowClick()">Add Margin</button>


                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="btnSubmitClick()">
                        Save Margins
                    </button>

                </div>


            </div>

        </div> {{--panel--}}
            <div>
                <a class="btn btn-default" a href="{{ route('agreements.index') }}"><span class="icon-return"></span>Back to Agreement list</a>
            </div>
        </div>
        </form>
    </div> {{--container--}}

@endsection


@section('script')

    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

<script>

    /**
     * Adds a new row
     *
     *
     */
    function btnAddRowClick(){


        var markup = "<tr>" +
                    /*"<td><input type='hidden' value = class='form-control'></td>" +*/
                    "<td><input type='text' name='start_date[]' data-date-format='yyyy-mm-dd' class='form-control' data-provide='datepicker' data-date-orientation='bottom'></td>" +
                    "<td><input type='text' name='end_date[]' data-date-format='yyyy-mm-dd' class='form-control' data-provide='datepicker' data-date-orientation='bottom'></td>" +
                    "<td><input type='number' name='margin[]' class='form-control' placeholder='%'></td>" +
                    "<td><button type = 'button' class='btn btn-danger' onClick=" + "$(this).closest('tr').remove();>Delete</button></td>"

                "</tr>";

    $("table tbody").append(markup);
    };

    /**
     * Check that all the margins are <100%.
     * If any fail, confirmation is required.
     *
     * @returns {boolean}
     */
    function btnSubmitClick() {

        if(checkMargins()) {
            $('#margins-form').submit();
        }else{
            swal({
                text: "Margin exceeds 100%, do you want to save?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (stillSave) {
                if (stillSave) {
                    //submit form from here
                    $('#margins-form').submit();
                }
            });
        }
    }

    /**
     * Iterates through all the margins and
     * fails if any are >100%
     * @returns {boolean}
     */
    function checkMargins(){

        var passed = true;

        //we need an alert if the margin is over 100
        $('input[name^="margin"]').each(function() {
            if($(this).val()>100){
                passed =  false;
            };
        });

        return passed;

    }




</script>

    {{--<script src="{{ asset('js/margins.js') }}"></script>--}}

@endsection