@extends('layouts/main')

@section('title', '')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-xs-12">
            <h1>Select a Customer</h1>
            <p>Search:
            {!! Form::open() !!}
            {!! Form::text('search') !!}
            {!! Form::submit() !!}
            {!! Form::close() !!}
            </p>
        </div>
    </div>

    <div class="row">
    @foreach($customers as $customer)
        @if(false and ($loop->index % 6) == 0)
            <div class="row">
        @endif
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <a href="{{ route('customers.select') }}">{{ $customer->name }}</a>
        </div>
        @if(false and ($loop->index % 6) == 5 and $loop->last)
            </div>
        @endif

    @endforeach
    </div>

    <div class="row">
        <div class="col-xs-12">
            {{ $customers->links() }} (total results: {{ $customers->total() }})
        </div>
    </div>
</div>
@endsection