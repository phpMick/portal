@extends('layouts/dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <h1>Partner Dashboard @if(isset($customer)) for {{$customer->name}} @endif</h1>
                <h4 style="color:blue">
                    @if(is_array($selectedCustomers) AND count($selectedCustomers)>0)
                        @if(count($selectedCustomers)>1)
                            Data is filtered to the selected {{ count($selectedCustomers) }} customers
                        @else
                            Data is filtered to the selected customer: {{ array_values($selectedCustomers)[0] }}
                        @endif
                    @else
                        Data is for all your customers
                    @endif
                </h4>

                <div class="row">
                    <div class="col-xs-6 col-md-4 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Customers</div>
                            <div class="panel-body">{{ $customerCount }}</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Azure Subscriptions</div>
                            <div class="panel-body">{{ $azureSubCount }}</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Consumption Last Month</div>
                            <div class="panel-body">{{ $azureSpend }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <canvas id="azure-spend-by-month"></canvas>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <canvas id="azure-spend-last-month"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('inline-css')

@endsection


@section('script')
    <script>

        var stackedBarChartOptions = {
            tooltips: {
                mode: 'index',
                    intersect: true,
                    callbacks: {
                    // Use the footer callback to display the sum of the items showing in the tooltip
                    footer: function(tooltipItems, data) {
                        var sum = 0;

                        tooltipItems.forEach(function(tooltipItem) {
                            sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        });
                        return 'Total: GBP ' + sum.toFixed(2);
                    },
                },
                footerFontStyle: 'normal'
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                    yAxes: [{
                    stacked: true
                }]
            }
        }

        var colours = [
            '#e6194b','#3cb44b','#ffe119','#0082c8','#f58231','#911eb4',
            '#46f0f0','#f032e6','#d2f53c','#fabebe','#008080','#e6beff',
            '#aa6e28','#fffac8','#800000','#aaffc3','#808000','#ffd8b1',
            '#000080','#808080','#ffffff','#000000'
        ];

        $(function(){
            $.getJSON("/data/azure/spend?by=dm&grp=cn&out=chartjs", function (result) {
                if (result !== null && typeof result=='object') {
                    for (var i=0; i<result.datasets.length; i++) {
                        result.datasets[i].backgroundColor = colours[i];
                    }
                    var spendCanvas = document.getElementById('azure-spend-by-month');
                    var chartInstance = new Chart(spendCanvas.getContext('2d'), {
                        type: 'bar',
                        data: result,
                        options: stackedBarChartOptions,
                    });

                    chartInstance.options.title = {
                        display: true,
                        text: "Azure Spend by Customer Name"
                    };
                    if(result.datasets[0].data.length>3) {
                        chartInstance.options.legend.display = false;
                    }
                    chartInstance.update();
                }
            });
        });

        $(function(){
            $.getJSON("/data/azure/spend?by=cn&dateRange=lastMonth&out=chartjs", function (result) {

                if (result !== null && typeof result=='object') {
                    result.datasets[0].backgroundColor = [];
                    for (var i=0; i<result.datasets[0].data.length; i++) {
                        result.datasets[0].backgroundColor.push(colours[i]);
                    }
                    var spendCanvas = document.getElementById('azure-spend-last-month');
                    var chartInstance = new Chart(spendCanvas.getContext('2d'), {
                        type: 'pie',
                        data: result,
                    });

                    chartInstance.options.title = {
                        display: true,
                        text: "Azure Spend by Customer Name Last Month"
                    };
                    if(result.datasets[0].data.length>3) {
                        chartInstance.options.legend.display = false;
                    }
                    chartInstance.update();
                }
            });
        });


    </script>
@endsection