@extends('layouts/dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                <h1>Cloud Insights Dashboard</h1>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-md-4 col-lg-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">Azure Subscriptions</div>
                                <div class="panel-body">{{ $azureSubCount }}</div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4 col-lg-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">Consumption Last Month</div>
                                <div class="panel-body">{{ $azureSpend }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <canvas id="azure-usage-by-month" width="400px" height="250px" style="width:400px; height:250px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $.getJSON("/data/azure/spend/all", function (result) {

                var labels=[], data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].month);
                    data.push(result[i].cost);
                }

                var spendData = {
                    labels : labels,
                    datasets : [
                        {
                            fillColor : "rgba(240, 127, 110, 0.3)",
                            strokeColor : "#f56954",
                            pointColor : "#A62121",
                            pointStrokeColor : "#741F1F",
                            data : data,
                            label : 'Azure Spend'
                        }
                    ]
                };
                var spendCanvas = document.getElementById('azure-usage-by-month').getContext('2d');

                var chartInstance = new Chart(spendCanvas, {
                    type: 'bar',
                    data: spendData,
                });
            });

        });


    </script>
@endsection