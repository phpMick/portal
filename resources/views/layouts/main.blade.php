<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(View::hasSection('title')) {{ 'Cloud Portal - '  }} @yield('title') @else {{ 'Cloud Portal' }} @endif</title>

    <!-- Styles -->
    <link href="/css/bootstrap-3.3.7.css" rel="stylesheet" />
    <link href="/css/site.css" rel="stylesheet" />
    <link href="/js/easy-autocomplete/easy-autocomplete.min.css" rel="stylesheet" />
    @yield('include-css')


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>



</head>
<body>
    <div id="app" style="margin-bottom:120px;">
        @include('partials/nav')

        @include('partials.message-bar')

        @yield('content')

    </div>



    @include('partials/footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/easy-autocomplete/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>

    @yield('scripts')
    @yield('script')
</body>
</html>
