@extends('layouts/main')

@section('title', 'List User Groups')


@section('include-css')
    <link  href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
@endsection

@section('content')



<div class="container">

    <div class="row">
        <div class="col-xs-12">
            <h1>Users
            @can('create', \App\Models\User::class)
                <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">Add User</a>
            @endcan
            </h1>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">

            <table class="table table-striped table-bordered dataTable" id ="users">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Role</th>

                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->roleName() }}</td>
                                <td>
                                    <a href="{{ route('users.show', $user->id) }}" class="btn btn-default pull-left" style="margin-right:4px">View Details</a>

                                    @can('edit', \App\Models\User::class)
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary pull-left" style="margin-right:4px">Edit</a>
                                        <a class="btn btn-danger" onclick="destroy({{$user->id}})">Delete</a>
                                    @endif


                                </td>
                            </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div> {{--row--}}



</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/users-common.js') }}"></script>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#users').DataTable();
    } );

</script>
@endsection