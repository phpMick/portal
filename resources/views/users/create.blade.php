@extends('layouts/main')

@section('title', 'Add a User')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add a User</div>
                    <div class="panel-body">
                        @include('partials/forms/errors')
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('users.store') }}">
                            {{ csrf_field() }}

                            @include('users/partials/fields')

                            <div id="app"> {{--The Vue application--}}
                                <bss-tree options-string="{{$userGroups}}"
                                          title="{{$title}}"
                                          initial-id = "{{$user->user_group_id}}">
                                </bss-tree>
                            </div> {{--app--}}

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add User
                                    </button>

                                    <a class="btn btn-link" href="{{ route('users.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection