{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$user->name) }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

{{--Email--}}
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Email</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email',$user->email) }}" required autofocus>

        @if ($errors->has('email'))
            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>

{{--Role--}}
<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
    <label for="role" class="col-md-4 control-label">Role</label>

    <div class="col-md-6">

        <select class="form-control" name="role">
            @foreach($roles as $role)
                <option value="{{$role['id']}}"  @if(old('role',$user->role) == "{$role['id']}") selected = 'selected'  @endif> {{$role['description']}}</option>
            @endforeach
        </select>

        @if ($errors->has('role'))
            <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
        @endif
    </div>
</div>




