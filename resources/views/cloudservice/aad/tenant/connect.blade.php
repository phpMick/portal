@extends('layouts/main')

@section('title', 'Connect Application to Azure AD')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <h1>Connect Azure Active Directory</h1>
                <p>Allow this web application to access the Azure Subscription {{ $tenant->guid }}</p>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-info-sign"></span> Information
                    </div>
                    <div class="panel-body">
                        <p>You will asked to sign in to Microsoft services with an administrator account for Azure Active Dirctory id {{ '{'.$tenant->guid.'}' }}</p>
                        <p>This process will do some crazy stuff</p>
                    </div>
                </div>

                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-alert"></span> Warning
                    </div>
                    <div class="panel-body">
                        <p>You will be prompted to log in to Microsoft services using an administrator login. If you do not have administrative rights, you will not be able to proceed, and an error will be shown.</p>
                    </div>
                </div>

                @include('partials/forms/errors')
                <form class="form-horizontal" role="form" method="POST" action="{{ route('aad.tenants.attach', $tenant) }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Proceed
                            </button>

                            <a class="btn btn-link" href="{{ route('azure.index') }}">
                                Cancel
                            </a>
                        </div>
                    </div>

                </form>



            </div>
        </div>
    </div>

@endsection