@extends('layouts/main')

@section('title', 'Connect Azure Subscription')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('partials/external_user_bar')
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h1>Connect an Azure Subscription</h1>
                <h3>
                    @if($showCustomerInfo)
                        <span class="label label-default">{{ $subscription->customer->name }}</span>&nbsp;&raquo;
                    @endif
                    <span class="label label-primary">{{ $subscription->getName($showCustomerInfo) }}</span>
                </h3>
                <h5><span class="label label-info">{{ $subscription->guid }}</span>
                </h5>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-info-sign"></span> Information
                    </div>
                    <div class="panel-body">
                        <p>Select the level of access you would like to give this application.</p>
                        <p>After completing this step, you may be prompted to log in to your Azure account that is associated with this subscription. You may also see a prompt to allow this application access to your Azure subscription.</p>
                    </div>
                </div>

                @include('partials/forms/errors')
                <form class="form-horizontal" role="form" method="POST" action="{{ route('azure.subscriptions.attach', $subscription) }}">
                    {{ csrf_field() }}

                    <div class="radio">
                        <label><input type="radio" name="access" value="r"{{ old('access')=='r' ? ' checked="checked"' : '' }} /> Allow read-only access to this subscription</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="access" value="rw"{{ old('access')!='rw' ? ' checked="checked"' : '' }} /> Allow read/write access to this subscription</label>
                    </div>


                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Connect Subscription
                            </button>

                            <a class="btn btn-link" href="{{ route('azure.index') }}">
                                Cancel
                            </a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection