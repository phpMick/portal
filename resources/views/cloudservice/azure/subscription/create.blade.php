@extends('layouts/main')

@section('title', 'Add Azure Subscription')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Add an Azure Subscription</h1>
                <p>Use this page to add a new monitored Azure Subscription to your account.</p>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-info-sign"></span> Information
                    </div>
                    <div class="panel-body">
                        <p>In the form below, you need to enter your Azure Subscription ID. This can be found in the Azure Portal quite easily</p>
                        <ol>
                            <li>Go to your <a href="https://portal.azure.com/#blade/Microsoft_Azure_Billing/SubscriptionsBlade" target="_blank">list of Subscriptions in your default Directory</a></li>
                            <li>If the subscription you want to add is not listed, ensure that you are signed in with the correct identity (see the top-right account name). If not - use the account menu to sign out, and then use the link above to sign in with the correct identity.</li>
                            <li>If the subscription you want to add is still not shown, then you may need to switch to an alternate Directory. Use the steps below:
                                <ol>
                                    <li>Use the account menu (top-right) to select an alternative Directory</li>
                                    <li>Click "More services" at the bottom of the navigation menu on the left</li>
                                    <li>Search for "Subscriptions" in the blade that appears and click the menu option</li>
                                    <li>You will now see a list of the Subscriptions in the currently active Directory</li>
                                </ol>
                            </li>
                            <li>Copy the Subscription ID of the Subscription you want to add to your account into the text box below</li>
                        </ol>
                    </div>
                </div>
                @include('partials/forms/errors')
                <form class="form-horizontal" role="form" method="POST" action="{{ route('azure.subscriptions.store') }}">
                    {{ csrf_field() }}



                    <div class="form-group{{ $errors->has('agreement-id') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Agreement</label>

                        <div class="col-md-6 staticlabel large">
                            @if($agreement->type) <span class="label label-default">{{ $agreement->getTypeText() }}</span> @endif
                            @if($agreement->identifier) <span class="label label-info">{{ $agreement->identifier }}</span> @endif
                            @if($agreement->name) <span class="label label-primary">{{ $agreement->name }}</span> @endif
                        </div>
                    </div>
                    <input type="hidden" name="agreement-id" value="{{ $agreement->id }}" />

                    <div class="form-group{{ $errors->has('guid') ? ' has-error' : '' }}">
                        <label for="guid" class="col-md-4 control-label">Subscription ID</label>

                        <div class="col-md-6">
                            <input id="guid" type="text" class="form-control" name="guid" value="{{ old('guid') }}" required autofocus />

                            @if ($errors->has('guid'))
                                <span class="help-block"><strong>{{ $errors->first('guid') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Add Subscription
                            </button>

                            <a class="btn btn-link" href="{{ back() }}">
                                Cancel
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection