@extends('layouts/main')

@section('title', '')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">Insufficient Permissions</div>
                    <div class="panel-body">
                        Unfortunately, you do not seem to have sufficient permission on the subscription to connect this application.
                        Note that you must have an admin level permission to the subscription.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection