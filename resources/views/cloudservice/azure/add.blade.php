@extends('layouts/main')

@section('content')

    logged in as {{ $aadUserId }}

    <table>
        <tr>
            <th>Sub ID</th>
            <th>GUID</th>
            <th>Name</th>
            <th>Permissions</th>
            <th>Actions</th>
        </tr>
        @foreach($subscriptions as $sub)
        <tr>
            <td>{{ $sub->id }}</td>
            <td>{{ $sub->guid }}</td>
            <td>{{ $sub->name }}</td>
            <td></td>
            <td><a href="{{ route('azure.connect', ['subscription_guid'=>$sub->guid]) }}" class="btn btn-default">Connect</a></td>
        </tr>
        @endforeach
    </table>
    <hr />

    <ul>
        <li>TIME NOW: {{ date(DateTime::ISO8601) }} [{{ time() }}]</li>
        <li>MSGRAPH expires at: {{ date(DateTime::ISO8601, $tokens['msgraph']->getExpires()) }} [{{ $tokens['msgraph']->getExpires() }}]</li>
        <li>AZURERM expires at: {{ date(DateTime::ISO8601, $tokens['azurerm']->getExpires()) }} [{{ $tokens['azurerm']->getExpires() }}]</li>
    </ul>

@endsection