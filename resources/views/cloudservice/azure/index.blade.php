@extends('layouts/main')

@section('title', 'Azure Agreements and Subscriptions')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('cloudservice/azure/partials/agreement_table')
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('cloudservice/azure/partials/subscription_table')
            </div>
        </div>
    </div>
@endsection