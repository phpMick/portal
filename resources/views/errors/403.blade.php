<html>
    <head>
        <title>Access Denied!</title>
    </head>
    <body>
        <h1>You are not authorised to view this page</h1>

        <a href="/">Go Home</a>

        @if(Auth::check())
            {{-- present an authenticated user with a logout form in case they get stuck --}}
            <h3>{{ Auth::user()->email }}</h3>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"">
                {{ csrf_field() }}
                <button type="submit">Logout</button>
            </form>
        @endif

    </body>
</html>