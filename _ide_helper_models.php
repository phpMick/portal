<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Connectors\AzureDb{
/**
 * Class JobModel
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property integer $job_num
 * @property integer $records_processed
 * @property string $error_message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
	class JobModel extends \Eloquent {}
}

namespace App\Connectors\AzureDb{
/**
 * App\Connectors\AzureDb\Model
 *
 */
	class Model extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspCustomer
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $guid
 * @property string $name
 * @property string $tenant_guid
 * @property string $default_domain
 * @property string $microsoft_domain
 * @property string $custom_domains
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomerDetail $detail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereCustomDomains($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereDefaultDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereMicrosoftDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereTenantGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomer whereUpdatedAt($value)
 */
	class CspCustomer extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspCustomer
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $currency
 * @property string $default_margin
 * @property integer $portal_customer_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereDefaultMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail wherePortalCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerDetail whereUpdatedAt($value)
 */
	class CspCustomerDetail extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspCustomerJob
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $new
 * @property integer $updated
 * @property int $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon|null $end_time
 * @property int|null $job_num
 * @property int|null $records_processed
 * @property string|null $error_message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereJobNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereRecordsProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspCustomerJob whereUpdatedAt($value)
 */
	class CspCustomerJob extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property double $margin
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMargin whereUpdatedAt($value)
 */
	class CspMargin extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $meter_id
 * @property string $guid
 * @property string $name
 * @property string $sub_category
 * @property string $category
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 * @property-read \App\Connectors\AzureDb\Models\Meter $meter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereMeterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereSubCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereUnitOfMeasure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspMeter whereUpdatedAt($value)
 */
	class CspMeter extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $csp_meter_id
 * @property string $currency
 * @property double $estimate_price
 * @property double $discount
 * @property double $estimate_cost_price
 * @property array $tags
 * @property array $prices
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspMeter $meter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereCspMeterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereEstimateCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereEstimatePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice wherePrices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPrice whereUpdatedAt($value)
 */
	class CspPrice extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspCustomer
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $new_prices
 * @property integer $updated_prices
 * @property integer $new_csp_meters
 * @property integer $updated_csp_meters
 * @property integer $new_meters
 * @property integer $new_region_lookups
 * @property integer $new_regions
 * @property int $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon|null $end_time
 * @property int|null $job_num
 * @property int|null $records_processed
 * @property string|null $error_message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereJobNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereNewCspMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereNewMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereNewPrices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereNewRegionLookups($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereNewRegions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereRecordsProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereUpdatedCspMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceJob whereUpdatedPrices($value)
 */
	class CspPriceJob extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * App\Connectors\AzureDb\Models\CspPriceRate
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $csp_price_id
 * @property double $min_quantity
 * @property double $estimate_price
 * @property double $discount
 * @property double $estimate_cost_price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspPrice $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereCspPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereEstimateCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereEstimatePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereMinQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspPriceRate whereUpdatedAt($value)
 */
	class CspPriceRate extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property string $uri
 * @property string $name
 * @property integer $resource_group_id
 * @property integer $region_id
 * @property array $tags
 * @property string $tag_hash
 * @property array $info
 * @property string $info_hash
 * @property array $marketplace
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 * @property Region $region
 * @property CspResourceGroup $resourceGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereInfoHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereMarketplace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereResourceGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereTagHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResource whereUri($value)
 */
	class CspResource extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property string $name
 * @property boolean $mixed_case
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\AzureDb\Models\CspResource[] $resources
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereMixedCase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspResourceGroup whereUpdatedAt($value)
 */
	class CspResourceGroup extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $guid
 * @property string $tenant_guid
 * @property string $offer_id
 * @property string $offer_name
 * @property string $friendly_name
 * @property integer $quantity
 * @property string $unit_type
 * @property string $parent_subscription_guid
 * @property Carbon $creation_date
 * @property Carbon $effective_start_date
 * @property Carbon $commitment_end_date
 * @property string $status
 * @property boolean $is_trial
 * @property boolean $auto_renew
 * @property string $billing_type
 * @property string $billing_cycle
 * @property string[] $suspension_reasons
 * @property string $contract_type
 * @property string $order_guid
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereAutoRenew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereBillingCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereBillingType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereCommitmentEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereContractType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereCreationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereEffectiveStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereFriendlyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereIsTrial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereOfferName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereOrderGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereParentSubscriptionGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereSuspensionReasons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereTenantGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereUnitType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscription whereUpdatedAt($value)
 */
	class CspSubscription extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscriptionJob
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $customer_id
 * @property integer $new_azure
 * @property integer $updated_azure
 * @property integer $inactive_azure
 * @property integer $active_azure
 * @property CspCustomer $customer
 * @property int $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon|null $end_time
 * @property int|null $job_num
 * @property int|null $records_processed
 * @property string|null $error_message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereActiveAzure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereInactiveAzure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereJobNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereNewAzure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereRecordsProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspSubscriptionJob whereUpdatedAzure($value)
 */
	class CspSubscriptionJob extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $resource_id
 * @property Carbon $reported_date
 * @property Carbon $usage_date
 * @property Carbon $usage_start_time
 * @property Carbon $usage_end_time
 * @property integer $csp_meter_id
 * @property integer $csp_price_id
 * @property string $quantity
 * @property string $estimate_cost
 * @property string $estimate_price
 * @property boolean $deleted
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 * @property CspResource $resource
 * @property CspMeter $meter
 * @property CspPrice $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereCspMeterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereCspPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereEstimateCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereEstimatePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereReportedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereUsageDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereUsageEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsage whereUsageStartTime($value)
 */
	class CspUsage extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspUsageJob
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $subscription_id
 * @property Carbon $query_date
 * @property boolean $old
 * @property integer $records_deleted
 * @property integer $new_resources
 * @property integer $updated_resources
 * @property integer $new_regions
 * @property integer $new_region_lookups
 * @property integer $error_count
 * @property CspSubscription $subscription
 * @property int $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon|null $end_time
 * @property int|null $job_num
 * @property int|null $records_processed
 * @property string|null $error_message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\AzureDb\Models\CspUsageJobError[] $errors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereErrorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereJobNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereNewRegionLookups($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereNewRegions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereNewResources($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereQueryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereRecordsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereRecordsProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJob whereUpdatedResources($value)
 */
	class CspUsageJob extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspUsageJob
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $job_id
 * @property integer $affected_row
 * @property string $error_type
 * @property string $error_detail
 * @property boolean $fixed
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Connectors\AzureDb\Models\CspUsage|null $affectedUsage
 * @property-read \App\Connectors\AzureDb\Models\CspUsageJob $job
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereAffectedRow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereErrorDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereErrorType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereFixed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\CspUsageJobError whereUpdatedAt($value)
 */
	class CspUsageJobError extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $meter_id
 * @property string $guid
 * @property string $name
 * @property string $sub_category
 * @property string $category
 * @property string $product
 * @property string $service
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property string $msdn_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 * @property bool $msdn_pricing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereMsdnPricing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereSubCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereUnitOfMeasure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Meter whereUpdatedAt($value)
 */
	class Meter extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $short_name
 * @property string $display_name
 * @property string $sort_name
 * @property integer $status
 * @property string $continent
 * @property string $country_code
 * @property string $location
 * @property double $latitude
 * @property double $longitude
 * @property integer $paired_region_id
 * @property string $arm_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Connectors\AzureDb\Models\Region|null $pairedRegion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereArmName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereContinent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region wherePairedRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereSortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\Region whereUpdatedAt($value)
 */
	class Region extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscription
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $name
 * @property integer $region_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\RegionLookup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\RegionLookup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\RegionLookup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\RegionLookup whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\RegionLookup whereUpdatedAt($value)
 */
	class RegionLookup extends \Eloquent {}
}

namespace App\Connectors\AzureDb\Models{
/**
 * Class CspSubscriptionJob
 *
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property integer $job_num
 * @property string $error_message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereJobNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\AzureDb\Models\SyncPortalJob whereUpdatedAt($value)
 */
	class SyncPortalJob extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class LoadAzureCoreDataJob
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $created_records
 * @property integer $updated_records
 * @property integer $deleted_records
 */
	class LoadAzureDataJob extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * App\Connectors\ReportsDb\Models\Model
 *
 */
	class Model extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class LoadJob
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon $end_time
 * @property integer $job_id
 * @property integer $processed_records
 * @property string $error_message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
	class ModelJob extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportModel
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $source_id
 */
	class ModelReport extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsAgreement
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property string $type
 * @property string $currency
 * @property ReportsCustomer $customer
 * @property Collection|ReportsAzureSubscription[] $subscriptions
 * @property int $source_type
 * @property int $source_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereSourceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement whereType($value)
 */
	class ReportsAzureAgreement extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsMeter
 *
 * @package App\Connectors\ReportsDb\Models
 * @property string $guid
 * @property string $name
 * @property string $category
 * @property string $sub_category
 * @property string $product
 * @property string $service
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property string $msdn_name
 * @property int $id
 * @property int $source_type
 * @property int $source_id
 * @property-read \App\Connectors\ReportsDb\Models\ReportsAzureMeter $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereMsdnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereSourceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereSubCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureMeter whereUnitOfMeasure($value)
 */
	class ReportsAzureMeter extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsRegion
 *
 * @package App\Connectors\ReportsDb\Models
 * @property string $display_name
 * @property int $id
 * @property int $source_type
 * @property int $source_id
 * @property string|null $sort_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureRegion whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureRegion whereSortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureRegion whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureRegion whereSourceType($value)
 */
	class ReportsAzureRegion extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsResource
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property string $resource_group
 * @property integer $meter_id
 * @property string $name
 * @property string $guid
 * @property string $instance_name
 * @property integer $region_id
 * @property string $tag_list
 * @property string[] $additional_info
 * @property-read \App\Connectors\ReportsDb\Models\ReportsAzureMeter $meter
 * @property-read \App\Connectors\ReportsDb\Models\ReportsAzureRegion $region
 * @property-read \App\Connectors\ReportsDb\Models\ReportsAzureSubscription $subscription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsTag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsAzureCspResourceUsage[] $usage
 */
	class ReportsAzureResource extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsSubscription
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $agreement_id
 * @property string $name
 * @property string $guid
 * @property string $department
 * @property string $cost_center
 * @property string $account_name
 * @property string $account_owner
 * @property boolean $is_msdn
 * @property ReportsAzureAgreement $agreement
 * @property int $id
 * @property int $source_type
 * @property int $source_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsAzureCspResource[] $resources
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereAccountOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereCostCenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereIsMsdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsAzureSubscription whereSourceType($value)
 */
	class ReportsAzureSubscription extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsCustomer
 *
 * @package App\Connectors\ReportsDb\Models
 * @property string $name
 * @property int $id
 * @property int $source_type
 * @property int $source_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsAzureAgreement[] $agreements
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsUser[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsCustomer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsCustomer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsCustomer whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsCustomer whereSourceType($value)
 */
	class ReportsCustomer extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsTag
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $resource_id
 * @property string $category
 * @property string $value
 * @property ReportsAzureCspResource $resource
 */
	class ReportsTag extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsUsage
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $resource_id
 * @property Carbon $date
 * @property float $consumed_quantity
 * @property float $resource_rate
 * @property float $base_cost
 * @property float $extended_cost
 * @property ReportsAzureCspResource $resource
 */
	class ReportsUsage extends \Eloquent {}
}

namespace App\Connectors\ReportsDb\Models{
/**
 * Class ReportsUser
 *
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property integer $portal_user_id
 * @property int $source_type
 * @property int $source_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Connectors\ReportsDb\Models\ReportsCustomer[] $customers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsUser whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Connectors\ReportsDb\Models\ReportsUser whereSourceType($value)
 */
	class ReportsUser extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Agreement
 *
 * @package App
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property integer $type
 * @property string $identifier
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property integer $billing_period_start_day
 * @property string $currency
 * @property integer $status
 * @property string $detail_type
 * @property integer $detail_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Customer $customer
 * @property AgreementMargin[] $margins
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $detail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Microsoft\Azure\Subscription[] $subscriptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereBillingPeriodStartDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereDetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereDetailType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agreement whereUpdatedAt($value)
 */
	class Agreement extends \Eloquent {}
}

namespace App\Models{
/**
 * Class AgreementBillingPeriod
 *
 * @package App\Models
 * @property integer $id
 * @property integer $agreement_id
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property integer $num_sub_periods
 * @property string $identifier
 * @property integer $billing_status
 * @property integer $status
 */
	class AgreementBillingPeriod extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Agreement
 *
 * @package App
 * @property integer $id
 * @property integer $agreement_id
 * @property string $margin
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Agreement $agreement
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AgreementMargin whereUpdatedAt($value)
 */
	class AgreementMargin extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Customer
 *
 * @package App
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $partner_users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer withoutTrashed()
 */
	class Customer extends \Eloquent {}
}

namespace App\Models\Microsoft{
/**
 * App\Models\Microsoft\AadServicePrincipal
 *
 * @property int $id
 * @property int $tenant_id
 * @property string $app_identifier
 * @property string $object_guid
 * @property bool $status
 * @property object $permissions
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $addedByUser
 * @property-read \App\Models\Microsoft\AadTenant $tenant
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereAppIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereObjectGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadServicePrincipal whereUpdatedAt($value)
 */
	class AadServicePrincipal extends \Eloquent {}
}

namespace App\Models\Microsoft{
/**
 * App\Models\Microsoft\AadUser
 *
 * @property int $id
 * @property int $user_id
 * @property int $tenant_id
 * @property string|null $guid
 * @property string|null $email
 * @property bool|null $is_admin
 * @property string|null $properties
 * @property string|null $tokens
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereTokens($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\AadUser whereUserId($value)
 */
	class AadUser extends \Eloquent {}
}

namespace App\Models\Microsoft\Azure{
/**
 * Class Subscription
 *
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Service whereName($value)
 */
	class Service extends \Eloquent {}
}

namespace App\Models\Microsoft\Azure{
/**
 * Class Subscription
 *
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property integer $customer_id
 * @property integer $agreement_id
 * @property string $guid
 * @property integer $aad_tenant_id
 * @property integer $status
 * @property string $name
 * @property string $partner_name
 * @property string $azure_name
 * @property string $offer_code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property \App\Models\Agreement $agreement
 * @property AadTenant $aadTenant
 * @property Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereAadTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereAzureName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereOfferCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription wherePartnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\Subscription whereUpdatedAt($value)
 */
	class Subscription extends \Eloquent {}
}

namespace App\Models\Microsoft\Azure{
/**
 * Class SubscriptionUsage
 *
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property integer $subscription_id
 * @property Carbon $usage_date
 * @property string $usage_month
 * @property integer $service_id
 * @property string $total_cost
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Models\Microsoft\Azure\Service $service
 * @property-read \App\Models\Microsoft\Azure\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereTotalCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereUsageDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Microsoft\Azure\SubscriptionUsage whereUsageMonth($value)
 */
	class SubscriptionUsage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Recommendation
 *
 */
	class Recommendation extends \Eloquent {}
}

namespace App\Models{
/**
 * Class User
 *
 * @package App
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string[] $roles
 * @property integer $customer_id
 * @property Customer $customer
 * @property integer $status
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $customers
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

