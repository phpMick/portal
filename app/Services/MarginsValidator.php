<?php
namespace App\Services;

use App\Models\Agreement;
use Carbon\Carbon;

class MarginsValidator{

    public $failureMessage = [];

    private $lastEndDate = NULL;
    private $agreement_id = NULL;

    public $rowsCollection;

    /**
     * Collect, sort and then validate the arrays
     * in he request.
     *
     * @param $request
     * @throws \Exception
     */
    public function validateMargins($request)
    {
        $this->agreement_id = $request->agreement_id;

        //combine the arrays into a collection
        $this->rowsCollection = $this->collectAndSort($request);

        if(count($this->rowsCollection)===0){
            showMessage('danger','This agreement must have at least one margin. ');
            throw new \Exception('Failed margin validation.');
        }

        if (count($this->rowsCollection) === 1) {

            $row = $this->rowsCollection->first();
            $this->validRow($row);

        } else { //multiple iterate
            $this->validateMultipleRows();
        }

        $this->marginDatesMatchAgreementDates();
    }

    /**
     * Collects the arrays into one collection
     * and assigns row id
     * @param $request
     * @return static
     */
    private function collectAndSort($request)
    {
        //sort the arrays
        $rows = [];

        $rowCount = count($request->start_date);

        //find a better way of doing this
        for($rowOn = 0; $rowOn < $rowCount; $rowOn++ ){
            $row = [
                'rowId' => $rowOn,
                'start_date' => $request->start_date[$rowOn],
                'end_date' => $request->end_date[$rowOn],
                'margin' => $request->margin[$rowOn],
            ];
            $rows[] = $row;
        }

        return  collect($rows)->sortBy('start_date');
    }

    /**
     * Set the object variable so calling
     * method can access.
     * @param $row
     * @param $element
     * @param $message
     */
    private function setFailMessage($row,$element,$message)
    {
        $this->failureMessage = ['rowId'=>$row['rowId'],'element'=>$element,'message'=>$message];
    }


    /**
     * Validate an element in an assoc
     *
     * @param $row
     * @param $element
     * @param $rules
     * @param $message
     * @return bool
     */
    private function validateElement($row,$element,$rules,$message)
    {

        $passed = true;

        if(!validateAssocElement($row,$element,$rules)){
            $this->setFailMessage ($row,$element,$message);
            $passed = false;
        }

        return $passed;
    }

    /**
     * Validation errors are held in the failure message
     * so just throw a generic error to escape.
     *
     * @throws \Exception
     */
    private function throwGenericError(){
        throw new \Exception('Failed margin validation.');
    }

    /**
     * Validate one row of the collection
     *
     * @param $row
     * @throws \Exception
     */
    private function validRow($row)
    {

        if (!$this->validateElement($row,'start_date','date|nullable','Invalid date format.')){
            $this->throwGenericError();
        }

        if (!$this->validateElement($row,'end_date','date|nullable','Invalid date format.')){
            $this->throwGenericError();
        }

        //if both dates: end must be after start
        if(isset($row['start_date']) && isset($row['end_date'])){
            if($row['start_date'] > $row['end_date']){
                $this->setFailMessage ($row,'end_date','Start Date must precede End Date. ');
                $this->throwGenericError();
            }
        }

        if (!$this->validateElement($row,'margin','numeric|min:0','Invalid margin.')){
            $this->throwGenericError();
        }

    }


    /**
     * Iterate through the rows and check that
     * the elements are valid and dates do not overlap
     *
     *
     */
    private function validateMultipleRows(){

        foreach($this->rowsCollection as $row){ //iterate through each margin record
            //standard row check
            $this->validRow($row);

            if($row['rowId'] != 0) { //not on the first row
                $this->consecutiveMargins($row);
            }
            $this->lastEndDate = $row['end_date'];
        } //foreach

        return true; //all rows validated
    }

    /**
     * Check that the margin start and end match the agreement start and end.
     * Nulls are allowed.
     */
    private function marginDatesMatchAgreementDates()
    {

        $agreement = Agreement::findOrFail($this->agreement_id);

        $firstMarginStart = $this->rowsCollection[0]['start_date'];
        if($firstMarginStart != $agreement->start_date){
            $this->setFailMessage($this->rowsCollection[0], 'start_date', "Start date does not correlate with agreement start ($agreement->start_date).");
            $this->throwGenericError();

        }

        $lastRowIndex = count($this->rowsCollection)-1;
        $lastMarginEnd = $this->rowsCollection[$lastRowIndex]['end_date'];
        if($lastMarginEnd != $agreement->end_date){
            $this->setFailMessage($this->rowsCollection[$lastRowIndex], 'end_date', "End date does not correlate with agreement end ($agreement->end_date).");
            $this->throwGenericError();

        }

    }


    /**
     * Check that the dates do no overlap.
     * start_date is inclusive
     * end_date is exclusive
     * start_date should be the day after the previous margins end_date
     *
     * @param $row
     * @throws \Exception
     */
    private function consecutiveMargins($row)
    {
        if(isset($this->lastEndDate)) {
            $nextStartDate = Carbon::createFromFormat('Y-m-d', $this->lastEndDate)->addDay()->toDateString();

            if(($row['start_date'] != $nextStartDate) ){
                $this->setFailMessage($row, 'start_date', 'Margins are not consecutive.');
                $this->throwGenericError();
            }
        }



        //store the end date, to compare in the next row
        $this->lastEndDate = $row['end_date'];

    }


}