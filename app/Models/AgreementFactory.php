<?php

namespace App\Models;


class AgreementFactory{

    //list of service providers

    const MS_ISS = 'ea.microsoftazure.com';

    const AGREEMENTS_DIR = 'App\Models\Microsoft\\';

    const SERVICE_PROVIDERS = [
        ['provider' => 'AZURE','description' => 'Microsoft Azure']
    ];


    //these are all the agreement types
    //AZURE
    const TYPE_UNKNOWN = 0;
    const TYPE_AZURE_PAYG = 101;
    const TYPE_AZURE_OPEN = 102;
    const TYPE_AZURE_MPSA = 104;
    const TYPE_AZURE_EA = 105;
    const TYPE_AZURE_SCE = 106;
    const TYPE_AZURE_CSP = 108;

    //AWS




    //list of agreement types - description, class name
    //used to populate the create agreement select
    const AZURE_TYPES = [
            ['id' => self::TYPE_AZURE_PAYG,'description' => 'Pay As You Go (azure.com)','className'=>'AzurePAYGAgreement','shortDesc'=>'PAYG','userCreatable'=>'true' ],
            ['id' => self::TYPE_AZURE_MPSA,'description' => 'Microsoft Products and Services Agreement','className'=>'AzureEAAgreement','shortDesc'=>'MPSA','userCreatable'=>'true'],
            ['id' => self::TYPE_AZURE_EA, 'description' => 'Enterprise Agreement','className'=>'AzureEAAgreement','shortDesc'=>'EA','hasMargins'=>'false','userCreatable'=>'true'],
            ['id' => self::TYPE_AZURE_SCE,'description' => 'Server and Cloud Agreement','className'=>'AzureEAAgreement','shortDesc'=>'SCE','userCreatable'=>'true'],
            ['id' => self::TYPE_AZURE_CSP,'description' => 'Cloud Service Provider','className'=>'AzureCSPAgreement','shortDesc'=>'CSP','userCreatable'=>'false']
    ];



    //Combined, used in Blade
    const PROVIDERS_AND_TYPES = [
        ['provider' => 'AZURE','types' => self::AZURE_TYPES]

    ];


    /**
     * Filter the PROVIDERS_AND_TYPES so
     * it only contains agreements which are user creatable
     *
     * @return array
     *
     */
    public static function userCreatableAgreements()
    {


        $providers = self::PROVIDERS_AND_TYPES;
        $newArray = [];

        foreach($providers as $provider){
            $newElement['provider'] = $provider['provider'];
            $newElement['types'] = collect($provider['types'])->where('userCreatable','true');
            $newArray[] = $newElement;
        }
        return $newArray;

    }


    /**
     * Build and agreement based in the agreement type id
     *
     * @param $agreement_type_id
     * @return mixed
     */
    public static function makeAgreementWithID($agreement_type_id)
    {

        if (self::findClassNameWithID($agreement_type_id,$className)) {

            $className = self::AGREEMENTS_DIR .  $className ;

            return new $className();

        }

    }

    /**
     * Lookup the className using the agreement_id;
     * @param $agreement_id
     * @param $className byRef
     * @return bool found
     */
    private static function findClassNameWithID($agreement_id,&$className = NULL){

        $found = false;

        $array = self::PROVIDERS_AND_TYPES;

        $className = collect(data_get($array, '*.types.*'))->where('id', $agreement_id)->first()['className'];

        if(isset($className)){
            $found = true;
        }

        return $found;

    }


    /**
     * Returns true if this agreement type has margins.
     * Currently only Azure CSP
     *
     * @param $type
     * @return int
     */
    public static function hasMargins($type)
    {

        return $type == self::TYPE_AZURE_CSP;
    }


    //Moved from Agreement.php

    /**
     * Just returns the description.
     *
     * @param $agreement_type_id
     * @return string
     * @throws \Exception
     */
    static public function typeTextLong($agreement_type_id) {

       return self::getAgreementElement($agreement_type_id,'description');
    }


    /**
     * Just returns the description.
     *
     * @param $agreement_type_id
     * @return string
     * @throws \Exception
     */
    static public function typeTextShort($agreement_type_id) {

        return self::getAgreementElement($agreement_type_id,'shortDesc');

    }


    /**
     *
     * Filters the PROVIDERS_AND_TYPES array by the agreement type
     * Returns the required element
     *
     * @param $agreement_type_id
     * @param $element
     * @return mixed
     * @throws \Exception
     */
    static private function getAgreementElement($agreement_type_id, $element){

        $array = self::PROVIDERS_AND_TYPES;

        $foundElement = collect(data_get($array, '*.types.*'))->where('id', $agreement_type_id)->first()[$element];

        if(!isset($foundElement)){
            throw new \Exception('Invalid Agreement type');
        }else{
            return $foundElement;
        }


    }








}