<?php

namespace App\Models;

use App\Models\Microsoft\Azure\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


/**
 * Class Agreement
 * @package App
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property integer $type
 * @property string $identifier
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property integer $billing_period_start_day
 * @property string $currency
 * @property integer $status
 * @property string $detail_type
 * @property integer $detail_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Customer $customer
 * @property AgreementMargin[] $margins
 */
class Agreement extends Model
{
    use SoftDeletes;

    const STATUS_NEW = 0;
    const STATUS_INITIALISING = 10; // set once access details have been defined but not tested
    const STATUS_OK = 100;
    const STATUS_ERROR_NONEXISTANT = -10;
    const STATUS_ERROR_NO_CREDENTIALS = -20;
    const STATUS_ERROR_DENIED = -25;
    const STATUS_ERROR_NO_DATA = -50;
    const STATUS_ERROR_APPLICATION_ERROR = -120;
    const STATUS_ERROR_UNKNOWN = -125;
    const STATUS_DELETED = -127;
    const STATUS_ALL = [
        self::STATUS_NEW,
        self::STATUS_INITIALISING,
        self::STATUS_OK,
        self::STATUS_ERROR_NONEXISTANT,
        self::STATUS_ERROR_NO_CREDENTIALS,
        self::STATUS_ERROR_DENIED,
        self::STATUS_ERROR_NO_DATA,
        self::STATUS_ERROR_APPLICATION_ERROR,
        self::STATUS_DELETED,
    ];

    //Relationships-----------------------------------------------------------------------------------------------------

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_group()
    {
        return $this->belongsTo('App\Models\UserGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function detail()
    {
        return $this->morphTo('detail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function margins()
    {
        //need the foreign key because it gets determined from class name and will be wrong for the subclasses
        return $this->hasMany(AgreementMargin::class,'agreement_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    //Relationships End-------------------------------------------------------------------------------------------------

    /**
     *
     * Returns the margins for this agreement for the selected user_group
     * @return \Illuminate\Support\Collection
     */
    public function getAgreementMarginsForUserGroup()
    {
        $results = DB::table('agreement_margins')->select('*')
            ->where('agreement_id', '=', $this->id)
            ->where('user_group_id', '=', UserGroup::getSelectedGroupID())
            ->orderBy('start_date')
            ->get();
        return $results;
    }


    /**
     * Get all the agreements for the selected group and it's descendants
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getAgreementsForSelectedAndDescendants()
    {
        //get an array of all the groups
        $groupIds = UserGroup::getSelectedAndDescendantsIds();

        $results = Agreement::whereIn('user_group_id',$groupIds)
            ->get();

        return $results;
    }

    /**
     * Delete all existing agreement margins
     * then add the new ones
     *
     * @param $margins
     */
    public function syncMargins($margins)
    {
        $selectedUserGroup = UserGroup::getSelectedGroupID();
        //delete them all - only for this user group
        $this->margins()->where('user_group_id','=',$selectedUserGroup)->delete();

        foreach ( $margins as $margin) {
            $agreementMargin = AgreementMargin::make([
                'user_group_id'=> $selectedUserGroup,
                'start_date'=>$margin['start_date'],
                'end_date'=>$margin['end_date'],
                'margin'=>$margin['margin'],
                        ]);

            $this->margins()->save($agreementMargin);
        }
    }

    /**
     * Just returns the description.
     * All this functionality now moved to the factory.
     *
     * @param $type
     * @return string
     * @throws \Exception
     */
    static public function typeTextLong($type)
    {
        return AgreementFactory::typeTextLong($type);
    }

    /**
     * Give me an agreement type and I
     * will tell you the description.
     *
     * All this functionality now moved to the factory.
     *
     * @param $type
     * @return string
     * @throws \Exception
     */
    static public function typeTextShort($type)
    {

        return AgreementFactory::typeTextShort($type);
    }

    /**
     * Get the text description for this agreement
     * @return string
     * @throws \Exception
     */
    public function getTypeTextLong()
    {
        if (is_null($this->type)) {
            return '';
        }
        return AgreementFactory::typeTextLong($this->type);
    }

    /**
     * Get the status of this agreement.
     *
     * @return string
     */
    public function getStatusText()
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'New - not connected';
            case self::STATUS_INITIALISING:
                return 'New - initialising data';
            case self::STATUS_OK:
                return 'OK';
            case self::STATUS_ERROR_NO_CREDENTIALS:
                return 'API credentials required';
            default:
                return '?';
        }
    }

    /**
     * Get the short text description for this agreement
     *
     * @return string
     * @throws \Exception
     */
    public function getTypeText()
    {
        if (is_null($this->type)) {
            return '';
        }
        return AgreementFactory::typeTextShort($this->type);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getIdentifierText()
    {
        $t = $this->getTypeText();
        $s = $this->identifier;
        if (empty($t)) {
            return $s ?: '';
        }
        if (empty($s)) {
            return '';
        }
        return $t . '-' . $s;
    }


    /**
     * Everyone should view
     * Admin can edit.
     *
     * @return array
     */
    public function getActionList()
    {
        $user = auth()->user();

        $actions = [
            'View Usage' => false
        ];

        $actions['View Details'] = route('agreements.show', [$this]);

        //are they an admin for this usergroup
        if($user->isAdminOfSelectedGroup())
        {
            $actions['Edit Details'] = route('agreements.edit', [$this]);
            $actions['Delete'] = route('agreements.delete', [$this]);

            if(AgreementFactory::hasMargins($this->type)){ //mbtodo is this correct?
                $actions['View Margins'] = route('agreements.margins.show', [$this]);
            }

            if ($this->isEA() AND $this->status <= 0) {
                $actions['Initialise'] = route('agreements.discover', [$this]);
            } elseif (!$this->isEA() AND $this->status <= 0) {
                $actions['Add Subscription'] = route('agreements.add-subscription', $this);
            }
        }
        return $actions;
    }

    /**
     * Is this an enterprise agreement?
     * @return bool
     */
    public function isEA()
    {
        return in_array($this->type, [
            AgreementFactory::TYPE_AZURE_EA,
            AgreementFactory::TYPE_AZURE_SCE,
            AgreementFactory::TYPE_AZURE_MPSA
        ]);
    }


    /**
     * If enrollment_number is in the details array
     * save it to the identifier field
     *
     * @param $value
     *
     */
    public function setDetailsAttribute($value)
    {
        if (array_has($value, 'enrollment_number')) {
            $this->identifier = $value['enrollment_number'];
        }

        //save array as JSON
        $this->attributes['details'] = json_encode($value);
    }

    /**
     * JSON to array.
     * @param $value
     * @return mixed
     */
    public function getDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

}

