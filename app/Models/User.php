<?php

namespace App\Models;

//Laravel
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

//Models
use App\Models\UserGroup;

/**
 * Class User
 * @package App
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string[] $roles
 * @property integer $customer_id
 * @property Customer $customer
 * @property integer $status
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ADMIN_NAME = 'Admin';
    const ADMIN = 1;

    const USER_NAME = 'User';
    const USER = 2;

    const ROLES = [
        ['id' => self::USER, 'description' => self::USER_NAME],
        ['id' => self::ADMIN, 'description' => self::ADMIN_NAME]
    ];


    const STATUS_INVITED = 0;
    const STATUS_REGISTERED = 5;
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = -10;
    const STATUS_SUSPENDED = -100;
    const STATUS_DELETED = -120;
    const STATUS_ALL = [
        self::STATUS_DELETED,
        self::STATUS_SUSPENDED,
        self::STATUS_INACTIVE,
        self::STATUS_INVITED,
        self::STATUS_REGISTERED,
        self::STATUS_ACTIVE,
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_group_id',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /*Relationships ---------------------------------------------------------------------------------------------------*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function aadUser()
    {
        return $this ->hasOne ( 'App\Models\Microsoft\AadUser' );
    }

    /**
     * Testing the new user hierarchy
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_group()
    {
        return $this->belongsTo('App\Models\UserGroup');
    }

    /*Relationships End------------------------------------------------------------------------------------------------*/

    public function isAadUser()
    {
        $passed = false;

        //must be in a group that has a tenant
        $userGroup = $this->user_group->aad_tenant;

        if(isset($userGroup)){
            $passed = true;
        }

        return  $passed;
    }


    /**
     *
     * Just check the password reset token for a new user.
     * @param string $token
     * @return bool
     */
    public function checkToken(string $token)
    {
        $passed = false;

        $resetRecord = DB::table('password_resets')
            ->select('token')
            ->where('email','=',$this->email)
            ->first();

        if(isset($resetRecord)){

            if(Hash::check($token, $resetRecord->token)){
                $passed = true;
            }
        }

        return $passed;
    }

    /**
     * 1, If a group is selected, are they admin?
     * 2, If no group selected, are they admin in own group
     */
    public function canPerformAdminTask()
    {
        $passed = false;

        if (session()->has('selectedUserGroup')) {
            if ($this->isAdminOfSelectedGroup()) {
                $passed = true;
            }
        } else {
            if ($this->role === User::ADMIN) {
                $passed = true;
            }
        }

        return $passed;

    }

    /**
     * Just swap the id from the description
     * @param $id
     * @return bool
     */
    public static function getRoleNameWithId($id)
    {

        $array = self::ROLES;

        $roleName = collect($array)->where('id',$id)->first()['description'];

        if(isset($roleName)){
            return $roleName;
        }else{
            return false;
        }

    }

    /**
     * Just swap the id from the description
     * @return bool
     */
    public  function roleName()
    {

        $array = self::ROLES;

        $roleName = collect($array)->where('id',$this->role)->first()['description'];

        if(isset($roleName)){
            return $roleName;
        }else{
            return false;
        }

    }

    /**
     * Retrieve all the groups for this user from the nested set
     *
     */
    public function getDescendants()
    {
        //get the top level user group
        $user_group = $this->user_group;

        $node = UserGroup::findOrFail($user_group->id);
        $descendants = $node->descendants;

        return $descendants;
    }

    /**
     * Get the roles (user_groups) from the db.
     * Save to the session.
     *
     */
    public function storeUserGroups()
    {

        //if we don't have a user group,
        //they are a no access user, boot out

      /*  if(!isset($this->user_group)){

            Auth::logout();

            session(['message' => 'Account not activated.']);
            session(['detail' =>'Please contact your administrator to activate your account.']);

            //this won't work here
            return view('message-page');

        }//else {*/

            //$descendants = $this->getDescendants();
            //session(['userGroup' => $this->user_group]);

         /*   session(['descendants' => $descendants]);

            if(count($descendants) === 0){// just a basic customer user
                session(['selectedUserGroup' => $this->user_group]);
            }*/
        //}
    }


    /**
     * Just returns an array of the Ids
     *
     *
     * @return mixed
     */
    public function getAllowedUserGroupIds()
    {
        $user_group = $this->user_group;

        $groups = UserGroup::descendantsAndSelf($user_group->id);

        $Ids = $groups->pluck('id');

        return $Ids->toArray();
    }

    /**
     * Get user's user_group and descendants
     * Could use session to improve performance
     *
     * @return mixed
     */
    public function getAllowedUserGroups()
    {
        $user_group = $this->user_group;

        $groups = UserGroup::descendantsAndSelf($user_group->id);
        return $groups;
    }

    /**
     * Is the user an admin in user_group Master
     *
     */
    public function isMasterAdmin()
    {
        $passed = false;
        $user =auth()->user();

        $masterGroup = UserGroup::where('name',UserGroup::MASTER_NAME)->first();
        if($masterGroup) {
            if ($user->user_group_id == $masterGroup->id && $user->role = self::ADMIN) {
                $passed = true;
            }
        }
        return $passed;
    }

    /**
     * Are they an admin for their user group?
     *
     * @return bool
     */
    public function isAdmin()
    {
        $passed = false;

        //are they an admin
        if ($this->role == User::ADMIN) {
            $passed = true;
        }

        return $passed;
    }

    /**
     * Can the user admin this group.
     *
     * @param $userGroup
     * @return bool
     */
    public function isGroupAdmin($userGroup)
    {
        $passed = false;

        //are they an admin
        if($this->role == User::ADMIN ){

            //is this user group in their user_group or descendants
            $allgroups = $this->getAllowedUserGroups();

            if($allgroups->contains('id',$userGroup->id)){
                $passed = true;
            }
        }
        return $passed;
    }

    /**
     * Is this user and Admin and is the selected
     * group in their user_groups
     */
    public function isAdminOfSelectedGroup()
    {
        $selectedGroup = session('selectedUserGroup');

        if($selectedGroup) {
            return $this->isGroupAdmin($selectedGroup);
        }

    }

    /**
     * Does this user have view access to the selected group
     * @return bool
     */
    public function canViewSelectedUserGroup()
    {
        $selectedGroup = session('selectedUserGroup');

        return $this->canViewUserGroup($selectedGroup);
    }

    /**
     * Does the user have view (or admin)
     * access to this group
     *
     * @param $userGroup
     * @return bool
     */
    public function canViewUserGroup($userGroup)
    {
        $passed = false;

        //is this user group in their user_group or descendants
        $allgroups = $this->getAllowedUserGroups();

        if($allgroups->contains('id',$userGroup->id)){
            $passed = true;
        }

        return $passed;
    }

    /**
     * Check that this user has this user_group on there user_groups;
     *
     *
     * @param $name
     * @return bool
     */
    public function hasUserGroupName($name)
    {
        $hasRole = false;

        $collection = session('user-groups');

        $found = $collection->where('name', $name);

        if(count($found) >0)
        {
          $hasRole = true;
        }

        return $hasRole;
    }

    /**
     *
     * String:  does the user have this user_group in their user_groups.
     * Array: does this user any of the user_groups in the array.
     * @param $nameOrArray
     * @return bool
     */
    public function hasAnyUserGroupName($nameOrArray)
    {
        $found = false;

        if (is_string($nameOrArray)) {

            $found =  $this->hasUserGroupName($nameOrArray);

        }elseif (is_array($nameOrArray)) {
            $found =  $this->hasAnyMatchingUserGroup($nameOrArray);
        }
        return $found;
    }

    /**
     * Give me an array of roles and I will see
     * if this user has at least one of them
     * @param $roleArray
     * @return bool
     */
    public function hasAnyMatchingUserGroup($roleArray)
    {
        foreach($roleArray as $role){
          if($this->hasUserGroupName($role)){
              return true;
          }
        }

        return false;
    }

    /**
     * @param $email
     * @return User
     */
    static public function findByEmail($email)
    {
        return self::where('email', '=', $email)->firstOrFail();
    }

    public function statusString()
    {
        switch ($this->status) {
            case self::STATUS_INVITED:
                return 'Invited';
            case self::STATUS_REGISTERED:
                return 'Registered';
            case self::STATUS_ACTIVE:
                return 'Active';
            case self::STATUS_INACTIVE:
                return 'Inactive';
            case self::STATUS_SUSPENDED:
                return 'Suspended';
        }
        return 'INVALID';
    }

}
