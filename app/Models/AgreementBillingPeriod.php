<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AgreementBillingPeriod
 * @package App\Models
 * @property integer $id
 * @property integer $agreement_id
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property integer $num_sub_periods
 * @property string $identifier
 * @property integer $billing_status
 * @property integer $status
 */
class AgreementBillingPeriod extends Model
{





}
