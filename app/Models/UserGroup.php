<?php

namespace App\Models;

use App\Models\Microsoft\AadTenant;
use Illuminate\Database\Eloquent\Model;
use Exception;
//nested set
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{

    use SoftDeletes;

    const MASTER_NAME = 'Master';
    const MASTER_TYPE = 1;

    const PARTNER_NAME = 'Partner';
    const PARTNER_TYPE = 2;

    const RESELLER_NAME = 'Reseller';
    const RESELLER_TYPE = 3;

    const CUSTOMER_NAME = 'Organisation';
    const CUSTOMER_TYPE = 4;

    const DEPARTMENT_NAME = 'Department';
    const DEPARTMENT_TYPE = 5;


    const TYPES = [
        ['id' => self::MASTER_TYPE, 'description' => self::MASTER_NAME],
        ['id' => self::PARTNER_TYPE, 'description' => self::PARTNER_NAME],
        ['id' => self::RESELLER_TYPE, 'description' => self::RESELLER_NAME],
        ['id' => self::CUSTOMER_TYPE, 'description' => self::CUSTOMER_NAME],
        ['id' => self::DEPARTMENT_TYPE, 'description' => self::DEPARTMENT_NAME]
    ];

    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'properties',
        'parent_name'

    ];

    use NodeTrait;

    protected $casts = [
        'properties' => 'array'
    ];


//Relationships----------------------------------------------------------------------------------------------------------

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function aad_tenant ()
    {
        return $this ->hasOne( AadTenant::class );
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aad_users() {
        return $this->hasMany(AadUser::class);
    }


    /**
     * Get the users in this group
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    /**
     * Get the agreements in this group
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }

//Relationships-End------------------------------------------------------------------------------------------------------

    /**
     * Return the user group tree for the
     * selected user
     *
     * @return mixed
     */
    public static function getTreeForSelectedUser()
    {
        //get the groups for this user
        $allowedIDs = self::getLoggedInGroupAndDescendantsIds();

        //$userGroupsString = UserGroup::get()->toTree()->toJson();
        $userGroups = UserGroup::select("id", "name AS label","type","_lft","_rgt","parent_id","properties","parent_name")
                    ->whereIn('id',$allowedIDs)
                    ->get()
                    ->toTree()
                    ->toJson();

        return $userGroups;
    }

    /**
     * For creating new user groups.
     * Only allow types below the current one.
     * @return array
     *
     */
    public static function getRolesBelowCurrent()
    {
        $userGroup = session('selectedUserGroup');
        $selectedType = $userGroup->type;
        //could be 1,2,3,4 only return greater than
        $allTypes = self::TYPES;
        $returnArray = [];

        foreach($allTypes as $type ){
            if($type['id'] > $selectedType){
                $returnArray[] = $type;
            }
        }

        return $returnArray;
    }

    /**
     * Just returns the name for the group type.
     * @return bool
     */
    public function getTypeName()
    {

        $array = self::TYPES;

        $typeName = collect($array)->where('id',$this->type)->first()['description'];

        if(isset($typeName)){
            return $typeName;
        }else{
            return false;
        }

    }

    /**
     * Has this group got any children?
     *
     * @return bool
     */
    public function hasUsers()
    {
        $users = $this->users;

        if(count($users)>0){
            return true;
        }
        return false;
    }

    /**
     * Has this group got any children?
     *
     * @return bool
     */
    public function hasDescendants()
    {
        $descendants = $this->descendants;

        if(count($descendants)>0){
            return true;
        }
        return false;
    }

    /**
     * Just return the name of the parent.
     *
     *
     */
    public function getParentName()
    {
        $parent = $this->parent;

        if(isset($parent)){
            return $parent->name;
        }else{
            return "None";
        }
    }

    /**
     * Retrieve all the groups for this user from the nested set
     *
     */
    public static function getSelectedAndDescendants()
    {
        //get the top level user group
        $userGroup = session('selectedUserGroup');

        $groups = self::descendantsAndSelf($userGroup->id);

        return $groups;
    }

    /**
     * Just returns an array of the Ids
     *
     *
     * @return mixed
     */
    public static function getSelectedAndDescendantsIds()
    {
        $userGroup = session('selectedUserGroup');

        $groups = UserGroup::descendantsAndSelf($userGroup->id);

        $Ids = $groups->pluck('id');

        return $Ids->toArray();
    }

    /**
     * Just returns an array of the Ids
     *
     *
     * @return mixed
     */
    public static function getLoggedInGroupAndDescendantsIds()
    {
        $user = auth()->user();

        $userGroup = $user->user_group_id;

        $groups = UserGroup::descendantsAndSelf($userGroup);

        $Ids = $groups->pluck('id');

        return $Ids->toArray();
    }

    /**
     * Just pass back the name of the selected group
     *
     * @return mixed
     */
    public static function getSelectedGroupName()
    {

        $userGroup = session('selectedUserGroup');

        if(isset($userGroup)){
            return $userGroup->name;
        }
    }

    /**
     * Just pass back the id of the selected group
     *
     * @return mixed
     */
    public static function getSelectedGroupID()
    {
        $userGroup = session('selectedUserGroup');

        if(isset($userGroup)){
            return $userGroup->id;
        }
    }

    /**
     * Just swap the id from the description
     * @param $id
     * @return bool
     */
    public static function findTypeNameWithID($id)
    {

        $array = self::TYPES;

        $typeName = collect($array)->where('id',$id)->first()['description'];

        if(isset($typeName)){
            return $typeName;
        }else{
            return false;
        }

    }

    public static function getMasterGroup()
    {
        return UserGroup::where('name','=',UserGroup::MASTER_NAME)->firstOrFail();
    }

    /**
     * Is this user group is a customer type is will have a code in the properties
     *
     */
    public function getCodeAttribute()
    {

        return $this->getCustomerProperty('code');

    }

    /**
     * Checks that this a customer
     * and returns the property from the JSON
     *
     * @param $property
     * @return bool
     */
    private function getCustomerProperty($property)
    {
        $return = false;

        if($this->type == self::CUSTOMER_TYPE) {
            $properties = $this->properties;
            if (isset($properties)) {
                $value = data_get($properties,$property);
                $return = $value;
            }
        }
        return $return;
    }

    /**
     * Delete the user group if:
     * 1, It has no users.
     * 2, It does not have any descendants.
     * 3, It is not the current users user_group_id
     *
     */
    public function safeDelete()
    {
        $user = auth()->user();

        try {

            if ($this->hasUsers()) {
                throw new Exception("You cannot delete a group which contains users.");
            }

            //does this group have children
            if ($this->hasDescendants()) {
                throw new Exception("You cannot delete group with contains subgroups.");
            }

            //cannot delete own group
            if ($user->user_group_id == $this->id) {
                throw new Exception('You cannot delete your own group.');
            }

            $this->delete();
            showMessage('success','User group deleted');

        } catch(Exception $e) {
            showMessage('danger',$e->getMessage());
        }
    }

}
