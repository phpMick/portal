<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;




/**
 * Class Agreement
 * @package App
 * @property integer $id
 * @property integer $agreement_id
 * @property string $margin
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Agreement $agreement
 */
class AgreementMargin extends Model
{

    protected $fillable = [
        'start_date',
        'end_date',
        'margin',
        'user_group_id',
        'agreement_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement() {
        return $this->belongsTo(Agreement::class);
    }

    public function getMarginAttribute($value){
        return number_format($value,2);
    }





}
