<?php

namespace App\Models\Microsoft;


use App\Models\Agreement;

class AzurePAYGAgreement extends Agreement {

    protected $table = 'agreements';


    const PARTIAL_NAME = 'agreement/partials/AzurePAYG';

    //details
    const DETAILS = [
    ];


    const CUSTOM_MESSAGES = [
    ];



    /**
     * The validation rules
     *
     * @return array
     */
    public static function getRules(){

        $rules = [
        ];

        return $rules;

    }



}