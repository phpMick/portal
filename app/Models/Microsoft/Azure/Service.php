<?php

namespace App\Models\Microsoft\Azure;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Service extends Model
{

    protected $table = 'azure_services';

    public $timestamps = FALSE;

    /**
     * @param $name
     * @return Model|null|static
     */
    static public function findByName($name) {
        return self::query()->where('name', '=', $name)->first();
    }

}
