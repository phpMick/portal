<?php

namespace App\Models\Microsoft\Azure;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Agreement
 * @package App\Models\Microsoft\Azure
 * @property int $id
 * @property int $customer_id
 * @property string|null $name
 * @property int|null $type
 * @property string|null $identifier
 * @property float|null $margin
 * @property string|null $purchase_region
 * @property string|null $access_key
 * @property string|null $access_secret
 * @property integer $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Microsoft\Azure\Subscription[] $subscriptions
 */
class Agreement extends Model
{

    const TYPE_EA = 10;
    const TYPE_SCE = 20;
    const TYPE_MPSA = 30;
    const TYPE_PAYG = 200;
    const TYPE_CSP = 100;
    const TYPE_ALL = [self::TYPE_EA, self::TYPE_SCE, self::TYPE_MPSA, self::TYPE_CSP, self::TYPE_PAYG];

    const STATUS_NEW = 0;
    const STATUS_INITIALISING = 10; // set once access details have been defined but not tested
    const STATUS_OK = 100;
    const STATUS_ERROR_NONEXISTANT = -10;
    const STATUS_ERROR_NO_CREDENTIALS = -20;
    const STATUS_ERROR_DENIED = -25;
    const STATUS_ERROR_NO_DATA = -50;
    const STATUS_ERROR_APPLICATION_ERROR = -120;
    const STATUS_ERROR_UNKNOWN = -125;
    const STATUS_DELETED = -127;
    const STATUS_ALL = [
        self::STATUS_NEW,
        self::STATUS_INITIALISING,
        self::STATUS_OK,
        self::STATUS_ERROR_NONEXISTANT,
        self::STATUS_ERROR_NO_CREDENTIALS,
        self::STATUS_ERROR_DENIED,
        self::STATUS_ERROR_NO_DATA,
        self::STATUS_ERROR_APPLICATION_ERROR,
        self::STATUS_DELETED,
    ];


    protected $table = 'azure_agreements';

    static public function typeTextLong($type) {
        switch($type) {
            case self::TYPE_EA: return  'Enterprise Agreement (EA)';
            case self::TYPE_SCE: return  'Server and Cloud Agreement (SCE)';
            case self::TYPE_MPSA: return  'Microsoft Products and Services Agreement (MPSA)';
            case self::TYPE_PAYG: return  'Pay as you Go (azure.com)';
            case self::TYPE_CSP: return  'Microsoft Cloud Agreement (MCA/CSP)';
        }
        throw new \Exception('Invalid Azure Agreement type');
    }

    static public function typeTextShort($type) {
        switch($type) {
            case self::TYPE_EA: return  'EA';
            case self::TYPE_SCE: return  'SCE';
            case self::TYPE_MPSA: return  'MPSA';
            case self::TYPE_PAYG: return  'PAYG';
            case self::TYPE_CSP: return  'MCA';
        }
        throw new \Exception('Invalid Azure Agreement type');
    }

    public function __construct(array $attributes = [])
    {
        throw new \Exception('deprecated');
        parent::__construct($attributes);
    }

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions() {
        return $this->hasMany(Subscription::class);
    }


    public function getStatusText() {
        switch($this->status) {
            case self::STATUS_NEW: return 'New - not connected';
            case self::STATUS_INITIALISING: return 'New - initialising data';
            case self::STATUS_OK: return 'OK';
            case self::STATUS_ERROR_NO_CREDENTIALS: return 'API credentials required';
            default: return '?';
        }
    }

    public function getTypeTextLong() {
        if(is_null($this->type)) return '';
        return self::typeTextLong($this->type);
    }

    public function getTypeText() {
        if(is_null($this->type)) return '';
        return self::typeTextShort($this->type);
    }

    public function getIdentifierText() {
        $t = $this->getTypeText();
        $s = $this->identifier;
        if(empty($t)) {
            return $s ?: '';
        }
        if(empty($s)) {
            return '';
        }
        return $t.'-'.$s;
    }

    public function getActionList() {
        // TODO: ensure user has permission before presenting them with the option
        $actions = [
            'View Usage' => FALSE,
            'Edit Details' => route('agreements.edit', [$this]),
        ];
        if($this->isEA() AND $this->status<=0) {
            $actions['Initialise'] = route('agreements.discover', [$this]);
        } elseif(!$this->isEA() AND $this->status<=0) {
            $actions['Add Subscription'] = route('agreements.add-subscription', $this);
        }

        $actions['Delete'] = route('agreements.delete', [$this]);

        return $actions;
    }

    public function isEA() {
        return in_array($this->type, [
            self::TYPE_EA,
            self::TYPE_SCE,
            self::TYPE_MPSA
        ]);
    }

}
