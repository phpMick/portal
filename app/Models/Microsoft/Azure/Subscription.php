<?php

namespace App\Models\Microsoft\Azure;

use App\Models\Microsoft\AadTenant;
use App\Models\Customer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property integer $customer_id
 * @property integer $agreement_id
 * @property string $guid
 * @property integer $aad_tenant_id
 * @property integer $status
 * @property string $name
 * @property string $partner_name
 * @property string $azure_name
 * @property string $offer_code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property \App\Models\Agreement $agreement
 * @property AadTenant $aadTenant
 * @property Customer $customer
 */
class Subscription extends Model
{

    const STATUS_NEW = 0;
    const STATUS_AAD_SP_PROVISIONED = 50;
    const STATUS_CONNECTED = 100;
    const STATUS_CONNECTION_OK = 110;
    const STATUS_REFRESHING = 127;
    const STATUS_ERROR_NO_TENANT_DEFINED = -50;
    const STATUS_ERROR_TENANT_CHANGED = -51;
    const STATUS_ERROR_NO_AAD_SP = -55;
    const STATUS_ERROR_AAD_SP_PROBLEM = -60;
    const STATUS_ERROR_AAD_SP_TOKEN_INVALID = -61;
    const STATUS_ERROR_NO_PERMISSION = -70;
    const STATUS_ERROR_NO_WRITE_PERMISSION = -90;
    const STATUS_DELETED = -127;


    protected $table = 'azure_subscriptions';

    protected $casts = [
        'properties' => 'object'
    ];


    public function getRouteKeyName()
    {
        return 'guid';
    }

    /**
     * @param array $properties
     * @param User|NULL $user
     * @param Agreement|NULL $agreement
     * @param bool $saveToDatabase
     * @return Subscription
     * @throws \Exception
     */
    static public function createOrUpdateFromApi($properties, User $user=NULL, Agreement $agreement=NULL, $saveToDatabase=TRUE) {
        assert('is_array($properties)');
        foreach(array('subscriptionId','displayName') as $r) {
            if(!array_key_exists($r, $properties)) {
                throw new \Exception('Incomplete response from Azure API');
            }
        }
        $sub = self::where('guid', '=', $properties['subscriptionId'])->first();
        if(!($sub instanceof Subscription) OR empty($sub->id)) {
            $sub = new Subscription();
        }
        $sub->guid = $properties['subscriptionId'];
        $sub->name = $properties['displayName'];
        if($agreement instanceof Agreement) {
            $sub->agreement()->associate($agreement);
        }
        if($saveToDatabase) {
            $sub->save();
        }
        return $sub;
    }

    /**
     * @param $guid
     * @return Subscription|NULL
     */
    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement() {
        return $this->belongsTo(\App\Models\Agreement::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aadTenant() {
        return $this->belongsTo(AadTenant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(Customer::class);
    }


    public function getName($partnerUser=FALSE) {
        return ($partnerUser ? $this->partner_name : NULL) ?: $this->name ?: $this->azure_name ?: '';
    }

    public function getStatusText() {
        switch ($this->status) {
            case self::STATUS_NEW: return 'New: connection required';
            case self::STATUS_AAD_SP_PROVISIONED: return 'New: connection required';
            case self::STATUS_CONNECTED: return 'OK: awaiting data';
            case self::STATUS_REFRESHING: return 'OK: refreshing data';
            case self::STATUS_CONNECTION_OK: return 'OK';
            case self::STATUS_ERROR_TENANT_CHANGED: return 'ERROR: Subscription migrated';
            case self::STATUS_ERROR_NO_AAD_SP: return 'ERROR: Application access removed';
            case self::STATUS_ERROR_AAD_SP_PROBLEM: return 'ERROR: Application access removed';
            case self::STATUS_ERROR_NO_PERMISSION: return 'ERROR: Application access removed';
            case self::STATUS_DELETED: return '';
        }
        throw new \Exception('Invalid Azure Subscription status');
    }

    public function getStatusClass() {
        if($this->status<0) {
            return 'danger';
        }
        if($this->status>=self::STATUS_CONNECTED) {
            return 'success';
        }
        return 'warning';
    }

    public function getActionList() {
        // TODO: ensure user has permission before presenting them with the option
        $actions = [
            'View Usage' => FALSE,
            'Edit Details' => route('azure.subscriptions.edit', [$this]),
        ];
        if($this->status<0 AND $this->status!==self::STATUS_DELETED) {
            $actions['Reconnect'] = route('azure.subscriptions.connect');
        }
        if(in_array($this->status, [self::STATUS_NEW, self::STATUS_AAD_SP_PROVISIONED])) {
            $actions['Connect'] = route('azure.subscriptions.connect', $this);
        }

        $actions['Delete'] = route('azure.subscriptions.delete', [$this]);

        return $actions;
    }


}
