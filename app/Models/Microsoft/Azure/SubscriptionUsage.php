<?php

namespace App\Models\Microsoft\Azure;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscriptionUsage
 * @package App\Models\Microsoft\Azure
 * @property integer $id
 * @property integer $subscription_id
 * @property Carbon $usage_date
 * @property string $usage_month
 * @property integer $service_id
 * @property string $total_cost
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class SubscriptionUsage extends Model
{
    protected $table = 'azure_subscription_usages';

    protected $dates = [
        'created_at',
        'updated_at',
        'usage_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service() {
        return $this->belongsTo(Service::class);
    }

    /**
     * @param integer|Subscription $subscription
     * @param string|Carbon $usage_date
     * @param integer|Service $service
     * @return Model|null|static
     * @throws \Exception
     */
    static public function findByValues($subscription, $usage_date, $service) {
        $query = self::query();
        if(is_integer($subscription)) {
            $query->where('subscription_id', '=', $subscription);
        } elseif($subscription instanceof Subscription) {
            $query->where('subscription_id', '=', $subscription->id);
        } else {
            throw new \Exception('Invalid type for subscription');
        }
        if(is_string($usage_date) AND preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $usage_date)) {
            $query->where('usage_date', '=', $usage_date);
        } elseif($usage_date instanceof Carbon) {
            $query->where('usage_date', '=', $usage_date->toDateString());
        } else {
            throw new \Exception('Invalid type/format for usage date');
        }
        if(is_integer($service)) {
            $query->where('service_id', '=', $service);
        } elseif($service instanceof Service) {
            $query->where('service_id', '=', $service->id);
        } else {
            throw new \Exception('Invalid type for service');
        }
        return $query->first();
    }


}
