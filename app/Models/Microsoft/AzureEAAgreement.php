<?php

namespace App\Models\Microsoft;


use App\Models\Agreement;

//Custom Rules
use App\Rules\ValidJWT;

class AzureEAAgreement extends Agreement {

    protected $table = 'agreements';

    const PARTIAL_NAME = 'agreement/partials/AzureEA';

    //details
    const DETAILS = [
        'access_key',
        'enrollment_number'

    ];




    //There needs to be an element in this array for each rule (rule, not field)
    //'details.access_key.numeric' => 'Just a test'
    const CUSTOM_MESSAGES = [
        //'details.access_key.required' => 'The access key is required.'
        'details.enrollment_number.required_with' => 'The enrolment number is required.',
        'details.enrollment_number.min' => 'The enrolment number must be at least 4 characters.'
    ];


    /**
     * The validation rules
     *
     * @return array
     */
    public static function getRules(){

        //the enrollment number is needed to validate the JWT
        $enrollmentNumber = request()->input('details.enrollment_number');

        $rules = [
            'details.access_key' => ['nullable',new ValidJWT($enrollmentNumber)],
            'details.enrollment_number' => 'required_with:details.access_key|nullable|min:4',

        ];


        return $rules;

    }


}