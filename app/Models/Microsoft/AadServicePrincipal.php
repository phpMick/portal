<?php

namespace App\Models\Microsoft;

use Illuminate\Database\Eloquent\Model;


class AadServicePrincipal extends Model
{

    const STATUS_EXISTS = 0;
    const STATUS_CONNECTED_READONLY = 50;
    const STATUS_CONNECTED_READWRITE = 100;
    const STATUS_ERROR_UNKNOWN = -127;

    protected $casts = [
        'permissions' => 'object'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant() {
        return $this->belongsTo(AadTenant::class, 'tenant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addedByUser() {
        return $this->belongsTo(\App\Models\User::class);
    }

}
