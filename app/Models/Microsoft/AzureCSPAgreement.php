<?php

namespace App\Models\Microsoft;


use App\Models\Agreement;

class AzureCSPAgreement extends Agreement {

    protected $table = 'agreements';

    const PARTIAL_NAME = 'agreement/partials/AzureCSP';

    const CUSTOM_MESSAGES = [
    ];

    /**
     * The validation rules
     *
     * @return array
     */
    public static function getRules(){

        $rules = [
        ];

        return $rules;

    }


}