<?php

namespace App\Models\Microsoft;


use Illuminate\Database\Eloquent\Model;

class AadUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'email',
        'is_admin',
        'tokens',
        'tenant_id'
    ];

    public function user()
    {
        return $this ->belongsTo ( 'App\Models\User' );
    }

}
