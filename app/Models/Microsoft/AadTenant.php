<?php

namespace App\Models\Microsoft;

use App\Models\UserGroup;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;

/**
 * Class AadTenant
 * @package App\Models\Microsoft
 * @property integer $id
 * @property string $guid
 * @property string name
 * @property string[] $domains
 * @property Carbon $created_at
 * @property Carbon $updated_st
 */
class AadTenant extends Model
{

    protected $fillable = [
        'guid',
        'name',
        'domains',
        'user_group_id'
    ];

    protected $casts = [
        'domains'=>'array'
    ];


    public function user_group()
    {
        return $this ->belongsTo (UserGroup::class);
    }




    /**
     * @param string $guid
     * @return static|NULL
     */
    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }

    /**
     * @param null $app_identifier
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicePrincipals($app_identifier = NULL) {
        $query = $this->hasMany(AadServicePrincipal::class, 'tenant_id');
        if(!is_null($app_identifier)) {
            if(is_array($app_identifier))  {
                $query->whereIn('app_identifier', $app_identifier);
            } elseif(is_string($app_identifier)) {
                $query->where('app_identifier', '=', $app_identifier);
            }
        }
        return $query;
    }

    /**
     * @param null $app_identifier
     * @return Model|null|static
     */
    public function servicePrincipal($app_identifier) {
        $query = $this->hasOne(AadServicePrincipal::class, 'tenant_id');
        $query->where('app_identifier', '=', $app_identifier);
        return $query->first();
    }

    public function servicePrincipalsWithPermission($permission='*') {
        $query = $this->hasMany(AadServicePrincipal::class, 'tenant_id');
        switch($permission) {
            case '*':
                return $query->where('status', '>', 0);
            case 'r':
                return $query->where('status', '=', AadServicePrincipal::STATUS_CONNECTED_READONLY);
            case 'w': case 'rw':
                return $query->where('status', '=', AadServicePrincipal::STATUS_CONNECTED_READWRITE);
            case '': case '-':
                return $query->where('status', '=', AadServicePrincipal::STATUS_EXISTS);
        }
        throw new \Exception('Invalid permission parameter');
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'guid';
    }

}
