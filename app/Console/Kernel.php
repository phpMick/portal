<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\UpdateAzureAgreementCommand',
        '\App\Console\Commands\DiscoverAzureAgreementFilesCommand',
        '\App\Connectors\ReportsDb\Commands\Migrate',
        '\App\Connectors\ReportsDb\Commands\LoadData',
        '\App\Connectors\ReportsDb\Commands\LoadCoreData',
        '\App\Connectors\ReportsDb\Commands\LoadAzureCoreData',
        '\App\Connectors\ReportsDb\Commands\LoadAzureCspData',
        '\App\Connectors\AzureDb\Commands\Migrate',
        '\App\Connectors\AzureDb\Commands\Initialise',
        '\App\Connectors\AzureDb\Commands\UpdateRegions',
        '\App\Connectors\AzureDb\Commands\SyncPortalDb',
        '\App\Connectors\AzureDb\Commands\CSP\UpdateCustomerList',
        '\App\Connectors\AzureDb\Commands\CSP\UpdateCustomerSubscriptionList',
        '\App\Connectors\AzureDb\Commands\CSP\UpdateAzurePriceList',
        '\App\Connectors\AzureDb\Commands\CSP\UpdateAzureSubscriptionUsage',
        '\App\Connectors\AzureDb\Commands\CSP\UpdateAll',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
