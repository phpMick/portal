<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseIntegrityCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the database integrity. Ensures unexpected situations don\'t occur that aren\'t enforced by constraints';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // TODO: check all users that have no customer_id have only got PARTNER_ roles
        // TODO: check all users that have a customer_id have only got CUSTOMER_ roles



    }
}
