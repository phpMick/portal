<?php

namespace App\Console\Commands;

use App\CloudServices\Azure\EaBilling\Api;
use App\CloudServices\Azure\Jobs\AgreementCheckForUpdates;
use App\CloudServices\Azure\Models\Agreement;
use Illuminate\Console\Command;

class UpdateAzureAgreementCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-agreement {agreement_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download and process any new/updated months for a specified (or all) Azure Enterprise Agreements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // if agreement id was passed, find it, otherwise find all agreements:
        if(empty($this->argument('agreement_id'))) {
            $agreements = Agreement::query()->get();
        } else {
            $agreements = Agreement::query()->where('id', '=', intval($this->argument('agreement_id')))->get();
        }
        if(empty($agreements) OR $agreements->count()==0) {
            $this->error('Agreement not found, or no agreements defined');
        } else {
            foreach($agreements as $agreement) {
                $job = new AgreementCheckForUpdates($agreement->id);
                // pull new api object from service container:
                $api = new Api();
                $job->handle($api);
            }
        }
    }
}
