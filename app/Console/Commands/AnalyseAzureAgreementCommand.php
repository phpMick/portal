<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AnalyseAzureAgreementCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:analyse {agreement_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Analysis on Azure Agreement(s)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    }
}
