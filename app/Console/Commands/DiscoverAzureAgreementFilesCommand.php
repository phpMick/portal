<?php

namespace App\Console\Commands;

use App\Connectors;
use App\CloudServices\Azure\Models\Agreement;
use App\CloudServices\Azure\Models\AgreementFile;
use function GuzzleHttp\Psr7\str;
use Illuminate\Console\Command;
use League\Flysystem\Directory;

class DiscoverAzureAgreementFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:find-files {--noprocessing}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Discover already existing agreement files in the filesystem, and populate the database from them. Fire off events as though they are newly downloaded';

    /**
     * @var Agreement
     */
    protected $agreement;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        // TODO: ammend to use FlySystem rather than filesystem calls
        $dir = env('DATA_DIR');
        assert('!empty($dir)', 'Datastore directory must be defined in the environment');
        $dir .= DIRECTORY_SEPARATOR . 'azure_ea_data';
        // discover agreements:
        $dirlist = scandir($dir);
        if(!is_array($dirlist)) {
            throw new \Exception('Unable to read datastore directory');
        }
        $dir2 = $dir . DIRECTORY_SEPARATOR;
        $agreementCount = 0;
        $fileCount = 0;
        foreach($dirlist as $name) {
            if($name=='.' OR $name=='..' OR !is_dir($dir2.$name) OR $name[0]=='_') {
                continue;
            }
            // parse name to get agreement num & possibly name:
            $parts = explode(' ', $name, 2);
            if(count($parts)>=1) {
                $agreement_num = $parts[0];
            }
            $agreement_name = NULL;
            if(count($parts)>=2) {
                $agreement_name = $parts[1];
                if(strlen($agreement_name)>=1) {
                    if($agreement_name[0]=='(' AND $agreement_name[strlen($agreement_name)-1]==')') {
                        $agreement_name = substr($agreement_name, 1, -1);
                    }
                }
                $agreement_name = trim($agreement_name);
                if(strlen($agreement_name)==0) {
                    $agreement_name = NULL;
                }
            }
            $fileCount += $this->processAgreement($agreement_num, $agreement_name, $dir2.$name);
            $agreementCount++;
        }
        $this->info('Processed ' . $agreementCount. ' agreements and ' . $fileCount . ' files.');
    }

    protected function processAgreement($number, $name, $dir) {
        // try to find it in DB first:
        $this->agreement = Agreement::query()->where('identifier', '=', $number)->first();
        if(!($this->agreement instanceof Agreement) OR empty($this->agreement->id)) {
            // not found - create:
            $this->agreement = new Agreement();
            $this->agreement->identifier = $number;
            $this->agreement->save();
            $service = new CloudService();
            $service->name = $name;
            $service->service()->associate($this->agreement);
            $service->save();
        }
        // find sub-directories to figure out what type of agreement & which files to process:
        $dirlist = scandir($dir);
        if(!is_array($dirlist)) {
            return 0;
        }
        $dir2 = $dir . DIRECTORY_SEPARATOR;
        $fileCount = 0;
        // check for EA data files:
        if(in_array('detail', $dirlist) OR in_array('summary', $dirlist) OR in_array('pricesheet', $dirlist)) {
            if(empty($this->agreement->type)) {
                $this->agreement->type = Agreement::TYPE_EA;
                $this->agreement->save();
            } elseif($this->agreement->type != Agreement::TYPE_EA) {
                throw new \Exception('Agreement type mis-match with existing database record');
            }
        }
        // process EA files:
        $ea_files = array('summary', 'detail', 'pricesheet');
        foreach($ea_files as $type) {
            if(in_array($type, $dirlist) AND is_dir($dir2.$type)) {
                $fileCount += $this->processEaFiles($dir2.$type, $type);
            }
        }
        // process other types of agreement files here:

        return $fileCount;
    }

    protected function processEaFiles($dir, $type) {
        // get list of files in dir:
        $dirlist = scandir($dir);
        if(!is_array($dirlist)) {
            return 0;
        }
        $dir2 = $dir . DIRECTORY_SEPARATOR;
        $processList = array();
        foreach($dirlist as $filename) {
            if(!is_file($dir2.$filename)) {
                continue; // not a file
            }
            // extract data from filename:
            $m = array();
            $regex = '/^([A-Za-z0-9]+)_([A-Za-z0-9]+)_(\d\d\d\d\-\d\d)_(\d+)\.([A-za-z0-9]+)$/';
            if(!preg_match($regex, $filename, $m) OR count($m)!=6) {
                continue; // not valid filename
            }
            if($m[1]!=$this->agreement->identifier) {
                throw new \Exception('Mis-matched agreement number in directory');
            }
            if($m[2]!=$type) {
                throw new \Exception('Invalid file type encountered in directory');
            }
            $month = $m[3];
            $time = intval($m[4]);
            $filetype = strtolower($m[5]);
            if(array_key_exists($month, $processList)) {
                if($time > $processList[$month]['time']) {
                    // this file is newer
                    $processList[$month] = array('type'=>$filetype, 'time'=>$time, 'filename'=>$filename);
                }
            } else {
                $processList[$month] = array('type'=>$filetype, 'time'=>$time, 'filename'=>$filename);
            }
        }
        $fileCount = 0;
        foreach($processList as $month=>$info) {
            // find file in DB:
            $types = [
                'summary'=>AgreementFile::TYPE_SUMMARY_CSV1,
                'detail'=>AgreementFile::TYPE_DETAIL_CSV,
                'pricesheet'=>AgreementFile::TYPE_PRICESHEET_CSV
            ];
            assert(array_key_exists($type, $types), 'Filetype unknown');
            $typeid = $types[$type];
            $file = AgreementFile::query()
                ->where('agreement_id', '=', $this->agreement->id)
                ->where('file_type', '=', $typeid)
                ->where('month', '=', $month)
                ->first();
            if(!($file instanceof AgreementFile) OR empty($file->id)) {
                $file = new AgreementFile();
                $file->agreement_id = $this->agreement->id;
                $file->file_type = $typeid;
                $file->month = $month;
            }
            $filepath = $dir2.$info['filename'];
            $this->info('Agreement '.$this->agreement->identifier.': Processing '.$type.' file for month '.$month.': '.$info['filename']);
            $file->filesize = filesize($filepath);
            $file->hash_md5 = md5_file($filepath);
            $file->hash_sha1 = sha1_file($filepath);
            $file->last_change = $info['time'];
            $file->last_check = $info['time'];
            $file->save();
            // issue event:
            switch($type) {
                case 'summary':
                    event(new Connectors\Azure\Events\AgreementSummaryFileUpdated());
                    break;
                case 'detail':
                    event(new Connectors\Azure\Events\AgreementDetailFileUpdated($file));
                    break;
                case 'pricesheet':
                    event(new Connectors\Azure\Events\AgreementPriceFileUpdated());
                    break;
            }
            $fileCount++;
        }
        return $fileCount;
    }



}
