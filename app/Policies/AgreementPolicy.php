<?php

namespace App\Policies;

use App\Models\Agreement;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgreementPolicy
{
    use HandlesAuthorization;

    /**
     * Overrides the policy for Master Admins
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isMasterAdmin()) {
            return true;
        }
    }

    /**
     * Global and Partner Admins can delete anything
     * Customer Admins can only delete their own agreements
     *
     * @param User $user
     * @param Agreement $agreement
     * @return bool
     */
    public function destroy(User $user, Agreement $agreement) {

        return $user->isAdminOfSelectedGroup();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user) {

        return $user->isAdminOfSelectedGroup();
    }

    /**
     * @param User $user
     * @param Agreement $agreement
     * @return bool
     */
    public function edit(User $user, Agreement $agreement) {

        return $user->isAdminOfSelectedGroup();
    }

    /**
     * @param User $user
     * @param Agreement $agreement
     * @return bool
     */
    public function show(User $user, Agreement $agreement) {

        return $user->canViewSelectedUserGroup();
    }

    /**
     * View the margins for agreement
     *
     * @param User $user
     * @return bool
     */
    public function viewMargins(User $user) {

        return $user->isAdminOfSelectedGroup();
    }

    /**
     * Store the margins for agreement
     *
     * @param User $user
     * @return bool
     */
    public function storeMargins(User $user) {

        return $user->isAdminOfSelectedGroup();

    }


}
