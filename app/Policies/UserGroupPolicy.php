<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Customer;
use App\Models\UserGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserGroupPolicy
{
    use HandlesAuthorization;

    /**
     * Overrides the policy for Master Admins
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isMasterAdmin()) {
            if($ability !="administer") { // only the customer admins can link to AAD
                return true;
            }else{
                return false;
            }
        }
    }


    /**
     * Can the user administer this user_group?
     *
     * param  User $user
     * @param User $user
     * @return mixed
     */
    public function administer(User $user)
    {
        return $user->canPerformAdminTask();
    }


    /**
     * Can the user select this user_group?
     *
     * param  User $user
     * @param User $user
     * @return mixed
     */
    public function selectList(User $user)
    {
        return $user->canPerformAdminTask();
    }

    /**
     * Can the user select this user_group?
     *
     * param  User $user
     * @param User $user
     * @return mixed
     */
    public function select(User $user)
    {
        return $user->canPerformAdminTask();
    }

    /**
     * Determine whether the user can view the customer.
     *
     * param  User $user
     * @param User $user
     * @param UserGroup $userGroup
     * @return mixed
     */
    public function show(User $user, UserGroup $userGroup)
    {
        return $user->canViewUserGroup($userGroup);
    }

    /**
     * They need to be an admin to edit.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdminOfSelectedGroup();
    }

    /**
     * They need to be an admin to edit.
     *
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->isAdminOfSelectedGroup();
    }

    /**
     * Determine whether the user can update the customer.
     * @param User $user
     * @param UserGroup $userGroup
     * @return mixed
     */
    public function update(User $user, UserGroup $userGroup)
    {
        return $user->isGroupAdmin($userGroup);
    }

    /**
     * Determine whether the user can link to AAD
     *
     * @param  User $user
     * @param UserGroup $userGroup
     * @return mixed
     */
    public function destroy(User $user, UserGroup $userGroup)
    {
        return $user->isGroupAdmin($userGroup);
    }




    /**
     * @param User $user
     * @param Customer $customer
     * @return bool
     * @throws \Exception
     */
    public function createCloudService(User $user, Customer $customer)
    {
        // the customer id for which we are creating a cloud service must be defined
        if (is_null($customer) OR is_null($customer->id)) {
            throw new \Exception('The customer must be specified and must exist');
        }
        if (is_null($user->customer_id)) {
            if ($user->hasAnyRole([User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN])) {
                return true;
            }
        } elseif ($user->customer_id == $customer->id) {
            return true;
        }
        return false;
    }


}
