<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Customer;
use App\Models\UserGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    /**
     * Overrides the policy for Master Admins
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->isMasterAdmin()) {
            return true;
        }
    }



    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }


    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }


    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }





    /**
     * @param User $user
     * @return bool
     */
    public function destroy(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }


    /**
     * @param User $user
     * @return bool
     */
    public function changeGroup(User $user)
    {
        if ($user->isAdminOfSelectedGroup()) {
            return true;
        }
    }







}
