<?php

namespace App\Auth;

use Illuminate\Http\Request;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\CssSelector\Parser\Token;
use TheNetworg\OAuth2\Client\Provider\Azure;

class AzureActiveDirectory
{

    const CONTEXT_LOGIN = 'portal';
    const CONTEXT_AZURE = 'azure';
    const CONTEXT_O365 = 'office365';
    const CONTEXT_ALL = array(
        self::CONTEXT_LOGIN, self::CONTEXT_AZURE, self::CONTEXT_O365
    );

    const RESOURCE_GRAPH = 'graph';     // Graph v1
    const RESOURCE_MSGRAPH = 'msgraph'; // Graph v2
    const RESOURCE_AZURESM = 'azuresm'; // Azure v1
    const RESOURCE_AZURERM = 'azurerm'; // Azure v2
    const RESOURCE_ALL = array(
        self::RESOURCE_GRAPH, self::RESOURCE_MSGRAPH, self::RESOURCE_AZURESM, self::RESOURCE_AZURERM
    );

    const STATE_LOGIN_REQUIRED = 'login_required';
    const STATE_LOGIN_ERROR = 'login_error';
    const STATE_LOGIN_COMPLETE = 'login_ok';
    const STATE_TOKEN_OK = 'token_ok';
    const STATE_TOKEN_EXPIRED = 'token_expired';
    const STATE_TOKEN_REJECTED = 'token_rejected';
    const STATE_TOKEN_ERROR = 'token_error';


    /**
     * @var Azure
     */
    protected $provider = NULL;

    /**
     * @var string
     */
    protected $context = NULL;

    /**
     * @var string
     */
    protected $tenant = NULL;

    /**
     * @var string
     */
    protected $auth_code = NULL;

    /**
     * @var integer
     */
    protected $auth_code_time = NULL;

    /**
     * @var array
     */
    protected $tokens = array();

    /**
     * @var string
     */
    protected $current_resource = NULL;

    /**
     * @var Request
     */
    protected $request = NULL;


    public static function getSessionKeyName($context=NULL, $part=NULL) {
        if(empty($context)) {
            return 'auth_aad_context';
        }
        $context = str_replace_first('to-', '', $context);
        if(!in_array($context,self::CONTEXT_ALL)) {
            throw new \Exception('Invalid authorisation context');
        }
        if(empty($part)) {
            return 'auth_aad_'.$context;
        }
        return 'auth_aad_'.$context.'_'.$part;
    }

    public function __construct(Request $request, $context=NULL) {
        throw new \Exception('Use of this component is deprecated');
        $this->request = $request;
        $this->setContext($context);
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getProviderConfig() {
        switch($this->context) {
            case self::CONTEXT_AZURE:
                return [
                    'clientId' => env('AZURE_ID'),
                    'clientSecret' => env('AZURE_SECRET'),
                    'redirectUri' => env('AZURE_REDIRECT_URI'),
                ];
        }
        throw new \Exception('Not implemented yet');
    }

    public function getApplicationId() {
        $config = $this->getProviderConfig();
        return $config['clientId'];
    }

    /**
     * @return Azure
     * @throws \Exception
     */
    public function getProvider() {
        if(!isset($this->provider)) {
            $this->provider = new Azure($this->getProviderConfig());
            if(isset($this->tenant)) {
                $this->provider->tenant = $this->tenant;
            }
        }
        return $this->provider;
    }

    /**
     * @param string $context
     * @throws \Exception
     * @return boolean Whether the auth_code has been set in this session
     */
    public function setContext($context=NULL) {
        $sessionkey = self::getSessionKeyName();
        if(empty($context)) {
            // check if there's context in session:
            $context = $this->request->session()->get($sessionkey);
            if(empty($context)) {
                $context = self::CONTEXT_LOGIN;
            }
        }
        $context = str_replace_first('to-', '', $context);
        if(!in_array($context, self::CONTEXT_ALL)) {
            throw new \Exception('Invalid authorisation context');
        }
        if($context!==$this->context) {
            $this->context = $context;
            $this->provider = NULL;
            $this->request->session()->put($sessionkey, $this->context);
            $this->loadSessionState();
        }
        return !empty($this->auth_code) AND (count($this->tokens) > 0);
    }

    /**
     * @param $resources
     * @return boolean Indicates whether all tokens were obtained - if not, sign-in required
     */
    public function obtainTokens($resources) {
        assert('!empty($this->context)', 'Context must be set before performing any AAD functions');
        assert('!empty($resources)', 'At least one AAD resource must be requested');
        if(!is_array($resources)) {
            $resources = array($resources);
        }
        // obtain tokens:
        foreach($resources as $resource) {
            assert(in_array($resource, self::RESOURCE_ALL));
            //try {
                $this->getAccessToken($resource);
            //} catch (\Exception $e) {
                // ignore - assume user needs to authenticate
            //}
        }
        // ensure they exist:
        foreach($resources as $resource) {
            if(!array_key_exists($resource, $this->tokens) OR !($this->tokens[$resource] instanceof AccessToken)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    protected function getSessionVar($part) {
        $keyname = self::getSessionKeyName($this->context, $part);
        return $this->request->session()->get($keyname);
    }

    protected function putSessionVar($part, $value) {
        $keyname = self::getSessionKeyName($this->context, $part);
        return $this->request->session()->put($keyname, $value);
    }

    protected function removeSessionVar($part) {
        $keyname = self::getSessionKeyName($this->context, $part);
        return $this->request->session()->remove($keyname);
    }

    protected function loadSessionState() {
        $authAge = $this->getSessionVar('authCodeTime');
        if(empty($authAge) OR (time()-intval($authAge))>60) {
            $this->auth_code = NULL;
        } else {
            $this->auth_code = $this->getSessionVar('authCode');
        }
        $this->tokens = array();
        foreach(self::RESOURCE_ALL as $r) {
            $this->tokens[$r] = $this->getSessionVar('token_'.$r);
        }
    }


    /**
     * @param null $resource
     * @return AccessToken
     * @throws \Exception
     */
    public function getAccessToken($resource=NULL) {
        //assert('!empty($this->auth_code)');
        assert('!empty($this->context)');
        if(empty($resource)) {
            $resource = $this->current_resource;
        }
        assert('in_array($resource, self::RESOURCE_ALL)', 'Resource name invalid');

        /* @var AccessToken $token */
        if(!array_key_exists($resource, $this->tokens) OR !($this->tokens[$resource] instanceof AccessToken)) {
            // get new token...
            $token = $this->getProvider()->getAccessToken('authorization_code', [
                'code' => $this->auth_code,
                'resource' => $this->getResourceName($resource)
            ]);
            // store new token in session:
            $this->putSessionVar('token_'.$resource, $token);
        } else {
            $token = $this->tokens[$resource];
            if($token->hasExpired()) {
                $token = $this->getProvider()->getAccessToken('refresh_token', [
                    'refresh_token' => $token->getRefreshToken()
                ]);
                // store new token in session:
                $this->putSessionVar('token_'.$resource, $token);
            }
        }
        // TODO: check $token is valid...
        //

        $this->tokens[$resource] = $token;
        return $this->tokens[$resource];
    }

    public function setTenant($tenant_id) {
        $this->tenant = $tenant_id;
    }

    public function doLogin() {
        if(empty($this->context)) {
            throw new \Exception('No authentication context');
        }
        if(!($this->request instanceof Request)) {
            throw new \Exception('No request object set');
        }

        if(!$this->request->has('code')) {
            // auth_code has not been requested - redirect to provider login page with appropriate parameters
            $params = array();
            // set any params...
            if($this->context==self::CONTEXT_O365) {
                // ensure we add the application as a service principal, and not with user context
                $params['prompt'] = 'admin_consent';
            }
            $authUrl = $this->getProvider()->getAuthorizationUrl($params);
            $this->putSessionVar('oauth2state', $this->getProvider()->getState());
            return redirect($authUrl);
        }
        if($this->request->has('error')) {
            // TODO: Some error checking & remediation
            throw new \Exception('Authentication error from provider');
        }
        $this->auth_code = $this->request->get('code');
        $this->auth_code_time = time();
        if(!empty($this->auth_code)) {
            $requestState = $this->request->get('state');
            $sessionState = $this->getSessionVar('oauth2state');
            $this->removeSessionVar('oauth2state');
            if(empty($requestState) OR $requestState!==$sessionState) {
                throw new \Exception('Invalid authentication state');
            }
            $this->putSessionVar('authCode', $this->auth_code);
            $this->putSessionVar('authCodeTime', $this->auth_code_time);
            $auth_redirect = $this->getSessionVar('intended');
            $this->removeSessionVar('intended');
            if(empty($auth_redirect)) {
                return redirect('/');
            } else {
                return redirect($auth_redirect);
            }
        } else {
            throw new \Exception('Invalid authorisation state');
        }

    }

    public function logout() {
        $this->setContext();
        $provider = $this->getProvider();
        $app_url = env('APP_URL');
        return redirect($provider->getLogoutUrl($app_url));
    }

    /**
     * @param string $resourceId
     * @return string
     * @throws \Exception
     */
    protected function getResourceUri($resourceId=NULL) {
        if(empty($resourceId)) {
            $resourceId = $this->current_resource;
        }
        assert('!empty($resourceId)');
        assert('in_array($resourceId, self::RESOURCE_ALL)');
        switch($resourceId) {
            case self::RESOURCE_GRAPH:
                return 'https://graph.windows.net/';
            case self::RESOURCE_MSGRAPH:
                return 'https://graph.microsoft.com/v1.0/';
            case self::RESOURCE_AZURESM:
                return 'https://management.core.windows.net/';
            case self::RESOURCE_AZURERM:
                return 'https://management.azure.com/';
            default:
                throw new \Exception('Invalid resource specified');
        }
    }

    /**
     * @param string $resourceId
     * @return string
     * @throws \Exception
     */
    protected function getResourceName($resourceId=NULL) {
        if(empty($resourceId)) {
            $resourceId = $this->current_resource;
        }
        assert('!empty($resourceId)');
        assert('in_array($resourceId, self::RESOURCE_ALL)');
        switch($resourceId) {
            case self::RESOURCE_GRAPH:
                return 'https://graph.windows.net';
            case self::RESOURCE_MSGRAPH:
                return 'https://graph.microsoft.com/';
            case self::RESOURCE_AZURESM:
                return 'https://management.core.windows.net/';
            case self::RESOURCE_AZURERM:
                return 'https://management.core.windows.net/';
            default:
                throw new \Exception('Invalid resource specified');
        }
    }


    /**
     * @param $resource
     * @return boolean
     * @throws \Exception
     */
    public function setResource($resource) {
        assert('in_array($resource, self::RESOURCE_ALL)');
        $this->current_resource = $resource;
        $this->getProvider()->urlAPI = $this->getResourceUri();
        $this->getProvider()->resource = $this->getResourceName($resource);
        $token = $this->getAccessToken($resource);
        $this->tokens[$resource] = $token;
        return ($token instanceof AccessToken);
    }

    public function getLoginUrl($params=array()) {
        $authUrl = $this->getProvider()->getAuthorizationUrl($params);
        $this->putSessionVar('oauth2state', $this->getProvider()->getState());
        return $authUrl;
    }

    public function setIntendedUri($uri) {
        $this->putSessionVar('intended', $uri);
    }

    public function apiGet($query, $headers=[]) {
        $token = $this->getAccessToken();
        return $this->getProvider()->get($query, $token, $headers);
    }

    public function getTokens() {
        return $this->tokens;
    }


}