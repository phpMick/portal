<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class SelectedCustomer implements Rule
{

    private $errorMessage;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Ensure that the posted customer, is the one selected
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $passes = false;

        $user = Auth::getUser();

        if($user->isCustomerUser()) {
            //check that the posted customer id is the correct one
            if($value == $user->customer_id){//two intention because value is a string
                $passes = true;
            }else{
                $this->errorMessage = 'You cannot add agreements for this customer';
            }

        } else { //admin

            //customer not selected but updating
            if (session()->get('selected_customers') ==null){
                $passes = true;

            }else {

                //alternatively check that the selected customer is the one posted
                $customerId = key(session()->get('selected_customers'));

                if ($customerId == $value) {//2 on purpose, $value is a string
                    $passes = true;
                } else {
                    $this->errorMessage = 'Error with customer choice - please select a customer.';
                }
            }
        }

        return $passes;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;


    }
}
