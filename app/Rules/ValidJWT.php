<?php

namespace App\Rules;


use App\Models\AgreementFactory;


//Laravel
use Illuminate\Contracts\Validation\Rule;



class ValidJWT implements Rule
{
    private $enrollmentNumber;
    private $failMessage;

    /**
     * Create a new rule instance.
     *
     * @param $enrollmentNumber
     */
    public function __construct($enrollmentNumber)
    {
        $this->enrollmentNumber = $enrollmentNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        //use this for testing
        //$value = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNTMxMDkxODAiLCJJZCI6IjQzN2Y1MjVlLTI5ZWEtNGY5Zi05MzQzLWY0ZDMxN2NmNDExMCIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTUxODk0OTE1NSwibmJmIjoxNTAzMDUxNTU1fQ.maN-R0d74BYkIZbywuFcwGb1pibYwVXM_o9Wl45ZrKKghdkB4vxcmWh0VFCetN0arvW8vY9kbAbIw0WDsVBD2AFWk0gvDRIx6jd_l2Gz_gabLSmoS9hgwJ_nuy_UIhaz9LVFTCsc9PEICJ4lwcQ3Vv-tcwrZZyPSPYbgwTZmwIMmN4TzM07Sj84PDhBQLJp-7JgYJ_NX3xyfw3C7qsZ_RWhckM3Nm-nOFYudGWblMyoiYjvTJJ4UaU0iX0fMv1041wbBXMGMFK6D82Pd0mATUzvc6ThExEM029iA1vbxGbfr2i7zSxVTBW4feCMxvX0RoVfrTeZAKTznlcEEiG3-Kg";

        $JWT = decodeJWT($value);

        if(is_null($JWT)){
            $this->failMessage = "Token is invalid.";
            return false;
        }

        //Enrolment Number (test case is 53109180)
        if($JWT->EnrollmentNumber != $this->enrollmentNumber ){
            $this->failMessage = "Incorrect enrollment number.";
            return false;
        }

        //ISS matches
        if($JWT->iss != AgreementFactory::MS_ISS ){
            $this->failMessage = "Incorrect token issuer.";
            return false;
        }

        //Not before is not before now
        if($JWT->nbf > time()){
            $this->failMessage = "Not before date is invalid.";
            return false;
        }

        //Expiry is not after now
        if($JWT->exp  < time()) {
            $this->failMessage = "Token has expired.";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->failMessage;
    }
}
