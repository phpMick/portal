<?php

namespace App\Rules;

use App\Models\AgreementFactory;
use Illuminate\Contracts\Validation\Rule;

use Illuminate\Support\Facades\Auth;

class AgreementTypeAllowed implements Rule
{

    private $failureMessage;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Check that this agreement ID exists.
     *
     * @param $value
     * @return bool
     */
    private function AgreementTypeExists($value){

        $passed = false;

        //for starters, just check that the selected one is in the array
        $providers = collect(AgreementFactory::PROVIDERS_AND_TYPES);


        $found = collect(data_get($providers, '*.types.*'))->where('id', $value)->first();

        if(isset($found)){
            $passed = true;
        }

        return $passed;

    }


    /**
     * Is the user allowed to create this type of agreement?
     * @param $value
     *
     * @return bool
     */
    private function UserIsAllowedToCreate($value){

        $passed = true;

        $user = Auth::getUser();

        //mca
        if($value===AgreementFactory::TYPE_AZURE_CSP) {
            if($user->isCustomerUser()) {
                $passed =  false;
            }
        }
        return $passed;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $passed = false;

        if($this->AgreementTypeExists($value)){

            if($this->UserIsAllowedToCreate($value)) {

                $passed = true;


            }else{
                $this->failureMessage = "You are not authorised to add this type of agreement.";
            }

        }else{
            $this->failureMessage = "Invalid agreement type.";
        }


        return $passed;



    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->failureMessage;
            //'You are not authorised to add a Microsoft Cloud Agreement - please speak to your account manager.';
    }
}
