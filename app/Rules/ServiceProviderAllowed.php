<?php

namespace App\Rules;

use App\Models\AgreementFactory;
use Illuminate\Contracts\Validation\Rule;

class ServiceProviderAllowed implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $passed = false;

        //for starters, just check that the selected one is in the array
        $providers = collect(AgreementFactory::SERVICE_PROVIDERS);

        $passed = $providers->contains('provider', $value);

        return $passed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Service Provider.';
    }
}
