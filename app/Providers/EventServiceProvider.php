<?php

namespace App\Providers;

use App\Http\Controllers\Auth\External\MicrosoftLiveAccountController;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    const CLASSPATH_AZURE_EVENTS = 'App\CloudServices\Azure\Events';
    const CLASSPATH_AZURE_LISTENERS = 'App\CloudServices\Azure\Listeners';

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\CloudServices\Azure\Events\AgreementAdded' => [
            'App\CloudServices\Azure\Listeners\AzureAgreementCreated'
        ],
        'App\CloudServices\Azure\Events\SubscriptionModified' => [
            'App\CloudServices\Azure\Listeners\SubscriptionModified'
        ],
        'App\CloudServices\Azure\Events\AgreementDetailFileUpdated' => [
            self::CLASSPATH_AZURE_LISTENERS.'\CreateSubscriptionFilesFromEaAgreementDetailFile',
            //'App\CloudServices\Azure\Listeners\AgreementDetailFile\Process',
        ],
        'App\CloudServices\Azure\Events\AgreementSummaryFileUpdated' => [
            'App\CloudServices\Azure\Listeners\AgreementSummaryFileStore',
        ],
        'App\CloudServices\Azure\Events\AgreementPriceFileUpdated' => [
            'App\CloudServices\Azure\Listeners\AgreementPriceFileStore',
        ],

        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\SuccessfulLogin'
        ],

        'App\Events\UserCreated' => [
            'App\Listeners\EmailNewUser',
        ]

    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
