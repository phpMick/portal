<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Models\User::class => \App\Policies\UserPolicy::class,
        \App\Models\UserGroup::class => \App\Policies\UserGroupPolicy::class,
        \App\Models\Agreement::class => \App\Policies\AgreementPolicy::class,
        \App\Models\Microsoft\Azure\Subscription::class => \App\Policies\AzureSubscriptionPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::define('seeAdminMenuOptions', function(User $user){

            return $user->canPerformAdminTask();
        });

    }
}
