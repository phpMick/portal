<?php

namespace App\Providers;


use App\Models\UserGroup;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //this just passes the variables to the view
        view()->composer('partials.nav.right-menu-user',function($view){
            $view->with('selectedGroupId', UserGroup::getSelectedGroupID());
            $view->with('selectedGroupName', UserGroup::getSelectedGroupName());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
