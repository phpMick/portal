<?php

namespace App\CloudServices\Azure\DataServices;


use App\Models\Microsoft\Azure\Agreement;
use App\Models\Microsoft\Azure\Subscription;
use App\Models\Microsoft\Azure\SubscriptionUsage;
use App\Models\Customer;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

class AzureSpend {

    // TODO: add financial months/quarters/years
    /*
    const BY_DATE_MONTH_NUM = 'by-month-num';
    const BY_DATE_YEAR = 'by-year';
    const BY_SUBSCRIPTION_NAME = 'by-sub-name';
    const BY_SUBSCRIPTION_GUID = 'by-sub-guid';
    const BY_SUBSCRIPTION_ID = 'by-sub-id';
    const BY_AGREEMENT_NAME = 'by-agr-name';
    const BY_AGREEMENT_ID = 'by-agr-id';
    const BY_CUSTOMER_NAME = 'by-cust-name';
    const BY_CUSTOMER_ID = 'by-cust-id';
    */

    const DIM_CUSTOMER_ID = 'cust_id';
    const DIM_CUSTOMER_NAME = 'cust_name';
    const DIM_AGREEMENT_NAME = 'agr_name';
    const DIM_AGREEMENT_ID = 'agr_id';
    const DIM_SUBSCRIPTION_NAME = 'sub_name';
    const DIM_SUBSCRIPTION_GUID = 'sub_guid';
    const DIM_SUBSCRIPTION_ID = 'sub_id';
    const DIM_DATE_MONTH = 'date_month';



    const FILTER_DATE_MONTH_START = 'monthStart';
    const FILTER_DATE_MONTH_END = 'monthEnd';


    /**
     * @var Azure
     */
    protected $azureDataService;

    /**
     * The Dimension we are analysing spend by (month/customer/agreement/subscription)
     * @var string
     */
    protected $by = NULL;

    /**
     * The Stacking dimension
     * @var string
     */
    protected $groupBy = NULL;

    /**
     * @var array
     */
    protected $filters = array();

    /**
     * @var Builder
     */
    protected $query = NULL;

    /**
     * @var array
     */
    protected $queryTables = array();

    /**
     * @var array
     */
    protected $queryColumns = array();

    static private $validBy = NULL;
    static private $validGroupBy = NULL;
    static private $validFilter = NULL;
    static private $validDimensions = NULL;


    public function __construct(Azure $azureDataService) {
        $this->azureDataService = $azureDataService;
    }

    /**
     * @return $this
     */
    public function clear() {
        $this->by = NULL;
        $this->groupBy = NULL;
        $this->filters = array();
        $this->query = NULL;
        $this->queryColumns = array();
        $this->queryTables = array();
        return $this;
    }

    protected function validateDimension($value) {
        if(!is_array(self::$validDimensions)) {
            self::$validDimensions = array();
            $refl = new \ReflectionClass($this);
            foreach ($refl->getConstants() as $k=>$v) {
                if (strpos($k, 'DIM_')===0) {
                    self::$validDimensions[] = $v;
                }
            }
        }
        if (!in_array($value, self::$validDimensions)) {
            throw new \Exception('Invalid Dimension specified');
        }
    }


    protected function validateBy($value) {
        if(!is_array(self::$validBy)) {
            self::$validBy = array();
            $refl = new \ReflectionClass($this);
            foreach ($refl->getConstants() as $k=>$v) {
                if (strpos($k, 'BY')===0) {
                    self::$validBy[] = $v;
                }
            }
        }
        if (!in_array($value, self::$validBy)) {
            throw new \Exception('Invalid Dimension specified');
        }
    }

    protected function validateGroupBy($value) {
        if(!is_array(self::$validGroupBy)) {
            self::$validGroupBy = array();
            $refl = new \ReflectionClass($this);
            foreach ($refl->getConstants() as $k=>$v) {
                if (strpos($k, 'GROUPBY')===0) {
                    self::$validGroupBy[] = $v;
                }
            }
        }
        if (!in_array($value, self::$validGroupBy)) {
            throw new \Exception('Invalid Grouping Dimension specified');
        }
    }

    protected function validateFilter($value) {
        if(!is_array(self::$validFilter)) {
            self::$validFilter = array();
            $refl = new \ReflectionClass($this);
            foreach ($refl->getConstants() as $k=>$v) {
                if (strpos($k, 'FILTER_')===0) {
                    self::$validFilter[] = $v;
                }
            }
        }
        if (!in_array($value, self::$validFilter)) {
            throw new \Exception('Invalid Filter specified');
        }
    }

    /**
     * @param string $dimensionName
     * @return $this
     * @throws \Exception
     */
    public function by($dimensionName) {
        $this->validateDimension($dimensionName);
        $this->by = $dimensionName;
        return $this;
    }

    /**
     * @param string $dimensionName
     * @return $this
     * @throws \Exception
     */
    public function groupBy($dimensionName) {
        $this->validateDimension($dimensionName);
        $this->groupBy = $dimensionName;
        return $this;
    }

    /**
     * @param null|array|mixed $filter
     * @param null|mixed $value
     * @return $this
     * @throws \Exception
     */
    public function filter($filter, $value=NULL) {
        if(is_array($filter)) {
            foreach ($filter as $k=> $v) {
                $this->filter($k, $v);
            }
        } elseif(!isset($filter)) {
            $this->filters = array();
        } else {
            $this->validateFilter($filter);
            $this->filters[$filter] = $value;
        }
        return $this;
    }


    protected function startQuery() {
        $this->query = \DB::table('azure_subscription_usages AS usage');
        $this->query->addSelect(\DB::raw('ROUND(SUM(usage.total_cost),2) AS spend'));
        $this->queryTables = ['usage'];
        $this->queryColumns = [];
    }

    protected function addTable($table) {
        if(!in_array($table, $this->queryTables)) {
            switch ($table) {
                case 'sub':
                    $this->query->join('azure_subscriptions AS sub', 'usage.subscription_id', '=', 'sub.id');
                    $this->queryTables[] = 'sub';
                    break;
                case 'agr':
                    $this->addTable('sub');
                    $this->query->join('agreements AS agr', 'sub.agreement_id', '=', 'agr.id');
                    $this->queryTables[] = 'agr';
                    break;/*
                case 'cs':
                    $this->addTable('sub');
                    $this->query->join('cloud_services AS cs', function(JoinClause $join) {
                        $join->on('sub.id', '=','cs.service_id')
                            ->where('cs.service_type','=',Subscription::class);
                    });
                    $this->queryTables[] = 'cs';
                    break;*/
                case 'cust':
                    $this->addTable('agr');
                    $this->query->join('customers AS cust', 'agr.customer_id', '=', 'cust.id');
                    $this->queryTables[] = 'cust';
                    break;
            }
        }
    }

    protected function addDimension($dimension) {
        if (!isset($dimension)) {
            return $this->query;
        }
        switch ($dimension) {
            case self::DIM_SUBSCRIPTION_ID:
                $this->queryColumns[] = 'usage.subscription_id AS '.self::DIM_SUBSCRIPTION_ID;
                break;
            case self::DIM_SUBSCRIPTION_GUID:
                $this->addTable('sub');
                $this->queryColumns[] = 'sub.guid AS '.self::DIM_SUBSCRIPTION_GUID;
                break;
            case self::DIM_SUBSCRIPTION_NAME:
                $this->addTable('sub');
                $this->queryColumns[] = 'sub.name AS '.self::DIM_SUBSCRIPTION_NAME;
                break;
            case self::DIM_AGREEMENT_ID:
                $this->addTable('sub');
                $this->queryColumns[] = 'sub.agreement_id AS '.self::DIM_AGREEMENT_ID;
                break;
            case self::DIM_AGREEMENT_NAME:
                $this->addTable('agr');
                $this->queryColumns[] = 'agr.name AS '.self::DIM_AGREEMENT_NAME;
                break;
            case self::DIM_CUSTOMER_ID:
                $this->addTable('agr');
                $this->queryColumns[] = 'agr.customer_id AS '.self::DIM_CUSTOMER_ID;
                break;
            case self::DIM_CUSTOMER_NAME:
                $this->addTable('cust');
                $this->queryColumns[] = 'cust.name AS '.self::DIM_CUSTOMER_NAME;
                break;
            case self::DIM_DATE_MONTH:
                $this->queryColumns[] = 'usage.usage_month AS '.self::DIM_DATE_MONTH;
                break;
        }
        $this->queryColumns = array_unique($this->queryColumns);
        return $this->query;
    }

    protected function makeQuery() {
        $this->startQuery();
        // if only one dimension is specified, ensure it is in "by":
        if(!isset($this->by) AND isset($this->groupBy)) {
            $this->by = $this->groupBy;
            $this->groupBy = NULL;
        }
        $this->addDimension($this->by);
        $this->addDimension($this->groupBy);
        // filter by selected customers/subscriptions:6
        $subs = $this->azureDataService->getSelectedSubscriptionIds();
        if(isset($subs)) {
            $this->query->whereIn('usage.subscription_id', $subs);
        }
        $this->query->addSelect(\DB::raw('ROUND(SUM(usage.total_cost),2) AS spend'));
        if(count($this->queryColumns)>0) {
            $this->query->addSelect($this->queryColumns);
            // grouping and sorting must be done with column names, not aliases:
            // note that ordering is critical to JSON operations
            $cols = [];
            foreach($this->queryColumns as $column) {
                // append additional AS clause to ensure there will be at least two array items
                list($col, $alias) = explode(' AS ', $column.' AS ALIAS');
                $cols[] = $col;
                $this->query->orderBy($col);
            }
            $this->query->groupBy($cols);
        }
        // add filters:
        foreach($this->filters as $filterName=>$filterValue) {
            switch($filterName) {
                case 'monthStart':
                    $this->query->where('usage.usage_month', '>=', $filterValue);
                    break;
                case 'monthEnd':
                    $this->query->where('usage.usage_month', '<=', $filterValue);
                    break;
            }
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function get() {
        $this->makeQuery();
        return $this->query->get();
    }

    /**
     * @return float
     */
    public function getNumber() {
        $this->makeQuery();
        return floatval($this->query->first()->spend);
    }

    /**
     * @return string
     */
    public function getJson() {
        $this->makeQuery();
        return json_encode($this->query->get()->toArray());
    }

    /**
     * @param string $dimension
     * @return string
     */
    protected function simplifyDimension($dimension) {
        if(!isset($dimension)) {
            return NULL;
        }
        switch($dimension) {
            case self::DIM_CUSTOMER_NAME:
                return self::DIM_CUSTOMER_ID;
            case self::DIM_AGREEMENT_NAME:
                return self::DIM_AGREEMENT_ID;
            case self::DIM_SUBSCRIPTION_NAME:
            case self::DIM_SUBSCRIPTION_GUID:
                return self::DIM_SUBSCRIPTION_ID;
        }
        return $dimension;
    }

    protected function getLabelData($dimension, Collection $data=NULL) {
        if(!isset($dimension)) {
            return NULL;
        }
        switch($dimension) {
            case self::DIM_CUSTOMER_NAME:
                return $this->azureDataService->getSelectedCustomers('name');
            case self::DIM_AGREEMENT_NAME:
                return $this->azureDataService->getSelectedAgreements('name');
            case self::DIM_SUBSCRIPTION_NAME:
                return $this->azureDataService->getSelectedSubscriptions('name');
            case self::DIM_SUBSCRIPTION_GUID:
                return $this->azureDataService->getSelectedSubscriptions('guid');
            default:
                return NULL;
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getChartJson() {
        // keep it simple - just get the results and shove it into a JSON object suitable for throwing at Chart.js:
        $this->makeQuery();
        $queryData = $this->get();

        $datasetNames = array();
        $chartData = new \stdClass();
        $chartData->labels = array();
        $chartData->datasets = array();

        if(!isset($this->by)) {
            // no dimensions specified - should just get one row with total spend amount
            if($queryData->count()==1) {
                $chartData->labels[] = 'All';
                $datasetNames[] = 'Azure Spend';
                $datasetObj = new \stdClass();
                $datasetObj->label = 'Azure Spend';
                $datasetObj->data = array(floatval($queryData[0]->spend));
                $chartData->datasets[] = $datasetObj;
            } elseif($queryData->count()==0) {
                $chartData->labels[] = 'All';
                $datasetNames[] = 'Azure Spend';
                $datasetObj = new \stdClass();
                $datasetObj->label = 'Azure Spend';
                $datasetObj->data = array(0.0);
                $chartData->datasets[] = $datasetObj;
            } else {
                throw new \Exception('Invalid database response');
            }
        } elseif(!isset($this->groupBy)) {
            // 1 dimension specified - throw into labels and data, in order:
            if($queryData->count()==0) {
                $chartData->labels[] = 'All';
                $datasetNames[] = 'Azure Spend';
                $datasetObj = new \stdClass();
                $datasetObj->label = 'Azure Spend';
                $datasetObj->data = array(0.0);
                $chartData->datasets[] = $datasetObj;
            } else {
                $prop = $this->by;
                $datasetNames[] = 'Azure Spend';
                $datasetObj = new\stdClass();
                $datasetObj->label = 'Azure Spend';
                $datasetObj->data = array();
                foreach($queryData as $row) {
                    $chartData->labels[] = $row->$prop;
                    $datasetObj->data[] = floatval($row->spend);
                }
                $chartData->datasets[] = $datasetObj;
            }
        } else {
            // 2 dimensions specified - this gets complex:
            if($queryData->count()==0) {
                $chartData->labels[] = 'All';
                $datasetNames[] = 'Azure Spend';
                $datasetObj = new \stdClass();
                $datasetObj->label = 'Azure Spend';
                $datasetObj->data = array(0.0);
                $chartData->datasets[] = $datasetObj;
            } else {
                $prop1 = $this->by;
                $prop2 = $this->groupBy;
                // this relies on the data from the DB server being ordered correctly
                $currentLabel = NULL; // tracks when we change label (unknown number of datasets)
                $currentDatasets = array(); // allows us to ensure all datasets have data for all labels
                foreach($queryData as $row) {
                    $label = $row->$prop1;
                    $datasetName = $row->$prop2;
                    if($label!==$currentLabel) {
                        // new label - close off all datasets first (if not first row)
                        if(isset($currentLabel)) {
                            foreach ($datasetNames as $i=>$l) {
                                if (!in_array($l, $currentDatasets)) {
                                    ($chartData->datasets[$i])->data[] = 0.0;
                                }
                            }
                        }
                        // begin new label:
                        $currentLabel = $label;
                        $currentDatasets = array();
                        $chartData->labels[] = $label;
                    }
                    $i = array_search($datasetName, $datasetNames, TRUE);
                    // add dataset if it doesn't exist:
                    if($i===FALSE) {
                        $i = count($datasetNames);
                        $datasetNames[$i] = $datasetName;
                        $datasetObj = new \stdClass();
                        $datasetObj->label = $datasetName;
                        $datasetObj->data = array();
                        $chartData->datasets[] = $datasetObj;
                        // add blank data before current row:
                        $count = count($chartData->labels)-1;
                        for($k=0;$k<$count;$k++) {
                            ($chartData->datasets[$i])->data[] = 0.0;
                        }
                    }
                    // $i = index of dataset - add data:
                    ($chartData->datasets[$i])->data[] = floatval($row->spend);
                    $currentDatasets[] = $datasetName;
                }
                // now just ensure we've got a value for every dataset for the last label:
                foreach ($datasetNames as $i=>$l) {
                    if (!in_array($l, $currentDatasets)) {
                        ($chartData->datasets[$i])->data[] = 0.0;
                    }
                }

            }

        }
        return json_encode($chartData);

    }



}