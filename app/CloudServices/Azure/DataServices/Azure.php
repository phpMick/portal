<?php

namespace App\CloudServices\Azure\DataServices;

use App\Connectors;
use App\Models\Agreement;
use App\Models\Customer;
use App\Models\Microsoft\Azure\Subscription;
use App\Models\Microsoft\Azure\SubscriptionUsage;
use App\Models\User;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Azure
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $selectedCustomers;

    /**
     * @var array
     */
    protected $allowedCustomers = null;

    /**
     * @var AzureSpend
     */
    protected $spend;

    public function __construct()
    {
        if (!Auth::check()) {
            throw new \Exception('Cannot query as a guest');
        }
        $this->setUser(Auth::user());
        $this->spend = new AzureSpend($this);
    }

    /**
     * @param User $user
     *
     * This is setting the class level
     * $user
     * $allowedCustomers
     * $selectedCustomers
     *
     */
    public function setUser(User $user)
    {
        $this->user = $user;

       // $this->allowedCustomers = $user->allowedUserGroups();

        /*if (isset($this->user->customer_id)) {
            // customer user - restrict to just this, don't allow changing
            $customer = $this->user->customer;
            $this->allowedCustomers = [$customer->id => $customer->name];
        } elseif ($this->user->hasAnyRole([User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN, User::ROLE_PARTNER_VIEWALL])) {
            $this->allowedCustomers = null;
        } else {
            $this->allowedCustomers = $user->customers()->pluck('id')->toArray();
        }*/
        //$this->selectedCustomers = $this->allowedCustomers;
    }

    /**
     * @param null $customersArray
     */
    public function setCustomers($customersArray = null)
    {
        if (!isset($customersArray)) {
            // set to defaults for this user:
            $this->selectedCustomers = $this->allowedCustomers;
        } else {
            if (isset($this->allowedCustomers)) {
                // user is restricted as to which customers they can see
                // build up array checking that each customer is allowed
                $this->selectedCustomers = array();
                if (is_integer($customersArray)) {
                    $customersArray = array($customersArray);
                }
                foreach ($customersArray as $customer_id) {
                    assert('is_integer($customer_id)', 'Customer ID must be integer');
                    assert('in_array($customer_id, $this->allowedCustomers)',
                        'Customer ID "' . $customer_id . '" has been incorrectly selected');
                    if (in_array($customer_id, $this->allowedCustomers)) {
                        $this->selectedCustomers[] = $customer_id;
                    }
                }
            } else {
                // user can select any customer
                // ensure passed array is all integers, then pull customer details from DB
                $this->selectedCustomers = array();
                foreach ($customersArray as $customer_id) {
                    assert('is_integer($customer_id)', 'Customer ID must be integer');
                    $this->selectedCustomers[] = $customer_id;
                }
            }
        }
    }

    /**
     * @param null $property
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSelectedCustomers($property = null)
    {
        $query = Customer::query();
        if (isset($this->selectedCustomers)) {
            $query->whereIn('id', $this->selectedCustomers);
        }
        if (!isset($property)) {
            return $query->get();
        } else {
            return $query->pluck($property, 'id')->toArray();
        }
    }

    /**
     * @return int
     */
    public function selectedCustomersCount()
    {
        if (is_array($this->selectedCustomers)) {
            return count($this->selectedCustomers);
        } elseif (isset($this->selectedCustomers)) {
            return 1;
        } else {
            return Customer::count(); // all customers are allowed and selected
        }
    }

    /**
     * @return array
     */
    public function getSelectedCustomerIds()
    {
        return $this->selectedCustomers;
    }

    /**
     * @return int
     */
    public function getSubscriptionCount()
    {
        $query = \DB::table('azure_subscriptions AS sub');
        $query->join('agreements AS agr', 'sub.agreement_id', '=', 'agr.id');

        if (isset($this->selectedCustomers)) {
            $query->whereIn('agr.customer_id', array_keys($this->selectedCustomers));
        }
        return $query->count();
    }

    /**
     * @return int
     */
    public function getAgreementCount()
    {
        $query = Agreement::query();

        if (isset($this->selectedCustomers)) {
            $query->whereIn('customer_id', $this->selectedCustomers);
        }
        return $query->count();
    }

    /**
     * @return array|null
     */
    public function getSelectedAgreementIds()
    {
        // return NULL if all customers are in scope:
        if (!isset($this->selectedCustomers)) {
            return null;
        }
        $query = Agreement::query();

        if (isset($this->selectedCustomers)) {
            $query->whereIn('customer_id', $this->selectedCustomers);
        }
        return $query->pluck('id')->toArray();
    }

    /**
     * @param null $property
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSelectedAgreements($property = null)
    {
        $query = Agreement::query();

        if (isset($this->selectedCustomers)) {
            $query->whereIn('customer_id', $this->selectedCustomers);
        }
        if (!isset($property)) {
            return $query->get();
        } else {
            return $query->pluck($property, 'id')->toArray();
        }
    }

    /**
     * @return array|null
     */
    public function getSelectedSubscriptionIds()
    {
        // return NULL if user can read everything and also has no specific customers selected:
        if (!isset($this->selectedCustomers)) {
            return null;
        }
        $query = \DB::table('azure_subscriptions AS sub');
        $query->join('agreements AS agr', 'sub.agreement_id', '=', 'agr.id');

        if (isset($this->selectedCustomers)) {
            $query->whereIn('agr.customer_id', $this->selectedCustomers);
        }
        return $query->pluck('sub.id')->toArray();
    }

    /**
     * @param null $property
     * @return array|\Illuminate\Database\Eloquent\Collection
     */
    public function getSelectedSubscriptions($property = null)
    {
        $query = Subscription::join('agreements', 'azure_subscriptions.agreement_id', '=', 'agreements.id');
        if (isset($this->selectedCustomers)) {
            $query->whereIn('agreements.customer_id', $this->selectedCustomers);
        }
        if (!isset($property)) {
            // return collection of models:
            return $query->select('azure_subscriptions.*')->get();
        } else {
            return $query->select(['azure_subscriptions.id', 'azure_subscriptions.' . $property])->pluck($property,
                'id')->toArray();
        }
    }


    /**
     * @return AzureSpend
     */
    public function spend()
    {
        return $this->spend;
    }


    /**
     * @param string $detailBy
     * @param array $filters
     * @param string $groupBy
     */
    public function getSpend($detailBy = null, $filters = null, $groupBy = null)
    {
        if (isset($groupBy)) {
            if (!is_array($groupBy)) {
                $groupBy = array($groupBy);
            }
            // possible groupings:
            // 'customer', 'agreement', 'subscription', 'meter'
            // also look for 'month', but treat this differently!
            if (array_search('customer', $groupBy)) {

            }

        }
        $query = SubscriptionUsage::query();
        if (isset($this->selectedCustomers)) {
            $query->whereIn('subscription_id', $this->getSelectedSubscriptionIds());
        }
        if (is_array($filters)) {
            foreach ($filters as $filter => $value) {
                switch (strtolower($filter)) {
                    case 'startdate':
                        $query->where('month', '>=', $value);
                        break;
                    case 'enddate':
                        $query->where('month', '<=', $value);
                        break;
                }
            }
        }
    }

}

