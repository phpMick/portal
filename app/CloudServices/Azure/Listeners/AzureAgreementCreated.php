<?php

namespace App\CloudServices\Azure\Listeners;

use App\CloudServices\Azure\Events\AgreementAdded;
use App\CloudServices\Azure\Jobs\AgreementCheckForUpdates;
use App\CloudServices\Azure\Jobs\Initialise;
use App\CloudServices\Azure\Jobs\AzureOtherAgreementInitialiseJob;
use App\CloudServices\Azure\Models\Agreement;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AzureAgreementCreated
{
    use DispatchesJobs;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgreementAdded  $event
     * @return void
     */
    public function handle(AgreementAdded $event)
    {
        // which type of agreement?
        if($event->azure_agreement->isEA()) {
            if(empty($event->azure_agreement->access_key) OR empty($event->azure_agreement->access_secret)) {
                $event->azure_agreement->status = Agreement::STATUS_ERROR_NO_CREDENTIALS;
                $event->azure_agreement->save();
            } elseif($event->azure_agreement->status!=Agreement::STATUS_INITIALISING) {
                $event->azure_agreement->status = Agreement::STATUS_INITIALISING;
                $event->azure_agreement->save();
                $job = new Initialise($event->azure_agreement->id);
                $job->onQueue('high');
                $this->dispatch($job);
            }
        }

//        $updateAgreementJob = new AgreementCheckForUpdates($event->azure_agreement->id);
//        $updateAgreementJob->onQueue('high');
//        $this->dispatch($updateAgreementJob);
    }
}
