<?php

namespace App\CloudServices\Azure\Listeners;

use App\CloudServices\Azure\Events\AgreementDetailFileUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AzureAgreementAnalyseVMs
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgreementDetailFileUpdated  $event
     * @return void
     */
    public function handle(AgreementDetailFileUpdated $event)
    {
        //
    }
}
