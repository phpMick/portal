<?php

namespace App\CloudServices\Azure\Listeners;

use App\CloudServices\Azure\Events\AgreementPriceFileUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AzureAgreementPriceFileStore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgreementPriceFileUpdated  $event
     * @return void
     */
    public function handle(AgreementPriceFileUpdated $event)
    {
        //
    }
}
