<?php

namespace App\CloudServices\Azure\Listeners;

use App\CloudServices\Azure\Events\AgreementSummaryFileUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AzureAgreementSummaryFileStore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgreementSummaryFileUpdated  $event
     * @return void
     */
    public function handle(AgreementSummaryFileUpdated $event)
    {
        //
    }
}
