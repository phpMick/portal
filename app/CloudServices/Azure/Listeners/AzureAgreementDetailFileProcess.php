<?php

namespace App\CloudServices\Azure\Listeners;

use App\Connectors;
use App\CloudServices\Azure\Events\AgreementDetailFileUpdated;
use App\CloudServices\Azure\Listeners\AzureAgreementCsvFileReader;
use App\CloudServices\Azure\Models\AgreementFile;
use App\CloudServices\Azure\Models\Subscription;
use App\CloudServices\Azure\Models\Meter;
use App\CloudServices\Azure\Models\Region;
use App\CloudServices\Azure\Models\SubscriptionResource;
use App\CloudServices\Azure\Models\SubscriptionUsage;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AzureAgreementDetailFileProcess
{
    use AzureAgreementCsvFileReader;

    /**
     * @var AgreementFile
     */
    protected $file;


    protected $expectedColumns = array(
        'AccountOwnerId',
        'Account Name',
        'ServiceAdministratorId',
        'SubscriptionId',
        'SubscriptionGuid',
        'Subscription Name',
        'Date',
        'Month',
        'Day',
        'Year',
        'Product',
        'Meter ID',
        'Meter Category',
        'Meter Sub-Category',
        'Meter Region',
        'Meter Name',
        'Consumed Quantity',
        'ResourceRate',
        'ExtendedCost',
        'Resource Location',
        'Consumed Service',
        'Instance ID',
        'ServiceInfo1',
        'ServiceInfo2',
        'AdditionalInfo',
        'Tags',
        'Store Service Identifier',
        'Department Name',
        'Cost Center',
        'Unit Of Measure',
        'Resource Group',
    );


    protected $resources = array();
    protected $subscriptions = array();
    protected $meters = array();
    protected $regions = array();
    protected $usage = array();


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AgreementDetailFileUpdated $event
     * @return void
     * @throws \Exception
     */
    public function handle(AgreementDetailFileUpdated $event)
    {
        $this->file = $event->file;
        try {

            $this->openCsvFile($this->file->getFileName(), $this->file->getFileDir());
            $colNames = $this->startDetailFile();
            $month = NULL;
            // TODO: check against expected cols
            while($row = $this->readRow()) {
                $this->addUsage($row);
                if(!isset($month)) {
                    $month = $row[9].'-'.str_pad($row[7],2,'0',STR_PAD_LEFT);
                }
            }

        } catch(\Exception $e) {
            $this->closeCsvFile();
            throw $e;
        }
        $this->closeCsvFile();
        $this->saveUsage($month);
    }

    protected function addUsage($row) {
        $sub = $this->getSubscription($row);
        $resource = $this->getResource($row, $sub);
        $meter = $this->getMeter($row);
        $key = $resource->id . '|' . $meter->id;
        if(!array_key_exists($key, $this->usage)) {
            $this->usage[$key] = array(
                's'=>$sub->id,
                'u'=>array(),
                'c'=>array()
            );
        }
        if(!array_key_exists($row[8], $this->usage[$key]['u'])) {
            $this->usage[$key]['u'][$row[8]] = floatval($row[16]);
        } else {
            $this->usage[$key]['u'][$row[8]] += floatval($row[16]);
        }
        if(!array_key_exists($row[8], $this->usage[$key]['c'])) {
            $this->usage[$key]['c'][$row[8]] = floatval($row[18]);
        } else {
            $this->usage[$key]['c'][$row[8]] += floatval($row[18]);
        }
    }

    protected function saveUsage($month) {
        foreach($this->usage as $key=>$details) {
            $which = explode('|', $key);
            $resource_id = intval($which[0]);
            $meter_id = intval($which[1]);

            $usage = SubscriptionUsage::query()
                ->where('subscription_id', '=', $details['s'])
                ->where('month', '=', $month)
                ->where('resource_id', '=', $resource_id)
                ->where('meter_id', '=', $meter_id)
                ->first();

            if(empty($usage)) {
                $usage = new SubscriptionUsage();
                $usage->subscription_id = $details['s'];
                $usage->month = $month;
                $usage->resource_id = $resource_id;
                $usage->meter_id = $meter_id;
            }
            $usage->usage = $details['u'];
            $usage->cost = $details['c'];
            $usage->total_usage = array_sum($details['u']);
            $usage->total_cost = array_sum($details['c']);
            $usage->save();
        }
    }

    /**
     * @param $row
     * @return Subscription
     */
    protected function getSubscription($row) {
        if(array_key_exists($row[4], $this->subscriptions)) {
            return $this->subscriptions[$row[4]];
        }
        $sub = Subscription::query()->where('guid', '=', $row[4])->first();
        if(empty($sub)) {
            $sub = new Subscription();
            $sub->agreement_id = $this->file->agreement_id;
            $sub->guid = $row[4];
            $sub->name = $row[5];
            $sub->identifier = empty($row[3]) ? NULL : $row[3];
            $sub->department_name = empty($row[27]) ? NULL : $row[27];
            $sub->cost_centre = empty($row[28]) ? NULL : $row[28];
            $sub->account_name = empty($row[1]) ? NULL : $row[1];
            $sub->account_owner = empty($row[0]) ? NULL : $row[0];
            $sub->service_admin = empty($row[2]) ? NULL : $row[2];
            $sub->save();
            // add cloud service:
            $service = new CloudService();
            $service->customer_id = $this->file->agreement->cloudService->customer_id;
            $service->name = $sub->name;
            $service->service()->associate($sub);
            $service->save();
        }
        $this->subscriptions[$row[4]] = $sub;
        return $sub;
    }

    /**
     * @param array $row
     * @param Subscription $sub
     * @return SubscriptionResource
     */
    protected function getResource($row, Subscription $sub) {
        $name = SubscriptionResource::extractInstanceName($row[21]);
        $rg = SubscriptionResource::extractResourceGroup($row[21]);
        $lookup = empty($rg) ? $name : $rg.'|'.$name;
        if(!array_key_exists($sub->id, $this->resources)) {
            $this->resources[$sub->id] = array();
        }
        if(array_key_exists($lookup, $this->resources[$sub->id])) {
            return $this->resources[$sub->id][$lookup];
        }
        $res = SubscriptionResource::query()
            ->where('subscription_id', '=', $sub->id)
            ->where('resource_group', '=', $rg)
            ->where('name', '=', $name)
            ->first();
        if(empty($res)) {
            $res = new SubscriptionResource();
            $res->subscription_id = $sub->id;
            $res->name = $name;
            $res->resource_group = $rg;
            $res->resource_uri = $row[21];
            $res->tags = empty($row[25]) ? NULL : json_decode($row[25]);
            $region = $this->getRegion($row[19]);
            if(!empty($region->actual_id)) {
                $region = $region->actual;
            }
            $res->region()->associate($region);
            $res->save();
        } else {
            // TODO: verify resource

        }
        $this->resources[$sub->id][$lookup] = $res;
        return $res;
    }

    /**
     * @param array $row
     * @return Meter
     * @throws \Exception
     */
    protected function getMeter($row) {
        // ensure row is compliant:
        $required = [11,12,13,15,10,29,14];
        foreach($required as $i) {
            if(empty(trim($row[$i]))) {
                throw new \Exception('Row contains empty meter data');
            }
        }
        if(array_key_exists($row[11], $this->meters)) {
            return $this->meters[$row[11]];
        }
        $meter = Meter::query()->where('guid', '=', $row[11])->first();
        if(empty($meter)) {
            $meter = new Meter();
            $meter->guid = $row[11];
            $meter->category = $row[12];
            $meter->subcategory = $row[13];
            $meter->name = $row[15];
            $meter->service_name = empty($row[20]) ? NULL : $row[20];
            $meter->product_name = $row[10];
            $meter->units = $row[29];
            $region = $this->getRegion($row[14]);
            if(!empty($region->actual_id)) {
                $region = $region->actual;
            }
            $meter->region()->associate($region);
            $meter->save();
        } else {
            // TODO: check everything matches:

        }
        $this->meters[$row[11]] = $meter;
        return $meter;
    }

    /**
     * @param string $region_name
     * @return Region
     */
    protected function getRegion($region_name) {
        if(array_key_exists($region_name, $this->regions)) {
            return $this->regions[$region_name];
        }
        $region = Region::query()->where('lookup_name', '=', $region_name)->first();
        if(empty($region)) {
            $region = new Region();
            $region->lookup_name = $region_name;
            $region->save();
        }
        $this->regions[$region_name] = $region;
        return $region;
    }


}
