<?php

namespace App\CloudServices\Azure\Listeners;

trait AzureAgreementCsvFileReader {

    protected $_zipResource;
    protected $_csvResource;
    protected $_tempdir;
    protected $_rownum;

    /**
     *
     * @return string
     */
    protected function getTempDir() {
        if(is_string($this->_tempdir)) {
            return $this->_tempdir;
        }
        $this->_tempdir = realpath($this->container->getParameter('storage.local_temp'));
        if($this->_tempdir===FALSE) {
            $this->_tempdir = sys_get_temp_dir(); // emergency use only
            $this->getLogger()->warning('Error using specified temp dir - using system temp dir instead');
        }
        return $this->_tempdir;
    }

    protected function getTempFilePath($filename) {
        assert('is_string($filename)','filename must be a string');
        $dirs = dirname($filename);
        if($dirs!='.') {
            // ensure parent dirs exist:
            $dirpath = $this->getTempDir().DIRECTORY_SEPARATOR.str_replace('/', DIRECTORY_SEPARATOR, $dirs);
            if(!is_dir($dirpath)) {
                if(!@mkdir($this->getTempDir().'/'.$dirs, NULL, TRUE)) {
                    throw new \Exception('Cannot create parent directory for temp file');
                }
            }
            return realpath($this->getTempDir().DIRECTORY_SEPARATOR.$dirs).DIRECTORY_SEPARATOR.basename($filename);
        }
        return $this->getTempDir().DIRECTORY_SEPARATOR.$filename;
    }


    protected function extractZipFile($filename, $dir) {
        // see if file is available in temp dir:
        $tempfilename = $this->getTempFilePath($filename);
        if(!file_exists($tempfilename)) {
            // figure out where the file is stored & get it
            // TODO: some clever stuff here, but just get it from flysystem:
            /* @var $store \League\Flysystem\Filesystem */
            $store = $this->container->get('oneup_flysystem.azureenrolment_filesystem');
            file_put_contents($tempfilename, $store->read($filename));
        }
        // extract file from zip:
        if($this->_zipResource instanceof \ZipArchive) {
            $this->_zipResource->close();
            $this->_zipResource = NULL;
        }
        $this->_zipResource = new \ZipArchive();
        if($this->_zipResource->open($tempfilename)!==TRUE) {
            $this->_zipResource->close();
            throw new \Exception('Error opening zip archive');
        }
        if($this->_zipResource->numFiles!=1) {
            $this->_zipResource->close();
            throw new \Exception('Zip archive does not have expected number of files');
        }
        // get name of first entry in zip archive, and open it as a stream (to save memory):
        $details = $this->_zipResource->statIndex(0);
        if(!is_array($details) OR !array_key_exists('name', $details)) {
            throw new \Exception('cannot open zip file');
        }
        if(substr($details['name'], -4)!='.csv') {
            throw new \Exception('cannot parse non-CSV files');
        }
        $this->_csvResource = $this->_zipResource->getStream($details['name']);
    }


    protected function openCsvFile($filename, $dir) {
        if(!isset($filename)) {
            throw new \Exception('Filename has not been generated');
        }
        $store_dir = env('DATA_DIR') . DIRECTORY_SEPARATOR . 'azure_ea_data';
        $filepath = $store_dir . '/' . $dir . '/' . $filename;

        // assume we're just working with datastore files & CSV files
        if(is_resource($this->_csvResource)) {
            fclose($this->_csvResource);
        }
        $this->_csvResource = fopen($filepath, 'r');
        if($this->_csvResource===FALSE) {
            throw new \Exception('failed to open CSV file for reading');
        }
    }

    protected function closeCsvFile() {
        if(isset($this->_csvResource) AND $this->_csvResource!==FALSE) {
            fclose($this->_csvResource);
        }
    }

    protected function startDetailFile() {
        // read first 3 rows & ensure they meet expectations:
        $row = fgetcsv($this->_csvResource);
        if(!is_array($row) OR count($row)<1 OR stripos($row[0], 'usage data extract')===FALSE) {
            throw new \Exception('File does not conform to expected format');
        }
        $row = fgetcsv($this->_csvResource);
        if(!is_array($row) OR count($row)<1 OR trim($row[0])!='') {
            throw new \Exception('File does not conform to expected format');
        }
        $row = fgetcsv($this->_csvResource);
        if(!is_array($row)) {
            throw new \Exception('File does not conform to expected format');
        }

        // extract $row into $colNames - eliminate empty strings
        $colNames = array();
        $i = 0;
        foreach($row as $s) {
            $s = trim($s);
            if($s!='') {
                $colNames[] = $s;
            }
        }

        return $colNames;
    }

    protected function readRow() {
        $row = fgetcsv($this->_csvResource);
        if(is_array($row)) {
            // check it's not a blank row...
            $blank = TRUE;
            foreach($row as $cell) {
                if(isset($cell) AND trim($cell)!='') {
                    $blank = FALSE;
                    break;
                }
            }
            if(!$blank) {
                return $row;
            }
        }
        return NULL;
    }




}