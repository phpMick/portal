<?php

namespace App\CloudServices\Azure;

//Laravel
use App\Models\Customer;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

//Models
use App\Models\User;
use App\Models\Microsoft\AadTenant;
use App\Models\Microsoft\AadUser;

//Connectors
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;

class AadLogin
{
    protected $MAADA;

    /**
     * AadLogin constructor.
     * @param MicrosoftAzureActiveDirectoryAuth $MAADA
     */
    public function __construct(MicrosoftAzureActiveDirectoryAuth $MAADA)
    {
        $this->MAADA = $MAADA;
    }

    /**
     * 1. Called when we have logged in and  we have an auth code.
     * Gets the access token and sets it in _stateData
     * Initiates the customer and user checks.
     * 2. Called when customer admin is linking account to MS
     * @param $requestArray
     * @return mixed
     * @throws \Exception
     */
    public function attemptOAuthLogin($requestArray)
    {
        $userDetailsArray = $this->MAADA->getUserDetails($requestArray['code']);

        //if user is logged in, admin has clicked - link to MS button
        if(auth()->user()) {
            return $this->setupNewAzureADCustomer($userDetailsArray);
        }else {
            return $this->checkTenant($userDetailsArray);
        }
    }


    /**
     * Gets details about the user.
     * Use their tenant_id to check if an existing customer.
     * @param $userDetailsArray
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     * @throws \Exception
     */
    private function checkTenant($userDetailsArray)
    {
        $tenantGuid = $this->MAADA->getTenantGuid();

        //Do we have their tenant_id in our database
        $aadTenant = AadTenant::findByGuid($tenantGuid);

        if(!$aadTenant){
            return $this->notExistingOAuthCustomer($userDetailsArray,$tenantGuid);
        }else {
            return $this->foundExistingOAuthCustomer($aadTenant->user_group_id,$userDetailsArray,$aadTenant);
        }
    }


    /**
     * Extract details (tenant_guid etc) and connect to their customer record
     * Create aad_tenant record with their customer_id
     * @param $userDetailsArray
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    private function setupNewAzureADCustomer($userDetailsArray)
    {
        $user = auth()->user();

        if($user->canPerformAdminTask()) {

            $tenantGuid = $this->MAADA->getTenantGuid();

            $aadTenant = $this->createAADTenant($user->user_group_id, $userDetailsArray, $tenantGuid);

            showMessage('success', 'Microsoft logins successfully enabled.');
            return redirect(route('user-groups.admin'));

        }else{
            App::abort(403, 'Access denied');
        }
    }


    /**
     * Not an existing OAuth customer.
     * Make sure not logged in and display message page.
     * @param $userDetailsArray
     * @param $tenantGuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function notExistingOAuthCustomer($userDetailsArray,$tenantGuid)
    {
            //do they have a standard user account?
            $user = User::where('email', '=', $userDetailsArray['email'])->first();

            if ($user) {//yes, just create the aad_records

                $aadTenant = $this->createAADTenant($user->user_group_id, $userDetailsArray, $tenantGuid);
                $this->createAADUser($user->id, $userDetailsArray, $aadTenant->id);

                return $this->loginUser($user);

            } else { //no, create the tenant record

                $aadTenant = $this->createAADTenant(null, $userDetailsArray, $tenantGuid);
                Auth::logout();
                Log::warning("{$userDetailsArray['email']} attempted login and is not an existing customer");

                session(['message' => 'Your company is not currently signed up to this service.']);
                session(['detail' => 'Please contact your Bytes Account Manager to sign up for CPM.']);

                return view('message-page');
            }
    }

    /**
     * There is a tenant with this guid
     * If they are first, make then admin.
     * If not check if they have an account.
     * @param $user_group_id
     * @param $userDetailsArray
     * @param $aadTenant
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    private function foundExistingOAuthCustomer($user_group_id,$userDetailsArray,$aadTenant)
    {
        if(isset($aadTenant->user_group_id)){ // Are they a valid OAuth customer

            return $this->currentOAuthUser($user_group_id,$userDetailsArray,$aadTenant);

        }else{
            session(['message' => 'Your company is not currently signed up to this service.']);
            session(['detail' =>'Please contact your Bytes Account Manager to sign up for CPM.']);
            return view('message-page');
        }
    }

    /**
     * They have an aad_tenant record with a customer_id
     *
     * @param $user_group_id
     * @param $userDetailsArray
     * @param $aadTenant
     * @return AadLogin|\Illuminate\Http\RedirectResponse
     */
    private function currentOAuthUser($user_group_id,$userDetailsArray,$aadTenant)
    {
        //do they have an aad_user record?
        $aadUser = AadUser::where('email', '=', $userDetailsArray['email'])->first();

        if($aadUser) {
            //just log them in
            return $this->loginUser($aadUser->user);
        }else{
            //Are they the first user for this customer?
            $foundCustomerUser = AadUser::where('tenant_id', '=', $aadTenant->id)->first();
            if (!$foundCustomerUser) {
                // create user record ROLE_CUSTOMER_ADMIN and log them in
                $user = $this->createStandardUser($user_group_id, $userDetailsArray, User::ADMIN);
                $this->createAADUser($user->id, $userDetailsArray, $aadTenant->id);
                return $this->loginUser($user);

            } else { //Existing customer and already some users
                return $this->checkUser($userDetailsArray, $user_group_id, $aadTenant->id);
            }
        }
    }

    /**
     * Check if this email matches and existing user.
     * If so, log them in.
     * If not, make them a NOACCESS user,
     * @param $userDetailsArray
     * @param $user_group_id
     * @param $aadTenant_id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    private function checkUser($userDetailsArray,$user_group_id,$aadTenant_id)
    {
        //do they already have an account?
        $user = User::where('email','=',$userDetailsArray['email'])->first();

        if($user){
            return $this->loginUser($user);
        }else{

            // create user record ROLE_CUSTOMER_NOACCESS
            $user = $this->createStandardUser( NULL,$userDetailsArray, User::USER);
            $this->createAADUser($user->id,$userDetailsArray, $aadTenant_id);

            session(['message' => 'New account created.']);
            session(['detail' =>'Please contact your administrator to activate your account.']);
            return view('message-page');
        }
    }

    /**
     * Just create a record in the aad_tenants table
     * @param $user_group_id
     * @param $userDetailsArray
     * @param $tenant_id
     * @return mixed
     */
    private function createAADTenant($user_group_id,$userDetailsArray,$tenant_id)
    {
        return AadTenant::create([
            'user_group_id' => $user_group_id,
            'domains' => [$userDetailsArray['domain']],
            'guid' => $tenant_id,
        ]);
    }

    /**
     * Just log in this user and send to home.
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginUser($user)
    {
        Auth::login($user);
        return redirect()->route('home');
    }

    /**
     * Create an AADUser
     * @param $user_id
     * @param $userDetailsArray
     * @param $aadTenant_id
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    private function createAADUser($user_id,$userDetailsArray,$aadTenant_id)
    {


        return  AadUser::updateOrCreate(['email'=> $userDetailsArray['email']],[
            'user_id'=> $user_id,
            'email'=> $userDetailsArray['email'],
            'is_admin'=> '0',
            'tokens'=> "",
            'tenant_id'=> $aadTenant_id
        ]);
    }

    /**
     * Create the standard user.
     * @param $user_group_id
     * @param $userDetailsArray
     * @param $role
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    private function createStandardUser($user_group_id,$userDetailsArray,$role){

        //create the standard user record
        return  User::updateOrCreate(['email' => $userDetailsArray['email']],[
            'name' => $userDetailsArray['name'],
            'user_group_id' => $user_group_id,
            'role' => $role
        ]);

    }










}