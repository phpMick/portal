<?php

namespace App\CloudServices\Azure\Events;

use App\CloudServices\Azure\Models\AgreementFile;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AgreementDetailFileUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var AgreementFile
     */
    public $file;

    /**
     * Create a new event instance.
     *
     * @param AgreementFile $file
     */
    public function __construct(AgreementFile $file)
    {
        $this->file = $file;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
