<?php

namespace App\CloudServices\Azure\Events;

use App\Models\Microsoft\Azure\Agreement;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AgreementAdded
{
    use Dispatchable, SerializesModels;

    /**
     * @var Agreement
     */
    public $azure_agreement;


    /**
     * Create a new event instance.
     *
     * @param Agreement $azure_agreement
     */
    public function __construct(Agreement $azure_agreement)
    {
        $this->azure_agreement = $azure_agreement;
    }


}
