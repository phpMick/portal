<?php

namespace App\CloudServices\Azure\Jobs;

use App\CloudServices\Azure\Events\AgreementDetailFileUpdated;
use App\CloudServices\Azure\Events\AgreementPriceFileUpdated;
use App\CloudServices\Azure\Events\AgreementSummaryFileUpdated;

use App\Models\Agreement;

//MB can't find this
//use App\CloudServices\Azure\Models\AgreementFile;

use App\Connectors\MicrosoftAzureEaBillingApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mockery\Exception;

class AgreementCheckForUpdates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $agreement_id;

    protected $agreementNum;

    /**
     * Create a new job instance.
     *
     * @param $agreementId
     */
    public function __construct($agreementId) {
        if($agreementId instanceof Agreement) {
            $this->agreement_id = $agreementId->id;
        } else {
            $this->agreement_id = $agreementId;
        }
    }

    protected function moveUpdatedTempFile($properFilename, $currentFilename, $tempdir) {
        if(substr($tempdir, -1)!='/' AND $tempdir!='/' AND $tempdir!='\\') {
            $tempdir .= '/';
        }
        if(file_exists($tempdir.$properFilename)) {
            if(!unlink($tempdir.$properFilename)) {
                throw new \Exception('Failed to delete temporary file');
            }
        }
        if(!rename($tempdir.$currentFilename, $tempdir.$properFilename)) {
            throw new \Exception('Failed to rename temporary file');
        }
    }

    protected function copyFileToStore($tempdir, $properFilename, $storeDir) {
        $save_dir = env('DATA_DIR') . DIRECTORY_SEPARATOR . 'azure_ea_data' . DIRECTORY_SEPARATOR;

        if(substr($tempdir, -1)!='/' AND $tempdir!='/' AND $tempdir!='\\') {
            $tempdir .= '/';
        }
        if(!file_exists($tempdir.$properFilename)) {
            throw new \Exception('File does not exist!');
        }
        // check target directory exists:
        if(!isset($storeDir)) {
            $storeDir = '';
        }
        if(!is_dir($save_dir.$storeDir)) {
            mkdir($save_dir.$storeDir, 0777, TRUE);
        }
        if($storeDir!='' AND substr($storeDir, -1)!='/' AND $storeDir!='/' AND $storeDir!='\\') {
            $storeDir .= '/';
        }
        // at this point, zip the file in temp dir, and then store permanently...
        // TODO: zipping file
        if(!copy($tempdir.$properFilename, $save_dir.$storeDir.$properFilename)) {
            throw new \Exception('Failed to copy file to permanent storage');
        }
    }

    protected function lookupFileTypeId($reportName) {
        switch($reportName) {
            case MicrosoftAzureEaBillingApi::REPORT_SUMMARY:
                return AgreementFile::TYPE_SUMMARY_CSV;
            case MicrosoftAzureEaBillingApi::REPORT_DETAIL:
                return AgreementFile::TYPE_DETAIL_CSV;
            case MicrosoftAzureEaBillingApi::REPORT_PRICESHEET:
                return AgreementFile::TYPE_PRICESHEET_CSV;
            default:
                throw new \Exception('Unknown report type');
        }
    }


    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle() {

        $temp_dir = env('TEMP_DIR', sys_get_temp_dir());
        // load agreement model from DB:


        $agreement = Agreement::findOrFail($this->agreement_id);

        //MB Just commented for now -access key
        //if(empty($agreement->access_key) OR empty($agreement->access_secret)) {
        //    throw new \Exception('Invalid configuration: API access key not set');
        //}

        //these now come from the details field
        $api = new MicrosoftAzureEaBillingApi($agreement->access_key, $agreement->access_secret, $temp_dir);


        // get list of files available & create in DB if needed:
        $reports = $api->getReportList();
        $now = time();
        foreach($reports as $month=>$reports) {
            foreach($reports as $type=>$url) {
                $file = $agreement->files()
                    ->where('month', '=', $month)
                    ->where('file_type', '=', $type)
                    ->first();
                if(!($file instanceof AgreementFile) OR empty($file->id)) {
                    $file = new AgreementFile();
                    $file->agreement()->associate($agreement);
                    $file->file_type = $this->lookupFileTypeId($type);
                    $file->month = $month;
                    $file->source_url = $url;
                }
                // figure out if we need to download it...
                if($file->needsDownload($now)) {
                    // yes, download it & see if it has changed:
                    $info = $api->downloadFile($month, $type);
                    if($info===FALSE OR $info['filesize']==0) {
                        // TODO: log an error
                        continue;
                    }
                    $size = $info['filesize'];
                    $sha1 = sha1_file($info['filepath']);
                    $md5 = md5_file($info['filepath']);
                    if($file->filesize!=$size OR $file->hash_sha1!=$sha1 OR $file->hash_md5!=$md5) {
                        // file changed:
                        $proper_filename = $file->getFileName();
                        $this->moveUpdatedTempFile($proper_filename, $info['filename'], $info['directory']);
                        $this->copyFileToStore($info['directory'], $proper_filename, $file->getFileDir());

                        // update file record:
                        $file->filesize = $size;
                        $file->hash_md5 = $md5;
                        $file->hash_sha1 = $sha1;
                        $file->checked(TRUE, $now)->save();
                        // run file updated jobs:
                        switch($file->file_type) {
                            case AgreementFile::TYPE_SUMMARY_CSV:
                                event(new AgreementSummaryFileUpdated($file));
                                break;
                            case AgreementFile::TYPE_DETAIL_CSV:
                                event(new AgreementDetailFileUpdated($file));
                                break;
                            case AgreementFile::TYPE_PRICESHEET_CSV:
                                event(new AgreementPriceFileUpdated($file));
                                break;
                        }
                    } else {
                        $file->checked(FALSE, $now)->save();
                    }
                }
            }
        }


    }
}
