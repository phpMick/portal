<?php

namespace App\CloudServices\Azure\Jobs\AzureEnterpriseAgreement;

use App\CloudServices\Azure\Models\Agreement;
use App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models\BillingPeriod;
use Illuminate\Contracts\Bus\Dispatcher;

class AgreementJob extends BaseJob
{

    /**
     * @var BillingPeriod[]
     */
    protected $_billingPeriods;

    /**
     * @param Agreement $agreement
     */
    public function __construct(Agreement $agreement) {
        // just store the agreement id - we will pull the record back from the DB when handling this job
        // this ensures it is fresh & reduces DB load on queue table
        assert('$agreement->exists()', 'Agreement must be saved to the database');
        $this->_agreementId = $agreement->id;
    }

    protected function checkAccess() {
        $this->_billingPeriods = $this->_api->BillingPeriods()->get();
        if(empty($this->_billingPeriods)) {
            // TODO: check why! we currently assume it's a credentials error, but might be enrolment is cancelled
            // just need to check API's lastResponse()->error

            $this->logAccessError('Invalid access key/secret', Agreement::STATUS_ERROR_DENIED);
            return FALSE;
        }
        // if we get here, then we got access - update the DB if needed:
        if($this->_agreement->status!=Agreement::STATUS_OK) {
            $this->_agreement->status = Agreement::STATUS_OK;
            $this->_agreement->save();
        }
        return TRUE;
    }

    protected function updateFileList() {

    }

    /**
     *
     * @return boolean
     */
    public function handle() {
        if(parent::handle()===FALSE) {
            return FALSE;
        }
        if($this->checkAccess()===FALSE) {
            activity(self::ACTIVITY_LOG)->on($this->_agreement)->log('Agreement update failed: in error state ('.$this->_agreement->getStatusText().')');
            return FALSE;
        }
        // ensure all available files are listed in the DB:


        return TRUE;
    }

    protected function dispatchSyncJob($job) {
        return app(Dispatcher::class)->dispatchNow($job);
    }

    protected function dispatchAsyncJob($job) {
        return app(Dispatcher::class)->dispatch($job);
    }

}