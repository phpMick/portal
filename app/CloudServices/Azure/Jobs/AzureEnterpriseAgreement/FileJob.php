<?php

namespace App\CloudServices\Azure\Jobs\AzureEnterpriseAgreement;


use App\CloudServices\Azure\Models\Agreement;
use App\CloudServices\Azure\Models\AgreementFile;
use App\Connectors\MicrosoftAzureEnterpriseReportingApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class FileJob extends BaseJob
{

    protected $_file;

    protected $_fileId;

    /**
     * @param AgreementFile|null $file
     */
    public function __construct(AgreementFile $file=NULL) {
        // just store the agreement and file ids - we will pull the records back from the DB when handling this job
        // this ensures it is fresh & reduces DB load on queue table
//        assert('$file->exists()', 'Agreement File must be saved to the database');
//        assert('optional($file->agreement)->exists()', 'Agreement must be saved to the database');
//        $this->_fileId = $file->id;
//        $this->_agreementId = $file->agreement->id;
    }


    /**
     *
     * @return boolean
     */
    public function handle() {/*
        if(parent::handle()===FALSE) {
            return FALSE;
        }
        $this->_file = AgreementFile::find($this->_fileId);
        if(!($this->_file instanceof AgreementFile)) {
            activity(self::ACTIVITY_LOG)->log('Agreement File does not exist: '.$this->_fileId);
            return FALSE;
        }*/
        return TRUE;
    }



}