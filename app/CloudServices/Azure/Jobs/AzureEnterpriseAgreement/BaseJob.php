<?php

namespace App\CloudServices\Azure\Jobs\AzureEnterpriseAgreement;


use App\CloudServices\Azure\Models\Agreement;
use App\Connectors\MicrosoftAzureEnterpriseReportingApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class BaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const ACTIVITY_LOG = 'azure-enterprise-job';

    /**
     * @var integer
     */
    protected $_agreementId;

    /**
     * @var Agreement
     */
    protected $_agreement;

    /**
     * @var MicrosoftAzureEnterpriseReportingApi
     */
    protected $_api;


    protected function logAccessError($message=NULL, $updateAgreementStatus=NULL) {
        if(isset($updateAgreementStatus)) {
            if(in_array($updateAgreementStatus, Agreement::STATUS_ALL)) {
                $this->_agreement->status = $updateAgreementStatus;
            } else {
                $this->_agreement->status = Agreement::STATUS_ERROR_UNKNOWN;
            }
            $this->_agreement->save();
        }
        if(empty($message)) {
            $message = 'An unspecified error occurred';
        }
        activity(self::ACTIVITY_LOG)->on($this->_agreement)->log($message);
    }


    /**
     *
     * @return boolean
     */
    public function handle() {
        // get a fresh copy of the agreement:
        $this->_agreement = Agreement::find($this->_agreementId);
        if(!($this->_agreement instanceof Agreement)) {
            activity(self::ACTIVITY_LOG)->log('Agreement does not exist: '.$this->_agreementId);
            return FALSE;
        }
        // only continue if the agreement does not have an error marked
        // this is valid, as if auth details are updated, then we set the agreement as initialising
        if($this->_agreement->status<0) {
            activity(self::ACTIVITY_LOG)->on($this->_agreement)->log('Skipping agreement: In error state ('.$this->_agreement->getStatusText().')');
            return FALSE;
        }
        if(empty($this->_agreement->access_key) OR empty($this->_agreement->access_secret)) {
            $this->logAccessError('Skipping agreement: No access key/secret', Agreement::STATUS_ERROR_NO_CREDENTIALS);
            return FALSE;
        }
        $this->_api = new MicrosoftAzureEnterpriseReportingApi(
            $this->_agreement->access_key,
            $this->_agreement->access_secret
        );
        return TRUE;
    }



}