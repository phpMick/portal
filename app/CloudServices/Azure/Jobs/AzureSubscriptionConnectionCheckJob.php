<?php

namespace App\CloudServices\Azure\Jobs;

use App\CloudServices\Azure\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AzureSubscriptionConnectionCheckJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $_subscription_id = NULL;

    /**
     * Create a new job instance.
     *
     * @param $subscription
     */
    public function __construct($subscription) {
        if($subscription instanceof Subscription) {
            $this->_subscription_id = $subscription->id;
        } else {
            $this->_subscription_id = $subscription;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $subscription = Subscription::findOrFail($this->_subscription_id);
        if($subscription) {

        }



    }
}
