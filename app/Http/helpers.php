<?php

use App\Models\Customer;

/**
 * Send a message to the message bar.
 *
 * @param $type - bootstrap class
 * @param $message
 */
function showMessage($type,$message) {


    Session::flash('message', $message);
    Session::flash($type,'true');

}

/**
 * Decodes a JWT and returns an object
 * @param $tokenString
 * @return mixed
 */

function decodeJWT($tokenString){

    $object = NULL;

    $array = explode (".", $tokenString);

    if(count($array)===3){

        list($header, $payload, $signature) = $array;

        $object = json_decode(base64_decode($payload));

    }

    return $object;
}

/**
 *
 * Uses the Laravel validation to validate a field in an assoc
 *
 * @param $assoc
 * @param $element
 * @param $rules Laravel standard rules: 'required|numeric'
 * @return bool
 *
 */
function validateAssocElement($assoc,$element,$rules)
{

    $passed = false;

    $validator = Validator::make($assoc, [
        $element => $rules,
    ]);

    if (!$validator->fails()) {
        $passed = true;
    }

    return $passed;

}


/**
 * Use the Laravel validation to validate a value.
 *
 * @param $value the value to test
 * @param $rules Laravel standard rules: 'required|numeric'
 * @return bool
 */
function validateValue($value,$rules)
{

    //needs to be an assoc
    $assoc = ['value'=>$value];

    $validator = Validator::make($assoc, [
        'value' => $rules,
    ]);

    return !$validator->fails();


}

/**
 * 1. Customer Users - just their customer_id
 * 2. Partner or Global - must be selected
 *
 *
 * @return bool
 */
function getCustomer(){

    $user = auth()->user();

    if (!$user->isCustomerUser()) {

        $test = session('selected_customers', []);

        if (count(session('selected_customers', [])) != 1) {
            return false; //none selected

        }else{
            $customer = Customer::find(session('selected_customers'));
        }
    }else{
        $customer = Customer::find($user->customer_id);
    }

    return $customer;

}










