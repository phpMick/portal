<?php

namespace App\Http\Requests;


use App\Models\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();

        return $user->can('store', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name'=>'required',
                'email'=>'required|email',
                'role'=>'required|in:' . User::USER . ',' . User::ADMIN,
                'user_group_id'=>'required|exists:user_groups,id'
        ];
    }

    public function messages()
    {
        return [
            'user_group_id.required' => 'A user group is required',
            'user_group_id.exists'  => 'You need to select a valid user group',
        ];
    }

}
