<?php

namespace App\Http\Requests;

//Models
use App\Models\Agreement;

//Custom Rules
use App\Rules\SelectedCustomer;
use App\Rules\AgreementTypeAllowed;
use App\Rules\ValidJWT;


use App\Models\AgreementFactory;

//Laravel
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreAgreement extends FormRequest
{


    private $customMessages;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        $user = Auth::getUser();

        return $user->can('create', Agreement::class);

    }


    /**
     * Builds the date rules, allowing:
     * Both null
     * Only on set
     * Both set
     *
     * @param $rules byRef
     * @return void
     */
    private function addDateRules(&$rules){

        //start date only
        if(isset($this->start_date) && !isset($this->end_date) ){
            $dateRules = [
                'start_date'=>'date',
            ];
        }elseif(!isset($this->start_date) && isset($this->end_date) ){ // end date only
            $dateRules = [
                'end_date'=>'date',
            ];
        }elseif(isset($this->start_date) && isset($this->end_date) ){//both dates
            $dateRules = [
                'start_date'=>'nullable|date|before:end_date',
                'end_date'=>'nullable|date|after:start_date'
            ];
        }else{ //no dates
            $dateRules = [];
        }

        $rules =  array_merge($rules,$dateRules);

    }


    /**
     * Adds the detail rules for this agreement type
     *
     * @param $rules byRef
     * @param $agreementType
     */
    private function addDetailRules(&$rules, $agreementType){

        //get the detail rules and their messages
        $detailRules = $agreementType::getRules();
        $this->customMessages = $agreementType::CUSTOM_MESSAGES;

        // merge the rules together
        $rules = array_merge($rules,$detailRules);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
          // 'customer_id'=>['required', new SelectedCustomer],
            'type'=>['required', new AgreementTypeAllowed],
            'name'=>'required'
        ];

        $this->addDateRules($rules);

        //create the agreement
        $agreementType = AgreementFactory::makeAgreementWithID($this['type']);

        $this->addDetailRules($rules, $agreementType);

        return $rules;

    }


    /**
     * Overide the messages to hide the dot notation.
     *
     * @return array
     */

    public function messages()
    {

        return $this->customMessages;
    }

}
