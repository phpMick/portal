<?php

namespace App\Http\Controllers;

//Models
use App\Http\Requests\StoreUserGroup;
use App\Models\Customer;
use App\Models\Microsoft\AadTenant;

use App\Models\UserGroup;

//Laravel


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserGroupController extends Controller
{


    public function __construct()
    {
        //User group must be selected for all these functions
        $this->middleware('userGroupSelected')->except(['select','selectList','test']);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //don't auth, only display a list of this users groups
        $user = auth()->user();

        //once we have a selected user_group, only show descendants
        $userGroups = UserGroup::getSelectedAndDescendants();

        $canEdit = $user->isAdmin();

        return view('user-groups.index',compact('userGroups','canEdit'));
    }


    /**
     * Show the form to create new.
     * Spin up an empty UserGroup for the fields.
     *
     * @param UserGroup $userGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(UserGroup $userGroup)
    {
        $this->authorize('create', UserGroup::class);

        //only allow types below
        $userGroupTypes = collect(UserGroup::getRolesBelowCurrent());

        return view('user-groups.create',compact('userGroupTypes','userGroup'));
    }


    /**
     * Validate and save to the db.
     *
     * @param StoreUserGroup $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserGroup $request)
    {
        //auth in form request

        $userGroup = new UserGroup();
        $userGroup->name = $request->input('name');
        $userGroup->type = $request->input('type');

        $userGroup->properties = $request->input('properties');

        $userGroup->save();

        //just make the selected group the parent
        $selectedGroup = session('selectedUserGroup');

        $selectedGroup->appendNode($userGroup);

        return redirect()->route('user-groups.show', ['userGroup' => $userGroup->id]);
    }


    /**
     * Just show the user group
     * @param UserGroup $userGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(UserGroup $userGroup)
    {
        $this->authorize('show', $userGroup);

        //get the description for this id
        $userGroupType = UserGroup::findTypeNameWithID($userGroup->type);

        $users = $userGroup->users;

        return view('user-groups.show',compact('userGroup','userGroupType','users'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param UserGroup $userGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(UserGroup $userGroup)
    {
        $this->authorize('update', $userGroup);

        //get the description for this id
        $userGroupType = UserGroup::findTypeNameWithID($userGroup->type);

        $userGroupTypes = collect(UserGroup::TYPES);

        return view('user-groups.edit',compact('userGroup','userGroupType','userGroupTypes'));
    }

    /**
     * Save the changes.
     *
     * @param Request $request
     * @param UserGroup $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,UserGroup $userGroup)
    {
        //auth in form request

        $userGroup->name = $request->input('name');
        $userGroup->type = $request->input('type');
        $userGroup->properties = $request->input('properties');

        $userGroup->save();

        showMessage('success','User group updated.');

        return redirect()->route('user-groups.show', ['userGroup' => $userGroup->id]);
    }

    /**
     * Remove the specified resource from storage.
     * AJAX
     * @param UserGroup $userGroup
     * @return void
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(UserGroup $userGroup)
    {
        $this->authorize('destroy', $userGroup);

        $userGroup->safeDelete();
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function selectNone(Request $request)
    {
        $request->session()->remove('selectedUserGroup');

        return redirect(route('home'));
    }

    /**
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deselect(Request $request, Customer $customer)
    {
        // unset this customer in session for future usage, if allowed:
        $this->authorize('view', $customer);

        $selected = $request->session()->get('selectedUserGroup');

        if (array_key_exists(strval($customer->id), $selected)) {
            unset($selected[strval($customer->id)]);
        }
        if (count($selected) == 0) {
            $request->session()->forget('selectedUserGroup');
        } else {
            $request->session()->put('selectedUserGroup', $selected);
        }
        return redirect(route('home'));
    }


    /**
     * User has selected a user group
     * add it in the session
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function select(Request $request)
    {
        $userGroup = UserGroup::findOrFail($request->user_group_id);

        $this->authorize('select', $userGroup);

        // select this specific customer only
        $request->session()->put('selectedUserGroup', $userGroup);

        return $this->redirectIfHaveIntendedRoute($request);

    }


    /**
     * Is the session has a saved route, then return to it
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function redirectIfHaveIntendedRoute($request)
    {
        if ($request->session()->has('intended_route')) {
            $redirectTo = $request->session()->pull('intended_route');
            if (strpos($redirectTo, 'https://') === 0) {
                return redirect($redirectTo);
            }
            return redirect()->route($redirectTo);
        }else{
            return redirect(route('home'));
        }

    }


    /**
     * Present the Select a Customer page
     * Should show all thus user's user_group descendants.
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function selectList(Request $request)
    {

        $this->authorize('selectList', UserGroup::class);

        $user = Auth::user();

        $userGroups = UserGroup::getTreeForSelectedUser();

        $userID = $user->id;
        $title = "User Group";

        return view('user-groups.select', compact('userGroups','userID','title','user'));
    }


    /**
     * Show the admin page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function admin()
    {

        $this->authorize('administer', UserGroup::class);

        //is this customer linked to aad?
        $aadTenant = AadTenant::where('user_group_id', '=', auth()->user()->user_group_id)->first();
        $aadLinked = ($aadTenant ? true : false);
        return view('user-groups.admin')->with(compact(['aadLinked']));


    }

}
