<?php

namespace App\Http\Controllers;

use App\CloudServices\Azure\Jobs\CSP\GetCspAzureUsageOnDateJob;
use App\CloudServices\Azure\Jobs\CSP\UpdateCspCustomerSubscriptionsJob;
use App\Connectors;
use App\Models\User;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;
use Illuminate\Http\Request;
use Mockery\Exception;

class TestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function ensureUserIsSuperUser() {
        $user = auth()->user();
        if(!($user instanceof User) OR !$user->isSuperUser()) {
            abort(403);
        }
    }

    protected function _azureTestAgreements() {
        $cspeer_user = User::findByEmail('cspeer@live.co.uk');

        $agreements = CloudService::where('customer_id', '=', $cspeer_user->customer_id)
            ->where('service_type', '=', Connectors\Azure\Models\Agreement::class)
            ->get();

        $azure_agreements = array(
            'bsp' => [
                'exists' => FALSE,
                'key' => '6621444',
                'secret' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNjYyMTQ0NCIsIklkIjoiOTU5ZGEzYjAtYzE5MS00OWYyLTk4M2ItYWQ1ZTMyMGUxOWM1IiwiUmVwb3J0VmlldyI6IkVudGVycHJpc2UiLCJQYXJ0bmVySWQiOiIiLCJEZXBhcnRtZW50SWQiOiIiLCJBY2NvdW50SWQiOiIiLCJpc3MiOiJlYS5taWNyb3NvZnRhenVyZS5jb20iLCJhdWQiOiJjbGllbnQuZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiZXhwIjoxNDk2MzI5Nzg3LCJuYmYiOjE0ODA2MDQ5ODd9.tCTZM1HUdxWZ_tUc3cBeuntrHTCVM9z4Z3rTXMyLsgkVGZAMab-kmV-f8fD5y96Hkr2n2b05C9Q1wSC-PugqST1CwknLabuxazBOtIJCwe3qQOtYOAF4cmtwIrxklvS1qf8RSce3Ii4zJyF-erlrTnx7geFfZ1840x22HAh5X8W5ceoos8Lbgt61rIR4cqlbjosyuvZ31SDbahxqFVS_GlbBTqC43fpJXMg4pPU-J3b2I3YT1rghyN5wK4QHve2he2Xl0XSki3BC8_PRi_6pehPf1355yjUPKoR7XSb71S4HWW60N52beKvC-fc9b3UTB4qyHTkeM4_NDmb6dEPEAQ',
                'name' => 'Bytes Security Partnerships',
                'label' => 'Bytes Security Partnerships Azure EA'
            ],
            'bb' => [
                'exists' => FALSE,
                'key' => '66776920',
                'secret' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNjY3NzY5MjAiLCJJZCI6IjI3YzJjMDhiLTQ1ZDUtNDg2Ny1iNTVjLThlYmVjNmNlOWM4MiIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTUwODkyODA2OCwibmJmIjoxNDkzMTE2ODY4fQ.dfQO5iOpzysPfwb5hJTEMpV9gJKY5lJd478zAlb6ERAoUHH-bCQ6Pb3b5kz5IJaIGsDUcgZQ0unmykwyw5ztW7zbjxV3OHOEacadypWDoOMVF-p9v2YYTFwZxyEAW4zSBIykMBuXvp3hltbw2GANwfmDy5GWA6BIUuAjbf0bty7VzWxL-beULQxKqqfXgzvqjitBJD_LYbQRaMaInguQSJpGFlmwfX4kYZfPniXoRHiXYJfMFqlw9q231BRsuNwoKscTSeJ_1QxxE9JI8zTnCu_xsvk_cN4-rYv44VGo99xa4hJiyjZP8XqHLfdmCcJk5x0EZdr7K9844aabph3U-A',
                'name' => 'Balfour Beatty',
                'label' => 'Balfour Beatty Azure EA'
            ],
            'hyp' => [
                'exists' => FALSE,
                'key' => '63370009',
                'secret' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNjMzNzAwMDkiLCJJZCI6IjU3ZDk3YmI0LWFkYzctNDg1Yi05NzExLTFjNmQxYjYxNzExMCIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTUxMDgzNjM3NiwibmJmIjoxNDk0OTM4Nzc2fQ.Yf72mwDCEUQyLP9Loh0fB7fJh3j5XdWPPWxEg39DWb6pFoLS3fa8gezW1ljA6yM3wd7HdX6rEwAl_jHOdt0Gyr8yKyjgVlpK3vk2qN809quk1CCN-rs5F3SfESVMDBSsdjhfaOjYutnHhFRkXtScldgbrWmr0GM90v6159g1MoPSvR1dzSJXWgaYcwPsULRqzZ6YMMRpaHq6fVFgWu-bpsgTQ2wxeOF0PgE4CmDtGsylelNhf_FwKl-DnWUZy0OfJyQtik3Racau1zJr5rdW-NrpACy0Dr1HnFK-grXxQSpkpM-xARqOkPjtlzkTwANagN-bmh23BLiZDa45doqTaw',
                'name' => 'Hyperion Group',
                'label' => 'Hyperion Group Azure EA'
            ],
            'asos' => [
                'exists' => FALSE,
                'key' => '57435477',
                'secret' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNTc0MzU0NzciLCJJZCI6ImZlM2Q3MDlmLThiZTktNGFmMy05NTBiLWIzNDYxMTU2ZTJlYiIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTQ5MjU5NTgxOSwibmJmIjoxNDc2ODcxMDE5fQ.hLcXeJcfHXQaAqCFUpSsp9ztvziUfDfAlr8cHq_EhOF0Shux15DAGNYEAaTaODxFHTsHtBlHqSchswpaIVGjeYkpiEvv4_wKKt91T2SM-Yk-iJiL34_beJ6_fnQWkMWrOuH0RQ6LiyYK4GS6aCOxKy2CVwju-g7DKkxm7_roJu0mU-8pWgijixNp6eFjIMmKUGmTL58ZizEjj5TBoYxwaMGFwIwWhQ7nW7OJZXcfr8YP1VTFwfxz-RDxDUl90bxdbyx89SwT0O1oqLo_q5j3QBgL7Se8Q2kWEcHplUcTyprY68UzBCstQCqQa7NaXLgsNa8l1AOvhdPtcADvxWM0Kw',
                'name' => 'ASOS',
                'label' => 'ASOS Azure EA'
            ],
        );
        /** @var CloudService $agreement */
        foreach($agreements as $agreement) {
            $enrolmentNumber = $agreement->service->access_key;
            foreach($azure_agreements as $k=>$az) {
                if($az['key']==$enrolmentNumber) {
                    $azure_agreements[$k]['exists'] = TRUE;
                    break;
                }
            }
        }
        return $azure_agreements;
    }

    public function index() {
        $this->ensureUserIsSuperUser();
        $azure_agreements = $this->_azureTestAgreements();

        return view('test/index')->with('azure_agreements', $azure_agreements);
    }

    public function addAzureAgreement($which) {
        $this->ensureUserIsSuperUser();
        $cspeer_user = User::findByEmail('cspeer@live.co.uk');
        $cs = new CloudService();
        $cs->customer_id = $cspeer_user->customer_id;

        $az = new Connectors\Azure\Models\Agreement();
        $az->type = 'EA';

        $test_agreements = $this->_azureTestAgreements();
        if(!array_key_exists($which, $test_agreements)) {
            throw new \Exception('Unknown test agreement');
        }
        $cs->name = $test_agreements[$which]['name'];
        $az->access_key = $test_agreements[$which]['key'];
        $az->access_secret = $test_agreements[$which]['secret'];

        $az->save();
        $cs->service()->associate($az);
        $cs->save();

        // send AgreementAdded event, which should start agreement initialisation:
        event(new \App\CloudServices\Azure\Events\AgreementAdded($az));

        return redirect('/test');
    }

    public function eaApi() {
        $hyp = [
            'exists' => FALSE,
            'key' => '63370009',
            'secret' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNjMzNzAwMDkiLCJJZCI6IjU3ZDk3YmI0LWFkYzctNDg1Yi05NzExLTFjNmQxYjYxNzExMCIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTUxMDgzNjM3NiwibmJmIjoxNDk0OTM4Nzc2fQ.Yf72mwDCEUQyLP9Loh0fB7fJh3j5XdWPPWxEg39DWb6pFoLS3fa8gezW1ljA6yM3wd7HdX6rEwAl_jHOdt0Gyr8yKyjgVlpK3vk2qN809quk1CCN-rs5F3SfESVMDBSsdjhfaOjYutnHhFRkXtScldgbrWmr0GM90v6159g1MoPSvR1dzSJXWgaYcwPsULRqzZ6YMMRpaHq6fVFgWu-bpsgTQ2wxeOF0PgE4CmDtGsylelNhf_FwKl-DnWUZy0OfJyQtik3Racau1zJr5rdW-NrpACy0Dr1HnFK-grXxQSpkpM-xARqOkPjtlzkTwANagN-bmh23BLiZDa45doqTaw',
            'name' => 'Hyperion Group',
            'label' => 'Hyperion Group Azure EA'
        ];

        $api = new Connectors\MicrosoftAzureEnterpriseReportingApi($hyp['key'], $hyp['secret']);
        $ps = $api->PriceSheet()->get();
        for($i=3424; $i<3450; $i++) {
            dump($ps[$i]);
        }

    }

    public function partnerApi() {
        $this->ensureUserIsSuperUser();
        $rssb_cust_id = 'e4e8771e-8bb7-4b0b-a40a-c28aff072750'; // customer, has azure subs (active and deleted), custom domain
        $proc_cust_id = '4917f4c0-7606-458f-aa7a-f6914bbd1c6f'; // customer, no azure, custom domain
        $prem_cust_id = '1c08cf1e-d3bc-448b-8ce3-85020ce1e2c2'; // large azure customer, custom domain
        $prem_azure_sub = '9F5E0D71-525A-476A-93FE-51296CE8D989';
        $bss_cust_id = '3bfe143d-1730-439b-8645-b77e94b08945'; // BBS test customer, has azure, .onmicrosoft domain
        $bss_azure_sub = '7A96D641-87CF-4305-905A-D880258F6B6C';
        $all_custs = [$rssb_cust_id,$proc_cust_id,$prem_cust_id,$bss_cust_id];

//        $api = new Connectors\MicrosoftPartnerCenterApi();
//        $api->enableCaching(120);


//        $sub = $api->Customers()->getSubscription($bss_cust_id, $bss_azure_sub);
//        dump($sub);
//
//        $query = $sub->azureUsage();
//
//        dump($query->makeQueryUrl());
//        foreach($query as $row) {
//            dump($row);
//        }
//        $count = $query->count();
//        dump('returned records: '.(isset($count) ? $count : 'NULL'));

        $this->dispatchNow(new Connectors\ReportsDb\Jobs\UpdateAzureCspResourcesJob());



        return view('test/index');

    }


}
