<?php

namespace App\Http\Controllers;

use App\CloudServices\Azure\DataServices\Azure;
use App\CloudServices\Azure\DataServices\AzureSpend;
use App\Models\User;
use App\Models\Microsoft\Azure\SubscriptionUsage;
use App\Models\Microsoft\Azure\Subscription;
use App\Models\Microsoft\Azure\Agreement;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * @var User
     */
    protected $_user = NULL;


    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * @return User
     */
    protected function user() {
        if(!isset($this->_user)) {
            $this->_user = auth()->user();
        }
        return $this->_user;
    }

    protected function lastMonth() {
        return gmdate('Y-m', strtotime('-1 month'));
    }

    protected function thisMonth() {
        return gmdate('Y-m');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryCloudServices() {
        if($this->user()->isCustomerUser()) {
            return CloudService::query()
                ->where('customer_id', '=', $this->_user->customer_id);
        } elseif($this->user()->isPartnerUser()) {
            if($this->_user->hasAnyRole([User::ROLE_PARTNER_ADMIN, User::ROLE_PARTNER_VIEWALL])) {
                return CloudService::query();
            } elseif($this->_user->hasAnyRole(User::ROLE_PARTNER_USER)) {
                return CloudService::query()
                    ->whereIn('customer_id', $this->_user->customers()->allRelatedIds());
            }
        } elseif($this->user()->isSuperUser()) {
            return CloudService::query();
        }
    }

    public function azureSubscriptionCount() {
        // TODO: fix this to match selections
        return Subscription::count();
    }

    public function azureAgreementCount() {
        // TODO: fix this to match selections
        return Agreement::count();
    }

    public function azureSpend(Request $request) {
        $dimensions = array(
            'si' => AzureSpend::DIM_SUBSCRIPTION_ID,
            'sg' => AzureSpend::DIM_SUBSCRIPTION_GUID,
            'sn' => AzureSpend::DIM_SUBSCRIPTION_NAME,
            'ai' => AzureSpend::DIM_AGREEMENT_ID,
            'an' => AzureSpend::DIM_AGREEMENT_NAME,
            'ci' => AzureSpend::DIM_CUSTOMER_ID,
            'cn' => AzureSpend::DIM_CUSTOMER_NAME,
            'dm' => AzureSpend::DIM_DATE_MONTH,
        );/*
        $dimNames = array(
            'si' => 'Subscription ID',
            'sg' => 'Subscription GUID',
            'sn' => 'Subscription Name',
            'ai' => 'Agreement ID',
            'an' => 'Agreement Name',
            'ci' => 'Customer ID',
            'cn' => 'Customer Name',
            'dm' => 'Month',
        );*/
        $filters = array(
            'monthStart', 'monthEnd', 'dateRange'
        );

        $az = new Azure();
        //return $az->spend()->by('date_month')->groupBy('cust_name')->getChartJson();
        if ($request->has('by') AND array_key_exists($request->get('by'), $dimensions)) {
            $az->spend()->by($dimensions[$request->get('by')]);
        }
        if ($request->has('grp') AND array_key_exists($request->get('grp'), $dimensions)) {
            $az->spend()->groupBy($dimensions[$request->get('grp')]);
        }
        foreach ($filters as $filter) {
            if($request->has($filter)) {
                if($filter=='dateRange') {
                    switch($request->get($filter)) {
                        case 'lastMonth':
                            $lastMonth = Carbon::now()->subMonth()->format('Y-m');
                            $az->spend()->filter(['monthStart'=>$lastMonth, 'monthEnd'=>$lastMonth]);
                            break;
                        case 'lastYear':
                            $lastMonth = Carbon::now()->subMonth()->format('Y-m');
                            $lastYear = Carbon::now()->subMonth()->subYear()->format('Y-m');
                            $az->spend()->filter(['monthStart'=>$lastYear, 'monthEnd'=>$lastMonth]);
                            break;
                    }
                } else {
                    $az->spend()->filter($filter, $request->get($filter));
                }
            }
        }

        if ($request->has('out')) {
            switch($request->get('out')) {
                case 'chartjs':
                    return $az->spend()->getChartJson();
                case 'js':
                    return $az->spend()->getJson();
                case 'csv':
                    throw new \Exception('Not implemented');
            }
        }
        if($request->wantsJson()) {
            return $az->spend()->getJson();
        } else {
            dd($az->spend()->get());
        }
    }

    public function customerCount() {
        return auth()->user()->customers->count();
    }

}
