<?php

namespace App\Http\Controllers;

//Form Request
use App\Events\UserCreated;
use App\Http\Requests\StoreUser;

//Models
use App\Models\User;
use App\Models\UserGroup;

//Events
use App\Listeners\EmailNewUser;

use Illuminate\Support\Facades\Request;

use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function __construct()
    {
        //User group must be selected for all these functions
        $this->middleware('userGroupSelected')->except('newUserReset','savePassword');
    }


    /**
     * This where the user gets sent, when they have clicked
     * the email link. Check the token, then send them to the
     * reset view.
     *
     * @param Request $request
     * @param $token
     * @param $user_id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function newUserReset(Request $request, $token,$user_id)
    {
        $user = User::findOrFail($user_id);

        if (! $user->checkToken($token)) {
            showMessage('danger','The link you clicked is invalid.');

            return redirect()->to('/login');
        }else {

            return view('users.welcome')->with([
                'token' => $token,
                'email' => $user->email,
                'user' => $user
            ]);
        }
    }

    /**
     * Display list of users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', User::class);

        //what user should we see
        //just the selected group?
        $users = User::where('user_group_id', '=', UserGroup::getSelectedGroupID())->get();

        $canEdit = true;

        return view('users.index', compact('users', 'canEdit'));
    }

    /**
     * Show the form to create new.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(User $user)
    {
        $this->authorize('create', User::class);

        $roles = collect(User::ROLES);

        $userGroups = UserGroup::getTreeForSelectedUser();

        $title = "User Group";

        $user->user_group_id = UserGroup::getSelectedGroupID();

        return view('users.create', compact('user', 'roles','userGroups','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUser $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        //auth in form request

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->status = User::STATUS_INVITED;
        $user->user_group_id = UserGroup::getSelectedGroupID();

        if (!$user->isAadUser()){
            $user->password = str_random(8);
        }

        $user->save();

        event(new UserCreated($user)); //fire event that send email

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('show', $user);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('edit', $user);

        $roles = collect(User::ROLES);

        $userGroups = UserGroup::getTreeForSelectedUser();

        $title = "User Group";

        return view('users.edit', compact('user', 'roles','userGroups','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUser $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, User $user)
    {

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->user_group_id = $request->input('user_group_id');
        $user->save();

        $request->session()->flash('message', 'User updated');

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', $user);

        User::destroy($user->id);

        showMessage('success', 'User deleted');
    }














}
