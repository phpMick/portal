<?php

namespace App\Http\Controllers\CloudService;

use App\CloudServices\Azure\Events\AgreementAdded;
use App\CloudServices\Azure\Jobs\AgreementCheckForUpdates;
use App\Models\Microsoft\Azure\Subscription;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agreement;
use Illuminate\Support\Facades\Auth;

class AzureAgreementController extends Controller
{

    const DIR_VIEW = 'cloudservice/azure/agreement/';

    /**
     * @var User
     */
    protected $user = NULL;

    public function __construct()
    {
        // auth middleware enforced in route file
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->user = Auth::getUser();
        $customer_ids = $this->user->selected_customer_ids();
        // get a list of all agreements and subscriptions the current user can access
        $agreements_query = Agreement::query()
            ->leftJoin('customers', 'agreements.customer_id', '=', 'customers.id')
            ->select(['agreements.*', 'customers.name AS customer_name'])
            ->orderBy('customers.name')
            ->orderBy('agreements.name')
            ->orderBy('agreements.id');
        if(isset($customer_ids)) {
            $agreements_query->whereIn('agreements.customer_id', $customer_ids);
        }

        return view(self::DIR_VIEW.'index')
            ->with('agreements', $agreements_query->get())
            ->with('showCustomerInfo', ($this->user->isPartnerUser() OR $this->user->isSuperUser()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Agreement::class);
        $user = Auth::getUser();
        // ensure partner user has a customer selected:
        if(!$user->isCustomerUser()) {
            if(count(session('selected_customers', []))!=1) {
                $request->session()->put('intended_route', 'azure.agreements.create');
                return redirect()->route('customers.select-list');
            }
        }

        return view(self::DIR_VIEW.'create')
            ->with('partnerUser', !$user->isCustomerUser());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ensure only customer/partner admin can create:
        $this->authorize('create', Agreement::class);
        $user = Auth::getUser();
        $data = $request->validate([
            'customer'=>'',
            'customer-id'=>'',
            'type'=>'required',
            'margin'=>'',
            'name'=>'required',
            'enrolment'=>'',
            'secret'=>''
        ]);
        if($user->isCustomerUser()) {
            $data['customer-id'] = $user->customer_id;
        } else {
            $customerId = key($request->session()->get('selected_customers'));
            if($customerId!=$data['customer-id']) {
                $data['customer'] = '';
                $data['customer-id'] = '';
                return redirect()->back()->withErrors(['customer'=>'Error with customer choice - please select a customer'])->withInput($data);
            }
        }
        // manual validation due to complexity:
        $errors = [];
        if($data['type']=='mca') {
            if($user->isCustomerUser()) {
                $errors['type'] = 'You are not authorised to add a Microsoft Cloud Agreement - please speak to your account manager';
            } elseif(floatval($data['margin'])<=0.0) {
                $errors['margin'] = 'A Microsoft Cloud Agreement must have a margin defined';
            }
        }
        switch($data['type']) {
            case 'mca':
                $data['type-id'] = Agreement::TYPE_AZURE_CSP;
                break;
            case 'ea':
                $data['type-id'] = Agreement::TYPE_AZURE_EA;
                break;
            case 'sce':
                $data['type-id'] = Agreement::TYPE_AZURE_SCE;
                break;
            case 'mpsa':
                $data['type-id'] = Agreement::TYPE_AZURE_MPSA;
                break;
            case 'payg':
                $data['type-id'] = Agreement::TYPE_AZURE_PAYG;
                break;
            default:
                $errors['type'] = 'Invalid selection - please choose an option';
        }
        // allow empty name & EA access info for now
        if(count($errors)>0) {
            return redirect()->back()->withErrors($errors)->withInput($data);
        }

        $agreement = new Agreement();
        $agreement->customer_id = $customerId;
        $agreement->type = $data['type-id'];
        $agreement->name = $data['name'];
        $agreement->identifier = $data['enrolment'];
        $agreement->access_key = $data['enrolment'];
        $agreement->access_secret = $data['secret'];
        $agreement->margin = $data['type-id']==Agreement::TYPE_AZURE_CSP ? $data['margin'] : NULL;
        $agreement->save();

        AgreementCheckForUpdates::dispatch($agreement->id)->onQueue('high');


        return redirect()->route('azure.agreements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Agreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function show(Agreement $agreement) {
        $this->authorize('show', $agreement);
        $user = Auth::getUser();

        return view('cloudservice/azure/agreement/show')
            ->with('agreement', $agreement)
            ->with('partnerUser', !$user->isCustomerUser());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Agreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function edit(Agreement $agreement) {
        $this->authorize('edit', $agreement);
        $user = Auth::getUser();


        return view('cloudservice/azure/agreement/edit')
            ->with('agreement', $agreement)
            ->with('partnerUser', !$user->isCustomerUser());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Agreement  $agreement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agreement $agreement)
    {
        $this->authorize('edit', $agreement);
        $user = Auth::getUser();
        $validation_rules = [
            'name' => 'required',
            'num' => '',
            'country' => '',
        ];
        if($agreement->type==Agreement::TYPE_AZURE_CSP AND !$user->isCustomerUser()) {
            $validation_rules['margin'] = 'required|numeric|min:0.01';
        }
        if($agreement->isEA()) {
            $validation_rules['enrolment'] = 'numeric';
            $validation_rules['secret'] = 'required_with:enrolment';
        }
        $data = $request->validate($validation_rules);
        $agreement->name = $data['name'];
        $agreement->identifier = $data['num'];
        if(!$user->isCustomerUser() AND $agreement->type==Agreement::TYPE_AZURE_CSP) {
            $agreement->margin = $data['margin'];
        }
        $entAccessChanged = FALSE;
        if($agreement->isEA()) {
            if($agreement->access_key!=$data['enrolment'] OR $agreement->access_secret!=$data['secret']) {
                $entAccessChanged = TRUE;
            }
            $agreement->access_key = $data['enrolment'];
            $agreement->access_secret = $data['secret'];
        } else {
            $agreement->access_key = NULL;
            $agreement->access_secret = NULL;
        }
        $agreement->save();

        if($entAccessChanged) {
            event(new AgreementAdded($agreement));
        }

        $request->session()->flash('message', 'Azure Agreement updated');
        return redirect()->route('azure.agreements.show', [$agreement]);
    }

    public function delete($id) {
        // confirm deletion, as this is a GET request
        return 'delete?';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function discover(Agreement $agreement) {
        $this->authorize('edit', $agreement);

        // ensure user is signed in to AAD:



    }


    public function addSubscription(Request $request, Agreement $agreement) {
        $this->authorize('edit', $agreement);
        $this->authorize('create', Subscription::class);
        $user = Auth::getUser();
        // ensure partner user has a customer selected:
        if($user->isCustomerUser()) {
            $customerName = $user->customer->name;
            $customerId = $user->customer->id;
            if($customerId!==$agreement->customer_id) {
                throw new \Exception('Invalid Azure Agreement authorisation');
            }
        } else {
            if(empty($agreement->customer_id)) {
                throw new \Exception('Invalid Azure Agreement');
            }
            $customerId = $agreement->customer_id;
            $customerName = $agreement->customer->name;
        }
        return view('cloudservice.azure.subscription.create')
            ->with('agreement', $agreement)
            ->with('customerId', $customerId)
            ->with('customerName', $customerName)
            ->with('showCustomerInfo', !$user->isCustomerUser());
    }

}
