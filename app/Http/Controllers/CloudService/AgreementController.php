<?php

namespace App\Http\Controllers\CloudService;

//Laravel

use App\Http\Requests\StoreMargins;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

//Models
use App\Models\Agreement;
use App\Models\User;
use App\Models\UserGroup;

//Form Requests
use App\Http\Requests\StoreAgreement;
use App\Models\Microsoft\Azure\Subscription;

//Events
use App\CloudServices\Azure\Events\AgreementAdded;

//Jobs
use App\CloudServices\Azure\Jobs\AgreementCheckForUpdates;

//Rules
use App\Rules\AgreementTypeAllowed;
use App\Rules\ServiceProviderAllowed;


use App\Models\AgreementFactory;
use App\Services\MarginsValidator;

class AgreementController extends Controller
{

    /**
     * @var User
     */

    public function __construct()
    {
        //User group must be selected for all these functions
        $this->middleware('userGroupSelected');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        //$agreements = $selectedUserGroup->fresh()->agreements;
        //I want all the agreements for the selected group and descendants

        $agreements = Agreement::getAgreementsForSelectedAndDescendants();

        return view('agreements.index',compact('agreements'));

    }

    /**
     *
     * Show the new agreement page where users select provider and agreement type.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function make(Request $request)
    {
        $this->authorize('create', Agreement::class);

        $user = auth()->user();

        //get the Service Providers
        $providers = AgreementFactory::SERVICE_PROVIDERS;

        //get the Agreement Types
        $providersAndAgreements = AgreementFactory::userCreatableAgreements();

        //$partnerUser = !$user->isCustomerUser();
        $partnerUser = true;

        $userGroupName = UserGroup::getSelectedGroupName();

        return view( 'agreements.make', compact('partnerUser', 'providers', 'providersAndAgreements','userGroupName'));
    }

    /**
     * Show the form for creating a new resource.
     * This is after a provider and agreement type has been selected (the make function).
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Agreement::class);


        $this->validate($request, [
            'provider' => ['required', new ServiceProviderAllowed],
            'type' => ['required', new AgreementTypeAllowed]

        ]);

        $provider = $request->input('provider');
        $type = $request->input('type');

        $customer_id = UserGroup::getSelectedGroupID();

        $partial = $this->getPartialName($type);
        $agreement = AgreementFactory::makeAgreementWithID($type);

        return view('agreements.create', compact('provider', 'type', 'customer_id', 'partial', 'agreement'));
    }

    /**
     * Gets the filename for this type of agreement
     * @param $type
     * @return mixed
     */
    private function getPartialName($type)
    {
        //get the partial for ths type
        $agreement = AgreementFactory::makeAgreementWithID($type);
        return $agreement::PARTIAL_NAME;
    }

    /**
     * Used by the store and the update, to do the save.
     *
     * @param $agreement
     * @param $request
     */
    private function storeOrUpdateFields($agreement, $request)
    {
        $agreement->user_group_id = UserGroup::getSelectedGroupID();
        $agreement->type = $request['type'];
        $agreement->name = $request['name'];
        $agreement->identifier = $request['identifier'];
        $agreement->start_date = $request['start_date'];
        $agreement->end_date = $request['end_date'];
        $agreement->currency = $request['currency'];
        $agreement->details = $request['details'];
        $agreement->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgreement $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreAgreement $request)
    {
        //auth is checked in StoreAgreement

        //instantiate the correct agreement
        $agreement = AgreementFactory::makeAgreementWithID($request['type']);

        $this->storeOrUpdateFields($agreement, $request);

        //AgreementCheckForUpdates::dispatch($agreement->id)->onQueue('high');

        if(AgreementFactory::hasMargins($request['type'])){
            showMessage('warning','At least one margin is required for this agreement.');
            return redirect()->route('agreements.margins.edit', [$agreement]);
        }else {
            return redirect()->route('agreements.show', [$agreement]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param Agreement $agreement
     * @return $this
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function show(Agreement $agreement)
    {
        $this->authorize('show', $agreement);

        $user = Auth::getUser();

        $margins = $agreement->getAgreementMarginsForUserGroup();

        return view('agreements.show', compact('agreement','margins'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Agreement $agreement
     * @return $this
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Agreement $agreement)
    {
        $this->authorize('edit', $agreement);

        $partial = $this->getPartialName($agreement->type);

        $margins = $agreement->getAgreementMarginsForUserGroup();

        return view('agreements.edit', compact('agreement', 'partial','margins'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param StoreAgreement $request
     * @param Agreement $agreement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreAgreement $request, Agreement $agreement)
    {
        //auth is checked in StoreAgreement
        $this->storeOrUpdateFields($agreement, $request);

        $request->session()->flash('message', 'Agreement updated');

        return redirect()->route('agreements.show', [$agreement]);
    }


    /**
     * Delete this agreement and it's margins
     *
     * @param Agreement $agreement
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Agreement $agreement)
    {
        $this->authorize('destroy', $agreement);

        $agreement->margins()->delete();

        $agreement->delete();

        showMessage('success', 'Agreement deleted');
    }


    /**
     * @param Agreement $agreement
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function discover(Agreement $agreement)
    {
        $this->authorize('edit', $agreement);

        // ensure user is signed in to AAD:

    }

    /**
     * @param Request $request
     * @param Agreement $agreement
     * @return mixed
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function addSubscription(Request $request, Agreement $agreement)
    {
        $this->authorize('edit', $agreement);
        $this->authorize('create', Subscription::class);
        $user = Auth::getUser();


        return view('cloudservice.azure.subscription.create')
            ->with('agreement', $agreement);


    }

    /**
     * Returns the margins for this agreement
     * @param Agreement $agreement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function showMargins(Agreement $agreement)
    {
        $this->authorize('viewMargins', $agreement);

        //agreement margins for this user_group_id

        //$margins = $agreement->margins->sortBy('start_date');
        $margins = $agreement->getAgreementMarginsForUserGroup();

        return view( 'agreements.margins.show', compact('margins', 'agreement'));
    }

    /**
     * Returns the margins for this agreement
     * @param Agreement $agreement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function editMargins(Agreement $agreement)
    {
        $this->authorize('storeMargins', $agreement);

        $margins = $agreement->getAgreementMarginsForUserGroup();

        return view('agreements.margins.edit', compact('margins', 'agreement'));

    }

    /**
     * Stores the margins for this agreement
     * @param Request $request
     * @param Agreement $agreement
     * @param MarginsValidator $marginsValidator
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function storeMargins(Request $request, Agreement $agreement, MarginsValidator $marginsValidator)
    {
        $this->authorize('storeMargins', $agreement); //move to validator?

        try {
            $marginsValidator->validateMargins($request);

            //sync the margins
            $agreement->syncMargins($marginsValidator->rowsCollection);//not set here
            //refresh
            $margins = $agreement->getAgreementMarginsForUserGroup();

            showMessage('success', 'Margins updated');

        }catch(\Exception $e){
            //failed validation
            $errors = $marginsValidator->failureMessage;
            $margins = $marginsValidator->rowsCollection;
        }
        return view('agreements.margins.edit', compact('margins', 'agreement', 'errors'));
    }
}
