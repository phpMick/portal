<?php

namespace App\Http\Controllers\CloudService;

use App\Auth\AzureActiveDirectory;

//Connectors
use App\Connectors\MicrosoftAzureActiveDirectoryAuth as AadAuth;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;
use App\Connectors\MicrosoftAzureApi;

//Laravel
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

//Models
use App\Models\Agreement;
use App\Models\Microsoft\AadTenant;
use App\Models\Microsoft\AadServicePrincipal;
use App\Models\Microsoft\Azure\Subscription;

//Rules
use App\Rules\GuidValidator;

class AzureSubscriptionController extends Controller
{

    const DIR_VIEW = 'cloudservice/azure/subscription/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $user = Auth::getUser();
        $customer_ids = $user->selected_customer_ids();
        // get a list of all agreements and subscriptions the current user can access
        $subscriptions_query = Subscription::query()
            ->leftJoin('customers', 'azure_subscriptions.customer_id', '=', 'customers.id')
            ->select(['azure_subscriptions.*', 'customers.name AS customer_name'])
            ->orderBy('customers.name');
        if(!$user->isCustomerUser()) {
            $subscriptions_query->orderBy('azure_subscriptions.partner_name');
        }
        $subscriptions_query->orderBy('azure_subscriptions.name')
            ->orderBy('azure_subscriptions.azure_name')
            ->orderBy('azure_subscriptions.id');
        if(isset($customer_ids)) {
            $subscriptions_query->whereIn('azure_subscriptions.customer_id', $customer_ids);
        }

        return view(self::DIR_VIEW.'index')
            ->with('subscriptions', $subscriptions_query->get())
            ->with('showCustomerInfo', !$user->isCustomerUser());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request) {
        $this->authorize('create', Subscription::class);
        $user = Auth::getUser();
        // ensure partner user has a customer selected:
        if($user->isCustomerUser()) {
            $customerName = $user->customer->name;
            $customerId = $user->customer->id;
        } else {
            $selectedCustomers = $request->session()->get('selected_customers', []);
            if(!is_array($selectedCustomers) OR count($selectedCustomers)!=1) {
                $request->session()->put('intended_route', 'azure.subscription.create');
                return redirect()->route('user-groups.select-list');
            }
            $customerId = key($selectedCustomers);
            $customerName = current($selectedCustomers);
        }
        return view(self::DIR_VIEW.'create')
            ->with('customerId', $customerId)
            ->with('customerName', $customerName)
            ->with('showCustomerInfo', !$user->isCustomerUser());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $this->authorize('create', Subscription::class);
        $user = Auth::getUser();
        $validation_rules = [
            'guid' => ['required', new GuidValidator(), 'unique:azure_subscriptions,guid'],
            'agreement-id' => 'required|exists:azure_agreements,id'
        ];
        if(!$user->isCustomerUser()) {
            $validation_rules['customer-id'] = 'required|exists:customers,id';
        }
        $params = $request->validate($validation_rules);

        $agreement = Agreement::findOrFail($params['agreement-id']);
        $this->authorize('edit', $agreement);

        $azure_api = new MicrosoftAzureApi();
        try {
            $tenantid = $azure_api->ResourceManagement()->getSubscriptionTenantGuid(strtolower($params['guid']));
        } catch (\Exception $e) {
            logger()->error('Exception when querying Azure Subscription Tenant', [
                'subscription_guid' => $params['guid'],
                'exception_msg' => $e->getMessage()
            ]);
            return redirect()->back()->withErrors(['guid'=>$e->getMessage()])->withInput($params);
        }

        // create subscription entry in DB: (we have already asserted that it doesn't exist in validation)
        $aadTenant = AadTenant::query()->where('guid', '=', $tenantid)->first();
        if(is_null($aadTenant)) {
            $aadTenant = new AadTenant();
            $aadTenant->guid = $tenantid;
            $aadTenant->save();
        }

        $sub = new Subscription();
        if($user->isCustomerUser()) {
            $sub->customer_id = $user->customer_id;
        } else {
            $sub->customer_id = $params['customer-id'];
        }
        $sub->guid = $params['guid'];
        $sub->aadTenant()->associate($aadTenant);
        $sub->agreement()->associate($agreement);
        $sub->save();

        // send user to connect the subscription to our application
        return redirect()->route('azure.subscriptions.connect', [$sub]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function delete() {
        return 'delete?';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
/*
    public function connectServicePrincipal(Request $request, Subscription $subscription) {
        $aadTenant = $subscription->aadTenant;
        $authService = AadAuth::interactive(AadAuth::APP_AZURE, $aadTenant->guid);
        if(!$authService->hasValidToken([AadAuth::RESOURCE_GRAPH, AadAuth::RESOURCE_AZURERM])) {
//            dd($request->session()->all());
            return $authService->getLoginRedirect($request);
        }
        // now access graph to get SPO
        $token = $authService->getAccessToken(AadAuth::RESOURCE_GRAPH);

        $client = new Client([
            'base_uri' => AadAuth::resourceUrl(AadAuth::RESOURCE_GRAPH),
            'exceptions' => FALSE
        ]);

        $url = '/'.$aadTenant->guid.'/servicePrincipals?api-version=1.5';
        $url .= "&\$filter=appId%20eq%20'".$authService->getApplicationId()."'";

        $response = $client->get(
//            '/beta/servicePrincipals',
            $url,
            ['headers'=>[
                'Authorization' => 'Bearer ' . $token->getToken()
            ]]
        );

        dd($response->getBody()->getContents());


    }
*/

    /**
     * @param MicrosoftAzureActiveDirectoryAuth $auth_service
     * @param Subscription $subscription
     * @return boolean
     * @throws \Exception
     */
    protected function _userHasPermissionToConnectApplication(AadAuth $auth_service, Subscription $subscription) {
        $azure_api = new MicrosoftAzureApi($auth_service);
        $perms = $azure_api->subscription($subscription)->Authorization()->getPermissions();
        assert('is_array($perms) AND array_key_exists(\'value\',$perms)', 'Invalid response from Azure (user permission check)');
        assert('is_array($perms[\'value\'])', 'Invalid response from Azure (user permission check)');
        $allowed = FALSE;
        // check for grant:
        foreach($perms['value'] as $perm) {
            assert('is_array($perm) AND array_key_exists(\'actions\', $perm) AND is_array($perm[\'actions\'])', 'Invalid response from Azure (user permission check)');
            foreach($perm['actions'] as $a) {
                $a = strtolower($a);
                if($a=='*' OR $a=='microsoft.authorization/roleassignments/write') {
                    $allowed = TRUE;
                }
            }
        }
        if($allowed) {
            // check for un-grant (which overrides):
            foreach($perms['value'] as $perm) {
                assert('is_array($perm) AND array_key_exists(\'notActions\', $perm) AND is_array($perm[\'notActions\'])', 'Invalid response from Azure (user permission check)');
                foreach($perm['notActions'] as $a) {
                    $a = strtolower($a);
                    if($a=='*' OR $a=='microsoft.authorization/roleassignments/write') {
                        $allowed = FALSE;
                    }
                }
            }
        }
        return $allowed;
    }

//    /**
//     * @param AadTenant $tenant
//     * @return AadServicePrincipal
//     * @throws \Exception
//     */
//    protected function _getAzureApiServicePrincipal(AadTenant $tenant, $permission='*') {
//        $sps = $tenant->servicePrincipals(AadAuth::APP_AZURE)->get();
//        if($sps->count()>0) {
//            $readers = [];
//            $writers = [];
//            $new = [];
//
//            /** @var AadServicePrincipal $sp */
//            foreach($sps as $sp) {
//                if($sp->status < 0) {
//                    $sp->delete();
//                    continue;
//                }
//                switch($sp->status) {
//                    case AadServicePrincipal::STATUS_EXISTS: $new[] = $sp; break;
//                    case AadServicePrincipal::STATUS_CONNECTED_READONLY: $readers[] = $sp; break;
//                    case AadServicePrincipal::STATUS_CONNECTED_READWRITE: $writers[] = $sp; break;
//                    default:
//                        if($sp->status<0) {
//                            $sp->delete();
//                        } else {
//                            throw new \Exception('Invalid Service Principal status');
//                        }
//                }
//            }
//            if($permission=='w' OR $permission=='rw' OR $permission=='*') {
//                if(count($writers)>0) {
//                    return $writers[0];
//                }
//                return NULL;
//            }
//            if($permission=='r' OR $permission=='*') {
//                if(count($readers)>0) {
//                    return $readers[0];
//                }
//                return NULL;
//            }
//            if($permission=='*') {
//                if(count($new)>0) {
//                    return $new[0];
//                }
//            }
//            if(count($new)>0) {
//                return $new[0];
//            }
//        }
//        // if we get here, we're in the process of creating a service principal, but one isn't created, so create one!
//        // get service principle object id from Graph:
//        $aad_sp_oid= AadAuth::getServicePrincipleObjectId(AadAuth::APP_AZURE, $tenant->guid);
//        if(empty($aad_sp_oid)) {
//            throw new \Exception('Invalid server response');
//        }
//        // add it to the database:
//        $spo = new AadServicePrincipal();
//        $spo->tenant()->associate($tenant);
//        $spo->app_identifier = AadAuth::APP_AZURE;
//        $spo->object_guid = $aad_sp_oid;
//        $spo->save();
//        return $spo;
//    }


    public function connect(Request $request, Subscription $subscription) {
        $this->authorize('create', $subscription);
        $user = Auth::getUser();
        // TODO: ensure subscription is not already connected

        // ensure user is logged in for Azure API:
        $aadTenant = $subscription->aadTenant;
        $authService = AadAuth::interactive(AadAuth::APP_AZURE, $aadTenant->guid);
        if(!$authService->hasValidToken([AadAuth::RESOURCE_AZURERM, AadAuth::RESOURCE_GRAPH])) {
            return $authService->getLoginRedirect($request);
        }

        // ensure user has sufficient permission to assign access to an application:
        if(!$this->_userHasPermissionToConnectApplication($authService, $subscription)) {
            return view('cloudservice/azure/subscription/no_connect_permission');
        }

        // update subscription information if it's not already loaded:
        if(empty($subscription->azure_name)) {
            $azure_api = new MicrosoftAzureApi($authService);
            $azure_sub = $azure_api->subscription($subscription)->ResourceManagement()->getSubscription();
            $subscription->azure_name = $azure_sub->name;
            $subscription->properties = $azure_sub;
            $subscription->save();
        }

        return view(self::DIR_VIEW.'connect')
            ->with('subscription', $subscription)
            ->with('showCustomerInfo', !$user->isCustomerUser());
    }

    /**
     * Attach the Azure application to a subscription in either Reader or Contributor mode. It requires a Service
     * Principal to be present on the subscription's AD tenant, and the currently signed-in user must have sufficient
     * access rights (it's assumed that the user is an admin of AD)
     * @param Subscription $subscription
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function attach(Request $request, Subscription $subscription) {
        $params = $request->validate([
            'access' => 'required'
        ]);
        $roleGuid = NULL;
        switch($params['access']) {
            case 'r':
                $roleGuid = MicrosoftAzureApi\Authorization::ROLE_READER;
                break;
            case 'rw':
                $roleGuid = MicrosoftAzureApi\Authorization::ROLE_CONTRIBUTOR;
                break;
            default:
                return redirect()->back()->withErrors(['access'=>'Invalid option selected'])->withInput($params);
        }

        $aadTenant = $subscription->aadTenant;
        $authUser = AadAuth::interactive(AadAuth::APP_AZURE, $aadTenant->guid);
        if(!$authUser->hasValidToken(AadAuth::RESOURCE_AZURERM)) {
            // TODO: the below will break, since we do a GET redirect, and this action requires a POST
            // so we'll lose the parameters, and it will redirect back(), which won't go well.
            return $authUser->getLoginRedirect($request);
        }
        $azureApi = new MicrosoftAzureApi($authUser);
        $azureApi->subscription($subscription);

        $sp = $aadTenant->servicePrincipal(AadAuth::APP_AZURE);
        if(is_null($sp)) {
            $sp = new AadServicePrincipal();
        }
        if(empty($sp->object_guid)) {
            // get service principle object id from Graph:
            $aad_sp_oid = AadAuth::getServicePrincipleObjectId(AadAuth::APP_AZURE, $aadTenant->guid);
            if(empty($aad_sp_oid)) {
                throw new \Exception('Application failure: cannot query AAD');
            }
            $sp->app_identifier = AadAuth::APP_AZURE;
            $sp->object_guid = $aad_sp_oid;
            $sp->tenant()->associate($aadTenant);
            $sp->save();
        }

        // check assigned roles:
        $roles = [MicrosoftAzureApi\Authorization::ROLE_READER, MicrosoftAzureApi\Authorization::ROLE_CONTRIBUTOR];
        $assignedRoles = $azureApi->Authorization()->hasRole($sp->object_guid, $roles);
        foreach($assignedRoles as $id=>$assignedRole) {
            if($id==$roleGuid) {
                // ensure it is assigned...
                if($assignedRole===FALSE) {
                    $assignedRoles[$id] = $azureApi->Authorization()->createRoleAssignment($sp->object_guid, $roleGuid);
                }
            } else {
                // ensure it is not assigned...
                if($assignedRole!==FALSE) {
                    if($azureApi->Authorization()->removeRoleAssignment($assignedRole)) {
                        $assignedRoles[$id] = FALSE;
                    }
                }
            }
        }

        switch($roleGuid) {
            case MicrosoftAzureApi\Authorization::ROLE_READER:
                $sp->status = AadServicePrincipal::STATUS_CONNECTED_READONLY;
                break;
            case MicrosoftAzureApi\Authorization::ROLE_CONTRIBUTOR:
                $sp->status = AadServicePrincipal::STATUS_CONNECTED_READWRITE;
                break;
        }
        $sp->permissions = $assignedRoles[$roleGuid];
        $sp->save();

        return redirect()->route('azure.subscriptions.check', [$subscription]);
    }

    /**
     * Check whether the Azure application has access to the subscription
     * @param Subscription $subscription
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function check(Subscription $subscription) {
        $authService = AadAuth::service(AadAuth::APP_AZURE, new AadAuth\AuthState(), $subscription->aadTenant->guid);
        $sp = $subscription->aadTenant->servicePrincipal(AadAuth::APP_AZURE);
        if(is_null($sp)) {
            $subscription->status = Subscription::STATUS_ERROR_AAD_SP_PROBLEM;
            $subscription->save();
            return redirect()->route('azure.subscriptions.index');
        }
        $azureApi = new MicrosoftAzureApi($authService);
        switch($sp->status) {
            case AadServicePrincipal::STATUS_CONNECTED_READONLY:
                $roleGuid = MicrosoftAzureApi\Authorization::ROLE_READER;
                break;
            case AadServicePrincipal::STATUS_CONNECTED_READWRITE:
                $roleGuid = MicrosoftAzureApi\Authorization::ROLE_CONTRIBUTOR;
                break;
            default:
                $subscription->status = Subscription::STATUS_ERROR_AAD_SP_PROBLEM;
                $subscription->save();
                return redirect()->route('azure.subscriptions.index');
        }

        $access = $azureApi->subscription($subscription)->Authorization()->hasRole($sp->object_guid, $roleGuid);
        if($access===FALSE) {
            $sp->status = AadServicePrincipal::STATUS_ERROR_UNKNOWN;
            $sp->permissions = NULL;
            $sp->save();
            $subscription->status = Subscription::STATUS_ERROR_AAD_SP_PROBLEM;
            $subscription->save();
        } else {
            $subscription->status = Subscription::STATUS_CONNECTED;
            $subscription->save();
        }
        return redirect()->route('azure.subscriptions.index');
    }

    public function usage(Subscription $subscription) {
        $authService = AadAuth::service(AadAuth::APP_AZURE, new AadAuth\AuthState(), $subscription->aadTenant->guid);
        $azureApi = new MicrosoftAzureApi($authService);

        dd($azureApi->subscription($subscription)->ResourceManagement()->listLocations());

        $query = $azureApi->subscription($subscription)->Billing()->resourceUsage();

        dd($query);
    }

}
