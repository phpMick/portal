<?php

namespace App\Http\Controllers\CloudService;

use App\Auth\AzureActiveDirectory;
use App\Models\Agreement;
use App\Models\Microsoft\Azure\Subscription;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;
use App\CloudServices\Azure\Jobs\AgreementCheckForUpdates;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AzureController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        /** @var User $user */
        $user = auth()->user();
        $customer_ids = $user->selected_customer_ids();
        // get a list of all agreements and subscriptions the current user can access or is selected
        // TODO: for partner users, just list customers and agreement/subscription counts
        $agreements_query = Agreement::query()
            ->leftJoin('customers', 'agreements.customer_id', '=', 'customers.id')
            ->select(['agreements.*', 'customers.name AS customer_name'])
            ->orderBy('customers.name')
            ->orderBy('agreements.name')
            ->orderBy('agreements.id');
        if(isset($customer_ids)) {
            $agreements_query->whereIn('agreements.customer_id', $customer_ids);
        }

        $subscriptions_query = Subscription::query()
            ->leftJoin('customers', 'azure_subscriptions.customer_id', '=', 'customers.id')
            ->select(['azure_subscriptions.*', 'customers.name AS customer_name'])
            ->orderBy('customers.name')
            ->orderBy('azure_subscriptions.name')
            ->orderBy('azure_subscriptions.id');
        if(isset($customer_ids)) {
            $subscriptions_query->whereIn('azure_subscriptions.customer_id', $customer_ids);
        }

        return view('cloudservice/azure/index')
            ->with('agreements', $agreements_query->get())
            ->with('subscriptions', $subscriptions_query->get())
            ->with('showCustomerInfo', !$user->isCustomerUser());
    }

    public function signIn(Request $request) {
        $authService = MicrosoftAzureActiveDirectoryAuth::interactive('portal');
        $aadUser = $authService->loggedInUser();
        if(is_null($aadUser)) {
            return $authService->getLoginRedirect($request);
        }
    }

    public function add(AzureActiveDirectory $aadAuth) {
        $user = auth()->user();


        $requiredResources = [
            AzureActiveDirectory::RESOURCE_MSGRAPH,
            AzureActiveDirectory::RESOURCE_GRAPH,
            AzureActiveDirectory::RESOURCE_AZURERM
        ];
        // check if user is signed in...
        if(!$aadAuth->setContext(AzureActiveDirectory::CONTEXT_AZURE)) {
            // TODO: deal with sign-in errors
            $aadAuth->setIntendedUri('/service/azure/add');
            return redirect('service/azure/signin');
        }

        // TODO: Refer to OneNote & connect to ASM endpoint with User Token

        //$aadAuth->setResource(AzureActiveDirectory::RESOURCE_AZURESM);
        //dd($aadAuth->apiGet('/subscriptions/4e9c1373-bb61-4f01-95f2-9d0803407829/providers/Microsoft.Authorization?api-version=2017-05-10&$expand'));
        //dd($aadAuth->apiGet('/subscriptions/4e9c1373-bb61-4f01-95f2-9d0803407829/providers/Microsoft.Authorization/permissions?api-version=2015-06-01'));

        $tokens = array();

        // retrieve user details from Microsoft Graph:
        $aadAuth->setResource(AzureActiveDirectory::RESOURCE_MSGRAPH);
        //dd($aadAuth->getAccessToken());
        $user_details = $aadAuth->apiGet('me');
        $extUser = ExternalUserDeleted::createOrUpdateFromMicrosoftGraph($user, $user_details);
        $tokens[AzureActiveDirectory::RESOURCE_MSGRAPH] = $aadAuth->getAccessToken();

        // retrieve list of tenants & subscriptions this user has access to:
        $aadAuth->setResource(AzureActiveDirectory::RESOURCE_AZURERM);
        $orgs = $aadAuth->apiGet('/tenants?api-version=2014-04-01-preview');

        $aadAuth->setResource(AzureActiveDirectory::RESOURCE_GRAPH);
        foreach($orgs as $k=>$org) {
            try {
                $servicePrincipal = $aadAuth->apiGet('/'.$org['tenantId'].'/servicePrincipals?$filter=appId eq \''.$aadAuth->getApplicationId().'\'');
                $tenantDetails = $aadAuth->apiGet('/'.$org['tenantId'].'/tenantDetails');
            } catch(\Exception $e) {
                $servicePrincipal = NULL;
                $tenantDetails = NULL;
            }
            $orgs[$k]['servicePrincipal'] = $servicePrincipal;
            if(is_array($tenantDetails) AND count($tenantDetails)==1) {
                $tenantDetails = $tenantDetails[0];
            }
            if(is_array($tenantDetails) AND array_key_exists('displayName', $tenantDetails)) {
                $orgs[$k]['tenantName'] = $tenantDetails['displayName'];
            } else {
                $orgs[$k]['tenantName'] = NULL;
            }
        }


        $aadAuth->setResource(AzureActiveDirectory::RESOURCE_AZURERM);
        $subs = $aadAuth->apiGet('/subscriptions?api-version=2016-06-01');
        $tokens[AzureActiveDirectory::RESOURCE_AZURERM] = $aadAuth->getAccessToken();
        $subscriptions = array();
        foreach($subs as $sub) {
            $subscriptions[] = Subscription::createOrUpdateFromApi($sub, $user);
        }



        /*
        // check user permissions for each subscription:
        $permission = array();
        foreach($subscriptions as $s) {
            $queryStr = '/subscriptions/' . $s->guid . '/providers/Microsoft.Authorization/permissions?api-version=2015-06-01';
            $perms = $aadAuth->apiGet($queryStr);
            $s->permissions = $perms;
        }
        */



        return view('cloudservice/azure/add')
            ->with('aadUserId', $extUser->identifier)
            ->with('subscriptions', $subscriptions)
            ->with('tokens', $tokens);
    }


    /**
     * Get user to sign in using some credentials, then query Graph & Azure to find all their Azure subscriptions
     */
    public function discover() {
        // check to see if the user is already signed into AAD



        // NOTE: need an additional step for Microsoft Account logins:
        // for an MSA, we need to specify the target tenant ID - this could be a domain name or a GUID
        // GUID can be found from subscription ID (see https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-manager-api-authentication#get-tenant-id-from-subscription-id)
        // domain name could just be a prompt
        // test case: cspeer@live.co.uk has access to multiple Azure subscriptions & tenants:
        // cspeer.me : MPN
        // bytescloud.onmicrosoft.com : Chris Speer



    }

    public function test(AzureActiveDirectory $aadAuth) {
        // check that we can log in with the application credentials
        // we use the token endpoint: https://login.microsoftonline.com/<tenant id>/oauth2/token
        // see https://docs.microsoft.com/en-gb/azure/active-directory/develop/active-directory-protocols-oauth-service-to-service#request-an-access-token

        // new test: fire off an event
        $this->dispatch(new AgreementCheckForUpdates(1));



    }





    public function addSubscription() {

    }

    public function storeSubscription(Request $request) {

    }


    public function connectSubscription(AzureActiveDirectory $aadAuth, $subscription_guid) {
        // guide to the manual process: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal
        // steps:
        //  1. get subscription ID
        //  2. get tenant ID of subscription - there should only be one
        //  3. get application object ID from tenant (use servicePrincipals call)
        //  4. add owner/contributor/reader role to the object ID





        // find the subscription:
        //$subscription = Subscription::findByGuid($subscription_guid);

        $aadAuth->setContext(AzureActiveDirectory::CONTEXT_AZURE);
        $aadAuth->setResource(AzureActiveDirectory::RESOURCE_GRAPH);



    }

}
