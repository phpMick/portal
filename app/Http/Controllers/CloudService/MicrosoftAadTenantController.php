<?php

namespace App\Http\Controllers\CloudService;

use App\Auth\AzureActiveDirectory;
use App\CloudServices\Microsoft\Models\AadTenant;
use App\Http\Controllers\Controller;

class MicrosoftAadTenantController extends Controller
{

    public function connect(AadTenant $aadTenant, $applicationName=NULL) {
        //dd($aadTenant->guid . ' :: ' . $applicationName);



        return view('cloudservice/aad/tenant/connect')
            ->with('tenant', $aadTenant);
    }

    public function attach(\App\Auth\AzureActiveDirectory $aadAuth, AadTenant $aadTenant, $applicationName=NULL) {


        if(!$aadAuth->setContext(AzureActiveDirectory::CONTEXT_LOGIN)) {
            // TODO: deal with sign-in errors
            $aadAuth->setIntendedUri(route('aad.tenants.connect', $aadTenant));
            //$aadAuth->setTenant($subscription->aadTenant->guid);

            //return view('cloudservice.azure.subscription.connect')->with('subscription', $subscription);
            //dd($aadAuth);
            return redirect()->route('login.aad', ['context'=>'azure', 'tenant'=>$aadTenant->guid]);
        }

        dd($aadAuth);
    }



}
