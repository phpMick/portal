<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


use App\CloudServices\Azure\DataServices\Azure;
//Models
use App\Models\User;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        if (auth()->guest()) {
            return view('home');
        }
        /** @var User $user */
        $user = auth()->user();

        $az = new Azure();

        $selectedCustomers = $this->getSelectedCustomers($az);

        //$azureSubCount = app('App\Http\Controllers\DataController')->azureSubscriptionCount();
        //$azureSpend = app('App\Http\Controllers\DataController')->azureSpend('last');
        $azureSubCount = $az->getSubscriptionCount();
        $lastMonth = Carbon::now()->subMonth()->format('Y-m');
        $azureSpend = $az->spend()->filter(['monthStart' => $lastMonth, 'monthEnd' => $lastMonth])->getNumber();
    //    $customerCount = $az->selectedCustomersCount();


       // if ($user->isCustomerUser()) {
            return view('dashboard/customer')->with(compact(['azureSubCount', 'azureSpend']));
        /*} elseif ($user->isPartnerUser() OR $user->isSuperUser()) {
            //$customerCount = Customer::query()->count();
            return view('dashboard/partner')
                ->with('azureSubCount', $azureSubCount)
                ->with('azureSpend', $azureSpend)
                ->with('customerCount', $customerCount)
                ->with('selectedCustomers', $selectedCustomers);
        } else {
            abort(403);
        }*/

    }


    /**
     * Just get the selected customers from the session.
     * @param $az
     * @return array
     */
    private function getSelectedCustomers($az)
    {
        $selectedCustomers = [];

        if (session()->has('selected_customers')) {
            foreach (session()->get('selected_customers') as $id => $name) {
                $selectedCustomers[intval($id)] = $name;
            }
            if (count($selectedCustomers) > 0) {
                $az->setCustomers(array_keys($selectedCustomers));
            }
        }

        return $selectedCustomers;
    }


    /**
     * Just display the message page.
     * Message should have already been set.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function messagePage()
    {
        return view('message-page');
    }


}
