<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class CheckUserGroupSelected
{
    /**
     * Handle an incoming request.
     * Just make sure that a user group is selected.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(session('selectedUserGroup') == null){

            //save the selected route
            $route = Route::current();

            session(['intended_route' => $route->getName()]);

            return redirect(route('user-groups.select-list'));
        }

        return $next($request);
    }
}
