<?php

namespace App\Connectors;



use Illuminate\Filesystem\FilesystemAdapter;

class MicrosoftAzureEaBillingApi {

    const REPORT_DETAIL = 'detail';
    const REPORT_SUMMARY = 'summary';
    const REPORT_PRICESHEET = 'pricesheet';
    const REPORT_LISTCACHE = 'listcache';
    const REPORT_ALL = array(
        self::REPORT_DETAIL,
        self::REPORT_SUMMARY,
        self::REPORT_PRICESHEET
    );

    const REGEX_MONTH = '/^\d\d\d\d\-\d\d$/';

    private $apiUrl = 'https://ea.azure.com/';
    private $enrolmentKey;
    private $enrolmentNumber;
    private $availableMonths;
    private $cacheEnabled = TRUE;
    private $cacheEntries = 5;
    private $cacheRefreshAge = 60*60*12; // 12 hours
    private $logOutput = TRUE;
    private $saveDir = NULL; // includes slash at end

    /**
     * MicrosoftAzureEaBillingApi constructor.
     * @param string $enrolmentNumber The enrolment number
     * @param string $accessKey The API access key for this enrolment (from EA portal)
     * @param string|null $saveDir The directory to save to (NULL=system temp dir)
     * @param bool $useEnrolmentDir Whether to add a dir for this enrolment to the save dir
     * @throws \Exception
     */
    public function __construct($enrolmentNumber, $accessKey, $saveDir=NULL, $useEnrolmentDir=TRUE) {
        $this->enrolmentNumber = $enrolmentNumber;
        $this->enrolmentKey = $accessKey;
        if(is_null($saveDir)) {
            $saveDir = realpath(sys_get_temp_dir());
            if(!is_dir($saveDir)) {
                throw new \Exception('Invalid system configuration - no temp dir');
            }
            // create our own directory there:
            $saveDir .= DIRECTORY_SEPARATOR.'AZURE_EA_DATA';
            if(!is_dir($saveDir)) {
                if(!mkdir($saveDir)) {
                    throw new \Exception('Cannot write to system temp dir');
                }
            }
        }
        // check save dir exists, and create enrolment dir if needed:
        if(!is_dir($saveDir)) {
            throw new \Exception('Provided directory does not exist');
        }
        if($useEnrolmentDir) {
            $saveDir .= DIRECTORY_SEPARATOR.'EA-'.$this->enrolmentNumber;
            if(!is_dir($saveDir)) {
                if(!mkdir($saveDir)) {
                    throw new \Exception('Cannot create enrolment directory in save directory');
                }
            }
        }
        $this->saveDir = $saveDir.DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $fileType The type of this file
     * @param integer $timestamp The timestamp (unix integer)
     * @param string|null $month The month in YYYY-MM format
     * @return string The filename (no path component)
     */
    private function makeFileName($fileType, $timestamp, $month=NULL) {
        assert('in_array($fileType, self::REPORT_ALL) OR ($filetype==self::REPORT_LISTCACHE)', 'Invalid report file requested');
        assert('preg_match(self::REGEX_MONTH, $month)', 'Invalid month format');
        assert('is_integer($timestamp)', 'Timestamp must be a unix integer');
        if($fileType==self::REPORT_LISTCACHE) {
            return 'EA-'.$this->enrolmentNumber.'_'.$fileType.'_'.$timestamp.'.json';
        }
        return 'EA-'.$this->enrolmentNumber.'_'.$fileType.'_'.$month.'_'.$timestamp.'.csv';
    }

    private function log($data, $params=[]) {
        if($this->logOutput) {
            echo $data."\n";
        }
        Log::info($data, $params);
    }
    /*
    public function _assertConfig() {
        assert('!empty($this->enrolmentNumber)', 'The Azure Enrolment Number must be set');
        assert('!empty($this->enrolmentKey)', 'The Azure API Key must be set');
        if(empty($this->dirRoot)) {
            $this->setSaveDir(__DIR__);
        }
    }
    */
    /**
     * @return string|bool The JSON content of the API call, or FALSE on failure
     */
    private function _downloadReportList() {
        // TODO: add failure check
        $url = $this->apiUrl . 'rest/' . $this->enrolmentNumber . '/usage-reports';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            //'User-Agent' => 'Cloud Manager Pro v0.01',
            'Accept: application/json',
            'Api-Version: 1.0',
            'Authorization: Bearer ' . $this->enrolmentKey
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, -1);
        //curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

        $json = curl_exec($ch);
        curl_close($ch);
        return $json;
    }

    private function _loadUsageReportFromJson($json) {
        try {
            $data = json_decode($json);
            // iterate over object:
            assert(is_object($data) AND property_exists($data, 'AvailableMonths') AND is_array($data->AvailableMonths), 'Invalid server response (no object)');
            $parts = array(
                self::REPORT_DETAIL=>'LinkToDownloadDetailReport',
                self::REPORT_SUMMARY=>'LinkToDownloadSummaryReport',
                self::REPORT_PRICESHEET=>'LinkToDownloadPriceSheetReport'
            );
            $this->availableMonths = array();
            foreach($data->AvailableMonths as $o) {
                assert(is_object($o) AND property_exists($o, 'Month'), 'Invalid server response (no month object)');
                assert(preg_match(self::REGEX_MONTH, $o->Month), 'Invalid server response (invalid month format)');
                assert(!array_key_exists($o->Month, $this->availableMonths), 'Invalid server response (duplicated month)');
                $m = array();
                foreach($parts as $type=>$link) {
                    assert(property_exists($o, $link) AND is_string($o->$link) AND !empty($o->$link), 'Invalid server response (invalid link)');
                    $m[$type] = $o->$link;
                }
                // all parts present
                $this->availableMonths[$o->Month] = $m;
            }
            return (count($this->availableMonths)>0);
        } catch(Exception $e) {
            return FALSE;
        }
    }

    private function _loadReportList() {
        // check for cache file that holds the last JSON response from the API
        $this->_assertConfig();
        $loaded = FALSE;
        if($this->cacheEnabled) {
            $files = scandir($this->saveDir);
            $cacheFiles = array();
            foreach($files as $file) {
                $matches = array();
                $regex = $this->makeFileName(self::REPORT_LISTCACHE, 9999999999999999);
                $regex = '/^'.str_replace('9999999999999999', '(\d)', $regex).'$/';
                $filename = $this->saveDir.$file;
                if(preg_match($regex, $file, $matches) AND is_file($filename)) {
                    $cacheFiles[intval($matches[1])] = $filename;
                }
            }
            krsort($cacheFiles);
            // load from first valid file if not too old, delete older files:
            $count = 0;
            $compareTime = time()-$this->cacheRefreshAge;
            foreach($cacheFiles as $downloadTime=>$file) {
                if(!$loaded) {
                    // file recent enough?
                    if($downloadTime>=$compareTime) {
                        // yes, try loading it:
                        $loaded = $this->_loadUsageReportFromJson(file_get_contents($file));
                    }
                }
                if($count>=$this->cacheEntries) {
                    unlink($file);
                }
                $count++;
            }
        }
        if(!$loaded) {
            // no appropriate cache file was found - download from API endpoint
            $json = $this->_downloadReportList();
            if(!$this->_loadUsageReportFromJson($json)) {
                return FALSE;
            }
            // cache it:
            if($this->cacheEnabled) {
                $filename = $this->saveDir.$this->makeFileName(self::REPORT_LISTCACHE, time());
                file_put_contents($filename, $json);
            }
        }
    }

    public function getReportList() {
        if(!isset($this->availableMonths)) {
            $this->_loadReportList();
        }
        return $this->availableMonths;
    }

    public function getAvailableMonths() {
        return array_keys($this->availableMonths);
    }

    private function _downloadFile($month, $reportType) {
        if(!array_key_exists($month, $this->availableMonths)
            OR !array_key_exists($reportType, $this->availableMonths[$month])
            OR empty($this->availableMonths[$month][$reportType])) {
                return FALSE;
        }
        // TODO: re-write download routine to use CURLOPT_WRITEFUNCTION so that we avoid memory issues
        // see https://stackoverflow.com/questions/7967531/php-curl-writing-to-file
        $url = $this->apiUrl . $this->availableMonths[$month][$reportType];
        $filename = $this->saveDir . $this->makeFileName($reportType, time(), $month);
        $this->log('Downloading month '.$month.' '.$reportType);
        $this->log(' -> from "'.$url.'"');
        $this->log(' -> to "'.$filename.'"');
        $fp = fopen($filename, 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            //'User-Agent' => 'Cloud Manager Pro v0.01',
            'Accept: application/json',
            'Api-Version: 1.0',
            'Authorization: Bearer ' . $this->enrolmentKey
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_TIMEOUT, -1);
        curl_setopt($ch, CURLOPT_VERBOSE, FALSE);

        $result = curl_exec($ch);
        //$info = curl_getinfo($ch);
        curl_close($ch);
        fclose($fp);
        if($result===FALSE) {
            $this->log(' -> download failed');
            return FALSE;
        }
        //$this->log(' -> transfer info:');
        //var_dump($info);
        $size = filesize($filename);
        $this->log(' -> downloaded '.$size.' bytes');
        if($size==0) {
            $this->log(' -> no data downloaded');
            return FALSE;
        }
        return $filename;
    }
/*
    private function _createDirs($reportTypes) {
        // $reportTypes is validated
        // create root dir...
        if($this->useEnrolmentDir) {
            $dir = $this->dirRoot . $this->enrolmentNumber;
            if(!empty($this->enrolmentName)) {
                $dir .= ' ('.$this->enrolmentName.')';
            }
            if(!is_dir($dir)) {
                assert(mkdir($dir),'Failed to create directory');
            }
            $this->dirEnrolment = $dir . DIRECTORY_SEPARATOR;
        } else {
            $this->dirEnrolment = $this->dirRoot;
        }
        if($this->useFileTypeDir) {
            foreach ($reportTypes as $r) {
                if (!is_dir($this->dirEnrolment . $r)) {
                    assert(mkdir($this->dirEnrolment . $r), 'Failed to create ' . $r . ' directory');
                }
            }
        }
    }
*/
    /**
     * @param string $month
     * @param string $reportType
     * @return array|bool
     */
    public function downloadFile($month, $reportType) {
        assert('in_array($reportType, self::REPORT_ALL)', 'Report type invalid');
        assert('preg_match(self::REGEX_MONTH, $month)', 'Month invalid');
        if(!isset($this->availableMonths)) {
            $this->_loadReportList();
        }
        return $this->_downloadFile($month, $reportType);
    }

    public function downloadFiles($months=NULL, $reportTypes=NULL) {
        if(!isset($this->availableMonths)) {
            $this->_loadReportList();
        }
        // validate months:
        if(!isset($months)) {
            $months = array_keys($this->availableMonths);
        } elseif(is_string($months)) {
            $months = [$months];
        } elseif(!is_array($months)) {
            throw new \Exception('Invalid parameter: months');
        }
        foreach($months as $month) {
            if(!preg_match(self::REGEX_MONTH, $month)) {
                throw new \Exception('Invalid month format');
            }
        }
        // validate reportTypes:
        if(!isset($reportTypes)) {
            $reportTypes = self::REPORT_ALL;
        } elseif(is_string($reportTypes)) {
            $reportTypes = [$reportTypes];
        } elseif(!is_array($reportTypes)) {
            throw new \Exception('Invalid parameter: report type');
        }
        foreach($reportTypes as $reportType) {
            if(!in_array($reportType, self::REPORT_ALL)) {
                throw new \Exception('Invalid report type');
            }
        }
        if($this->logOutput) {
            $this->log('Months to download: ' . implode(', ', $months));
        }
        $results = array();
        foreach($months as $month) {
            $results[$month] = array();
            foreach($reportTypes as $reportType) {
                $results[$month][$reportType] = $this->_downloadFile($month, $reportType);
            }
        }
        return $results;
    }

    /*
    public function setApiKey($key) {
        $this->enrolmentKey = (string)$key;
    }

    public function setEnrolmentNumber($number) {
        $this->enrolmentNumber = (string)$number;
    }

    public function setEnrolmentName($name) {
        $this->enrolmentName = $name;
    }

    public function setSaveDir($dir, $allowCreate=FALSE) {
        if($allowCreate) {
            if(!file_exists($dir) OR !is_dir($dir)) {
                mkdir($dir, NULL, TRUE);
            }
        }
        $dir = realpath($dir);
        assert($dir!==FALSE, 'Save path must exist');
        assert(is_dir($dir), 'Save path must be a directory');
        $this->dirRoot = $dir . DIRECTORY_SEPARATOR;
    }

    public function setMonths($months) {
        if(is_string($months)) {
            $input = $months;
            $months = array();
            // try and interpret english requests:
            $matches = array();
            if(preg_match('/last\s+(\d+)\s+month/i', $input, $matches)) {
                assert(count($matches)>=2, 'regex failure');
                $i = intval($matches[1]);
                while($i>0) {
                    $months[] = date('Y-m', strtotime($i.' month ago'));
                    $i--;
                }
            }
            if(preg_match('/this\s+month/i', $input)) {
                $months[] = date('Y-m');
            }
            // last-ditch effort to extract data
            if(empty($months)) {
                $months = explode(',', $input);
            }
        }
        assert(is_array($months), 'Months must be specified as an array');
        $this->months = array();
        foreach($months as $month) {
            $m = trim($month);
            assert(preg_match(self::REGEX_MONTH, $m), 'Month must be specified in the format YYYY-MM');
            $this->months[] = $m;
        }
    }

    public function getMonths() {
        return $this->months;
    }

    public function setUseDirs($useEnrolmentDir, $useFileTypeDir) {
        assert(is_bool($useEnrolmentDir));
        assert(is_bool($useFileTypeDir));
        $this->useEnrolmentDir = $useEnrolmentDir;
        $this->useFileTypeDir = $useFileTypeDir;
    }
    */
//    public function saveInFlysystemDisk($diskName, $pathPrefix='') {
//        $this->diskName = $diskName;
//        $this->disk = \Storage::disk($diskName);
//    }


}
