<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 10/09/2017
 * Time: 12:49
 */

namespace App\Connectors\MicrosoftAzureActiveDirectoryAuth;


class AuthUser
{

    public $tenantGuid = NULL;
    public $name = '';
    public $objectGuid = NULL;
    public $upn = '';
    public $email = '';


}