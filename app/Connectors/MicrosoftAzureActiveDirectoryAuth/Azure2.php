<?php

namespace App\Connectors\MicrosoftAzureActiveDirectoryAuth;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Auth2Endpoint
 * Implements the basics for interacting with the AAD v2.0 endpoing which allows
 * us to authenticate both Microsoft Personal and Organisational Accounts
 * @package App\CloudServices\AccessServices\MicrosoftAzureActiveDirectoryAuth
 */
class Azure2 extends AbstractProvider {


    public $tenant = 'common';

    protected function getEndpointUrl() {
        return 'https://login.microsoftonline.com/' . $this->tenant;
    }

    /**
     * Returns the base URL for authorizing a client.
     *
     * Eg. https://oauth.service.com/authorize
     *
     * @return string
     */
    public function getBaseAuthorizationUrl() {
        return $this->getEndpointUrl().'/oauth2/v2.0/authorize';
    }

    /**
     * Returns the base URL for requesting an access token.
     *
     * Eg. https://oauth.service.com/token
     *
     * @param array $params
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params) {
        return $this->getEndpointUrl().'/oauth2/v2.0/token';
    }

    /**
     * Returns the URL for requesting the resource owner's details.
     *
     * @param AccessToken $token
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        // TODO: Implement getResourceOwnerDetailsUrl() method.
    }

    /**
     * Returns the default scopes used by this provider.
     *
     * This should only be the scopes that are required to request the details
     * of the resource owner, rather than all the available scopes.
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        // scopes required for basic access:
        $scopes = [
            'User.Read', // get basic user details including access to organisation info if available
            'profile', // include basic user information in the id_token data
            'openid', // pulls id_token data
            'email', // include email address in the id_token data
            //'offline_access' // provides an oAuth Refresh Token
        ];
        return implode(' ', $scopes);
    }

    /**
     * Checks a provider response for errors.
     *
     * @param  ResponseInterface $response
     * @param  array|string $data Parsed response data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        // TODO: Implement checkResponse() method.
    }

    /**
     * Generates a resource owner object from a successful resource owner
     * details request.
     *
     * @param  array $response
     * @param  AccessToken $token
     * @return void
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        // TODO: Implement createResourceOwner() method.
    }

    public function extractUserDetails(AccessToken $token) {
        // this is a very naive implementation, but will suffice for now...
        $userData = [];
        $values = $token->getValues();
        if(is_array($values) AND array_key_exists('id_token', $values) AND strlen($values['id_token'])>0) {
            $id_tokens = explode('.', $values['id_token']);
            if(count($id_tokens)>=2) {
                $userData = json_decode(base64_decode($id_tokens[1]), true);
            }
        }
        return $userData;
    }
}