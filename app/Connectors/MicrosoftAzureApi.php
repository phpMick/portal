<?php

namespace App\Connectors;


use App\Connectors\Common\HasModules;
use App\Connectors\Common\MicrosoftAuthRestApi;


/**
 * Class MicrosoftAzureApi
 * @package App\Connectors
 * @method \App\Connectors\MicrosoftAzureApi\Authorization Authorization()
 * @method \App\Connectors\MicrosoftAzureApi\ResourceManagement ResourceManagement()
 * @method \App\Connectors\MicrosoftAzureApi\Billing Billing()
 */
class MicrosoftAzureApi extends MicrosoftAuthRestApi
{
    use HasModules;

    protected $_clientConfig = [
        'base_uri' => 'https://management.azure.com',
        'exceptions' => FALSE
    ];

    protected $_authResource = 'azurerm';

    /**
     * @var string
     */
    protected $_subscription_guid = NULL;

    /**
     * @param \App\CloudServices\Azure\Models\Subscription|string $subscription
     * @return $this|string
     */
    public function subscription($subscription=NULL) {
        if(is_null($subscription)) {
            return $this->_subscription_guid;
        }
        // set subscription by GUID or from DB:
        if($subscription instanceof \App\CloudServices\Azure\Models\Subscription) {
            $this->_subscription_guid = $subscription->guid;
        } elseif(is_string($subscription) AND preg_match('/^'.self::REGEX_UUID.'$/', $subscription)) {
            $this->_subscription_guid = $subscription;
        }
        return $this;
    }


    public function listResourceGroups() {
        if(!isset($this->_subscription_guid)) {
            return NULL;
        }
        $url = '/subscriptions/'.$this->_subscription_guid.'/resourceGroups?api-version=2017-05-10';
        return $this->get($url)->errorAsEmptyArray()->decodeResponseBody();
    }


}