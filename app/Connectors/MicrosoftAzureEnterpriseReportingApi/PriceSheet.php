<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;


use App\Connectors\MicrosoftAzureEnterpriseReportingApi;

/**
 * Class PriceSheet
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi
 * @property string $month
 * @property string $monthId
 * @property string $currency
 * @property MicrosoftAzureEnterpriseReportingApi\Models\PriceSheetRow[] $rows
 */
class PriceSheet extends MicrosoftAzureEnterpriseReportingApiModule
{

    /**
     * @var string
     */
    protected $_monthId = NULL;

    protected $_currency = NULL;

    protected $_rows = [];


    /**
     * @param string|NULL $month
     * @return MicrosoftAzureEnterpriseReportingApi\Models\PriceSheet
     * @throws \Exception
     */
    public function get($month=NULL) {
        if(empty($month)) {
            $url = 'pricesheet';
        } else {
            $month = str_replace('-', '', $month);
            if(!preg_match('/^\d{6}$/', $month)) {
                throw new \Exception('Invalid month format');
            }
            $url = 'billingPeriods/'.$month.'/pricesheet';
        }
        return new MicrosoftAzureEnterpriseReportingApi\Models\PriceSheet($this->_get($url)->errorAsEmptyArray()->decodeResponseBody());
    }

}