<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;


use App\Connectors\Common\RestApiModule;

/**
 * Class MicrosoftAzureEnterpriseReportingApiModule
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi
 * @method \App\Connectors\MicrosoftAzureEnterpriseReportingApi _getApi()
 */
class MicrosoftAzureEnterpriseReportingApiModule extends RestApiModule
{

}