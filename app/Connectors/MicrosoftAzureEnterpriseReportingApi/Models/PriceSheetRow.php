<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;

use App\Connectors\Common\ApiObject;

/**
 * Class PriceSheetRow
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models
 * @property string $id
 * @property string $url
 * @property string $meterGuid
 * @property string $meterName
 * @property string $unitOfMeasure
 * @property string $includedQuantity
 * @property string $partNumber
 * @property string $unitPrice
 *
 */
class PriceSheetRow extends ApiObject
{

    const PROPERTY_MAP = [
        'url' =>'id',
        'meterGuid'=>'meterId',
        'meterName',
        'unitOfMeasure',
        'includedQuantity',
        'partNumber',
        'unitPrice'
    ];
/*
    protected $objectMap = [
        'id' => '',
        //'billingPeriodId' => 'month', // not required - should be the same over whole collection
        'meterId' => 'meterGuid',
        'meterName' => 'meterName',
        'unitOfMeasure' => 'unitOfMeasure',
        'includedQuantity' => 'includedQuantity',
        'partNumber' => 'partNumber',
        'unitPrice' => 'unitPrice',
        //'currencyCode' => 'currency' // not required - should be the same over whole collection
    ];
*/

    public function loadFromArray($data)
    {
        if(parent::loadFromArray($data)) {
            // extract the product id from the url:
            $matches = [];
            if(preg_match('/\/products\/(\d+)\/pricesheets/', $this->url, $matches)) {
                $this->id = $matches[1];
            } else {
                $this->id = '';
            }
        }
        return FALSE;
    }

}