<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 16/10/2017
 * Time: 23:23
 */

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;


use App\Connectors\Common\ApiObject;

/**
 * Class SummaryDetailItem
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models
 * @property string $name
 * @property string $value
 */
class SummaryDetailItem extends ApiObject
{

    const PROPERTY_MAP = [
        'name',
        'value'
    ];

}