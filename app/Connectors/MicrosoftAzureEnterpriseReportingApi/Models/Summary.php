<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;

use App\Connectors\Common\ApiObject;

/**
 * Class Summary
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models
 * @property string $month
 * @property string $monthId
 * @property string $url
 * @property string $currency
 * @property string $balanceStart
 * @property string $balanceEnd
 * @property string $purchases
 * @property string $adjustments
 * @property string $usedBalance
 * @property string $usedOverage
 * @property string $usedOther
 * @property string $totalOverage
 * @property string $totalUsage
 * @property string $usedMarketplace
 * @property SummaryDetailItem[] $purchaseDetails
 * @property SummaryDetailItem[] $adjustmentDetails
 */
class Summary extends ApiObject {

    const PROPERTY_MAP = [
        'url'                => 'id',
        'monthId'            => 'billingPeriodId',
        'currency'           => 'currencyCode',
        'balanceStart'       => 'beginningBalance',
        'balanceEnd'         => 'endingBalance',
        'purchases'          => 'newPurchases',
        'adjustments'        => 'adjustments',
        'usedBalance'        => 'utilized',
        'usedOverage'        => 'serviceOverage',
        'usedMarketplace'    => 'azureMarketplaceServiceCharges',
        'usedOther'          => 'chargesBilledSeparately',
        'totalOverage'       => 'totalOverage',
        'totalUsage'         => 'totalUsage',
    ];


    public function loadFromArray($data) {
        if(parent::loadFromArray($data)) {
            $this->month = preg_match('/^\d{6}$/', $this->monthId) ? (substr($this->monthId, 0, 4) . '-' . substr($this->monthId, 4, 2)) : NULL;
            $this->purchaseDetails = [];
            if(array_key_exists('newPurchasesDetails', $data) AND is_array($data['newPurchasesDetails'])) {
                foreach($data['newPurchasesDetails'] as $row) {
                    $this->purchaseDetails[] = new SummaryDetailItem($row);
                }
            }
            $this->adjustmentDetails = [];
            if(array_key_exists('adjustmentDetails', $data) AND is_array($data['adjustmentDetails'])) {
                foreach($data['adjustmentDetails'] as $row) {
                    $this->adjustmentDetails[] = new SummaryDetailItem($row);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

}