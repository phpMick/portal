<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;


use Illuminate\Support\Collection;

class PriceSheet implements \ArrayAccess, \Iterator
{

    protected $_monthId = NULL;

    protected $_currency = NULL;

    protected $_rows = [];

    protected $_rowId = 0;

    protected $_duplicates = [
        'meterName' => [],
        'meterGuid' => [],
        'partNumber' => [],
        'id' => []
    ];

    protected $_duplicatesFound = FALSE;

    public function __construct($dataArray) {
        if(!is_array($dataArray)) {
            throw new \Exception('Invalid data format');
        }
        if(count($dataArray)>0) {
            if(!array_key_exists('billingPeriodId', $dataArray[0]) OR !array_key_exists('currencyCode', $dataArray[0])) {
                throw new \Exception('Invalid data format');
            }
            $this->_monthId = $dataArray[0]['billingPeriodId'];
            $this->_currency = $dataArray[0]['currencyCode'];
        }

        foreach($dataArray as $row) {
            if(!array_key_exists('billingPeriodId', $row) OR !array_key_exists('currencyCode', $row)) {
                throw new \Exception('Invalid data format');
            }
            if($row['billingPeriodId']!=$this->_monthId OR $row['currencyCode']!=$this->_currency) {
                throw new \Exception('Invalid data file');
            }
            $this->_rows[] = new PriceSheetRow($row);
        }
    }

    public function __get($name) {
        switch ($name) {
            case 'monthId': return $this->_monthId;
            case 'month': return (strlen($this->_monthId)==6) ? (substr($this->_monthId, 0, 4) . '-' . substr($this->_monthId, 4, 2)) : NULL;
            case 'currency': return $this->_currency;
        }
    }




    /**
     * @param string $partNumber
     * @return PriceSheetRow[]
     */
    public function findByPartNumber($partNumber) {
        $results = [];
        foreach($this->_rows as $row) {
            if($row->partNumber == $partNumber) {
                $results[] = $row;
            }
        }
        return $results;
    }

    /**
     * @param string $meterGuid
     * @return PriceSheetRow[]
     */
    public function findByMeterGuid($meterGuid) {
        $results = [];
        foreach($this->_rows as $row) {
            if($row->meterGuid == $meterGuid) {
                $results[] = $row;
            }
        }
        return $results;
    }

    /**
     * @param string $name
     * @return PriceSheetRow[]
     */
    public function findByMeterName($name) {
        $results = [];
        foreach($this->_rows as $row) {
            if($row->meterName == $name) {
                $results[] = $row;
            }
        }
        return $results;
    }

    public function findDuplicates() {
        if($this->_duplicatesFound) {
            return $this;
        }
        $meterNames = [];
        $meterGuids = [];
        $partNumbers = [];
        $ids = [];
        foreach($this->_rows as $row) {
            foreach(['meterName', 'meterGuid', 'partNumber', 'id'] as $propName) {
                $arrayName = $propName.'s';
                ($$arrayName)[] = $row->$propName;
                if(empty($row->$propName)) {
                    continue;
                }
                // does the value already exist in the array?
                $first_index = array_search($row->$propName, $$arrayName);
                if($first_index!==FALSE) {
                    // does the value exist in the duplicates array?
                    if(!array_key_exists($row->$propName, $this->_duplicates[$propName])) {
                        // no - add the first one (from the search index):
                        $this->_duplicates[$propName][$row->$propName] = $this->_rows[$first_index];
                    }
                    // add the current row:
                    $this->_duplicates[$propName][$row->$propName] = $row;
                }
            }
        }
        $this->_duplicatesFound = TRUE;
        return $this;
    }

    public function hasDuplicatedMeterGuid() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return count($this->_duplicates['meterGuid']);
    }

    public function hasDuplicatedMeterName() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return count($this->_duplicates['meterName']);
    }

    public function hasDuplicatedPartNumber() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return count($this->_duplicates['partNumber']);
    }

    public function hasDuplicatedId() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return count($this->_duplicates['id']);
    }

    public function duplicatedMeterGuids() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return $this->_duplicates['meterGuid'];
    }

    public function duplicatedMeterNames() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return $this->_duplicates['meterName'];
    }

    public function duplicatedPartNumbers() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return $this->_duplicates['partNumber'];
    }

    public function duplicatedIds() {
        if(!$this->_duplicatesFound) {
            $this->findDuplicates();
        }
        return $this->_duplicates['id'];
    }


    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return PriceSheetRow Can return any type.
     * @since 5.0.0
     */
    public function current() {
        return $this->_rows[$this->_rowId];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next() {
        $this->_rowId++;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key() {
        return $this->_rowId;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid() {
        return $this->offsetExists($this->_rowId);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind() {
        $this->_rowId = 0;
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset) {
        return ($offset >= 0) AND ($offset < count($this->_rows));
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return PriceSheetRow Can return all value types.
     * @since 5.0.0
     * @throws \Exception
     */
    public function offsetGet($offset) {
        if($this->offsetExists($offset)) {
            return $this->_rows[$offset];
        }
        throw new \Exception('Invalid offset');
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws \Exception
     */
    public function offsetSet($offset, $value) {
        throw new \Exception('Not implemented');
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws \Exception
     */
    public function offsetUnset($offset) {
        throw new \Exception('Not implemented');
    }
}