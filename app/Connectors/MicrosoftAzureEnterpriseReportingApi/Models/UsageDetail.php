<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;

/**
 * Class UsageDetail
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models
 * @property string $accountOwner
 * @property string $accountName
 * @property string $serviceAdmin
 * @property string $departmentName
 * @property string $costCenter
 * @property string $subscriptionGuid
 * @property string $subscriptionName
 * @property string $date
 * @property string $meterGuid
 * @property string $meterCategory
 * @property string $meterSubCategory
 * @property string $meterRegion
 * @property string $meterName
 * @property string $product
 * @property string $service
 * @property string $resourceGroup
 * @property string $resourceId
 * @property string $resourceRegion
 * @property string $resourceRate
 * @property string $consumedQuantity
 * @property string $extendedCost
 * @property string $unitOfMeasure
 */
class UsageDetail extends _BaseApiModel
{

    /**
     * @var string[]
     */
    public $serviceInfo = [];

    /**
     * @var string[]
     */
    public $resourceInfo = [];

    /**
     * @var string[]
     */
    public $tags = [];


    protected $objectMap = [
        //'accountId', // deprecated
        //'productId', // deprecated
        //'resourceLocationId', // deprecated
        //'consumedServiceId', // deprecated
        //'departmentId', // deprecated
        'accountOwnerEmail' => 'accountOwner',
        'accountName' => 'accountName',
        'serviceAdministratorId' => 'serviceAdmin',
        //'subscriptionId', // deprecated
        'subscriptionGuid' => 'subscriptionGuid',
        'subscriptionName' => 'subscriptionName',
        'date' => 'date',
        'product' => 'product',
        'meterId' => 'meterGuid',
        'meterCategory' => 'meterCategory',
        'meterSubCategory' => 'meterSubCategory',
        'meterRegion' => 'meterRegion',
        'meterName' => 'meterName',
        'consumedQuantity' => 'consumedQuantity',
        'resourceRate' => 'resourceRate',
        'Cost' => 'extendedCost',
        'resourceLocation' => 'resourceRegion',
        'consumedService' => 'service',
        'instanceId' => 'resourceId',
        //'serviceInfo1' => '', // loaded separately
        //'serviceInfo2' => '', // loaded separately
        //'additionalInfo' => '', // loaded separately
        //'tags', // loaded separately
        //'storeServiceIdentifier', // deprecated
        'departmentName' => 'departmentName',
        'costCenter' => 'costCenter',
        'unitOfMeasure' => 'unitOfMeasure',
        'resourceGroup' => 'resourceGroup'
    ];

    public function load($data)
    {
        assert('is_array($data)', 'Array expected');
        $this->loadFromArray($data);
        foreach(['serviceInfo1','serviceInfo2','additionalInfo','tags'] as $prop) {
            if(!array_key_exists($prop, $data)) {
                throw new \Exception('Invalid data format');
            }
        }
        if(is_array($data['serviceInfo1'])) {
            foreach($data['serviceInfo1'] as $k=>$v) {
                if(!empty($k) AND !empty($v)) {
                    $this->serviceInfo[$k] = $v;
                }
            }
        }
        if(is_array($data['serviceInfo2'])) {
            foreach($data['serviceInfo2'] as $k=>$v) {
                if(!empty($k) AND !empty($v)) {
                    $this->serviceInfo[$k] = $v;
                }
            }
        }
        if(is_array($data['additionalInfo'])) {
            foreach($data['additionalInfo'] as $k=>$v) {
                if(!empty($k) AND !empty($v)) {
                    $this->resourceInfo[$k] = $v;
                }
            }
        }
        if(is_array($data['tags'])) {
            foreach($data['tags'] as $k=>$v) {
                if(!empty($k) AND !empty($v)) {
                    $this->tags[$k] = $v;
                }
            }
        }
    }

    protected function loadDefaults()
    {
        parent::loadDefaults();
        $this->tags = [];
        $this->serviceInfo = [];
        $this->resourceInfo = [];
    }

    public function getDataColumns()
    {
        return array_merge($this->attributes, ['serviceInfo','resourceInfo','tags']);
    }

    public function getDataArray()
    {
        ksort($this->serviceInfo);
        ksort($this->resourceInfo);
        ksort($this->tags);
        return array_merge(
            array_values($this->attributes),
            [
                json_encode($this->serviceInfo),
                json_encode($this->resourceInfo),
                json_encode($this->tags)
            ]
        );
    }

}