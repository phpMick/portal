<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;

use App\Connectors\Common\ApiObject;
use Carbon\Carbon;

/**
 * Class BillingPeriod
 * @package App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models
 * @property string $monthId
 * @property string $month
 * @property string $balanceSummaryUrl
 * @property string $usageDetailsUrl
 * @property string $marketplaceChargesUrl
 * @property string $priceSheetUrl
 * @property Carbon $startTime
 * @property Carbon $endTime
 */
class BillingPeriod extends ApiObject
{

    const PROPERTY_MAP = [
        'monthId'=>'billingPeriodId',
        'balanceSummaryUrl'=>'balanceSummary',
        'usageDetailsUrl'=>'usageDetails',
        'marketplaceChargesUrl'=>'marketplaceCharges',
        'priceSheetUrl'=>'priceSheet',
    ];

    public function loadFromArray($data)
    {
        if(parent::loadFromArray($data)) {
            $this->month = preg_match('/^\d{6}$/', $this->monthId) ? (substr($this->monthId, 0, 4) . '-' . substr($this->monthId, 4, 2)) : NULL;
            $this->startTime = empty($data['billingStart']) ? NULL : new Carbon($data['billingStart'], 'UTC');
            $this->endTime = empty($data['billingEnd']) ? NULL : new Carbon($data['billingEnd'], 'UTC');
            return TRUE;
        }
        return FALSE;
    }


}