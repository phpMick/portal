<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 28/09/2017
 * Time: 22:12
 */

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models;


class _BaseApiModel
{

    public $attributes = [];
    protected $objectMap = [];


    public function __construct($data=NULL)
    {
        if(isset($data)) {
            $this->load($data);
        } else {
            $this->loadDefaults();
        }
    }

    public function load($data)
    {
        if(is_array($data)) {
            $this->loadFromArray($data);
        }
    }

    protected function loadFromArray($data)
    {
        $this->loadDefaults();
        assert('is_array($data)', 'Invalid data format');
        foreach ($this->objectMap as $srcProp=>$prop) {
            // ensure every $srcProp exists on passed array, and push into attributes array (unless $prop=NULL)
            if(!array_key_exists($srcProp, $data)) {
                throw new \Exception('Invalid data format');
            }
            if(!empty($prop)) {
                $this->attributes[$prop] = $data[$srcProp];
            }
        }
    }

    protected function loadDefaults() {
        $this->attributes = [];
    }


    public function __get($attribute) {
        if(!array_key_exists($attribute, $this->attributes)) {
            throw new \Exception('Attribute not defined');
        }
        return $this->attributes[$attribute];
    }

    public function __set($attribute, $value) {
        if(!array_key_exists($attribute, $this->attributes)) {
            throw new \Exception('Attribute not defined');
        }
        $this->attributes[$attribute] = $value;
    }

    public function getDataColumns() {
        return array_keys($this->attributes);
    }

    public function getDataArray() {
        return array_values($this->attributes);
    }


}