<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 28/09/2017
 * Time: 23:51
 */

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;


use App\Connectors\MicrosoftAzureEnterpriseReportingApi;

class UsageDetailOld
{

    /**
     * @var MicrosoftAzureEnterpriseReportingApi
     */
    protected $_api;

    protected $_error = NULL;

    protected $_data = [];

    /**
     * The index in the current data array for the next read. NULL = not loaded
     * @var integer|NULL
     */
    protected $_nextRow = NULL;

    /**
     * The total number of rows loaded so far for this query (may have more)
     * @var integer
     */
    protected $_totalRowsLoaded = 0;

    /**
     * Indicates if more data is available from the API for this query
     * @var boolean
     */
    protected $_moreRowsAvailable = TRUE;

    /**
     * The query type
     * @var string
     */
    protected $_queryType = 'month';

    /**
     * The URL for the next query - updated by the query functions and if there is more data available
     * @var string|NULL
     */
    protected $_queryUrl = 'usagedetails';

    public function __construct(MicrosoftAzureEnterpriseReportingApi $api) {
        $this->_api = $api;
    }

    public function queryMonth($month=NULL) {
        $this->_data = [];
        $this->_queryType = 'month';
        $this->_nextRow = NULL;
        $this->_totalRowsLoaded = 0;
        $this->_moreRowsAvailable = TRUE;
        if(empty($month)) {
            $this->_queryUrl = 'usagedetails';
        } else {
            if(preg_match('/^\d\d\d\d\-\d\d$/', $month)) {
                $month = implode('',explode('-',$month));
            }
            if(!preg_match('/^\d{6}$/', $month)) {
                throw new \Exception('Invalid month format');
            }
            $this->_queryUrl = 'billingPeriods/'.$month.'/usagedetails';
        }
        return $this;
    }

    public function queryTimePeriod($startDate, $endDate) {
        $this->_data = [];
        $this->_queryType = 'custom';
        $this->_nextRow = NULL;
        $this->_totalRowsLoaded = 0;
        $this->_moreRowsAvailable = TRUE;
        if(!preg_match('/^\d\d\d\d\-\d\d-\d\d$/', $startDate) OR !preg_match('/^\d\d\d\d\-\d\d-\d\d$/', $endDate)) {
            throw new \Exception('Invalid custom date format');
        }
        $this->_queryUrl = 'usagedetailsbycustomdate?startTime='.$startDate.'&endTime='.$endDate;
        return $this;
    }

    public function loadData() {
        $data = $this->_api->getResponseBody('GET', $this->_queryUrl);
        // wrap number properties in quotes so there is no float rounding errors:
        $this->_data = $this->_api->stringifyAndDecodeJson($data, TRUE);
        if(!array_key_exists('data', $this->_data)) {
            throw new \Exception('Invalid data format');
        }
        if(array_key_exists('nextLink', $this->_data) AND !empty($this->_data['nextLink'])) {
            $this->_queryUrl = $this->_data['nextLink'];
            $this->_moreRowsAvailable = TRUE;
        } else {
            $this->_queryUrl = NULL;
            $this->_moreRowsAvailable = FALSE;
        }
    }

    public function getNextRow() {
        if(is_null($this->_nextRow)) {
            $this->loadData();
            $this->_nextRow = 0;
            $this->_totalRowsLoaded = count($this->_data['data']);
        }
        if($this->_nextRow >= count($this->_data['data'])) {
            if($this->_moreRowsAvailable) {
                $this->loadData();
                $this->_nextRow = 0;
                $this->_totalRowsLoaded += count($this->_data['data']);
            } else {
                return NULL;
            }
        }
        return new MicrosoftAzureEnterpriseReportingApi\Models\UsageDetail($this->_data['data'][$this->_nextRow++]);
    }

    public function moreAvailable() {
        return $this->_moreRowsAvailable;
    }

    public function rowsLoaded() {
        return $this->_totalRowsLoaded;
    }

    public function setRow($val) {
        $this->_nextRow = $val;
    }

}