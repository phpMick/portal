<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;

use App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models\Summary;

class BalanceAndSummary extends MicrosoftAzureEnterpriseReportingApiModule
{


    protected $_defaultMonth = NULL;

    protected $_data = [];


    public function get($month=NULL) {
        if(empty($month)) {
            if(empty($this->_defaultMonth)) {
                /** @var Summary $obj */
                $obj = $this->_normaliseObjectResponse(
                      $this->_get('balancesummary')->errorAsNull()->decodeResponseBody(),
                      'Models\Summary'
                );
                // save it in the array in the right place:
                $this->_defaultMonth = $obj->monthId;
                $this->_data[$this->_defaultMonth] = $obj;
                return $obj;
            } else {
                return $this->_data[$this->_defaultMonth];
            }
        } else {
            // remove any hyphen in month so we can just throw it at the API:
            $month = str_replace('-', '', $month);
            if(!preg_match('/^\d{6}$/', $month)) {
                throw new \Exception('Invalid month format');
            }
            if(!array_key_exists($month, $this->_data)) {
//                return $this->_get('billingPeriods/'.$month.'/balancesummary')->decodeResponseBody();
                $this->_data[$month] = $this->_normaliseObjectResponse(
                    $this->_get('billingPeriods/'.$month.'/balancesummary')->errorAsNull()->decodeResponseBody(),
                    'Models\Summary'
                );
            }
            return $this->_data[$month];
        }
    }




}