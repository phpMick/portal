<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;


use App\Connectors\Common\RestApiPagedQuery;

abstract class UsageDetail extends RestApiPagedQuery
{

    protected function decodeResponse() {
        return TRUE;
    }



    /**
     * @return \GuzzleHttp\Psr7\Response
     */
    public function getResponse() {
        return $this->_response->response();
    }

}