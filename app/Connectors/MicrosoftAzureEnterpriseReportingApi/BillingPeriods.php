<?php

namespace App\Connectors\MicrosoftAzureEnterpriseReportingApi;

use App\Connectors\MicrosoftAzureEnterpriseReportingApi\Models\BillingPeriod;

class BillingPeriods extends MicrosoftAzureEnterpriseReportingApiModule
{

    protected $_data = NULL;

    /**
     * @return BillingPeriod[]
     * @throws \Exception
     */
    public function get() {
        if(!isset($this->_data)) {
            $this->_data = $this->_normaliseListResponse(
                $this->_get('billingPeriods')->errorAsEmptyArray()->decodeResponseBody(),
                'Models\BillingPeriod',
                'month',
                NULL
            );
        }
        return $this->_data;
    }

}