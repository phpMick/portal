<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureEaPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hprec = config('app.decimal_high.precision');
        $hscale = config('app.decimal_high.scale');
        $prec = config('app.decimal_normal.precision');
        $scale = config('app.decimal_normal.scale');

        Schema::create('azure_ea_prices', function (Blueprint $table) use($prec, $scale, $hprec, $hscale) {
            $table->unsignedBigInteger('source_id');
            $table->unsignedBigInteger('agreement_id');
            $table->unsignedBigInteger('ea_meter_id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->decimal('included_qty', $prec, $scale)->default(0);
            $table->decimal('price', $hprec, $hscale);
            $table->decimal('unit_price', $hprec, $hscale)->nullable();

            $table->unique('source_id');
            $table->foreign('agreement_id')->references('source_id')->on('azure_agreements');
            $table->foreign('ea_meter_id')->references('source_id')->on('azure_ea_meters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_ea_prices');
    }
}
