<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_subscriptions', function(Blueprint $table) {
            $table->unsignedInteger('source_id');
            $table->unsignedInteger('agreement_id');
            $table->string('name')->nullable();
            $table->string('guid')->nullable();
            $table->string('offer_code')->nullabe();
            $table->string('department')->nullable();
            $table->string('cost_center')->nullable();
            $table->string('account_name')->nullable();
            $table->string('account_owner')->nullable();
            $table->boolean('is_msdn')->default(0);

            $table->unique('source_id');
            $table->foreign('agreement_id')->references('source_id')->on('azure_agreements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_subscriptions');
    }
}
