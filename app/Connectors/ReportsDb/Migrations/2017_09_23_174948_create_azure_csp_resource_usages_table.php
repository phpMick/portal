<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureCspResourceUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_csp_resource_usages', function(Blueprint $table) {

            $prec = config('app.decimal_high.precision');
            $scale = config('app.decimal_high.scale');

            // don't need source id, since we're aggregrating & deleting en-masse when needed
            // don't need id either, assuming eloquent can deal with it
            $table->unsignedBigInteger('resource_id');
            $table->date('usage_date');
            $table->unsignedInteger('meter_id')->nullable();
            $table->unsignedDecimal('consumed_quantity', $prec, $scale);
            $table->unsignedDecimal('extended_cost', $prec, $scale)->nullable();

            $table->unique(['resource_id', 'usage_date', 'meter_id']);
            $table->foreign('resource_id')->references('source_id')->on('azure_csp_resources');
            $table->foreign('meter_id')->references('source_id')->on('azure_meters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_csp_resource_usages');
    }
}
