<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureCspResourceGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_csp_resource_groups', function(Blueprint $table) {
            $table->unsignedInteger('source_id');
            $table->unsignedInteger('subscription_id');
            $table->string('name')->nullable();

            $table->unique('source_id');
            $table->foreign('subscription_id')->references('source_id')->on('azure_subscriptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_csp_resource_groups');
    }
}
