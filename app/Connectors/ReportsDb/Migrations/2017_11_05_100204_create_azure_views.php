<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureViews extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        if(!in_array(config('database.connections.reports.driver'), ['mssql', 'sqlsrv'])) {
            throw new \Exception('Only SQL Server supported for views');
        }
        $usage_view = <<<USAGEEND
CREATE VIEW CustomerUsage AS
SELECT 
    c.name AS CustomerName,
    c.source_id AS CustomerID,
    a.name AS AgreementID,
    a.type AS AgreementType,
    a.currency AS Currency,
    s.department AS DepartmentName,
    s.account_name AS AccountName,
    s.cost_center AS CostCentre,
    s.guid AS SubscriptionGUID,
    s.name AS SubscriptionName,
    m.category AS MeterCategory,
    m.sub_category AS MeterSubCategory,
    m.service AS Service,
    m.product AS Product,
    CASE s.is_msdn WHEN 0 THEN m.name ELSE m.msdn_name END AS MeterName,
    m.guid AS MeterGuid,
    m.unit_of_measure AS UnitOfMeasure,
    mr.display_name AS MeterRegionDisplayName,
    mr.sort_name AS MeterRegionName,
    r.instance_id AS InstanceId,
    rg.name AS ResourceGroup,
    r.instance_name AS InstanceName,
    r.source_id AS ResourceId,
    rr.display_name AS InstanceRegionDisplayName,
    rr.sort_name AS InstanceRegionName,
    u.usage_date AS [Date],
    SUM(u.consumed_quantity) AS ConsumedQuantity,
    SUM(u.extended_cost) AS ExtendedCost
FROM dbo.azure_csp_resource_usages AS u
INNER JOIN dbo.azure_csp_resources AS r ON u.resource_id = r.source_id
LEFT OUTER JOIN dbo.azure_csp_resource_groups rg ON r.resource_group_id = rg.source_id
INNER JOIN dbo.azure_regions AS rr ON r.region_id = rr.source_id
INNER JOIN dbo.azure_meters AS m ON u.meter_id = m.source_id
INNER JOIN dbo.azure_regions AS mr ON m.region_id = mr.source_id
INNER JOIN dbo.azure_subscriptions AS s ON r.subscription_id = s.source_id
INNER JOIN dbo.azure_agreements AS a ON s.agreement_id = a.source_id
INNER JOIN dbo.customers AS c ON a.customer_id = c.source_id
WHERE u.extended_cost IS NOT NULL
GROUP BY c.name, c.source_id, a.name, a.type, a.currency, s.department, s.account_name, s.cost_center, s.guid, s.name, 
    m.category, m.sub_category, m.service, m.product, s.is_msdn, m.name, m.msdn_name, m.guid, m.unit_of_measure, mr.display_name, 
    mr.sort_name, r.instance_id, rg.name, r.instance_name, r.source_id, rr.display_name, rr.sort_name, u.usage_date
;
USAGEEND;
        $tag_view = <<<TAGEND
CREATE VIEW CustomerTags AS
SELECT
    c.name AS CustoemrName,
    rt.resource_id,
    t.name,
    t.value,
    t.combined
FROM dbo.azure_csp_resource_tags AS rt
INNER JOIN dbo.azure_tags AS t ON rt.tag_id=t.id
INNER JOIN dbo.azure_csp_resources AS r ON rt.resource_id = r.source_id
INNER JOIN dbo.azure_subscriptions AS s ON r.subscription_id = s.source_id
INNER JOIN dbo.azure_agreements AS a ON s.agreement_id = a.source_id
INNER JOIN dbo.customers AS c ON a.customer_id = c.source_id
;
TAGEND;
        \Illuminate\Support\Facades\DB::statement($usage_view);
        \Illuminate\Support\Facades\DB::statement($tag_view);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(['CustomerTags', 'CustomerUsage'] as $viewName) {
            \Illuminate\Support\Facades\DB::statement('DROP VIEW dbo.'.$viewName.';');
        }
    }
}
