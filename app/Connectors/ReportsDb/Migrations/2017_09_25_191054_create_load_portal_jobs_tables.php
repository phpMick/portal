<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadPortalJobsTables extends Migration
{

    protected $records = ['users', 'customers', 'customer_users'];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->records as $r) {
            Schema::create('load_portal_'.$r.'_jobs', function(Blueprint $table) {
                $table->increments('id');
                $table->dateTime('start_time');
                $table->dateTime('end_time')->nullable();
                $table->unsignedBigInteger('job_id')->nullable();
                $table->integer('processed_records')->nullable();
                $table->integer('created_records')->nullable();
                $table->integer('updated_records')->nullable();
                $table->integer('deleted_records')->nullable();
                $table->text('error_message')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = array_reverse($this->records);
        foreach($tables as $t) {
            Schema::dropIfExists('load_portal_'.$t.'_jobs');
        }

    }
}
