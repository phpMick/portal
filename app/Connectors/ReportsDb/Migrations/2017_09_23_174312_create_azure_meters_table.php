<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_meters', function(Blueprint $table) {
            $table->unsignedInteger('source_id');
            $table->string('guid');
            $table->string('name');
            $table->string('category');
            $table->string('sub_category');
            $table->string('product')->nullable();
            $table->string('service')->nullable();
            $table->unsignedInteger('region_id');
            $table->string('unit_of_measure');
            $table->string('msdn_name')->nullable();

            $table->unique('source_id');
            $table->foreign('region_id')->references('source_id')->on('azure_regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_meters');
    }
}
