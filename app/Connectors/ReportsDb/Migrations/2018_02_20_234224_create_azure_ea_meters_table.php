<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureEaMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prec = config('app.decimal_normal.precision');
        $scale = config('app.decimal_normal.scale');

        Schema::create('azure_ea_meters', function (Blueprint $table) use($prec, $scale) {
            $table->unsignedBigInteger('source_id');
            $table->string('sku');
            $table->string('guid')->nullable();
            $table->string('name');
            $table->string('unit_of_measure');
            $table->decimal('price_divisor', $prec, $scale)->nullable();
            $table->string('unit')->nullable();

            $table->unique('source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_ea_meters');
    }
}
