<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureCspResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_csp_resources', function(Blueprint $table) {
            $table->unsignedBigInteger('source_id');
            $table->unsignedInteger('subscription_id');
            $table->string('instance_id', 2047)->nullable();
            $table->unsignedInteger('resource_group_id')->nullable();
            $table->string('instance_name')->nullable();
            $table->unsignedInteger('region_id')->nullable();

            //$table->index('instance_id');
            //$table->index('resource_group');

            $table->unique('source_id');
            $table->foreign('subscription_id')->references('source_id')->on('azure_subscriptions');
            $table->foreign('resource_group_id')->references('source_id')->on('azure_csp_resource_groups');
            $table->foreign('region_id')->references('source_id')->on('azure_regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_csp_resources');
    }
}
