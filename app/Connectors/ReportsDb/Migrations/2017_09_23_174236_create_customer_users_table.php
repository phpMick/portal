<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_users', function(Blueprint $table) {
            $table->unsignedInteger('source_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('customer_id');

            $table->unique(['user_id', 'customer_id']);

            $table->unique('source_id');
            $table->foreign('customer_id')->references('source_id')->on('customers');
            $table->foreign('user_id')->references('source_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_users');
    }
}
