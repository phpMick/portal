<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureEaPricesheetView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $price_view = <<<PRICEEND
CREATE VIEW CustomerPrices AS
SELECT
    c.name AS CustomerName,
    a.currency AS Currency,
    m.guid AS MeterGuid,
    m.sku AS PartNumber,
    m.unit AS Units,
    p.unit_price AS UnitPrice,
    m.unit_of_measure AS PricingQty,
    p.price AS QtyPrice
FROM dbo.azure_ea_prices p
INNER JOIN dbo.azure_ea_meters m ON p.ea_meter_id=m.source_id
INNER JOIN dbo.azure_agreements AS a ON p.agreement_id = a.source_id
INNER JOIN dbo.customers AS c ON a.customer_id = c.source_id
;
PRICEEND;

        \Illuminate\Support\Facades\DB::statement($price_view);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement('DROP VIEW dbo.CustomerPrices;');
    }
}
