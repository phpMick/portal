<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_agreements', function(Blueprint $table) {
            $table->unsignedInteger('source_id');
            $table->unsignedInteger('customer_id');
            $table->string('type', 31)->nullable();
            $table->char('currency', 3)->nullable();
            $table->string('name')->nullable();

            $table->foreign('customer_id')->references('source_id')->on('customers');
            $table->unique('source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_agreements');
    }
}
