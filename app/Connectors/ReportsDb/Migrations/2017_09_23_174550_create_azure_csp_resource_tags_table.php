<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAzureCspResourceTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azure_csp_resource_tags', function(Blueprint $table) {
            $table->unsignedBigInteger('resource_id');
            $table->unsignedBigInteger('tag_id');

            $table->foreign('resource_id')->references('source_id')->on('azure_csp_resources');
            $table->foreign('tag_id')->references('id')->on('azure_tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azure_csp_resource_tags');
    }
}
