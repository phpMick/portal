<?php

namespace App\Connectors\ReportsDb\Commands;

use Illuminate\Console\Command;

class Migrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:migrate {action?} {--force} {--pretend} {--step} {--seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform database migration commands on the reports database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = strtolower($this->argument('action'));
        if(empty($action)) {
            $action = 'migrate';
        }
        $allowed = ['migrate', 'fresh', 'install', 'refresh', 'reset', 'rollback', 'status'];
        if(!in_array($action, $allowed)) {
            $this->error('Action not allowed. Valid options are: ' . implode(', ', $allowed));
            return FALSE;
        }
        if($action!='migrate') {
            $action = 'migrate:'.$action;
        }
        $parameters = ['--database'=>'reports', '--path'=>'app/Connectors/ReportsDb/Migrations'];
        foreach(['force', 'pretend', 'step', 'seed'] as $opt) {
            if($this->hasOption($opt) AND $this->option($opt)) {
                $parameters['--'.$opt] = TRUE;
            }
        }
        $this->call($action, $parameters);
    }
}
