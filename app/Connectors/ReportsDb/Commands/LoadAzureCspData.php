<?php

namespace App\Connectors\ReportsDb\Commands;

use App\Connectors\ReportsDb\Jobs\UpdateAzureCspResourceGroupsJob;
use App\Connectors\ReportsDb\Jobs\UpdateAzureCspResourcesJob;
use App\Connectors\ReportsDb\Jobs\UpdateAzureCspResourceUsagesJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class LoadAzureCspData extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:load-azure-csp {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load CSP Azure data into the reports database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        // need to copy data across in the correct order to ensure foreign keys are obeyed
        // ORDER: resource_groups, resources, resource_usages
        $rg_job = new UpdateAzureCspResourceGroupsJob((bool) $this->option('reset'));
        $rg_job->dispatchChildJobs = TRUE;
        $rg_job->cmd = $this;
        $this->dispatchNow($rg_job);

        $r_job = new UpdateAzureCspResourcesJob((bool) $this->option('reset'));
        $r_job->dispatchChildJobs = TRUE;
        $r_job->cmd = $this;
        $this->dispatchNow($r_job);

        $u_job = new UpdateAzureCspResourceUsagesJob();
        $u_job->dispatchChildJobs = TRUE;
        $u_job->cmd = $this;
        $this->dispatchNow($u_job);

    }
}
