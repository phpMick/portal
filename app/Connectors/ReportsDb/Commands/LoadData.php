<?php

namespace App\Connectors\ReportsDb\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class LoadData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:load {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load data into the reports database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if($this->option('reset')) {
            $args = ['--reset'];
        } else {
            $args = [];
        }

        $this->call('reports:load-core', $args);
        $this->call('reports:load-azure-core', $args);
        $this->call('reports:load-azure-csp', $args);

    }
}
