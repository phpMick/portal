<?php

namespace App\Connectors\ReportsDb\Commands;

use App\Connectors\ReportsDb\Jobs\UpdateAzureCoreDataJob;
use App\Connectors\ReportsDb\Jobs\UpdateAzureMetersJob;
use App\Connectors\ReportsDb\Jobs\UpdateAzureRegionsJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class LoadAzureCoreData extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:load-azure-core {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load core Azure data into the reports database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $job = new UpdateAzureRegionsJob((bool) $this->option('reset'));
        $job->dispatchChildJobs = TRUE;
        $job->cmd = $this;
        $this->dispatchNow($job);

        $job = new UpdateAzureMetersJob((bool) $this->option('reset'));
        $job->dispatchChildJobs = TRUE;
        $job->cmd = $this;
        $this->dispatchNow($job);
    }
}
