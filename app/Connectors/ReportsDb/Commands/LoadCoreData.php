<?php

namespace App\Connectors\ReportsDb\Commands;

use App\Connectors\ReportsDb\Jobs\UpdateAzureAgreementsJob;
use App\Connectors\ReportsDb\Jobs\UpdateAzureSubscriptionsJob;
use App\Connectors\ReportsDb\Jobs\UpdateCustomersJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class LoadCoreData extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:load-core {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load core data into the reports database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $job = new UpdateCustomersJob((bool) $this->option('reset'));
        $job->dispatchChildJobs = TRUE;
        $job->cmd = $this;
        $this->dispatchNow($job);

        $job = new UpdateAzureAgreementsJob((bool) $this->option('reset'));
        $job->dispatchChildJobs = TRUE;
        $job->cmd = $this;
        $this->dispatchNow($job);

        $job = new UpdateAzureSubscriptionsJob((bool) $this->option('reset'));
        $job->dispatchChildJobs = TRUE;
        $job->cmd = $this;
        $this->dispatchNow($job);
    }
}
