<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsSubscription
 * @package App\Connectors\ReportsDb\Models
 * @property integer $agreement_id
 * @property string $name
 * @property string $guid
 * @property string $offer_code
 * @property string $department
 * @property string $cost_center
 * @property string $account_name
 * @property string $account_owner
 * @property boolean $is_msdn
 * @property ReportsAzureAgreement $agreement
 */
class ReportsAzureSubscription extends ModelReport {

    protected $table = 'azure_subscriptions';

    protected $casts = [
        'is_msdn'=>'boolean'
    ];

    public function agreement() {
        return $this->belongsTo(ReportsAzureAgreement::class, 'agreement_id', 'id');
    }

    public function resources() {
        switch($this->offer_code) {
            case '0145P': return $this->hasMany(ReportsAzureCspResource::class, 'subscription_id', 'id');

            default: return $this->hasMany(ReportsAzureCspResource::class, 'subscription_id', 'id');
        }
    }

    /**
     * @param string $guid
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByGuid($guid) {
        return self::query()->where('guid', '=', $guid)->first();
    }

}
