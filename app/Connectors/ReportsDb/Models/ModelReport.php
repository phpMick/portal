<?php

namespace App\Connectors\ReportsDb\Models;

/**
 * Class ReportModel
 * @package App\Connectors\ReportsDb\Models
 * @property integer $source_id
 */
class ModelReport extends Model
{

    public $timestamps = FALSE;

//    const SOURCE_UNKNOWN = 0;
//    const SOURCE_PORTAL = 10;
//    const SOURCE_AZURE_CORE = 20;
//    const SOURCE_AZURE_EA = 21;
//    const SOURCE_AZURE_CSP = 25;
//    const SOURCE_AZURE_PAYG = 28;

    protected $guarded = [];

    protected $primaryKey = 'source_id';

    public $incrementing = FALSE;

    /**
     * @param integer $source_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findBySource($source_id) {
        return self::query()->where('source_id', '=', $source_id)->first();
    }

}