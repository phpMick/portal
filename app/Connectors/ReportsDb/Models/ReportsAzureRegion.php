<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsRegion
 * @package App\Connectors\ReportsDb\Models
 * @property string $display_name
 *
 */
class ReportsAzureRegion extends ModelReport
{

    protected $table = 'azure_regions';

}
