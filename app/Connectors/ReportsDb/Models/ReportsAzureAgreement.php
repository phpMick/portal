<?php

namespace App\Connectors\ReportsDb\Models;

use Illuminate\Support\Collection;


/**
 * Class ReportsAgreement
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property string $type
 * @property string $currency
 * @property ReportsCustomer $customer
 * @property Collection|ReportsAzureSubscription[] $subscriptions
 */
class ReportsAzureAgreement extends ModelReport {

    protected $table = 'azure_agreements';

    public function customer() {
        return $this->belongsTo(ReportsCustomer::class, 'customer_id', 'id');
    }

    public function subscriptions() {
        $this->hasMany(ReportsAzureSubscription::class, 'agreement_id', 'id');
    }

}
