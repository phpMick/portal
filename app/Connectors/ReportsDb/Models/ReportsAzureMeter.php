<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsMeter
 * @package App\Connectors\ReportsDb\Models
 * @property string $guid
 * @property string $name
 * @property string $category
 * @property string $sub_category
 * @property string $product
 * @property string $service
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property string $msdn_name
 */
class ReportsAzureMeter extends ModelReport
{

    protected $table = 'azure_meters';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo(ReportsAzureRegion::class, 'region_id');
    }

}
