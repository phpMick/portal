<?php


namespace App\Connectors\ReportsDb\Models;


/**
 * Class LoadAzureCoreDataJob
 * @package App\Connectors\ReportsDb\Models
 * @property integer $created_records
 * @property integer $updated_records
 * @property integer $deleted_records
 */
class LoadAzureDataJob extends ModelJob
{

    public function __construct(array $attributes = [], $tableName=NULL)
    {
        $this->table = $tableName;
        parent::__construct($attributes);
    }

}