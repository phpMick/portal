<?php


namespace App\Connectors\ReportsDb\Models;

use Carbon\Carbon;

/**
 * Class LoadJob
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon $end_time
 * @property integer $job_id
 * @property integer $processed_records
 * @property string $error_message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ModelJob extends Model
{

    protected $dates = [
        'start_time', 'end_time'
    ];

}