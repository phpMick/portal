<?php


namespace App\Connectors\ReportsDb\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;


class Model extends EloquentModel
{

    protected $connection = 'reports';


}