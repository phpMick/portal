<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsAzureCspResourceUsage
 * @package App\Connectors\ReportsDb\Models
 * @property integer $resource_id
 * @property \Carbon\Carbon $usage_date
 * @property integer $meter_id
 * @property string $consumed_quantity
 * @property string $extended_cost
 * @property ReportsAzureCspResource $resource
 * @property ReportsAzureMeter $meter
 */
class ReportsAzureCspResourceUsage extends Model {

    protected $table = 'azure_csp_resource_usages';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = FALSE;

    public function resource() {
        return $this->belongsTo(ReportsAzureCspResource::class, 'resource_id', 'source_id');
    }

    public function meter() {
        return $this->belongsTo(ReportsAzureMeter::class, 'meter_id', 'source_id');
    }

    /**
     * @param integer $resource_id
     * @param string $usage_date
     * @param integer $meter_id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByKey($resource_id, $usage_date, $meter_id) {
        return self::query()
            ->where('resource_id', '=', $resource_id)
            ->where('usage_date', '=', $usage_date)
            ->where('meter_id', '=', $meter_id)
            ->first();
    }

}
