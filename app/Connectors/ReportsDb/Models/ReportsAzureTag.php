<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsAzureTag
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $combined
 */
class ReportsAzureTag extends Model {

    protected $table = 'azure_tags';

    public $timestamps = FALSE;

    /**
     * @param string $tagName
     * @param string $tagValue
     * @return null|string
     */
    static public function getCombinedValue($tagName, $tagValue) {
        if(empty($tagName) OR empty($tagValue)) {
            return NULL;
        }
        return $tagName.':'.$tagValue;
    }

    /**
     * @param string $tagName
     * @param string $tagValue
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByCombined($tagName, $tagValue) {
        $combined = self::getCombinedValue($tagName, $tagValue);
        if(is_null($combined)) {
            return NULL;
        }
        return self::query()->where('combined', '=', $combined)->first();
    }

}
