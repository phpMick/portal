<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsAzureCspResource
 * @package App\Connectors\ReportsDb\Models
 * @property integer $subscription_id
 * @property string $instance_id
 * @property integer $resource_group_id
 * @property string $instance_name
 * @property integer $region_id
 * @property ReportsAzureSubscription $subscription
 * @property ReportsAzureCspResourceGroup $resourceGroup
 * @property ReportsAzureRegion $region
 */
class ReportsAzureCspResource extends ModelReport {

    protected $table = 'azure_csp_resources';

    public function subscription() {
        return $this->belongsTo(ReportsAzureSubscription::class, 'subscription_id', 'source_id');
    }

    public function resourceGroup() {
        return $this->belongsTo(ReportsAzureCspResourceGroup::class, 'resource_group_id', 'source_id');
    }

    public function region() {
        return $this->belongsTo(ReportsAzureRegion::class, 'region_id', 'source_id');
    }

    public function tags() {
        return $this->belongsToMany(ReportsAzureTag::class, 'azure_csp_resource_tags', 'resource_id', 'tag_id');
    }

    public function usage() {
        return $this->hasMany(ReportsAzureCspResourceUsage::class, 'resource_id', 'source_id');
    }

}
