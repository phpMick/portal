<?php

namespace App\Connectors\ReportsDb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsUser
 * @package App\Connectors\ReportsDb\Models
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property integer $portal_user_id
 */
class ReportsUser extends Model {

    protected $connection = 'reports';
    protected $table = 'users';


    public function customers() {
        return $this->belongsToMany(ReportsCustomer::class, 'customer_users', 'user_id', 'customer_id');
    }

}
