<?php

namespace App\Connectors\ReportsDb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsCustomer
 * @package App\Connectors\ReportsDb\Models
 * @property string $name
 */
class ReportsCustomer extends ModelReport
{

    protected $table = 'customers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany(ReportsUser::class, 'customer_users', 'customer_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agreements() {
        return $this->hasMany(ReportsAzureAgreement::class, 'customer_id', 'id');
    }

}
