<?php

namespace App\Connectors\ReportsDb\Models;


class LoadAzureDataResult
{
    public $created = 0;
    public $updated = 0;
    public $deleted = 0;
    public $processed = 0;
    public $errorMessage = NULL;

}