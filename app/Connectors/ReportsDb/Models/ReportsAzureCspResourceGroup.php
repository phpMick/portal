<?php

namespace App\Connectors\ReportsDb\Models;


/**
 * Class ReportsAzureCspResourceGroup
 * @package App\Connectors\ReportsDb\Models
 * @property integer $subscription_id
 * @property string $name
 */
class ReportsAzureCspResourceGroup extends ModelReport {

    protected $table = 'azure_csp_resource_groups';

    public function subscription() {
        return $this->belongsTo(ReportsAzureSubscription::class, 'subscription_id', 'source_id');
    }

    public function resources() {
        return $this->hasMany(ReportsAzureCspResource::class, 'resource_group_id', 'source_id');
    }

}
