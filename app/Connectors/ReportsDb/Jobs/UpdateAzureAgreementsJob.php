<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsAzureAgreement;
use App\Connectors\ReportsDb\Models\ReportsCustomer;
use App\Models\Agreement;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureAgreementsJob extends BaseJob
{

    protected $_table = 'azure_agreements';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     * @throws \Exception
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $query = Agreement::query()
            ->whereIn('type', Agreement::TYPE_AZURE)
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());
        $db1 = DB::connection('portal')->getPdo();
        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());

        while($row = $statement->fetch()) {
            /** @var Agreement $agreement */
            $agreement = new Agreement();
            $agreement->forceFill($row);
            // find existing row in reports db:
            $reportsAgreement = ReportsAzureAgreement::find($agreement->id);
            if(is_null($reportsAgreement)) {
                // create it!
                $reportsAgreement = new ReportsAzureAgreement();
                $reportsAgreement->source_id = $agreement->id;
            }
            // fill in data fields:
//            $reportsCustomer = ReportsCustomer::find($agreement->customer_id);
            $reportsAgreement->customer_id = $agreement->customer_id;
            $reportsAgreement->type = $agreement->getTypeText();
            $reportsAgreement->currency = $agreement->currency ?: 'GBP';
            $reportsAgreement->name = $agreement->name;

            // save if anything has changed, and record result:
            if($reportsAgreement->isDirty()) {
                $exists = $reportsAgreement->exists;
                $reportsAgreement->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }


}
