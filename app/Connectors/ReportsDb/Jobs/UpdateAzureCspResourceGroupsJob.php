<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\AzureDb\Models\CspResource;
use App\Connectors\AzureDb\Models\CspResourceGroup;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsAzureAgreement;
use App\Connectors\ReportsDb\Models\ReportsAzureCspResource;
use App\Connectors\ReportsDb\Models\ReportsAzureCspResourceGroup;
use App\Connectors\ReportsDb\Models\ReportsAzureSubscription;
use App\Models\Microsoft\Azure\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureCspResourceGroupsJob extends BaseJob
{

    protected $_table = 'azure_csp_resource_groups';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $query = CspResourceGroup::query()
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());

        $db1 = DB::connection('azure')->getPdo();
        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());

        while($row = $statement->fetch()) {
            /** @var CspResourceGroup $resourceGroup */
            $resourceGroup = new CspResourceGroup();
            $resourceGroup->forceFill($row);
            // find existing row in reports db:
            $reportsResourceGroup = ReportsAzureCspResourceGroup::find($resourceGroup->id);
            if(is_null($reportsResourceGroup)) {
                // create it!
                $reportsResourceGroup = new ReportsAzureCspResourceGroup();
                $reportsResourceGroup->source_id = $resourceGroup->id;
            }
            // fill in data fields:
            $reportsResourceGroup->subscription_id = $resourceGroup->subscription_id;
            $reportsResourceGroup->name = $resourceGroup->name;

            // save if anything has changed, and record result:
            if($reportsResourceGroup->isDirty()) {
                $exists = $reportsResourceGroup->exists;
                $reportsResourceGroup->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }


}
