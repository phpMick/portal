<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\AzureDb\Models\Meter;
use App\Connectors\ReportsDb\Models\ReportsAzureMeter;
use App\Connectors\ReportsDb\Models\ReportsAzureRegion;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureMetersJob extends BaseJob
{

    protected $_table = 'azure_meters';


    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        // SHOULD BE RETIRED: this takes way too long to run.
        // run-time to create 11375 records = 32 minutes!

        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        // get all regions that have been updated since the last run:
        $query = Meter::query()
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());
        $db1 = DB::connection('azure')->getPdo();
        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());


        while($row = $statement->fetch()) {
            $meter = new Meter();
            $meter->forceFill($row);

            // find existing row in reports db:
            $reportsMeter = ReportsAzureMeter::find($meter->id);
            if(is_null($reportsMeter)) {
                // create it!
                $reportsMeter = new ReportsAzureMeter();
                $reportsMeter->source_id = $meter->id;
            }
            // fill in data fields:
            $reportsMeter->guid = $meter->guid;
            $reportsMeter->name = $meter->name;
            $reportsMeter->category = $meter->category;
            $reportsMeter->sub_category = $meter->sub_category;
            $reportsMeter->product = $meter->product;
            $reportsMeter->service = $meter->service;
            $reportsMeter->msdn_name = $meter->msdn_name;
            $reportsMeter->unit_of_measure = $meter->unit_of_measure;
//            $rep_region = ReportsAzureRegion::find($meter->region_id);
            $reportsMeter->region_id = $meter->region_id;
            // save if anything has changed, and record result:
            if($reportsMeter->isDirty()) {
                $exists = $reportsMeter->exists;
                $reportsMeter->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }

}
