<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsCustomer;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateCustomersJob extends BaseJob
{

    protected $_table = 'portal_customers';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $db1 = DB::connection('portal')->getPdo();
        $query = Customer::query()
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());

        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());

        while($row = $statement->fetch()) {
            /** @var Customer $customer */
            $customer = new Customer();
            $customer->forceFill($row);
            // find existing row in reports db:
            $reportsCustomer = ReportsCustomer::find($customer->id);
            if(is_null($reportsCustomer)) {
                // create it!
                $reportsCustomer = new ReportsCustomer();
                $reportsCustomer->source_id = $customer->id;
            }
            // fill in data fields:
            $reportsCustomer->name = $customer->name;

            // save if anything has changed, and record result:
            if($reportsCustomer->isDirty()) {
                $exists = $reportsCustomer->exists;
                $reportsCustomer->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }


}
