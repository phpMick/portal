<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\Common\Job;
use App\Connectors\ReportsDb\Models\LoadAzureDataJob;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

abstract class BaseJob extends Job
{

    protected $_table = '';

    protected $_refresh_all;

    /**
     * Create a new job instance.
     *
     * @param bool $refresh_all
     */
    public function __construct($refresh_all=FALSE)
    {
        $this->_refresh_all = $refresh_all;
    }

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    abstract protected function process(Carbon $last_success_time);


    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        assert('!empty($this->_table)', 'Table must be defined on sub-classes');
        optional($this->cmd)->info('Updating Azure data: '.$this->_table);
        $tableName = 'load_'.$this->_table.'_jobs';
        // create a record of this call:
        $job = new LoadAzureDataJob([], $tableName);
        $job->job_id = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->save();

        // get last success time:
        $timeStr = DB::connection('reports')
            ->table($tableName)
            ->select(DB::raw('MAX(start_time) as start_time'))
            ->whereNotNull('end_time')
            ->whereNull('error_message')
            ->first();
        if(is_null($timeStr)) {
            $startTime = new Carbon('2000-01-01', 'UTC');
        } elseif(is_string($timeStr)) {
            $startTime = new Carbon($timeStr, 'UTC');
        } elseif(is_array($timeStr) AND array_key_exists('start_time', $timeStr)) {
            $startTime = new Carbon($timeStr['start_time'], 'UTC');
        } elseif(is_object($timeStr)) {
            if(isset($timeStr->start_time)) {
                $startTime = new Carbon($timeStr->start_time, 'UTC');
            } else {
                $startTime = new Carbon('2000-01-01', 'UTC');
            }
        } else {
            throw new \Exception('Invalid response from database query');
        }

        $exception = NULL;
        try {
            // call subclass process function:
            $result = $this->process($startTime);
            if($result instanceof LoadAzureDataResult) {
                $job->error_message = $result->errorMessage;
                $job->created_records = $result->created;
                $job->updated_records = $result->updated;
                $job->deleted_records = $result->deleted;
                $job->processed_records = $result->processed;
            } elseif(is_bool($result)) {
                if(!$result) {
                    $job->error_message = 'Unknown error occurred';
                }
            } elseif(is_string($result)) {
                $job->error_message = $result;
            } else {
                $job->error_message = 'ERROR - no result returned';
            }
        } catch (\Exception $e) {
            $job->error_message = 'EXCEPTION {'.get_class($e).'} on line '.$e->getLine().' in "'.$e->getFile().'": '.$e->getMessage();
            $exception = $e;
        }
        $job->end_time = new Carbon();
        $job->save();
        if(!is_null($exception)) {
            throw $exception;
        }
    }
}
