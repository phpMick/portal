<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\AzureDb\Models\CspResource;
use App\Connectors\AzureDb\Models\CspUsage;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsAzureAgreement;
use App\Connectors\ReportsDb\Models\ReportsAzureCspResource;
use App\Connectors\ReportsDb\Models\ReportsAzureCspResourceUsage;
use App\Connectors\ReportsDb\Models\ReportsAzureTag;
use App\Connectors\ReportsDb\Models\ReportsAzureSubscription;
use App\Models\Microsoft\Azure\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureCspResourceUsagesJob extends BaseJob
{

    protected $_table = 'azure_csp_resource_usages';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $select_count = 'SELECT COUNT(DISTINCT u.resource_id, u.usage_date, m.id) ';
        $select_data = 'SELECT u.resource_id, u.usage_date, m.meter_id, SUM(u.quantity) AS quantity, SUM(u.estimate_price) AS price ';
        $group_data = ' GROUP BY u.resource_id, u.usage_date, m.meter_id';

        $usage_sql = <<<USAGE_QUERY
FROM csp_usages AS u
JOIN (
  SELECT DISTINCT resource_id, usage_date, csp_meter_id
  FROM csp_usages
  WHERE updated_at>=?
) AS s ON u.resource_id=s.resource_id AND u.usage_date=s.usage_date AND u.csp_meter_id=s.csp_meter_id
JOIN csp_meters AS m ON u.csp_meter_id=m.id
WHERE u.deleted=0
USAGE_QUERY;

        // first, get the number of rows we will be updating (so we can do a progress bar)...
        $db1 = DB::connection('azure')->getPdo();
        $statement = $db1->prepare($select_count . $usage_sql . ';');
        $statement->setFetchMode(\PDO::FETCH_NUM);
        $statement->execute([$last_success_time->toDateTimeString()]);

        $row = $statement->fetch();
        $rowcount = -1;
        if(is_array($row) AND count($row)==1) {
            $rowcount = intval($row[0]);
        }
        $statement->closeCursor();

        optional($this->cmd)->info(' > Usage records to process: '.$rowcount);
        if($rowcount>0) {
            optional(optional($this->cmd)->getOutput())->write(' >> ');
        }
        $i = 0;

        $statement = $db1->prepare($select_data . $usage_sql . $group_data . ';');
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute([$last_success_time->toDateTimeString()]);


        while($row = $statement->fetch()) {
            // no need to delete rows in target DB, since the matching criteria are the primary key (and therefore unique)
            $reportsUsage = ReportsAzureCspResourceUsage::findByKey($row['resource_id'], $row['usage_date'], $row['meter_id']);

            if(is_null($reportsUsage)) {
                // create new record:
                $reportsUsage = new ReportsAzureCspResourceUsage();
                $reportsUsage->resource_id = $row['resource_id'];
                $reportsUsage->usage_date = $row['usage_date'];
                $reportsUsage->meter_id = $row['meter_id'];
            }
            // fill in variable properties:
            $reportsUsage->consumed_quantity = $row['quantity'];
            $reportsUsage->extended_cost = $row['price'];

            // save if anything has changed, and record result:
            if($reportsUsage->isDirty()) {
                $exists = $reportsUsage->exists;
                if ($exists) {
                    ReportsAzureCspResourceUsage::where(['resource_id'=>$row['resource_id'], 'usage_date'=>$row['usage_date'], 'meter_id'=>$row['meter_id']])
                        ->update(['consumed_quantity'=>$reportsUsage->consumed_quantity, 'extended_cost'=>$reportsUsage->extended_cost]);
//                    $reportsUsage->save();
                    $result->updated++;
                } else {
                    $reportsUsage->save();
                    $result->created++;
                }
            }
            $result->processed++;
            if ($i % 500 == 0) {
                optional(optional($this->cmd)->getOutput())->write($i);
            } elseif ($i % 50 == 0) {
                optional(optional($this->cmd)->getOutput())->write('.');
            }
            $i++;
        }
        optional(optional($this->cmd)->getOutput())->write('', TRUE);
        $statement->closeCursor();
        return $result;
    }


}
