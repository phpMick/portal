<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsAzureAgreement;
use App\Connectors\ReportsDb\Models\ReportsAzureSubscription;
use App\Connectors\ReportsDb\Models\ReportsCustomer;
use App\Models\Microsoft\Azure\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureSubscriptionsJob extends BaseJob
{

    protected $_table = 'azure_subscriptions';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $query = Subscription::query()
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());

        $db1 = DB::connection('portal')->getPdo();
        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());

        while($row = $statement->fetch()) {
            /** @var Subscription $subscription */
            $subscription = new Subscription();
            $subscription->forceFill($row);
            // find existing row in reports db:
            $reportsSubscription = ReportsAzureSubscription::find($subscription->id);
            if(is_null($reportsSubscription)) {
                // create it!
                $reportsSubscription = new ReportsAzureSubscription();
                $reportsSubscription->source_id = $subscription->id;
            }

            // fill in data fields:
//            $reportsAgreement = ReportsAzureAgreement::find($subscription->agreement_id);
            $reportsSubscription->agreement_id = $subscription->agreement_id;
            if(!empty($subscription->name)) {
                $reportsSubscription->name = $subscription->name;
            } elseif(!empty($subscription->azure_name)) {
                $reportsSubscription->name = $subscription->azure_name;
            } else {
                $reportsSubscription->name = NULL;
            }
            $reportsSubscription->guid = $subscription->guid;
            $reportsSubscription->offer_code = $subscription->offer_code;
            $reportsSubscription->department = NULL;
            $reportsSubscription->cost_center = NULL;
            $reportsSubscription->account_name = NULL;
            $reportsSubscription->account_owner = NULL;
            $reportsSubscription->is_msdn = 0;

            // save if anything has changed, and record result:
            if($reportsSubscription->isDirty()) {
                $exists = $reportsSubscription->exists;
                $reportsSubscription->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }


}
