<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\AzureDb\Models\CspResource;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use App\Connectors\ReportsDb\Models\ReportsAzureAgreement;
use App\Connectors\ReportsDb\Models\ReportsAzureCspResource;
use App\Connectors\ReportsDb\Models\ReportsAzureTag;
use App\Connectors\ReportsDb\Models\ReportsAzureSubscription;
use App\Models\Microsoft\Azure\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateAzureCspResourcesJob extends BaseJob
{

    protected $_table = 'azure_csp_resources';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     * @throws \Exception
     */
    protected function process(Carbon $last_success_time)
    {
        $result = new LoadAzureDataResult();

        // use PDO to get data row-by-row (as we may have a large result set):
        $query = CspResource::query()
            ->where('updated_at', '>=', $last_success_time->toDateTimeString());

        $db1 = DB::connection('azure')->getPdo();
        $statement = $db1->prepare($query->toSql());
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute($query->getBindings());

        while($row = $statement->fetch()) {
            /** @var CspResource $resource */
            $resource = new CspResource();
            $resource->forceFill($row);
            // find existing row in reports db:
            $reportsResource = ReportsAzureCspResource::find($resource->id);
            if(is_null($reportsResource)) {
                // create it!
                $reportsResource = new ReportsAzureCspResource();
                $reportsResource->source_id = $resource->id;
            }
            // fill in data fields:
            // need to find subscription by GUID, since subscription table is shared across CSP and others
            $subguid = $resource->subscription->guid;
            $reportsSubscription = ReportsAzureSubscription::findByGuid($subguid);
            if(is_null($reportsSubscription)) {
                throw new \Exception('Subscription {'.$subguid.'} not found in reports DB');
            }
            $reportsResource->subscription_id = $reportsSubscription->source_id;
            $reportsResource->instance_id = $resource->uri;
            $reportsResource->resource_group_id = $resource->resource_group_id;
            $reportsResource->instance_name = $resource->name;
            $reportsResource->region_id = $resource->region_id;

            // save if anything has changed, and record result:
            if($reportsResource->isDirty()) {
                $exists = $reportsResource->exists;
                $reportsResource->save();
                if($exists) {
                    $result->updated++;
                    // note: tags will be the same - a new resource is created if the tags are changed in any way
                } else {
                    $result->created++;
                    // new resource has been created. We need to create the associated tags
                    foreach($resource->tagArray() as $name=>$value) {
                        // try to find the tag:
                        $reportsTag = ReportsAzureTag::findByCombined($name, $value);
                        if(is_null($reportsTag)) {
                            $reportsTag = new ReportsAzureTag();
                            $reportsTag->name = $name;
                            $reportsTag->value = $value;
                            $reportsTag->combined = ReportsAzureTag::getCombinedValue($name, $value);
                            $reportsTag->save();
                        }
                        $reportsResource->tags()->save($reportsTag);
                    }
                }
            }
            $result->processed++;
        }
        return $result;
    }


}
