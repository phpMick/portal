<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\AzureDb\Models\Region;
use App\Connectors\ReportsDb\Models\ReportsAzureRegion;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use Carbon\Carbon;

class UpdateAzureRegionsJob extends BaseJob
{

    protected $_table = 'azure_regions';

    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     */
    protected function process(Carbon $last_success_time)
    {
        // get all regions that have been updated since the last run:
        $rows = Region::where('updated_at', '>=', $last_success_time)->get();
        $result = new LoadAzureDataResult();

        foreach($rows as $row) {
            // find existing row in reports db:
            $reprow = ReportsAzureRegion::find($row->id);
            if(is_null($reprow)) {
                // create it!
                $reprow = new ReportsAzureRegion();
                $reprow->source_id = $row->id;
            }
            // fill in data fields:
            $reprow->display_name = $row->display_name;
            // save if anything has changed, and record result:
            if($reprow->isDirty()) {
                $exists = $reprow->exists;
                $reprow->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
            $result->processed++;
        }
        return $result;
    }
}
