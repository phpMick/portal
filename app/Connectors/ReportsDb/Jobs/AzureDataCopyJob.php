<?php

namespace App\Connectors\ReportsDb\Jobs;


use App\Connectors\Common\Job;
use App\Connectors\ReportsDb\Models\LoadAzureDataJob;
use App\Connectors\ReportsDb\Models\LoadAzureDataResult;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

abstract class AzureDataCopyJob extends Job
{

    protected $_refresh_all;

    /**
     * Create a new job instance.
     *
     * @param bool $refresh_all
     * @throws \Exception
     */
    public function __construct($refresh_all=FALSE)
    {
        throw new \Exception('deprecated');
        $this->_refresh_all = $refresh_all;
    }


    abstract protected function getJobTableName();
    abstract protected function getSourceTableQuery();
    abstract protected function getDestinationClassName();
    abstract protected function getDestinationLookupColumns();
    abstract protected function getColumnMap();


    /**
     * @param Carbon $last_success_time
     * @return LoadAzureDataResult
     * @throws \Exception
     */
    protected function process(Carbon $last_success_time) {
        $result = new LoadAzureDataResult();

        $sourceQuery = $this->getSourceTableQuery();
        assert('is_string($sourceQuery) AND !empty($sourceQuery)', 'Source query must be returned from subclass');
        assert('strtoupper(substr($sourceQuery, 0, 6))==\'SELECT\'', 'Source query must be a SELECT statement');
        $destinationClass = $this->getDestinationClassName();
        assert('is_string($destinationClass) AND !empty($destinationClass)', 'Destination class name must be returned from subclass');
        $columnMap = $this->getColumnMap();
        assert('is_array($columnMap) AND count($columnMap)>0', 'Column map must be returned from subclass');
        $lookupColumns = $this->getDestinationLookupColumns();
        assert('is_array($lookupColumns) AND count($lookupColumns)>0', 'Lookup columns must be specified in subclass');

        // construct source query using PDO, since that way we can buffer rows rather than load everything into RAM:
        $db1 = DB::connection('azure')->getPdo();
        $statement = $db1->prepare($sourceQuery);
        switch(substr_count($sourceQuery, '?')) {
            case 0:  $statement->execute(); break;
            case 1:  $statement->execute([$last_success_time->toDateTimeString()]); break;
            default: throw new \Exception('Too many parameters specified in source query');
        }

        // iterate over result rows:
        while($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            // construct destination lookup query:
            /** @var Builder $query */
            $query = $destinationClass::query();
            foreach($lookupColumns as $column=>$sourceProp) {
                $query->where($column, '=', $row[$sourceProp]);
            }
            switch($query->count()) {
                case 0: $rep_row = NULL; break;
                case 1: $rep_row = $query->first(); break;
                default: throw new \Exception('Too many matching rows found in destination table');
            }
            if(is_null($rep_row)) {
                $rep_row = new $destinationClass();
                foreach($lookupColumns as $column=>$sourceProp) {
                    $rep_row->$column = $row[$sourceProp];
                }
            }
            // copy all other data:
            foreach($columnMap as $sourceProp=>$column) {
                $rep_row->$column = $row[$sourceProp];
            }
            if($rep_row->isDirty()) {
                $exists = $rep_row->exists;
                $rep_row->save();
                if($exists) {
                    $result->updated++;
                } else {
                    $result->created++;
                }
            }
        }

        return $result;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $tableName = $this->getJobTableName();
        assert('!empty($tableName)', 'Table must be defined on sub-classes');
        optional($this->cmd)->info('Updating Azure data: '.$tableName);
        // create a record of this call:
        $job = new LoadAzureDataJob([], $tableName);
        $job->job_id = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->save();

        // get last success time:
        $timeStr = DB::connection('reports')
            ->table($tableName)
            ->select(DB::raw('MAX(start_time) as start_time'))
            ->whereNotNull('end_time')
            ->whereNull('error_message')
            ->first();
        if(is_null($timeStr)) {
            $startTime = new Carbon('2000-01-01', 'UTC');
        } elseif(is_string($timeStr)) {
            $startTime = new Carbon($timeStr, 'UTC');
        } elseif(is_array($timeStr) AND array_key_exists('start_time', $timeStr)) {
            $startTime = new Carbon($timeStr['start_time'], 'UTC');
        } elseif(is_object($timeStr)) {
            if(isset($timeStr->start_time)) {
                $startTime = new Carbon($timeStr->start_time, 'UTC');
            } else {
                $startTime = new Carbon('2000-01-01', 'UTC');
            }
        } else {
            throw new \Exception('Invalid response from database query');
        }

        try {
            // call subclass process function:
            $result = $this->process($startTime);
            if($result instanceof LoadAzureDataResult) {
                $job->error_message = $result->errorMessage;
                $job->created_records = $result->created;
                $job->updated_records = $result->updated;
                $job->deleted_records = $result->deleted;
            } elseif(is_bool($result)) {
                $job->error_message = 'Unknown error occurred';
            } elseif(is_string($result)) {
                $job->error_message = $result;
            } else {
                $job->error_message = 'ERROR - no result returned';
            }
        } catch (\Exception $e) {
            $job->error_message = 'EXCEPTION on line '.$e->getLine().' in "'.$e->getFile().'": '.$e->getMessage();
        }
        $job->end_time = new Carbon();
        $job->save();
    }
}
