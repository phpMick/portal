<?php

namespace App\Connectors\MicrosoftPartnerCenterApi;



use App\Connectors\Common\RestApi;
use App\Connectors\MicrosoftPartnerCenterApi\Models\AzureMeter;
use App\Connectors\MicrosoftPartnerCenterApi\Models\AzureUsageQuery;

class Billing extends MicrosoftPartnerCenterApiModule
{

    /**
     * @param string $customerTenantGuid
     * @param string $subscriptionGuid
     * @return AzureUsageQuery
     */
    public function azureUsage($customerTenantGuid, $subscriptionGuid) {
        return new AzureUsageQuery($this->_api, $customerTenantGuid, $subscriptionGuid);
    }

    /**
     * @param array $listProperties OUTPUT parameter - captures the returned currency, locale, isTaxIncluded properties
     * @param string $currency
     * @param string $region
     * @param null $loadDate
     * @return AzureMeter[]
     * @throws \Exception
     */
    public function azureRateCard(&$listProperties=NULL, $currency='GBP', $region='GB', $loadDate=NULL) {
        assert('strlen($currency)==3', 'Invalid currency');
        assert('strlen($region)==2', 'Invalid country code');
        $auditFilePath = 'prices/azure/'.$region.'_'.$currency.'_{$date}_{$time}';
        if(isset($loadDate)) {
            $loadDate = str_replace('-','',$loadDate);
            if(!preg_match('/^\d{8}$/', $loadDate)) {
                throw new \Exception('Invalid date format');
            }
            $auditFilePath = str_replace('{$date}', $loadDate, $auditFilePath);
            // if a date is specified to load from, then we really mean it - don't fall back to API:
            $this->_api->loadFromAuditStore(RestApi::AUDIT_LOAD_STRICT);
        }

        $url = '/v1/ratecards/azure?currency=' . urlencode($currency) . '&region=' . urlencode($region);
        $data = $this->_api->get(
            $url,
            ['audit'=>$auditFilePath]
        )->errorAsEmptyArray()->decodeResponseBody();

        if(is_array($listProperties)) {
            if(count($listProperties)!=0) {
                foreach(array_keys($listProperties) as $k=>$v) {
                    unset($listProperties[$k]);
                }
            }
            foreach(['currency'=>'currency', 'locale'=>'locale', 'isTaxIncluded'=>'tax_included'] as $prop=>$key) {
                if(array_key_exists($prop, $data)) {
                    $listProperties[$key] = $data[$prop];
                }
            }
            if(array_key_exists('offerTerms', $data) AND is_array($data['offerTerms'])) {
                if(array_key_exists('discount', $data['offerTerms'])) {
                    $listProperties['offer_terms'] = $data['offerTerms'];
                } elseif(count($data['offerTerms'])==1 AND is_array($data['offerTerms'][0]) AND array_key_exists('discount', $data['offerTerms'][0])) {
                    $listProperties['offer_terms'] = $data['offerTerms'][0];
                }
            }
        }
        return $this->_api->_normaliseListResponse(
            $data,
            AzureMeter::class,
            NULL,
            'meters'
        );
    }

    /**
     * @param $customerId
     * @param string $orderId
     * @return NULL|array
     * @throws \Exception
     */
    public function order($customerId, $orderId) {
        $this->_checkGuid($customerId);
        $this->_checkGuid($orderId);
        return $this->_api->get(
            '/v1/customers/'.$customerId.'/orders/'.$orderId,
            ['audit'=>'customers/'.$customerId.'/orders/'.$orderId.'_{$time}']
        )->decodeResponseBody(FALSE);

    }


}