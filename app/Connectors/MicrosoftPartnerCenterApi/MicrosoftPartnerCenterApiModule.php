<?php


namespace App\Connectors\MicrosoftPartnerCenterApi;


use App\Connectors\Common\ApiResponse;
use App\Connectors\Common\MicrosoftAuthRestApi;
use App\Connectors\Common\RestApiModule;
use App\Connectors\MicrosoftPartnerCenterApi;
use GuzzleHttp\Psr7\Request;

class MicrosoftPartnerCenterApiModule extends RestApiModule
{

    /**
     * @var MicrosoftPartnerCenterApi
     */
    protected $_api;

    public function __construct(MicrosoftPartnerCenterApi $api) {
        parent::__construct($api);
    }


    /**
     * @param Request $request
     * @param array $options
     * @param bool $withAuthToken
     * @return ApiResponse
     * @throws \Exception
     */
    public function _makeRequest(Request $request, $options=[], $withAuthToken=TRUE) {
        $this->_lastResponse = $this->_api->makeRequest($request, $options, $withAuthToken);
        return $this->_lastResponse;
    }


    protected function _checkGuid($guid) {
        if(!preg_match('/^'.MicrosoftAuthRestApi::REGEX_UUID.'$/', $guid)) {
            throw new \Exception('Invalid GUID');
        }
    }

}