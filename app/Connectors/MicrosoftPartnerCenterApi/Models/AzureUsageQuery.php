<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 20/10/2017
 * Time: 22:29
 */

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;


use App\Connectors\Common\RestApi;
use Carbon\Carbon;

/**
 * Class AzureUsageQuery
 * @package App\Connectors\MicrosoftPartnerCenterApi\Models
 * @method AzureUtilisationRecord current()
 */
class AzureUsageQuery extends PartnerCenterPagedQuery
{

    const DETAIL_RESOURCE = 'resource';
    const DETAIL_SERVICE = 'service';
    const GRANULARITY_HOURLY = 'hourly';
    const GRANULARITY_DAILY = 'daily';

    protected $_rowClass = AzureUtilisationRecord::class;


    /**
     * @var string
     */
    protected $tenantId;

    /**
     * @var string
     */
    protected $subscriptionId;

    /**
     * @var Carbon
     */
    protected $startTime;

    /**
     * @var Carbon
     */
    protected $endTime;

    protected $queryDate;

    /**
     * @var string
     */
    protected $granularity = self::GRANULARITY_DAILY;

    /**
     * @var boolean
     */
    protected $resourceDetail = TRUE;

    public function __construct(RestApi $api, $customerTenantGuid=NULL, $subscriptionGuid=NULL) {
        parent::__construct($api);
        $this->tenantId = $customerTenantGuid;
        $this->subscriptionId = $subscriptionGuid;
        // just get last day's usage by default
        $this->startTime = Carbon::today('UTC')->subDay();
        $this->endTime = Carbon::today('UTC');
    }


    /**
     * @param string $tenantGuid
     * @return $this
     * @throws \Exception
     */
    public function customerTenantGuid($tenantGuid) {
        if(!preg_match('/^'.self::REGEX_UUID.'$/', $tenantGuid)) {
            throw new \Exception('Invalid Tenant GUID');
        }
        $this->tenantId = $tenantGuid;
        return $this;
    }

    /**
     * @param string $subscriptionGuid
     * @return $this
     * @throws \Exception
     */
    public function subscriptionGuid($subscriptionGuid) {
        if(!preg_match('/^'.self::REGEX_UUID.'$/', $subscriptionGuid)) {
            throw new \Exception('Invalid Subscription GUID');
        }
        $this->subscriptionId = $subscriptionGuid;
        return $this;
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function startTime(Carbon $time) {
        $this->startTime = $time->copy();
        return $this;
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function endTime(Carbon $time) {
        $this->endTime = $time->copy();
        return $this;
    }

    public function forDate($date) {
        if(is_string($date)) {
            if(preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $date)) {
                $date = new Carbon($date, 'UTC');
            }
        }
        if($date instanceof Carbon) {
            $d = $date->copy()->startOfDay();
            $this->queryDate = $d->format('Ymd');
            $this->startTime($d);
            $this->endTime($d->addDay(1));
        }
    }

    /**
     * @param string $granularity Must be 'hourly' or 'daily'
     * @return $this
     * @throws \Exception
     */
    public function granularity($granularity) {
        $granularity = strtolower($granularity);
        if(!in_array($granularity, ['hourly','daily'])) {
            throw new \Exception('Invalid usage granularity');
        }
        $this->granularity = $granularity;
        return $this;
    }

    /**
     * @param string $detailLevel Must be 'resource' or 'service'
     * @return $this
     * @throws \Exception
     */
    public function detailLevel($detailLevel) {
        $detailLevel = strtolower($detailLevel);
        switch ($detailLevel) {
            case 'resource': $this->resourceDetail = TRUE; break;
            case 'service': $this->resourceDetail = FALSE; break;
            default:
                throw new \Exception('Invalid detail level');
        }
        return $this;
    }

    /**
     * @return string
     */
    public function makeQueryUrl() {
        $url = '/v1/customers/'.$this->tenantId.'/subscriptions/'.$this->subscriptionId.'/utilizations/azure?';
        $url .= 'start_time=' . urlencode($this->startTime->toIso8601String());
        $url .= '&end_time=' . urlencode($this->endTime->toIso8601String());
        $url .= '&granularity='.$this->granularity.'&show_details='.($this->resourceDetail ? 'True' : 'False').'&size='.intval($this->pageSize);
        // populate audit path on first query (will be called before any get operations)
        if(empty($this->_auditPath)) {
            $aggregation = $this->resourceDetail ? self::DETAIL_RESOURCE : self::DETAIL_SERVICE;
            $date = empty($this->queryDate) ? $this->startTime->format('YmdHis').'-'.$this->endTime->format('YmdHis') : $this->queryDate;
            $this->_auditPath = 'customers/'.$this->tenantId.'/subscriptions/'.$this->subscriptionId.'/'.$aggregation.'-usage-'.$this->granularity.'/'.$date.'_{$time}-{$page}';
        }
        return $url;
    }

}