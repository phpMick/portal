<?php

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;

use App\Connectors\Common\ApiObject;
use Carbon\Carbon;

/**
 * Class AzureUtilisationRecord
 * @package App\Connectors\MicrosoftPartnerCenterApi\Models
 * @property Carbon $usageStartTime
 * @property Carbon $usageEndTime
 * @property string $meterGuid
 * @property string $meterName
 * @property string $meterCategory
 * @property string $meterSubCategory
 * @property string $meterRegion
 * @property string $consumedQuantity Actually a decimal number
 * @property string $unitOfMeasure
 * @property string $resourceUri
 * @property  string $resourceRegion
 * @property string $marketplacePartNumber
 * @property string $marketplaceOrderNumber
 * @property array $tags
 * @property array $resourceInfo
 */
class AzureUtilisationRecord extends ApiObject
{

    const PROPERTY_MAP = [
        'meterGuid'=>'resource.id',
        'meterName'=>'resource.name',
        'meterCategory'=>'resource.category',
        'meterSubCategory'=>'resource.subcategory',
        'meterRegion'=>'resource.region',
        'consumedQuantity'=>'quantity',
        'unitOfMeasure'=>'unit',
        'resourceUri'=>'instanceData.resourceUri',
        'resourceRegion'=>'instanceData.location',
        'marketplacePartNumber'=>'instanceData.partNumber',
        'marketplaceOrderNumber'=>'instanceData.orderNumber',
        'tags'=>'instanceData.tags',
        'resourceInfo'=>'infoFields'
    ];


    public function loadFromArray($data) {
        if(parent::loadFromArray($data)) {
            // load Carbon instances:
            foreach(['usageStartTime', 'usageEndTime'] as $prop) {
                if(array_key_exists($prop, $data)) {
                    $this->$prop = Carbon::createFromFormat(Carbon::ISO8601, $data[$prop])->setTimezone('UTC');
                    // fix for stupid timezone issue in API where the server sends the wrong timezone
                    if($this->$prop->hour = 16) {
                        $this->$prop->addHours(8);
                    }
                }
            }
            if(empty($this->resourceUri)) {
                // there might be something in the property arrays we can use as an identifier:
                if(is_array($this->resourceInfo) AND array_key_exists('project', $this->resourceInfo) AND !empty($this->resourceInfo['project'])) {
                    $this->resourceUri = $this->resourceInfo['project'];
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @return boolean|NULL
     */
    public function isMarketplaceItem() {
        return empty($this->resourceGuid) ? NULL : !empty($this->marketplacePartNumber);
    }

}