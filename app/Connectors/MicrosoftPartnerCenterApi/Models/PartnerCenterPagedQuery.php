<?php

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;


use App\Connectors\Common\ApiResponse;
use App\Connectors\Common\RestApiPagedQuery;

abstract class PartnerCenterPagedQuery extends RestApiPagedQuery
{

    /**
     * @var array
     */
    protected $_nextLink = NULL;

    /**
     * @var integer The number of rows to get in each query page
     */
    public $pageSize = 250;

    abstract public function makeQueryUrl();

    protected function loadNextPage() {
        // set up parent variables in case there's an error
        $this->_data = NULL;
        $this->_response = NULL;
        if($this->_nextLink===FALSE) {
            return FALSE;
        }
        // if nextLink is not set, then we need to get the first page, so just send a GET query to the right URL:
        if(!isset($this->_nextLink)) {
            $this->get($this->makeQueryUrl());
        } else {
            // navigate to correct array:
            if(!is_array($this->_nextLink)) {
                throw new \Exception('Invalid response from server');
            }
            $a =& $this->_nextLink; // reference to shorten checks below
            if(!array_key_exists('uri', $a) OR !array_key_exists('method', $a) OR !array_key_exists('headers', $a)) {
                throw new \Exception('Invalid response format from server');
            }
            // ensure values conform:
            if(!is_string($a['uri']) OR empty($a['uri']) OR $a['method']!='GET' OR !is_array($a['headers']) OR count($a['headers'])!=1) {
                throw new \Exception('Invalid response data from server');
            }
            // ensure header is defined:
            $h = $a['headers'][0];
            if(!array_key_exists('key', $h) OR $h['key']!='MS-ContinuationToken' OR !array_key_exists('value', $h) OR empty($h['value'])) {
                throw new \Exception('Invalid response content from server');
            }
            // all validated - send query:
            $url = $a['uri'];
            if(substr($url, 0, 1)!='/') {
                $url = '/'.$url;
            }
            // prepend 'v1' as that is the version of the API we understand, and it looks like the links MS sends don't include this...
            if(substr($url, 0, 4)!='/v1/') {
                $url = '/v1' . $url; // $url already has leading slash
            }
            $options = ['headers' => [ $h['key'] => $h['value'] ] ];
            $this->get($url, $options);
        }
        $this->_nextLink = FALSE;
        if(!($this->_response instanceof ApiResponse)) {
            throw new \Exception('Invalid object returned as response');
        }
        if($this->_response->getStatusCode()!=200) {
            // the assumption is that this API will always be using GET requests, and therefore non-200 requests are errors
            // e.g. a 204 has been historically returned when there was a server issue (which returned an empty response erroneously)
            throw new \Exception('The server experienced en error');
        }

        $data = $this->_response->decodeResponseBody();
        if(!is_array($data) OR !array_key_exists('items', $data) OR !array_key_exists('links', $data)) {
            return FALSE;
        }
        $this->_data = $data['items'];
        if(array_key_exists('next', $data['links'])) {
            $this->_nextLink = $data['links']['next'];
        } else {
            $this->_nextLink = FALSE;
        }
        return TRUE;
    }

    /**
     * @return boolean
     */
    protected function morePagesAvailable() {
        return $this->_nextLink!==FALSE;
    }


}