<?php

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;


use App\Connectors\Common\ApiObject;
use Carbon\Carbon;

/**
 * Class Subscription
 * @package App\Connectors\MicrosoftPartnerCenterApi\Models
 * @property string $guid
 * @property string $tenant_guid
 * @property string $offer_id
 * @property string $offer_name
 * @property string $friendly_name
 * @property integer $quantity
 * @property string $unit_type
 * @property string $parent_subscription_guid
 * @property Carbon $creation_date
 * @property Carbon $effective_start_date
 * @property Carbon $commitment_end_date
 * @property string $status
 * @property boolean $is_trial
 * @property boolean $auto_renew
 * @property string $billing_type
 * @property string $billing_cycle
 * @property string[] $suspension_reasons
 * @property string $contract_type
 * @property string $order_guid
 */
class Subscription extends ApiObject
{
    const PROPERTY_MAP = [
        'guid'=>'id',
        'tenant_guid'=>'',
        'offer_id'=>'offerId',
        'offer_name'=>'offerName',
        'friendly_name'=>'friendlyName',
        'quantity',
        'unit_type'=>'unitType',
        'parent_subscription_guid'=>'parentSubscriptionId',
        'status',
        'is_trial'=>'isTrial',
        'auto_renew'=>'autoRenewEnabled',
        'billing_type'=>'billingType',
        'billing_cycle'=>'billingCycle',
        'suspension_reasons'=>'suspensionReasons',
        'contract_type'=>'contractType',
        'order_guid'=>'orderId',
        'creation_date'=>'',
        'effective_start_date'=>'',
        'commitment_end_date'=>''
    ];

    public function loadFromArray($data) {
        if(parent::loadFromArray($data)) {
            $this->creation_date = empty($data['creationDate']) ? NULL : (new Carbon($data['creationDate']))->setTimezone('UTC');
            $this->effective_start_date = empty($data['effectiveStartDate']) ? NULL : (new Carbon($data['effectiveStartDate']))->setTimezone('UTC');
            $this->commitment_end_date = empty($data['commitmentEndDate']) ? NULL : (new Carbon($data['commitmentEndDate']))->setTimezone('UTC');
            // get the tenant GUID from the self link:
            if(array_key_exists('links', $data) AND is_array($data['links']) AND array_key_exists('self', $data['links'])
                    AND is_array($data['links']['self']) AND array_key_exists('uri', $data['links']['self'])) {
                $matches = array();
                if(preg_match('/\/customers\/('.self::REGEX_UUID.')\/subscriptions\/'.self::REGEX_UUID.'/', $data['links']['self']['uri'], $matches)) {
                    $this->tenant_guid = $matches[1];
                }
            }
            $this->guid = strtolower($this->guid);
            $this->tenant_guid = strtolower($this->tenant_guid);
            $this->order_guid = strtolower($this->order_guid);
            return TRUE;
        }
        return FALSE;
    }

    public function isAzure() {
        return strtoupper(substr($this->offer_id, 0, 7))=='MS-AZR-';
    }

    public function azureUsage() {
        if(!$this->isAzure()) {
            throw new \Exception('Subscription is not an Azure Subscription');
        }
        return new AzureUsageQuery($this->_api, $this->tenant_guid, $this->guid);
    }

}