<?php

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;

use App\Connectors\Common\ApiResponse;


/**
 * Class CustomerList
 * @package App\Connectors\MicrosoftPartnerCenterApi\Customers
 * @method SimpleCustomer current()
 */
class CustomerQuery extends PartnerCenterPagedQuery
{

    const FIELD_COMPANYNAME = 'CompanyName';
    const FIELD_DOMAIN = 'Domain';
    const FIELD_ALL = [self::FIELD_COMPANYNAME, self::FIELD_DOMAIN];

    protected $_rowClass = SimpleCustomer::class;

    protected $_filter = NULL;

    protected $_auditPath = 'customers_{$date}_{$time}-{$page}';

    /**
     * @param string $field
     * @param string $value
     * @return $this
     */
    public function whereStartsWith($field, $value) {
        assert('in_array($field, self::FIELD_ALL)', 'Invalid search field name');
        $this->_filter = '{"Field":"'.$field.'","Operator":"starts_with","Value":"' . $value . '"}';
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function whereCompanyNameStartsWith($value) {
        return $this->whereStartsWith(self::FIELD_COMPANYNAME, $value);
    }

    /**
     * @param string $value
     * @return $this
     */
    public function whereDomainNameStartsWith($value) {
        return $this->whereStartsWith(self::FIELD_DOMAIN, $value);
    }

    public function makeQueryUrl() {
        $url = '/v1/customers?size='.intval($this->pageSize);
        if(!empty($this->_filter)) {
            $url .= '&filter=' . urlencode($this->_filter);
        }
        return $url;
    }


}