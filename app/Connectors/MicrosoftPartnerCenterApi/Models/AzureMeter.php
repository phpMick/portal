<?php

namespace App\Connectors\MicrosoftPartnerCenterApi\Models;


use App\CloudServices\Azure\Models\Region;
use App\Connectors\Common\ApiObject;
use Carbon\Carbon;

/**
 * Class AzureMeter
 * @package App\Connectors\MicrosoftPartnerCenterApi
 * @property string $guid
 * @property string $name
 * @property string $category
 * @property string $sub_category
 * @property string $region
 * @property string $unit_of_measure
 * @property string $included_quantity
 * @property Carbon $start_date
 * @property string[] $tags
 * @property string[] $rates
 */
class AzureMeter extends ApiObject
{

    const PROPERTY_MAP = [
        'guid'=>'id',
        'name',
        'category',
        'sub_category'=>'subcategory',
        'region',
        'unit_of_measure'=>'unit',
        'included_quantity'=>'includedQuantity',
        'tags', // should just load directly, even though it's an array
        'rates' // should just load directly, even though it's an object
    ];

    public function loadFromArray($data) {
        if(parent::loadFromArray($data)) {
            if(!empty($data['effectiveDate'])) {
                $this->start_date = Carbon::createFromFormat(Carbon::ISO8601, $data['effectiveDate'])->setTimezone('UTC');
            }
        }
    }

    public function toArray($dataMode='all') {
        $dataMode = strtolower($dataMode);
        $data = parent::toArray();
        switch($dataMode) {
            case 'cspmeter':
            case 'csp_meter': return array_except($data, ['region', 'included_quantity', 'tags', 'rates', 'start_date']);
            case 'meter': return array_except($data, ['region', 'included_quantity', 'tags', 'rates', 'start_date']);
        }
        return $data;
    }


}