<?php


namespace App\Connectors\MicrosoftPartnerCenterApi\Models;


use App\Connectors\Common\ApiObject;
use App\Connectors\Common\ApiResponse;
use Illuminate\Support\Facades\Cache;

/**
 * Class SimpleCustomer
 * @package App\Connectors\MicrosoftPartnerCenterApi\Customers
 * @property string $guid
 * @property string $tenant_guid
 * @property string $default_domain
 * @property string $microsoft_domain
 * @property string[] $custom_domains
 * @property string $name
 */
class SimpleCustomer extends ApiObject
{
    const PROPERTY_MAP = [
        'guid'=>'id',
        'tenant_guid'=>'companyProfile.tenantId',
        'default_domain'=>'companyProfile.domain',
//        'microsoft_domain'=>'',
//        'custom_domains'=>'',
        'name' => 'companyProfile.companyName'
    ];

    public function loadFromArray($data)
    {
        if(parent::loadFromArray($data)) {
            if(!empty($this->default_domain)) {
                $this->default_domain = strtolower($this->default_domain);
                if(substr($this->default_domain, -16)=='.onmicrosoft.com') {
                    $this->microsoft_domain = $this->default_domain;
                } else {
                    $this->custom_domains = [$this->default_domain];
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @return Subscription[]
     * @throws \Exception
     */
    public function subscriptions() {
        if(!preg_match('/^'.self::REGEX_UUID.'$/', $this->guid)) {
            throw new \Exception('Invalid customer ID');
        }
        return $this->_api->_normaliseListResponse(
            $this->_api->get(
                '/v1/customers/'.$this->guid.'/subscriptions',
                ['audit'=>'customers/'.$this->guid.'/subscriptions_{$date}_{$time}']
            )->errorAsEmptyArray()->decodeResponseBody(FALSE),
            Subscription::class
        );
    }

    /**
     * @param boolean $activeOnly Should we only return active Azure subscriptions (ignore status=[none/suspended/deleted])
     * @return Subscription[]
     * @throws \Exception
     */
    public function azureSubscriptions($activeOnly=FALSE) {
        $all_subs = $this->subscriptions();
        // filter the list and return only subscriptions with an offer type for Azure (begins 'MS-AZR-')
        $results = [];
        foreach($all_subs as $sub) {
            if($sub->isAzure() AND (!$activeOnly OR strtolower($sub->status)=='active')) {
                $results[] = $sub;
            }
        }
        return $results;
    }

    /**
     * @param string $subscriptionGuid
     * @return Subscription
     * @throws \Exception
     */
    public function subscription($subscriptionGuid) {
        if(!preg_match('/^'.self::REGEX_UUID.'$/', $this->guid)) {
            throw new \Exception('Invalid customer ID');
        }
        if(!preg_match('/^'.self::REGEX_UUID.'$/', $subscriptionGuid)) {
            throw new \Exception('Invalid subscription ID');
        }
        return $this->_api->_normaliseObjectResponse(
          $this->_api->get('/v1/customers/'.$this->guid.'/subscriptions/'.$subscriptionGuid)->errorAsNull()->decodeResponseBody(FALSE),
          Subscription::class
        );
    }

}