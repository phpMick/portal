<?php


namespace App\Connectors\MicrosoftPartnerCenterApi;


use App\Connectors\Common\MicrosoftAuthRestApi;
use App\Connectors\MicrosoftPartnerCenterApi\Models\CustomerQuery;
use App\Connectors\MicrosoftPartnerCenterApi\Models\SimpleCustomer;

class Customers extends MicrosoftPartnerCenterApiModule
{

    const AUTH_LOGINFORM  = 'form';
    const AUTH_AAD  = 'aad';

    /**
     * @return CustomerQuery
     */
    public function customers()
    {
        return new CustomerQuery($this->_api);
    }

    /**
     * @param $customerId
     * @return array|null|object
     * @throws \Exception
     */
    public function customer($customerId)
    {
        $this->_checkGuid($customerId);
        return $this->_api->get('/v1/customers/' . $customerId)->errorAsNull()->decodeResponseBody(false);
    }

    protected $casts = [
        'auth_providers'=>'array'
    ];


    /**
     * @param string $customerId
     * @return Models\Subscription[]
     * @throws \Exception
     */
    public function getSubscriptions($customerId)
    {
        // params are checked in SimpleCustomer code
        $cust = new SimpleCustomer(['id' => $customerId], $this->_api);
        return $cust->subscriptions();
    }

    /**
     * @param string $customerId
     * @param boolean $activeOnly
     * @return Models\Subscription[]
     */
    public function getAzureSubscriptions($customerId, $activeOnly = false)
    {
        // params are checked in SimpleCustomer code
        $cust = new SimpleCustomer(['id' => $customerId], $this->_api);
        return $cust->azureSubscriptions($activeOnly);
    }

    /**
     * @param string $customerId
     * @param string $subscriptionId
     * @return Models\Subscription
     * @throws \Exception
     */
    public function getSubscription($customerId, $subscriptionId)
    {
        // params are checked in SimpleCustomer code
        $cust = new SimpleCustomer(['id' => $customerId], $this->_api);
        return $cust->subscription($subscriptionId);
    }

}