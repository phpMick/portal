<?php

namespace App\Connectors;

use App\Connectors\Common\RestApi;
use App\Connectors\Common\HasModules;
use GuzzleHttp\Psr7\Request;


/**
 * Class MicrosoftAzureEnterpriseReportingApi
 * @package App\Connectors
 * @method \App\Connectors\MicrosoftAzureEnterpriseReportingApi\BillingPeriods BillingPeriods()
 * @method \App\Connectors\MicrosoftAzureEnterpriseReportingApi\BalanceAndSummary BalanceAndSummary()
 * @method \App\Connectors\MicrosoftAzureEnterpriseReportingApi\PriceSheet PriceSheet()
 * @method \App\Connectors\MicrosoftAzureEnterpriseReportingApi\UsageDetail UsageDetail()
 */
class MicrosoftAzureEnterpriseReportingApi extends RestApi
{
    use HasModules;

    protected $enrolmentNumber;
    protected $enrolmentAccessKey;



    public function __construct($enrolmentNumber=NULL, $accessKey=NULL) {
        if(!empty($enrolmentNumber) AND !empty($accessKey)) {
            $this->connect($enrolmentNumber, $accessKey);
        }
    }

    public function connect($enrolmentNumber, $accessKey) {
        $this->enrolmentNumber = $enrolmentNumber;
        $this->enrolmentAccessKey = $accessKey;
        // update client config, and then kill the client to ensure we recreate it on next request:
        $this->_clientConfig = [
            'base_uri' => 'https://consumption.azure.com/v2/enrollments/' . $this->enrolmentNumber . '/',
            'exceptions' => FALSE,
        ];
        unset($this->_client);
    }

    public function makeRequest(Request $request, $options = []) {
        // update the request with accept header & auth:
        $request = $request->withHeader('Authorization', 'Bearer ' . $this->enrolmentAccessKey);
        if(!$request->hasHeader('Content-Type')) {
            $request = $request->withHeader('Content-Type', 'application/json');
        }
        return parent::makeRequest($request, $options);
    }


    public function MarketplaceCharges() {
        throw new \Exception('Not implemented yet');
    }



}