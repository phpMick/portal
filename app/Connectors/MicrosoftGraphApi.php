<?php


namespace App\Connectors;


use GuzzleHttp\Client;

class MicrosoftGraphApi
{

    /**
     * @var MicrosoftAzureActiveDirectoryAuth
     */
    protected $_authService = NULL;

    protected $_client = NULL;


    public function __construct(MicrosoftAzureActiveDirectoryAuth $authService)
    {
        $this->_authService = $authService;
        $this->_client = new Client([
            'base_uri' => MicrosoftAzureActiveDirectoryAuth::resourceUri(MicrosoftAzureActiveDirectoryAuth::RESOURCE_MSGRAPH),
            'exceptions' => FALSE
        ]);
    }



}