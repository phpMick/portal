<?php

namespace App\Connectors;

//Connectors
use App\Connectors\MicrosoftAzureActiveDirectoryAuth\Azure2;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth\AuthState;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth\AuthUser;


//Laravel
use Illuminate\Http\Request;


//Oauth
use League\OAuth2\Client\Token\AccessToken;
use TheNetworg\OAuth2\Client\Provider\Azure;



/**
 * Class MicrosoftInteractiveAuth
 * @package App\CloudServices\AccessServices
 */
class MicrosoftAzureActiveDirectoryAuth
{

    const APP_PORTAL = 'portal';
    const APP_PORTAL2 = 'portal2';
    const APP_DEPLOY = 'deploy';
    const APP_AZURE = 'azure';
    const APP_O365 = 'o365';
    const APP_PARTNER = 'partner';
    const APP_ALL = [
        self::APP_PORTAL,
        self::APP_PORTAL2,
        self::APP_DEPLOY,
        self::APP_AZURE,
        self::APP_O365,
        self::APP_PARTNER
    ];

    const MODE_INTERACTIVE = 'interactive';
    const MODE_SERVICE = 'service';

    const RESOURCE_GRAPH = 'graph';     // Graph v1
    const RESOURCE_MSGRAPH = 'msgraph'; // Graph v2
    const RESOURCE_AZURESM = 'azuresm'; // Azure v1
    const RESOURCE_AZURERM = 'azurerm'; // Azure v2
    const RESOURCE_PARTNER_CENTER = 'partnercenter'; // Microsoft Partner Center
    const RESOURCE_ALL = array(
        self::RESOURCE_GRAPH,
        self::RESOURCE_MSGRAPH,
        self::RESOURCE_AZURESM,
        self::RESOURCE_AZURERM,
        self::RESOURCE_PARTNER_CENTER
    );

    /**
     * Defines the mode of operation - either for interactive user use (via browser) or for backend services
     * @var string Must be one of the self::MODE_* constants
     */
    protected $_mode = null;

    /**
     * Defines the AAD application we are operating as
     * @var string Must be one of the self::APP_* constants
     */
    protected $_app = null;

    protected $_appInfo = [];

    /**
     * @var string The AAD tenant GUID we are authenticating against (or the one populated by AAD from common endpoint)
     */
    protected $_tenantGuid = null;

    /**
     * @var string The URL we need to redirect to after login is complete (interactive mode only)
     */
    protected $_intendedUrl = null;

    /**
     * @var AuthState State data for this object
     */
    protected $_stateData = null;

    /**
     * @var bool
     */
    protected $_stateDateModified = false;

    protected $_saveStateCallback = null;

    /**
     * @var Azure
     */
    protected $_oauthService = null;

    /**
     * @var Azure[]
     */
    protected $_oauthServices = [];


    /**
     * @param string $resourceId
     * @return string
     * @throws \Exception
     */
    static public function resourceUrl($resourceId)
    {
        switch ($resourceId) {
            case self::RESOURCE_GRAPH:
                return 'https://graph.windows.net/';
            case self::RESOURCE_MSGRAPH:
                return 'https://graph.microsoft.com/v1.0/';
            case self::RESOURCE_AZURESM:
                return 'https://management.core.windows.net/';
            case self::RESOURCE_AZURERM:
                return 'https://management.azure.com/';
            case self::RESOURCE_PARTNER_CENTER:
                return 'https://api.partnercenter.microsoft.com';
        }
        throw new \Exception('Invalid resource specified');
    }

    /**
     * @param string $resourceId
     * @return string
     * @throws \Exception
     */
    static public function resourceName($resourceId)
    {
        switch ($resourceId) {
            case self::RESOURCE_GRAPH:
                return 'https://graph.windows.net';
            case self::RESOURCE_MSGRAPH:
                return 'https://graph.microsoft.com/';
            case self::RESOURCE_AZURESM:
                return 'https://management.core.windows.net/';
            case self::RESOURCE_AZURERM:
                return 'https://management.core.windows.net/';
            case self::RESOURCE_PARTNER_CENTER:
                return 'https://graph.windows.net';
        }
        throw new \Exception('Invalid resource specified');
    }

    /**
     * @param string $appId Must be one of self::APP_*
     * @return string[]
     * @throws \Exception
     */
    static public function appResources($appId)
    {
        switch ($appId) {
            case self::APP_PORTAL:
                return [self::RESOURCE_GRAPH, self::RESOURCE_MSGRAPH, self::RESOURCE_AZURESM, self::RESOURCE_AZURERM];
            case self::APP_PORTAL2:
                return [self::RESOURCE_MSGRAPH];
            case self::APP_DEPLOY:
                return [];
            case self::APP_AZURE:
                return [self::RESOURCE_AZURERM];
            case self::APP_O365:
                return [];
            case self::APP_PARTNER:
                return [self::RESOURCE_PARTNER_CENTER];
        }
        throw new \Exception('Invalid application specified');
    }

    /**
     * @return AuthUser|NULL
     * @throws \Exception
     */
    public function loggedInUser()
    {
        if ($this->_mode == self::MODE_SERVICE) {
            throw new \Exception('Cannot log in a user in service mode');
        }
        return optional($this->_stateData)->user;
    }

    /**
     * @param string $part
     * @return string
     */
    protected function configKey($part)
    {
        return 'aad.' . $this->_app . '.' . $part;
    }

    /**
     * @return string
     */
    protected function sessionKey()
    {
        return 'aad_auth_' . $this->_app;
    }

    /**
     * @param $resources
     * @return bool
     * @throws \Exception
     */
    public function hasValidToken($resources)
    {
        // state must have been loaded already
        if (is_null($this->_stateData)) {
            return false;
        }
        if ($this->_mode == self::MODE_INTERACTIVE) {
            // first check if we have a logged in user
            if (is_null($this->_stateData->user)) {
                return false;
            }
            if (!is_array($resources)) {
                $resources = [$resources];
            }
            foreach ($resources as $resourceId) {
                $token = $this->getAccessToken($resourceId);
                if (!($token instanceof AccessToken) OR $token->hasExpired()) {
                    return false;
                }
            }
            return true;
        } elseif ($this->_mode == self::MODE_SERVICE) {
            if (!is_array($resources)) {
                $resources = [$resources];
            }
            foreach ($resources as $resourceId) {
                $token = $this->getAccessToken($resourceId);
                if (!($token instanceof AccessToken) OR $token->hasExpired()) {
                    return false;
                }
            }
            return true;
        } else {
            throw new \Exception('Unsupported mode of operation');
        }
    }

    /**
     * @param $resourceId
     * @throws \Exception
     */
    public function deleteToken($resourceId)
    {
        if (!is_array($resourceId)) {
            if ($resourceId == '*') {
                $resourceId = self::RESOURCE_ALL;
            } else {
                $resourceId = [$resourceId];
            }
        }
        foreach ($resourceId as $res) {
            $this->_stateData->deleteToken($res);
        }
        $this->saveState();
    }

    /**
     * @param null $targetUrl
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function getLogoutRedirect($targetUrl = null)
    {
        $this->_stateData->user = null;
        $this->_stateData->tokens = [];
        $this->_stateData->oauthCode = null;
        $this->_stateData->oauthState = null;
        $this->_stateData->oauthCodeTime = 0;
        $this->saveState();
        if (strlen($targetUrl) == 0) {
            $targetUrl = route('home');
        }
        return redirect($this->_oauthService->getLogoutUrl($targetUrl));
    }


    /**
     * Called from login start - user has clicked Sign in with MS
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function getLoginRedirect(Request $request)
    {
        if ($this->_mode !== self::MODE_INTERACTIVE) {
            throw new \Exception('Can only perform login in interactive mode');
        }
        if (isset($this->_tenantGuid)) {
            $this->_oauthService->tenant = $this->_tenantGuid;
        }
        $params = [];
        // if we need to prompt for admin consent, add the following:
        // $params['prompt'] = 'admin_consent';
        $loginUrl = $this->_oauthService->getAuthorizationUrl($params);
        $this->_stateData = new AuthState();
        $requestUrl = $request->fullUrl();
        // don't store intended URL if login was initiated from login controller
        if (strpos($requestUrl, '/login/with-aad') === false) {
            $this->_stateData->intendedUrl = $request->fullUrl();
        }
        $this->_stateData->oauthState = $this->_oauthService->getState();
        $this->_stateDateModified = true;
        $this->saveState();
        return redirect($loginUrl);
    }

    /**
     * Does the work to handle a response from AAD after a login
     * @param Request $request
     * @return AuthUser|NULL
     * @throws \Exception
     */
    public function receiveResponse(Request $request)
    {
        if ($this->_mode !== self::MODE_INTERACTIVE) {
            throw new \Exception('Can only perform login in interactive mode');
        }
        // first, check if the response is ok or an error:
        if ($request->has('error')) {
            logger()->error('AAD authentication error', ['request' => $request]);
            throw new \Exception('Authentication failed');
        }
        if (!$request->has('code')) {
            logger()->error('AAD authentication state error: no code', ['request' => $request]);
            throw new \Exception('Authentication error');
        }
        // check state is valid (stop forgery)
        $response_state = $request->get('state');
        if (is_null($response_state) OR $response_state !== $this->_stateData->oauthState) {
            throw new \Exception('AAD authentication state error: invalid request/response');
        }

        $this->_stateData->oauthState = null;
        $this->_stateData->oauthCode = $request->get('code');
        $this->_stateData->oauthCodeTime = time();
      //  $this->_stateData->user = $this->getUserFromResource();
        $this->_stateData->user = new AuthUser();
        $this->_stateDateModified = true;
        $this->saveState();
        return $this->_stateData->user;
    }

    /**
     * @param string $resourceId
     * @return AuthUser
     * @throws \Exception
     */
    protected function getUserFromResource($resourceId = null)
    {
        if (is_null($resourceId)) {
            $appResources = self::appResources($this->_app);
            if (count($appResources) == 0) {
                throw new \Exception('Not implemented');
            }
            $resourceId = $appResources[0];
        }
        $token = $this->getAccessToken($resourceId);//token null
        $user = new AuthUser();
        if ($this->_oauthService instanceof Azure) {
            $userResource = $this->_oauthService->getResourceOwner($token);
            $user->name = trim($userResource->getFirstName() . ' ' . $userResource->getLastName());
            $user->tenantGuid = $userResource->getTenantId();
            $user->objectGuid = $userResource->getId();
            $user->upn = $userResource->getUpn();
        } elseif ($this->_oauthService instanceof Azure2) {
            $userResource = $this->_oauthService->extractUserDetails($token);
            if (array_key_exists('oid', $userResource) AND strlen($userResource['oid']) > 0) {
                $user->objectGuid = $userResource['oid'];
            }
            if (array_key_exists('tid', $userResource) AND strlen($userResource['tid']) > 0) {
                if ($userResource['tid'] == '9188040d-6c67-4c5b-b112-36a304b66dad') {
                    $user->tenantGuid = 'consumers'; // Microsoft Personal Account Tenant ID
                } else {
                    $user->tenantGuid = $userResource['tid'];
                }
            }
            if (array_key_exists('name', $userResource)) {
                $user->name = trim($userResource['name']);
            }
            if (array_key_exists('email', $userResource)) {
                $user->email = $userResource['email'];
            }
            if (array_key_exists('preferred_username', $userResource)) {
                $user->upn = $userResource['preferred_username'];
            }
        }
        return $user;
    }

    /**
     * @return null
     * @throws \Exception
     */
    public function getIntendedUrl()
    {
        $url = null;
        if (strlen($this->_stateData->intendedUrl) > 0) {
            $url = $this->_stateData->intendedUrl;
            // remove from session:
            $this->_stateData->intendedUrl = null;
            $this->saveState();
        }
        return $url;
    }

    /**
     * @param null $tenantGuid
     */
    protected function setTenantGuid($tenantGuid = null)
    {
        if (is_null($tenantGuid)) {
            $this->_oauthService->tenant = 'common';
        }
        $this->_tenantGuid = $tenantGuid;
    }

    /**
     * @param string $resourceId Must be one of self::RESOURCE_*
     * @return string
     * @throws \Exception
     */
    public function getAuthHeader($resourceId)
    {
        $token = $this->getAccessToken($resourceId);
        if (!($token instanceof AccessToken)) {
            throw new \Exception('Authentication error: no token available');
        }
        return 'Bearer ' . $token->getToken();
    }

    /**
     * @param string $resourceId
     * @return AccessToken|NULL
     * @throws \Exception
     */
    public function getAccessToken($resourceId)
    {
        $resourceName = self::resourceName($resourceId); // throws exception if not a valid resource
        // set resource info for AADv1 authentication requests (AADv2 uses scopes correctly)
        if ($this->_oauthService instanceof Azure) {
            // set resource and API access details correctly:
            $this->_oauthService->resource = self::resourceName($resourceId);
            $this->_oauthService->urlAPI = self::resourceUrl($resourceId);
            $this->_oauthService->tenant = $this->_tenantGuid;
        } elseif ($this->_oauthService instanceof Azure2) {
            $this->_oauthService->tenant = $this->_tenantGuid;
        }
        /** @var AccessToken $token */
        $token = $this->_stateData->getToken($resourceId);
        if (($token instanceof AccessToken) AND $token->hasExpired()) {
            // try to refresh token (if token has a refresh token) and store it:
            $refreshToken = $token->getRefreshToken();
            if (strlen($refreshToken) > 0) {
                $token = $this->_oauthService->getAccessToken('refresh_token', [
                    'refresh_token' => $refreshToken
                ]);
                // store refreshed token:
                $this->_stateData->setToken($resourceId, $token);
                $this->_stateDateModified = true;
                $this->saveState();
            } else {
                $token = null;
            }
        }
        if ($token instanceof AccessToken) {
            return $token;
        }
        switch ($this->_mode) {
            case self::MODE_INTERACTIVE:
                // need to get the auth code - hopefully in session!
                if ($this->_stateData->oauthCode) {
                    if (time() > ($this->_stateData->oauthCodeTime + 300)) {
                        return null; // force a renewed login attempt (hopefully)
                    }
                    $params = ['code' => $this->_stateData->oauthCode];
                    $token = $this->_oauthService->getAccessToken('authorization_code', $params);
                    $this->_stateData->setToken($resourceId, $token);
                    // clear auth code - it's a one-time thing (I think)
//                    $this->_stateData->oauthCode = NULL;
//                    $this->_stateData->oauthCodeTime = 0;
                    $this->_stateDateModified = true;
                    $this->saveState();
                    return $token;
                }
                return null; // this should prompt a fresh login attempt (that might not require a username/password)
            case $this->_mode == self::MODE_SERVICE:
                $token = $this->_oauthService->getAccessToken('client_credentials');
                $this->_stateData->setToken($resourceId, $token);
                $this->_stateDateModified = true;
                return $token;
        }
        throw new \Exception('Not implemented');
    }

    /**
     * @return mixed|null
     */
    public function getApplicationId()
    {
        return array_key_exists('id', $this->_appInfo) ? $this->_appInfo['id'] : null;
    }

    /**
     * @return mixed|null
     */
    public function getApplicationSecret()
    {
        return array_key_exists('secret', $this->_appInfo) ? $this->_appInfo['secret'] : null;
    }

    /**
     * @return mixed|null
     */
    public function getApplicationResponseUrl()
    {
        return array_key_exists('response_url', $this->_appInfo) ? $this->_appInfo['response_url'] : null;
    }

    /**
     * MicrosoftAzureActiveDirectoryAuth constructor.
     * @param $applicationName
     * @throws \Exception
     */
    private function __construct($applicationName)
    {
        // do not allow instantiation outside of factory methods below
        if (!in_array($applicationName, self::APP_ALL)) {
            throw new \Exception('Invalid application');
        }
        $this->_app = $applicationName;
        $this->_appInfo = [
            'id' => config($this->configKey('id')),
            'secret' => config($this->configKey('secret')),
            'response_url' => config($this->configKey('response_url')),
            'auth_tenant' => config($this->configKey('auth_tenant'))
        ];
        // construct the appropriate oAuth provider:
        if ($this->_app == self::APP_PORTAL2) {
            $this->_oauthService = new Azure2($this->getOauthConfigArray());
        } else {
            $this->_oauthService = new Azure($this->getOauthConfigArray());
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getOauthConfigArray()
    {
        if (!is_array($this->_appInfo)) {
            throw new \Exception('Application error: configuration not defined');
        }
        foreach (['id', 'secret', 'response_url'] as $k) {
            if (!array_key_exists($k, $this->_appInfo) OR strlen(trim($this->_appInfo[$k])) == 0) {
                throw new \Exception('Application error: configuration not defined');
            }
        }
        return [
            'clientId' => $this->_appInfo['id'],
            'clientSecret' => $this->_appInfo['secret'],
            'redirectUri' => $this->_appInfo['response_url']
        ];
    }

    /**
     *
     */
    protected function loadSessionData()
    {
        if ($this->_mode == self::MODE_INTERACTIVE) {
            $this->_stateData = session($this->sessionKey());
            if (!($this->_stateData instanceof AuthState)) {
                $this->_stateData = new AuthState();
            }
        }
    }

    /**
     * Provides the state data object so that we can save it somewhere in service mode
     * @return AuthState
     */
    public function getState()
    {
        return $this->_stateData;
    }

    /**
     * @return bool
     */
    public function isStateModified()
    {
        return $this->_stateDateModified;
    }

    /**
     * @throws \Exception
     */
    public function saveState()
    {
        if ($this->_mode == self::MODE_INTERACTIVE) {
            \request()->session()->put($this->sessionKey(), $this->_stateData);
            \request()->session()->save();
//            session([$this->sessionKey() => $this->_stateData]);
            $this->_stateDateModified = false;
        } elseif (isset($this->_saveStateCallback)) {
            // TODO: figure out how to use a callback function
            throw new \Exception('Not implemented');
            if ($this->_saveStateCallback($this->_stateData)) {
                // if function was called successfully and returns TRUE, then we can clear the modified state
                $this->_stateDateModified = false;
            }
        }
    }

    /**
     * @param $func
     * @throws \Exception
     */
    public function setStateSaveFunction($func)
    {
        // TODO: figure out how to do a callback
        throw new \Exception('Not implemented');
    }

    /**
     * @param $applicationName
     * @param null $tenantGuid
     * @return static
     * @throws \Exception
     */
    static public function interactive($applicationName, $tenantGuid = null)
    {
        // TODO: MASSIVE ISSUE: we're storing tokens in session, but not associating the tenant they are issued from
        // we need to check the issuing tenant ID before loading state data
        $instance = new static($applicationName);
        $instance->_mode = self::MODE_INTERACTIVE;
        $instance->_tenantGuid = $tenantGuid;
        // in interactive mode, we use the user session to store our state:
        $instance->loadSessionData();
        return $instance;
    }

    /**
     * @param $applicationName
     * @param AuthState $state
     * @param null $tenantGuid
     * @return static
     * @throws \Exception
     */
    static public function service($applicationName, AuthState $state, $tenantGuid = null)
    {
        // TODO: as above: tenant check before load state data
        if (!($state instanceof AuthState)) {
            $state = new AuthState();
        }
        $instance = new static($applicationName);
        // load in default authorisation tenant from config if there is not one set (only relevant for APP_PARTNER)
        if (empty($tenantGuid) AND !empty($instance->_appInfo['auth_tenant'])) {
            $tenantGuid = $instance->_appInfo['auth_tenant'];
        }
        $instance->_mode = self::MODE_SERVICE;
        $instance->_tenantGuid = $tenantGuid;
        $instance->_stateData = $state;
        return $instance;
    }


    /**
     * @param $applicationName
     * @param $tenantGuid
     * @return null
     * @throws \Exception
     */
    static public function getServicePrincipleObjectId($applicationName, $tenantGuid)
    {
        // query GRAPH v1 with service token
        $instance = new static($applicationName);
        $instance->_mode = self::MODE_SERVICE;
        $instance->_tenantGuid = $tenantGuid;
        $instance->_stateData = new AuthState();

        $token = $instance->getAccessToken(self::RESOURCE_GRAPH);
        if (is_null($token)) {
            return null;
        }
        $instance->_oauthService->API_VERSION = '1.5';
        $instance->_oauthService->urlAPI = self::resourceUrl(self::RESOURCE_GRAPH);
        $instance->_oauthService->resource = self::resourceName(self::RESOURCE_GRAPH);
        $params = '$filter=appId%20eq%20\'' . $instance->getApplicationId() . "'";
        $res = $instance->_oauthService->get($tenantGuid . '/servicePrincipals?' . $params, $token);
        if (!is_array($res) OR count($res) == 0 OR !array_key_exists('objectId', $res[0])) {
            return null;
        }
        return $res[0]['objectId'];
    }


    /**
     * Gets the user details from the access token
     * @param $code
     * @return array
     */
    public function getUserDetails($code)
    {
        $token = $this->_oauthService->getAccessToken('authorization_code', [
            'code' => $code
        ]);

        $this->_stateData->setToken(self::RESOURCE_GRAPH, $token);

        $userDetailsArray = $this->getUserAndDomain( $token->getIdTokenClaims());

        return $userDetailsArray;

    }

    /**
     * Just extract the username and domain from the email
     * @param $token
     * @return array
     */
    private function  getUserAndDomain( $token)
    {
        $userDetailsArray = [];

        $userDetailsArray['email'] = $token['unique_name'];
        $exploded = explode('@',$token['unique_name']);
        $userDetailsArray['username'] = $exploded[0];
        $userDetailsArray['domain'] = $exploded[1];
        $userDetailsArray['name'] = $token['name'];

        return $userDetailsArray;
    }


    /**
     * Get tenant id - using token
     * @return null
     * @throws \Exception
     */
    public function getTenantGuid()
    {
        $authUser = $this->getUserFromResource(self::RESOURCE_GRAPH);

        return  $authUser->tenantGuid;
    }



}

