<?php

namespace App\Connectors;

use App\Connectors\Common\HasModules;
use App\Connectors\Common\MicrosoftAuthRestApi;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth as AadAuth;
use App\Connectors\MicrosoftPartnerCenterApi\Billing;
use App\Connectors\MicrosoftPartnerCenterApi\Customers;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Storage;


/**
 * Class MicrosoftPartnerCenterApi
 * @package App\Connectors
 * @method Customers Customers()
 * @method Billing Billing()
 */
class MicrosoftPartnerCenterApi extends MicrosoftAuthRestApi
{
    use HasModules;

    protected $_clientConfig = [
        'base_uri' => 'https://api.partnercenter.microsoft.com',
        'exceptions' => FALSE
    ];

    protected $_authResource = 'partnercenter'; //  = MicrosoftAzureActiveDirectoryAuth::RESOURCE_PARTNER_CENTER


    public function __construct(AadAuth $authProvider = NULL) {
        // all functions in this API require authentication. If authProvider is NULL, then create a new service from scratch with defaults:
        if(is_null($authProvider)) {
            $authProvider = AadAuth::service(AadAuth::APP_PARTNER, new AadAuth\AuthState());
        }
        // we always want to audit this API:
        $this->enableAuditStore(Storage::disk('mspartner'), '');
        parent::__construct($authProvider);
    }

    /**
     * @param Request $request
     * @param array $options
     * @param bool $withAuthToken
     * @return Common\ApiResponse
     * @throws \Exception
     */
    public function makeRequest(Request $request, $options = [], $withAuthToken = TRUE) {
        if(strtoupper($request->getMethod())!=='GET') {
            throw new \Exception('Only GET operations are permitted');
        }
        // TODO: find a way to increase the CURL timeout, as this API is pretty slow

        return parent::makeRequest($request, $options, $withAuthToken);
    }

}