<?php

namespace App\Connectors\MicrosoftAzureApi\Authorization;

use App\Connectors\Common\ApiObject;

/**
 * Class RoleAssignment
 * @package App\Connectors\MicrosoftAzureApi\Authorization
 * @property string $id
 * @property string $guid
 * @property string $principalId
 * @property string $scope
 * @property string $createdOn
 * @property string $updatedOn
 * @property string $createdBy
 * @property string $updatedBy
 * @property string $roleDefinitionId
 * @property string $roleGuid
 */
class RoleAssignment extends ApiObject
{

    const PROPERTY_MAP = [
        'id',
        'guid'=>'name',
        'properties.principalId',
        'properties.scope',
        'properties.createdOn',
        'properties.updatedOn',
        'properties.createdBy',
        'properties.updatedBy',
        'properties.roleDefinitionId',
        'roleGuid'
    ];

    public function loadFromArray($data) {
        if(parent::loadFromArray($data)) {
            if (empty($this->roleGuid) AND !empty($this->roleDefinitionId)) {
                $matches = [];
                if (preg_match('/\/(' . self::REGEX_UUID . ')$/', $this->roleDefinitionId, $matches)) {
                    $this->roleGuid = strtolower($matches[1]);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

}