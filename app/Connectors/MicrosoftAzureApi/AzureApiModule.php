<?php

namespace App\Connectors\MicrosoftAzureApi;

use App\CloudServices\Azure\Models\Subscription;
use App\Connectors\Common\ApiResponse;
use App\Connectors\Common\RestApiModule;
use App\Connectors\MicrosoftAzureApi;
use GuzzleHttp\Psr7\Request;

/**
 * Class AzureApiModule
 * @package App\Connectors\MicrosoftAzureApi
 */
class AzureApiModule extends RestApiModule
{

    const REGEX_UUID = '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}';

    /**
     * @var MicrosoftAzureApi
     */
    protected $_api;

    public function __construct(MicrosoftAzureApi $api) {
        parent::__construct($api);
    }

    /**
     * @return MicrosoftAzureApi
     */
    protected function _getApi() {
        return $this->_api;
    }

    /**
     * @param string|Subscription $subscription
     * @return string
     * @throws \Exception
     */
    protected function _getSubscriptionGuid($subscription=NULL) {
        if(empty($subscription)) {
            $subscription = $this->_api->subscription();
        }
        if($subscription instanceof Subscription) {
            $subscription = $subscription->guid;
        }
        if(empty($subscription)) {
            throw new \Exception('Subscription must be set');
        }
        if(!is_string($subscription) OR !preg_match('/'.self::REGEX_UUID.'/', $subscription)) {
            throw new \Exception('Subscription must be a UUID');
        }
        return $subscription;
    }

    /**
     * @param Request $request
     * @param array $options
     * @param bool $withAuthToken
     * @return ApiResponse
     * @throws \Exception
     */
    public function _makeRequest(Request $request, $options=[], $withAuthToken=TRUE) {
        $this->_lastResponse = $this->_api->makeRequest($request, $options, $withAuthToken);
        return $this->_lastResponse;
    }


}