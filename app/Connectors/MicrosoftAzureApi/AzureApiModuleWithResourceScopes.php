<?php

namespace App\Connectors\MicrosoftAzureApi;


class AzureApiModuleWithResourceScopes extends AzureApiModule
{

    /**
     * @var string|NULL
     */
    protected $_currentScope = NULL;

    /**
     * @throws \Exception
     */
    protected function _enforceScope() {
        // check a scope has been defined:
        if(is_null($this->_currentScope)) {
            $sub = $this->_getSubscriptionGuid();
            if(isset($sub)) {
                $this->scopeSubscription($sub);
            } else {
                throw new \Exception('Application error: scope must be defined for authorization query');
            }
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function _getSubscriptionGuidFromScope() {
        if(empty($this->_currentScope)) {
            return $this->_getSubscriptionGuid('');
        }
        $m = [];
        if(preg_match('/\/?subscriptions\/(' . self::REGEX_UUID .')\/?/i', $this->_currentScope, $m)) {
            return $m[1];
        }
        throw new \Exception('Subscription must be set');
    }

    /**
     * @param string|Subscription|NULL $subscription
     * @return $this
     * @throws \Exception
     */
    public function scopeSubscription($subscription=NULL) {
        $this->_currentScope = '/subscriptions/'.$this->_getSubscriptionGuid($subscription);
        return $this;
    }

    /**
     * @param string $resourceGroupName
     * @param null $subscription
     * @return $this
     * @throws \Exception
     */
    public function scopeResourceGroup($resourceGroupName, $subscription=NULL) {
        $this->_currentScope = '/subscriptions/'.$this->_getSubscriptionGuid($subscription).'/resourceGroups/'.$resourceGroupName;
        return $this;
    }

    /**
     * @param $resourceUrl
     * @param string|Subscription|NULL $subscription
     * @return $this
     * @throws \Exception
     */
    public function scopeResource($resourceUrl, $subscription=NULL) {
        // check if resourceUrl has subscription already in it:
        if(preg_match('/^\/?subscriptions\/' . self::REGEX_UUID . '\//i', $resourceUrl)) {
            if($resourceUrl[0]=='/') {
                $this->_currentScope = $resourceUrl;
            } else {
                $this->_currentScope = '/' . $resourceUrl;
            }
        } else {
            if($resourceUrl[0]=='/') {
                $this->_currentScope = '/subscriptions/' . $this->_getSubscriptionGuid($subscription) . $resourceUrl;
            } else {
                $this->_currentScope = '/subscriptions/' . $this->_getSubscriptionGuid($subscription) . '/' . $resourceUrl;
            }
        }
        return $this;
    }

}