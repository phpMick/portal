<?php

namespace App\Connectors\MicrosoftAzureApi\Billing;


use App\Connectors\MicrosoftAzureApi;
use App\Connectors\MicrosoftAzureApi\AzureApiModule;
use App\Connectors\Common\PagedQuery;
use Carbon\Carbon;

class ResourceUsageQuery extends AzureApiModule
{

    // TODO: remove this trait and move to new RestApiPagedQuery class
    use PagedQuery;


    /**
     * @var Carbon
     */
    protected $_startDateTime;

    /**
     * @var Carbon
     */
    protected $_endDateTime;

    /**
     * @var string
     */
    protected $_granularity = 'Daily';

    /**
     * @var string
     */
    protected $_showDetails = TRUE;

    protected $_continueToken = '';


    public function __construct(MicrosoftAzureApi $api)
    {
        parent::__construct($api);
        $this->_stringifyNumerics = TRUE;
        $this->_getDataAsObject = FALSE;
        $this->_startDateTime = Carbon::yesterday('UTC');
        $this->_endDateTime = Carbon::today('UTC');
    }

    protected function getQueryUrl() {
        $url = '/subscriptions/' . $this->_getSubscriptionGuid() . '/providers/Microsoft.Commerce/UsageAggregates?api-version=2015-06-01-preview';
        if(empty($this->_startDateTime) OR empty($this->_endDateTime)) {
            throw new \Exception('Usage query dates must be defined');
        }
        $url .= '&reportedStartTime=' . urlencode($this->_startDateTime->toIso8601String());
        $url .= '&reportedEndTime=' . urlencode($this->_endDateTime->toIso8601String());
        $url .= '&aggregationGranularity=' . $this->_granularity;
        $url .= '&showDetails=' . ($this->_showDetails ? 'true' : 'false');
        if(!empty($this->_continueToken)) {
            $url .= '&continuationToken=' . urlencode($this->_continueToken);
        }
        return $url;
    }

    protected function getRowData($index)
    {
        // TODO: Implement getRowData() method.
    }

    protected function decodeResponse()
    {
        dd($this->_response->errorAsEmptyArray()->decodeResponseBody());
    }

    public function getRowCount()
    {
        // TODO: Implement getRowCount() method.
    }

    public function setStartDateTime(Carbon $startDateTime) {
        $this->_startDateTime = clone $startDateTime;
        $this->_startDateTime->timezone = 'UTC';
    }

    public function setEndDateTime(Carbon $endDateTime) {
        $this->_endDateTime = clone $endDateTime;
        $this->_endDateTime->timezone = 'UTC';
    }

    public function setGranularity($hourlyOrDaily) {
        switch(strtolower($hourlyOrDaily)) {
            case 'hourly':
                $this->_granularity = 'Hourly';
                break;
            case 'daily':
                $this->_granularity = 'Daily';
                break;
        }
    }

    public function resourceLevelDetail($flag=TRUE) {
        $this->_showDetails = (bool) $flag;
    }

    public function serviceLevelDetail($flag=TRUE) {
        $this->_showDetails = ! ((bool) $flag);
    }




}