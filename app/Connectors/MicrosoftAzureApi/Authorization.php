<?php

namespace App\Connectors\MicrosoftAzureApi;

use App\Connectors\MicrosoftAzureApi;
use App\Connectors\MicrosoftAzureApi\Authorization\RoleAssignment;
use Ramsey\Uuid\Uuid;


class Authorization extends AzureApiModuleWithResourceScopes
{
    const MAP_ROLE_ASSIGNMENT = RoleAssignment::class;

    const ROLE_READER = 'acdd72a7-3385-48ef-bd42-f606fba81ae7';
    const ROLE_CONTRIBUTOR = 'b24988ac-6180-42a0-ab88-20f7382dd24c';
    const ROLE_VM_CONTRIBUTOR = 'd73bb868-a0df-4d4d-bd69-98a00b01fccb';
    const ROLE_VNET_CONTRIBUTOR = 'b34d265f-36f7-4a0d-a4d4-e158ca92e90f';
    const ROLE_STORAGE_CONTRIBUTOR = '86e8f5dc-a6e9-4c67-9d15-de283e8eac25';
    const ROLE_WEBSITE_CONTRIBUTOR = 'de139f84-1756-47ae-9be6-808fbbe84772';
    const ROLE_WEBPLAN_CONTRIBUTOR = '2cc479cb-7b4d-49a8-b449-8c00fd0f0a4b';
    const ROLE_SQLSERVER_CONTRIBUTOR = '6d8ee4ec-f05a-4a1d-8b00-a9b17e38b437';
    const ROLE_SQLDB_CONTRIBUTOR = '9b7fa17d-e63e-47b0-bb0a-15c516ac86ec';

    public function createRoleAssignment($aadObjectGuid, $roleGuid) {
        $this->_enforceScope();
        // we are going to be putting a new role assignment in a list - first of all, we need to create a new GUID (random)
        $objectGuid = Uuid::uuid4()->toString();
        $url = $this->_currentScope.'/providers/Microsoft.Authorization/roleAssignments/'.$objectGuid.'?api-version=2015-07-01';
        $requestObject = new \stdClass();
        $requestObject->properties = new \stdClass();
        $requestObject->properties->principalId = $aadObjectGuid;
        $roleId = '/subscriptions/'.$this->_getSubscriptionGuidFromScope().'/providers/Microsoft.Authorization/roleDefinitions/'.$roleGuid;
        $requestObject->properties->roleDefinitionId = $roleId;
        // perform action:
        $response = $this->_put($url, $requestObject);
        if($response->hasError()) {
            if($response->getError()->errorCode()=='RoleAssignmentExists') {
                return $this->hasRole($aadObjectGuid, $roleGuid);
            }
            return FALSE;
        }
        return $this->_normaliseObjectResponse(
            $response->decodeResponseBody(),
            self::MAP_ROLE_ASSIGNMENT
        );
    }

    public function removeRoleAssignment(RoleAssignment $roleAssignment) {
        $url = $roleAssignment->id . '?api-version=2015-07-01';
        return !$this->_delete($url)->hasError();
    }

    /**
     * @param null $aadObjectGuid
     * @return RoleAssignment[]
     * @throws \Exception
     */
    public function getAssignedRoles($aadObjectGuid=NULL) {
        $this->_enforceScope();
        $url = $this->_currentScope . '/providers/Microsoft.Authorization/roleAssignments?api-version=2015-07-01';
        if(!empty($aadObjectGuid)) {
            $url .= '&$filter=principalId eq \'' . $aadObjectGuid . '\'';
        }
        return $this->_normaliseListResponse(
            $this->_get($url)->errorAsEmptyArray()->decodeResponseBody(),
            self::MAP_ROLE_ASSIGNMENT
        );
    }

    /**
     * @param string $aadObjectGuid
     * @param string|string[] $roleGuids
     * @return RoleAssignment|RoleAssignment[]
     */
    public function hasRole($aadObjectGuid, $roleGuids) {
        $roles = $this->getAssignedRoles($aadObjectGuid);
        if(!is_array($roleGuids)) {
            $roleGuids = [$roleGuids];
        }
        $result = [];
        foreach($roleGuids as $roleGuid) {
            $result[$roleGuid] = FALSE;
        }
        foreach($roles as $role) {
            if($role instanceof RoleAssignment) {
                foreach($result as $id=>$val) {
                    if($role->roleGuid==$id) {
                        $result[$id] = $role;
                    }
                }
            }
        }
        if(count($result)==1) {
            return reset($result);
        }
        return $result;
    }

    public function getPermissions() {
        $this->_enforceScope();
        $url = $this->_currentScope . '/providers/Microsoft.Authorization/permissions?api-version=2015-07-01';
        return $this->_get($url)->errorAsEmptyArray()->decodeResponseBody();
    }






}