<?php

namespace App\Connectors\MicrosoftAzureApi;


use GuzzleHttp\Psr7\Request;

class ResourceManagement extends AzureApiModule
{

    const MAP_SUBSCRIPTION = [
        'name'=>'displayName',
        'state',
        'location'=>'subscriptionPolicies.locationPlacementId',
        'quota'=>'subscriptionPolicies.quotaId',
        'subscriptionPolicies.spendingLimit'
    ];

    const MAP_LOCATION = [
        'displayName',
        'id',
        'latitude',
        'longitude',
        'name',
    ];

    public function getSubscriptionTenantGuid($subscription=NULL) {
        $url = '/subscriptions/' . $this->_getSubscriptionGuid($subscription) . '?api-version=2015-01-01';
        // just send an unauthenticated request to subscriptions endpoint:
        $response = $this->_get($url, ['withoutAuthToken'=>TRUE]);
        switch($response->getStatusCode()) {
            case 401:
                break;
            case 404:
                throw new \Exception('Invalid Subscription ID');
            default:
                throw new \Exception('Unknown error');
        }

        $authHeader = $response->response()->getHeaderLine('WWW-Authenticate');
        $regex = '/authorization_uri\="https\:\/\/login\.windows\.net\/('.self::REGEX_UUID.')"/';
        $matches = array();
        if(!preg_match($regex, $authHeader, $matches) OR count($matches)!==2) {
            throw new \Exception('Subscription ID is not associated with a tenant');
        }
        return $matches[1];
    }


    /**
     * @param null $subscription
     * @return mixed
     * @throws \Exception
     */
    public function getSubscription($subscription=NULL) {
        $url = '/subscriptions/' . $this->_getSubscriptionGuid($subscription) . '?api-version=2016-06-01';
        return $this->_normaliseObjectResponse(
            $this->_get($url)->errorAsEmptyArray()->decodeResponseBody(),
            self::MAP_SUBSCRIPTION
        );
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function listSubscriptions() {
        $url = '/subscriptions?api-version=2016-06-01';
        return $this->_normaliseListResponse(
            $this->_get($url)->errorAsEmptyArray()->decodeResponseBody(),
            self::MAP_SUBSCRIPTION
        );
    }

    public function listLocations() {
        $url = '/subscriptions/' . $this->_getSubscriptionGuid() . '/locations?api-version=2016-06-01';
        return $this->_normaliseListResponse(
            $this->_get($url)->errorAsEmptyArray()->decodeResponseBody(),
            self::MAP_LOCATION
        );
    }


}