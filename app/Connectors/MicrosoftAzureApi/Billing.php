<?php

namespace App\Connectors\MicrosoftAzureApi;


use App\Connectors\MicrosoftAzureApi\Billing\ResourceUsageQuery;
use Carbon\Carbon;

class Billing extends AzureApiModule
{


    /**
     * @param Carbon|NULL $startDate
     * @param Carbon|NULL $endDate
     * @return ResourceUsageQuery
     * @throws \Exception
     */
    public function resourceUsage(Carbon $startDate=NULL, Carbon $endDate=NULL) {
        $usageQuery = new ResourceUsageQuery($this->_getApi());
        $usageQuery->setGranularity('hourly');
        $usageQuery->resourceLevelDetail();
        $usageQuery->setStartDateTime((new Carbon())->subYear());
        $usageQuery->load();
        return $usageQuery;
    }

    /**
     * @param Carbon|NULL $startDate
     * @param Carbon|NULL $endDate
     * @return ResourceUsageQuery
     * @throws \Exception
     */
    public function serviceUsage(Carbon $startDate=NULL, Carbon $endDate=NULL) {
        $usageQuery = new ResourceUsageQuery($this->_getApi());
        $usageQuery->setGranularity('hourly');
        $usageQuery->serviceLevelDetail();
        $usageQuery->setStartDateTime((new Carbon())->subYear());
        $usageQuery->load();
        return $usageQuery;
    }

}