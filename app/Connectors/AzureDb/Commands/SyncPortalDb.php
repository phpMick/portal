<?php

namespace App\Connectors\AzureDb\Commands;

use App\Connectors\AzureDb\Jobs\SyncPortalDbJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SyncPortalDb extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:sync-portal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronise changes with portal database (both ways)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new SyncPortalDbJob();
        $job->cmd = $this;
        $this->dispatchNow($job);
    }
}
