<?php

namespace App\Connectors\AzureDb\Commands;

use App\Connectors\AzureDb\Jobs\CSP\LoadOldCspAzurePricesJob;
use App\Connectors\AzureDb\Jobs\CSP\UpdateCspAzurePricesJob;
use App\Connectors\AzureDb\Jobs\UpdateAzureRegionsJob;
use App\Connectors\AzureDb\Models\Region;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Initialise extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialise database from stored data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // check database is empty:
        $region_count = Region::count();
        if($region_count!=0) {
            $this->error('The database is not empty - cannot initialise!');
        }

        // populate regions:
        $job = new UpdateAzureRegionsJob();
        $job->cmd = $this;
        $this->dispatchNow($job);

        // load old prices:
        $job = new LoadOldCspAzurePricesJob();
        $job->cmd = $this;
        $this->dispatchNow($job);

        // load all stored prices:
        $files = $this->_getPriceAuditFiles();
        foreach($files as $file) {
            $job = new UpdateCspAzurePricesJob(
                $file->currency,
                $file->locale,
                $file->date
            );
            $job->dispatchChildJobs = FALSE;
            $job->cmd = $this;
            $this->dispatchNow($job);
        }


    }

    /**
     * @return array
     */
    protected function _getPriceAuditFiles() {
        $disk = Storage::disk('mspartner');
        $items = $disk->listContents('prices/azure');
        $files = array();
        foreach($items as $info) {
            if($info['type']!='file' OR $info['extension']!='log')
                continue;
            $m = array();
            if(preg_match('/^([A-Za-z]{2})_([A-Za-z]{3})_(\d{8})_(\d+)$/', $info['filename'], $m)) {
                $obj = new \stdClass();
                $obj->locale = $m[1];
                $obj->currency = $m[2];
                $obj->date = $m[3];
                $files[] = $obj;
            }
        }
        usort($files, function($a, $b) {
            if($a->date == $b->date) {
                if($a->currency==$b->currency) {
                    if($a->locale==$b->locale) {
                        return 0;
                    }
                    return ($a->locale < $b->locale) ? -1 : 1;
                }
                return ($a->currency < $b->currency) ? -1 : 1;
            }
            return ($a->date < $b->date) ? -1 : 1;
        });
        // iterate over array to ensure all values are unique (now they're in order, this is easy):
        $prev = new \stdClass();
        $prev->locale = '';
        $prev->currency = '';
        $prev->date = '';
        $output = array();
        // the "most" uppercase version will be first in the list
        foreach($files as $file) {
            if(strtoupper($file->locale)!=$prev->locale OR strtoupper($file->currency)!=$prev->currency OR strtoupper($file->date)!=$prev->date) {
                $output[] = $file;
                $prev = new \stdClass();
                $prev->locale = strtoupper($file->locale);
                $prev->currency = strtoupper($file->currency);
                $prev->date = strtoupper($file->date);
            }
        }
        return $output;
    }
}
