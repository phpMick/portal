<?php

namespace App\Connectors\AzureDb\Commands;

use App\Connectors\AzureDb\Jobs\UpdateAzureRegionsJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateRegions extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-regions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Azure regions from stored data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new UpdateAzureRegionsJob();
        $job->cmd = $this;
        $this->dispatchNow($job);
    }
}
