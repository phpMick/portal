<?php

namespace App\Connectors\AzureDb\Commands\CSP;

use App\Connectors\AzureDb\Jobs\CSP\UpdateCspCustomerSubscriptionsJob;
use App\Connectors\AzureDb\Models\CspCustomer;
use App\Connectors\AzureDb\Models\CspSubscriptionJob;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;

class UpdateCustomerSubscriptionList extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-csp-subscriptions {customer-id?} {--resume}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update CSP Subscriptions for a Customer or all customers from Partner Center API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        if(empty($this->argument('customer-id'))) {
            // check for resume flag (to continue on from an error)
            if($this->option('resume')) {
                // run for all CSP customers, except those that have had a completed response already today, or >3 errors
                $today = Carbon::today('UTC');
                $tomorrow = $today->copy()->addDay();
                // get list of completed customers:
                $completed_customers = CspSubscriptionJob::query()
                    ->select('customer_id')
                    ->where('start_time', '>=', $today)
                    ->where('end_time', '<', $tomorrow)
                    ->whereNull('error_message')
                    ->pluck('customer_id');
                // get list of customers who have had some errors, but not too many:
                $error_customers = CspSubscriptionJob::query()
                    ->select('customer_id')
                    ->where('start_time', '>=', $today)
                    ->where('end_time', '<', $tomorrow)
                    ->whereNotNull('error_message')
                    ->get()
                    ->groupBy('customer_id')->map(function($record) {
                        return $record->count();
                    })
                    ->filter(function ($value, $key) {
                        return $value > 3;
                    })
                    ->keys();
                // now get all customers which we still need to process today...
                $customers = CspCustomer::select(['id'])
                    ->get()
                    ->whereNotIn('id', $completed_customers)
                    ->whereNotIn('id', $error_customers);
            } else {
                // run for all CSP customers:
                $customers = CspCustomer::select(['id'])->get();
            }
            foreach ($customers as $cust) {
                $this->info('Getting subscriptions for customer # ' . $cust->id . '...');
                $job = new UpdateCspCustomerSubscriptionsJob($cust);
                $job->dispatchChildJobs = FALSE;
                $this->dispatchNow($job);
            }
        } else {
            $cust = intval($this->argument('customer-id'));
            $this->info('Getting subscriptions for customer # '.$cust.'...');
            $job = new UpdateCspCustomerSubscriptionsJob($cust);
            $job->dispatchChildJobs = FALSE;
            $this->dispatchNow($job);
        }
    }
}
