<?php

namespace App\Connectors\AzureDb\Commands\CSP;

use App\Connectors\AzureDb\Jobs\CSP\UpdateCspCustomersJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateCustomerList extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-csp-customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update CSP Customers from Partner Center API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new UpdateCspCustomersJob();
        $job->dispatchChildJobs = FALSE;
        $job->cmd = $this;
        $this->dispatchNow($job);
    }
}
