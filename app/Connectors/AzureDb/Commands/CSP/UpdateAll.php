<?php

namespace App\Connectors\AzureDb\Commands\CSP;

use App\Connectors\AzureDb\Jobs\CSP\UpdateCspAzurePricesJob;
use App\Connectors\AzureDb\Jobs\CSP\UpdateCspCustomersJob;
use App\Connectors\AzureDb\Jobs\CSP\UpdateCspCustomerSubscriptionsJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateAll extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-csp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all CSP data from Partner Center API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // update prices - default to GBP & UK
        $job = new UpdateCspAzurePricesJob();
        $job->cmd = $this;
        $this->dispatchNow($job);

        // update customers - this will also do their subscriptions & usage
        $job = new UpdateCspCustomersJob();
        $job->cmd = $this;
        $this->dispatchNow($job);

    }
}
