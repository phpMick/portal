<?php

namespace App\Connectors\AzureDb\Commands\CSP;

use App\Connectors\AzureDb\Jobs\CSP\GetCspAzureSubscriptionUsageOnDateJob;
use App\Connectors\AzureDb\Jobs\CSP\UpdateCspAzureSubscriptionUsageJob;
use App\Connectors\AzureDb\Jobs\CSP\UpdateCspAzureUsageJob;
use App\Connectors\AzureDb\Models\CspSubscription;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateAzureSubscriptionUsage extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azure:update-csp-usage {subscription-id?} {date?} {--force} {--rerun} {--load}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get CSP Azure Subscription usage from Partner Center API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        // find the subscription:
        $subid = $this->argument('subscription-id');
        $loadFromStore = (bool) $this->option('load');
        if(!empty($subid)) {
            // subscription has been specified - try to find it:
            $sub = NULL;
            if(is_numeric($subid)) {
                $sub = CspSubscription::find(intval($subid));
            } else {
                $sub = CspSubscription::findByGuid($subid);
            }
            if(!($sub instanceof CspSubscription)) {
                $this->error('Could not find subscription "'.$subid.'"');
                return FALSE;
            }
            // there might be a date specified:
            $date = $this->argument('date');
            if(!empty($date)) {
                if(!preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $date)) {
                    $this->error('Date must be in the format "YYYY-MM-DD"');
                    return FALSE;
                }
                $job = new GetCspAzureSubscriptionUsageOnDateJob($sub, $date, $loadFromStore);
                $job->dispatchChildJobs = FALSE;
                $job->cmd = $this;
                $this->dispatchNow($job);
            } else {
                // no date specified - dispatch update job:
                $job = new UpdateCspAzureSubscriptionUsageJob($sub, (bool) $this->option('rerun'), (bool) $this->option('force'), $loadFromStore);
                $job->dispatchChildJobs = TRUE;
                $job->cmd = $this;
                $this->dispatchNow($job);
            }
        } else {
            // no subscription defined - run for all
            $job = new UpdateCspAzureUsageJob((bool) $this->option('rerun'), (bool) $this->option('force'), $loadFromStore);
            $job->dispatchChildJobs = TRUE;
            $job->cmd = $this;
            $this->dispatchNow($job);
        }
    }
}
