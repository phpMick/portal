<?php


namespace App\Connectors\AzureDb;

use Carbon\Carbon;

/**
 * Class JobModel
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property integer $job_num
 * @property integer $records_processed
 * @property string $error_message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class JobModel extends Model
{

    protected $dates = [
        'start_time', 'end_time'
    ];

}