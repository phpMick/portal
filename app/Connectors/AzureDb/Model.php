<?php


namespace App\Connectors\AzureDb;

use Illuminate\Database\Eloquent\Model as EloquentModel;


class Model extends EloquentModel
{

    protected $connection = 'azure';

    /**
     * @var array Allow all attributes to be assignable as these models are not exposed externally
     */
    protected $guarded = [];

    protected function _snakeArray($array) {
        $res = [];
        foreach($array as $k=>$v) {
            if(is_array($v)) {
                $res[snake_case($k)] = $this->_snakeArray($v);
            } else {
                $res[snake_case($k)] = $v;
            }
        }
        return $res;
    }

    public function fillFromCamelCaseArray(array $attributes)
    {
        return parent::fill($this->_snakeArray($attributes));
    }


}