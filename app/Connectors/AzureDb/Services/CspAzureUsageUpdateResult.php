<?php

namespace App\Connectors\AzureDb\Services;


use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspUsageJob;
use Carbon\Carbon;

class CspAzureUsageUpdateResult
{
    /**
     * @var integer
     */
    public $processedRows = 0;

    /**
     * @var integer
     */
    public $deletedRows = 0;

    public $createdResources = 0;

    public $updatedResources = 0;

    public $createdRegions = 0;

    public $createdRegionLookups = 0;

    /**
     * @var Carbon
     */
    public $queryDate = NULL;

    /**
     * @var CspSubscription
     */
    public $subscription = NULL;

    public $decimalPrecision = NULL;

    /**
     * @var string
     */
    public $errorMessage;

    public $errorCount = 0;

    /**
     * @var CspUsageJob
     */
    public $job = NULL;

}