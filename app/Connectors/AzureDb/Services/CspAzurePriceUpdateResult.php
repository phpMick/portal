<?php

namespace App\Connectors\AzureDb\Services;


class CspAzurePriceUpdateResult
{

    public $processedRows = 0;
    public $createdCspMeters = 0;
    public $updatedCspMeters = 0;
    public $createdMeters = 0;
    public $createdRegionLookups = 0;
    public $createdRegions = 0;
    public $createdPrices = 0;
    public $updatedPrices = 0;

    public $errorMessage;

}