<?php

namespace App\Connectors\AzureDb\Services;


use App\Connectors\AzureDb\Models\CspMargin;
use App\Connectors\AzureDb\Models\CspMeter;
use App\Connectors\AzureDb\Models\CspPrice;
use App\Connectors\AzureDb\Models\CspPriceRate;
use App\Connectors\AzureDb\Models\CspResource;
use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspUsage;
use App\Connectors\AzureDb\Models\CspUsageJob;
use App\Connectors\AzureDb\Models\CspUsageJobError;
use App\Connectors\AzureDb\Models\Meter;
use App\Connectors\AzureDb\Models\Region;
use App\Connectors\AzureDb\Models\RegionLookup;
use App\Connectors\MicrosoftPartnerCenterApi\Models\AzureUtilisationRecord;
use App\Connectors\MicrosoftPartnerCenterApi\Models\AzureMeter;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CspAzureService
{

    const DEFAULT_MARGIN = NULL; // 0.15
    const DEFAULT_CURRENCY = 'GBP';

    /**
     * @var CspAzureUsageUpdateResult
     */
    protected $usageUpdateResult = NULL;

    /**
     * @var CspAzurePriceUpdateResult
     */
    protected $priceUpdateResult = NULL;

    protected $priceUpdateCurrency;
    protected $priceUpdateLocale;
    protected $priceUpdateTaxIncluded;
    protected $priceUpdateDiscount;
    protected $priceUpdateDiscountMultiplier;
    protected $priceUpdateNoDiscountMeterGuids;
    protected $priceUpdatePrecision;


    /**
     * @param Carbon $usageDate
     * @return string|boolean
     */
    protected function getMarginMultiplier(Carbon $usageDate) {
        $margins = CspMargin::where('customer_id', '=', $this->usageUpdateResult->subscription->customer_id)
            ->where(function ($query) use ($usageDate) {
                $query->whereNull('start_date')->orWhere('start_date', '<=', $usageDate->toDateString());
            })
            ->where(function ($query) use ($usageDate) {
                $query->whereNull('end_date')->orWhere('end_date', '>=', $usageDate->toDateString());
            })
            ->get();
        if($margins->count()==0) {
            $margin = self::DEFAULT_MARGIN;
        } elseif($margins->count()==1) {
            $margin = $margins->first()->margin;
        } else {
            return FALSE;
        }
        // table stores margins as decimal (9,4), which means we store it as a percentage number, e.g. 19.8731
        // the calculations in this service require the number as a normal multiplier, so we need to divide the number by 100
        // and add 1.0 (so it becomes 1.198731)
        // old code used to check if margin was under 1.0, in which case it added 1.0, but that shouldn't be used, since we
        // could be setting a tiny margin
        // the HTTP Controller code checks for inputs over 0.0, so the data in the table should be valid (no negative margins),
        // so no other checks required
        $margin = bcdiv($margin, 100, 10);
        return bcadd($margin, 1.0, 10);
    }


    /**
     * @param AzureUtilisationRecord $row
     * @param $which
     * @return array|boolean|NULL
     */
    protected function cleanArrayObject(AzureUtilisationRecord $row, $which) {
        switch(strtolower($which)) {
            case 'tag':
            case 'tags':
                $dataArray = $row->tags;
                $identifier = 'tag';
                break;
            case 'info':
            case 'resourceinfo':
                $dataArray = $row->resourceInfo;
                $identifier = 'info';
                break;
            default:
                $this->usageUpdateResult->errorMessage = 'Invalid code path (cleanObjectArray:which)';
                return FALSE;
        }
        if(is_array($dataArray)) {
            // ensure we only store string and non-empty values to save space and recreation of resources
            $outArray = array();
            foreach($dataArray as $tag=>$value) {
                if(!empty($value)) {
                    if(is_string($value)) {
                        $outArray[$tag] = $value;
                    } else {
                        $this->usageUpdateResult->errorMessage = 'Invalid '.$identifier.' value ['.json_encode($value).'] on resource "'.$row->resourceUri.'"';
                        return FALSE;
                    }
                }
            }
            if(count($outArray)==0) {
                return NULL;
            }
            ksort($outArray);
            return $outArray;
        } elseif(isset($dataArray)) {
            $this->usageUpdateResult->errorMessage = 'Invalid '.$identifier.' object ['.json_encode($dataArray).'] on resource "'.$row->resourceUri.'"';
            return FALSE;
        }
        return NULL;
    }

    /**
     * @param AzureUtilisationRecord $record
     * @return CspResource|NULL
     * @throws \Exception
     */
    protected function getResource(AzureUtilisationRecord $record) {
        // build out tags and info arrays in key order, then jsonify & hash to find them in the DB:
        $tagArray = $this->cleanArrayObject($record, 'tag');
        if($tagArray===FALSE) {
            return NULL;
        }
        //$tagArrayJson = json_encode($tagArray);
        //$tagArrayHash = is_null($tagArray) ? NULL : md5($tagArrayJson);
        $infoArray = $this->cleanArrayObject($record, 'info');
        if($infoArray===FALSE) {
            return NULL;
        }
        //$infoArrayJson = json_encode($infoArray);
        //$infoArrayHash = is_null($infoArray) ? NULL : md5($infoArrayJson);
        $uri_hash = md5($record->resourceUri);
        $detail_hash = md5(json_encode([
            'tags'=>$tagArray,
            'info'=>$infoArray
        ]));

        $region = $this->getRegion($record->resourceRegion);
        $query = CspResource::where('subscription_id', '=', $this->usageUpdateResult->subscription->id)
                    ->where('uri_hash','=', $uri_hash)
                    ->where('detail_hash', '=', $detail_hash)
                    ->where('region_id', '=', $region->id);
        $rows = $query->get();
        if($rows->count()==0) {
            // nothing matching, but see if there has been a record of this resource before:
            $count = CspResource::where('subscription_id', '='. $this->usageUpdateResult->subscription->id)
                    ->where('uri_hash', '=', $uri_hash)
                    ->count();
            // make new resource:
            $resource = new CspResource();
            $resource->subscription()->associate($this->usageUpdateResult->subscription);
            $resource->uri = $record->resourceUri;
            $resource->region()->associate($region);
            $resource->tags = $tagArray;
            $resource->info = $infoArray;
            $resource->uri_hash = $uri_hash;
            $resource->detail_hash = $detail_hash;
            if(!empty($record->marketplacePartNumber) OR !empty($record->marketplaceOrderNumber)) {
                $resource->marketplace = array('part'=>$record->marketplacePartNumber, 'order'=>$record->marketplaceOrderNumber);
            } else {
                $resource->marketplace = NULL;
            }
            $resource->save();
            if($count==0) {
                $this->usageUpdateResult->createdResources++;
            } else {
                $this->usageUpdateResult->updatedResources++;
            }
            return $resource;
        } elseif($rows->count()==1) {
            // only one matching row - use this:
            return $rows->first();
        } else {
            $this->usageUpdateResult->errorMessage = 'Multiple records exist for resource "'.$record->resourceUri.'"';
            return NULL;
        }
    }


    /**
     * @param string $regionName
     * @return Region
     * @throws \Exception
     */
    protected function getRegion($regionName) {
        if(empty($regionName)) {
            $regionName = 'Any';
        }
        $regionStr = strtolower($regionName);
        // look it up in the lookup table first:
        $dbrow = RegionLookup::findByName($regionStr);
        if(is_null($dbrow)) {
            // try and find a match in regions table:
            $regions = Region::where('short_name', '=', $regionName)
                ->orWhere('display_name', '=', $regionName)
                ->orWhere('sort_name', '=', $regionName)
                ->orWhere('arm_name', '=', $regionName)
                ->get();
            if($regions->count()==0) {
                // create something on the fly - we'll tidy it up manually at some point:
                $region = new Region();
                $region->short_name = $regionName;
                $region->display_name = $regionName;
                $region->save();
                if(isset($this->usageUpdateResult)) {
                    $this->usageUpdateResult->createdRegions++;
                }
                if(isset($this->priceUpdateResult)) {
                    $this->priceUpdateResult->createdRegions++;
                }
            } elseif($regions->count()==1) {
                $region = $regions->first();
            } else {
                throw new \Exception('unspecific region "'.$regionName.'"');
            }
            $dbrow = new RegionLookup();
            $dbrow->name = $regionStr;
            $dbrow->region()->associate($region);
            $dbrow->save();
            if(isset($this->usageUpdateResult)) {
                $this->usageUpdateResult->createdRegionLookups++;
            }
            if(isset($this->priceUpdateResult)) {
                $this->priceUpdateResult->createdRegionLookups++;
            }
        } else {
            $region = $dbrow->region;
        }
        return $region;
    }

    /**
     * @param AzureMeter $azureMeter
     * @param bool $readOnly
     * @return CspMeter
     * @throws \Exception
     */
    protected function getCspMeter(AzureMeter $azureMeter, $readOnly=FALSE) {
        $csp_meter = CspMeter::findByGuid($azureMeter->guid);
        if(!$readOnly) {
            if (is_null($csp_meter)) {
                // construct a new CspMeter
                $csp_meter = CspMeter::make($azureMeter->toArray('CspMeter'));
                // get or make a new region & associate it:
                $csp_meter->region()->associate($this->getRegion($azureMeter->region));
                // find or make a global meter & associate it:
                /** @var Meter $meter */
                $meter = Meter::findByGuid($azureMeter->guid);
                if (is_null($meter)) {
                    $meter = Meter::make($azureMeter->toArray('meter'));
                    $meter->region()->associate($csp_meter->region);
                    $meter->save();
                }
                $csp_meter->meter()->associate($meter);
                $csp_meter->save();
                $this->priceUpdateResult->createdCspMeters++;
            } else {
                // check if it needs to be updated:
                $csp_meter->fill($azureMeter->toArray('CspMeter'));
                if ($csp_meter->isDirty()) {
                    $csp_meter->save();
                    $this->priceUpdateResult->updatedCspMeters++;
                }
            }
        }
        return $csp_meter;
    }

    /**
     * @param string $guid
     * @return boolean
     */
    protected function isDiscountedMeterGuid($guid) {
        // check if the GUID is not found in the NoDiscount array:
        return array_search($guid, $this->priceUpdateNoDiscountMeterGuids)===FALSE ? TRUE : FALSE;
    }

    /**
     * @param string $guid
     * @return string
     */
    protected function getDiscount($guid) {
        if(array_search($guid, $this->priceUpdateNoDiscountMeterGuids)===FALSE) {
            // not found in excluded array, so it is discountable:
            return $this->priceUpdateDiscount;
        }
        return '0.0';
    }

    /**
     * @param string $guid
     * @param $price
     * @return string
     */
    protected function getDiscountedAmount($guid, $price) {
        if(array_search($guid, $this->priceUpdateNoDiscountMeterGuids)===FALSE) {
            // not found in excluded array, so it is discountable:
            return bcmul($price, $this->priceUpdateDiscountMultiplier, $this->priceUpdatePrecision);
        }
        return $price;
    }

    /**
     * @param string $guid
     * @param $price
     * @return string
     */
    protected function getRetailAmount($guid, $price) {
        if(array_search($guid, $this->priceUpdateNoDiscountMeterGuids)===FALSE) {
            // not found in excluded array, so it is discountable:
            //return bcmul($price, $this->priceUpdateDiscountMultiplier, $this->priceUpdatePrecision);
            return bcdiv($price, $this->priceUpdateDiscountMultiplier, $this->priceUpdatePrecision);
        }
        return $price;
    }

    /**
     * @param AzureMeter $azureMeter
     * @param CspMeter $cspMeter
     * @return CspPrice
     */
    protected function makeCspPrice(AzureMeter $azureMeter, CspMeter $cspMeter) {
        $price = new CspPrice();
        $price->meter()->associate($cspMeter);
        $price->currency = $this->priceUpdateCurrency;
        //$price->locale = $this->priceUpdateLocale;
        //$price->tax_included = $this->priceUpdateTaxIncluded;
        $price->start_date = $azureMeter->start_date->copy();
        if(!empty($azureMeter->tags)) {
            $price->tags = $azureMeter->tags;
        }
        $price->save(); // need to save for relationship building below
        // build out prices:
        $maxPrice = '0.0';
        $rateArray = array();
        $upliftFirst = FALSE;
        // using bccomp because all numbers should be stored as strings for accuracy, even this one
        if(bccomp($azureMeter->included_quantity,'0.0', $this->priceUpdatePrecision)!=0) {
            // there is an included (free) quantity - record it:
            $dbrow = new CspPriceRate();
            $dbrow->price()->associate($price);
            $dbrow->min_quantity = '0.0';
            $dbrow->estimate_price = '0.0';
            $dbrow->discount = $this->getDiscount($azureMeter->guid);
            $dbrow->estimate_cost_price = '0.0';
            $dbrow->save();
            $rateArray['included'] = $azureMeter->included_quantity;
            $upliftFirst = TRUE;
        }
        $first = TRUE;
        foreach($azureMeter->rates as $qty=>$cost) {
            $dbrow = new CspPriceRate();
            $dbrow->price()->associate($price);
            //$dbrow->estimate_price = $cost;
            $dbrow->estimate_price = $this->getRetailAmount($azureMeter->guid, $cost);
            $dbrow->discount = $this->getDiscount($azureMeter->guid);
            //$dbrow->estimate_cost_price = $this->getDiscountedAmount($azureMeter->guid, $cost);
            $dbrow->estimate_cost_price = $cost;
            if($first AND $upliftFirst) {
                $dbrow->min_quantity = $azureMeter->included_quantity;
                $first = FALSE;
            } else {
                $dbrow->min_quantity = $qty;
            }
            $dbrow->save();
            // add decimal value to ensure we use string keys
            if(strpos($qty, '.')===FALSE) {
                $qty .= '.0';
            }
            $rateArray[$qty] = $cost;
            if(bccomp($cost, $maxPrice, $this->priceUpdatePrecision)==1) {
                $maxPrice = $cost;
            }
        }
        $price->discount = $this->getDiscount($azureMeter->guid);
        //$price->estimate_price = $maxPrice;
        //$price->estimate_cost_price = $this->getDiscountedAmount($azureMeter->guid, $maxPrice);
        $price->estimate_cost_price = $maxPrice;
        $price->estimate_price = $this->getRetailAmount($azureMeter->guid, $maxPrice);
        $price->prices = $rateArray;
        $price->save();
        return $price;
    }

    /**
     * @param CspMeter $meter
     * @param Carbon $usageDate
     * @return CspPrice|NULL
     */
    protected function getCspPrice(CspMeter $meter, Carbon $usageDate) {
        $currency = optional($this->usageUpdateResult->subscription->customer->detail)->currency ?: self::DEFAULT_CURRENCY;
        $prices = CspPrice::where('csp_meter_id', '=', $meter->id)
            ->where('currency', '=', $currency)
            ->where(function ($query) use ($usageDate) {
                $query->whereNull('start_date')->orWhere('start_date', '<=', $usageDate->toDateString());
            })
            ->where(function ($query) use ($usageDate) {
                $query->whereNull('end_date')->orWhere('end_date', '>', $usageDate->toDateString());
            })
            ->get();
        if($prices->count()==0) {
            $this->usageUpdateResult->errorMessage = 'No CSP Price found for meter {'.$meter->guid.'} on date '.$usageDate->toDateString();
            return NULL;
        } elseif($prices->count()==1) {
            return $prices->first();
        } else {
            $this->usageUpdateResult->errorMessage = 'Multiple CSP Prices found for meter {'.$meter->guid.'} on date '.$usageDate->toDateString();
            return NULL;
        }
    }


    /**
     * @param string $currency
     * @param string $locale
     * @param boolean $taxIncluded
     * @param array $offerTerms
     * @return boolean
     */
    public function beginPriceUpdate($currency, $locale, $taxIncluded, $offerTerms) {
        $this->priceUpdateResult = new CspAzurePriceUpdateResult();
        if(strtolower($locale)!='en-us') {
            $this->priceUpdateResult->errorMessage = 'Unsupported locale in API response';
            return FALSE;
        }
        if($taxIncluded) {
            $this->priceUpdateResult->errorMessage = 'Tax is included in API response - this is not supported';
            return FALSE;
        }
        $this->priceUpdateCurrency = $currency;
        $this->priceUpdateLocale = $locale;
        $this->priceUpdateTaxIncluded = $taxIncluded;
        $this->priceUpdateDiscount = 0.0;
        $this->priceUpdateNoDiscountMeterGuids = array();
        if(is_array($offerTerms)) {
            if(array_key_exists('discount', $offerTerms)) {
                $this->priceUpdateDiscount = floatval($offerTerms['discount']);
            }
            if(array_key_exists('excludedMeterIds', $offerTerms)) {
                $this->priceUpdateNoDiscountMeterGuids = array();
                if(is_array($offerTerms['excludedMeterIds'])) {
                    foreach($offerTerms['excludedMeterIds'] as $guid) {
                        $this->priceUpdateNoDiscountMeterGuids[] = $guid;
                    }
                }
            }
        }
        $this->priceUpdatePrecision = $precision = config('app.decimal_high.scale');
        $this->priceUpdateDiscountMultiplier = bcsub('1.0', $this->priceUpdateDiscount, $this->priceUpdatePrecision);
    }

    /**
     * @param AzureMeter $azureMeter
     * @return boolean
     */
    public function updatePrice(AzureMeter $azureMeter) {
        assert('isset($this->priceUpdateResult)', 'Price Update Process must be started');
        $csp_meter = $this->getCspMeter($azureMeter);
        if(is_null($csp_meter)) {
            return FALSE;
        }
        // pull all existing prices for this meter - we may need to update or create them:
        $current_prices = CspPrice::where('csp_meter_id', '=', $csp_meter->id)
            ->where('currency', '=', $this->priceUpdateCurrency)
            //->where('locale', '=', $this->priceUpdateLocale)
            //->where('tax_included', '=', $this->priceUpdateTaxIncluded)
            ->whereNull('end_date')
            ->get();
        if($current_prices->count()==0) {
            // no price stored - create new:
            $this->makeCspPrice($azureMeter, $csp_meter);
            $this->priceUpdateResult->createdPrices++;
        } elseif($current_prices->count()==1) {
            // record found - update if appropriate, or create new and expire this one:
            /** @var CspPrice $csp_price */
            $csp_price = $current_prices->first();
            // check if start_date has been changed:
            if($azureMeter->start_date->gt($csp_price->start_date)) {
                // yes - update curent_price to end, and create new record:
                $csp_price->end_date = $azureMeter->start_date->copy();
                $csp_price->save();
                $this->makeCspPrice($azureMeter, $csp_meter);
                $this->priceUpdateResult->updatedPrices++;
            }
            // otherwise, there hasn't been a change that we need to record
        } else {
            $this->priceUpdateResult->errorMessage = 'Multiple current CSP prices saved for meter = {'.$azureMeter->guid.'}';
            return FALSE;
        }
        $this->priceUpdateResult->processedRows++;
        return TRUE;
    }

    /**
     * @return CspAzurePriceUpdateResult
     */
    public function endPriceUpdate() {
        assert('isset($this->priceUpdateResult)', 'Price Update Process must be started');
        $result = $this->priceUpdateResult;
        $this->priceUpdateResult = NULL;
        $this->priceUpdateCurrency = NULL;
        $this->priceUpdateLocale = NULL;
        $this->priceUpdateTaxIncluded = NULL;
        return $result;
    }

    /**
     * @param CspSubscription $subscription
     * @param Carbon|string $queryDate
     * @param CspUsageJob $job
     * @return boolean
     */
    public function beginUsageUpdate(CspSubscription $subscription, $queryDate, CspUsageJob $job) {
        $this->usageUpdateResult = new CspAzureUsageUpdateResult();
        if(!isset($subscription)) {
            $this->usageUpdateResult->errorMessage = 'Subscription has not been set';
            return FALSE;
        }
        if(!$subscription->exists) {
            $this->usageUpdateResult->errorMessage = 'Subscription has not been saved to the database';
            return FALSE;
        }
        $this->usageUpdateResult->subscription = $subscription;
        if(is_string($queryDate)) {
            if(!preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $queryDate)) {
                $this->usageUpdateResult->errorMessage = 'Date is not in the correct format (YYYY-MM-DD)';
                return FALSE;
            }
            $this->usageUpdateResult->queryDate = new Carbon($queryDate, 'UTC');
            if($this->usageUpdateResult->queryDate->toDateString()!=$queryDate) {
                $this->usageUpdateResult->errorMessage = 'Error converting date string to date object';
                return FALSE;
            }
        } elseif($queryDate instanceof Carbon) {
            $this->usageUpdateResult->queryDate = $queryDate->copy()->startOfDay();
        }
        if(is_null($job) OR !$job->exists) {
            $this->usageUpdateResult->errorMessage = 'Job object not saved to database';
            return FALSE;
        }
        $this->usageUpdateResult->job = $job;
        $this->usageUpdateResult->decimalPrecision = config('app.decimal_high.scale');
        // setup complete - delete all usage records for this subscription & query date:
        $updated_time = $this->usageUpdateResult->job->start_time->toDateTimeString();
        $this->usageUpdateResult->deletedRows = DB::connection('azure')->table('csp_usages')
            ->where('reported_date', '=', $this->usageUpdateResult->queryDate)
            ->where('subscription_id', '=', $this->usageUpdateResult->subscription->id)
            ->update(['deleted'=>1, 'updated_at'=>$updated_time]);
        return TRUE;
    }

    /**
     * @param AzureUtilisationRecord $usageRecord
     * @return AzureMeter
     */
    protected function makeAzureMeter(AzureUtilisationRecord $usageRecord) {
        $meter = new AzureMeter();
        $meter->guid = $usageRecord->meterGuid;
        $meter->name = $usageRecord->meterName;
        $meter->sub_category = $usageRecord->meterSubCategory;
        $meter->category = $usageRecord->meterCategory;
        $meter->region = $usageRecord->meterRegion;
        $meter->unit_of_measure = $usageRecord->unitOfMeasure;
        return $meter;
    }

    /**
     * @param string $errorType
     * @param string|NULL $errorMessage
     * @param integer|NULL $usageRow
     * @return CspUsageJobError
     */
    protected function makeUsageError($errorType='unknown', $errorMessage=NULL, $usageRow=NULL) {
        $error = new CspUsageJobError();
        $error->job_id = $this->usageUpdateResult->job->id;
        $error->error_type = $errorType;
        if(empty($errorMessage)) {
            if(!empty($this->usageUpdateResult->errorMessage)) {
                $error->error_detail = $this->usageUpdateResult->errorMessage;
                $this->usageUpdateResult->errorMessage = NULL;
            } else {
                $error->error_detail = 'UNKNOWN ERROR';
            }
        } else {
            $error->error_detail = $errorMessage;
        }
        $error->affected_row = $usageRow;
        $this->usageUpdateResult->errorCount++;
        return $error;
    }

    /**
     * @param AzureUtilisationRecord $usageRecord
     * @return CspUsageJobError|boolean
     */
    public function addUsageRecord(AzureUtilisationRecord $usageRecord) {
        assert('isset($this->usageUpdateResult)', 'Usage update must be started');
        if(!empty($this->usageUpdateResult->errorMessage)) {
            return FALSE;
        }
        $usageDateStr = $usageRecord->usageStartTime->toDateString();
        if($usageDateStr!=$usageRecord->usageEndTime->toDateString()) {
            // it's ok if the end time is the start of the next day, so check that scenario too:
            if($usageDateStr!=$usageRecord->usageEndTime->copy()->subSecond()->toDateString()) {
                return $this->makeUsageError(
                    'dates',
                    'Usage start and end times are not on the same date'
                );
            }
        }
        $usageDate = new Carbon($usageDateStr, 'UTC');
        // get or create the resource:
        $resource = $this->getResource($usageRecord);
        if(!isset($resource)) {
            return $this->makeUsageError('resource'); // grabs error out of result object
        }
        // get the meter:
        $cspMeter = $this->getCspMeter($this->makeAzureMeter($usageRecord), TRUE);
        if(!isset($cspMeter)) {
            return $this->makeUsageError('meter', 'Resource "'.$usageRecord->resourceUri.'": meter {'.$usageRecord->meterGuid.'} does not exist');
        }
        // get correct price record:
        $price = $this->getCspPrice($cspMeter, $usageDate);
        if(!isset($price)) {
            return $this->makeUsageError('price');
        }
        // get the customer's margin for the usage date:
        $margin = $this->getMarginMultiplier($usageDate);
        if($margin===FALSE) {
            return $this->makeUsageError('margin', 'Multiple margins set for customer for date '.$usageDateStr);
        }
        // we now have everything we need to create the record:
        $usage = new CspUsage();
        $usage->subscription()->associate($this->usageUpdateResult->subscription);
        $usage->resource()->associate($resource);
        $usage->reported_date = $this->usageUpdateResult->queryDate;
        $usage->usage_date = $usageDate;
        //$usage->usage_start_time = $usageRecord->usageStartTime;
        //$usage->usage_end_time = $usageRecord->usageEndTime;
        $usage->meter()->associate($cspMeter);
        $usage->price()->associate($price);
        $usage->quantity = $usageRecord->consumedQuantity;
        $usage->estimate_cost = bcmul($usage->quantity, $price->estimate_cost_price, $this->usageUpdateResult->decimalPrecision);
        $usage->estimate_price = isset($margin) ? bcmul($usage->estimate_cost, $margin, $this->usageUpdateResult->decimalPrecision) : NULL;
        $usage->save();
        $this->usageUpdateResult->processedRows++;
        return TRUE;
    }

    /**
     * @return CspAzureUsageUpdateResult
     */
    public function endUsageUpdate() {
        assert('isset($this->usageUpdateResult)', 'Usage update must be started');
        return $this->usageUpdateResult;
    }

}

