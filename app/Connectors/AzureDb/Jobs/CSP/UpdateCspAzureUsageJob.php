<?php

namespace App\Connectors\AzureDb\Jobs\CSP;


use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\Common\Job;

class UpdateCspAzureUsageJob extends Job
{

    protected $_forceAll;

    protected $_rerunErrors;

    protected $_loadFromStore;

    /**
     * Create a new job instance.
     *
     * @param bool $rerunErrors
     * @param bool $forceAll
     * @param bool $loadFromStore
     */
    public function __construct($rerunErrors=FALSE, $forceAll=FALSE, $loadFromStore=FALSE)
    {
        $this->_forceAll = (bool) $forceAll;
        $this->_rerunErrors = (bool) $rerunErrors;
        $this->_loadFromStore = (bool) $loadFromStore;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->_forceAll) {
            optional($this->cmd)->info('### Refreshing all CSP Azure subscriptions usage data ###');
        } else {
            optional($this->cmd)->info('### Updating all CSP Azure subscriptions usage data ###');
        }
        // get a list of subscriptions from the database
        /** @var CspSubscription $sub */
        foreach(CspSubscription::get() as $sub) {
            $job = new UpdateCspAzureSubscriptionUsageJob($sub, $this->_rerunErrors, $this->_forceAll, $this->_loadFromStore);
            $job->dispatchChildJobs = TRUE;
            $job->cmd = $this->cmd;
            $this->dispatchNextJob($job);
        }
    }
}
