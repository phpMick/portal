<?php

namespace App\Connectors\AzureDb\Jobs\CSP;


use App\Connectors\AzureDb\Models\CspPriceJob;
use App\Connectors\AzureDb\Services\CspAzureService;
use App\Connectors\Common\Job;
use App\Connectors\MicrosoftPartnerCenterApi;
use App\Connectors\MicrosoftPartnerCenterApi\Models\AzureMeter;
use Carbon\Carbon;

class LoadOldCspAzurePricesJob extends Job
{


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $job = NULL;
        optional($this->cmd)->info('Loading old CSP prices...');
        try {
            // get data from json file:
            $filename = __DIR__ . DIRECTORY_SEPARATOR . 'old_csp_prices.json';
            if(!file_exists($filename)) {
                throw new \Exception('Source prices JSON file does not exist');
            }
            $json = file_get_contents($filename);
            if($json===FALSE) {
                throw new \Exception('Failed to read JSON file');
            }

            $data = json_decode($json, TRUE);
            if (!is_array($data) OR !array_key_exists('data', $data) OR !is_array($data['data'])) {
                throw new \Exception('Invalid JSON file');
            }

            $azSvc = new CspAzureService();

            foreach($data['data'] as $index=>$price_file) {
                // create a record of this call:
                $job = new CspPriceJob();
                $job->job_num = optional($this->job)->getJobId();
                $job->start_time = new Carbon();
                $job->save();

                // ensure header data is complete:
                if(!array_key_exists('header', $price_file) OR !is_array($price_file['header'])) {
                    throw new \Exception('No header present in prices file (index = '.$index.')');
                }
                foreach (['currency', 'locale', 'tax_included', 'offer_terms'] as $prop) {
                    if (!array_key_exists($prop, $price_file['header'])) {
                        throw new \Exception('Header incomplete in prices file (missing "'.$prop.'")');
                    }
                }
                if(!array_key_exists('items', $price_file) OR !is_array($price_file['items'])) {
                    throw new \Exception('No price items present in prices file (index = '.$index.')');
                }
                optional($this->cmd)->info(' > Processing prices in '.$price_file['header']['currency'].'...');
                optional($this->cmd)->info(' >> Meters to process: ' . count($price_file['items']));

                // initialise the price update service:
                $azSvc->beginPriceUpdate(
                    $price_file['header']['currency'],
                    $price_file['header']['locale'],
                    $price_file['header']['tax_included'],
                    $price_file['header']['offer_terms']
                );
                // record the info for posterity:
                $job->source_date = array_key_exists('source_date', $price_file['header']) ? new Carbon($price_file['header']['source_date']) : NULL;
                $job->currency = $price_file['header']['currency'];
                $job->save();

                // iterate over price records:
                if (count($price_file['items']) > 0) {
                    optional(optional($this->cmd)->getOutput())->write(' >>> ');
                    $i = 0;
                    foreach ($price_file['items'] as $item) {
                        $meter = new AzureMeter($item);
                        if (!$azSvc->updatePrice($meter)) {
                            break;
                        }
                        if ($i % 500 == 0) {
                            optional(optional($this->cmd)->getOutput())->write($i);
                        } elseif ($i % 50 == 0) {
                            optional(optional($this->cmd)->getOutput())->write('.');
                        }
                        $i++;
                    }
                    optional(optional($this->cmd)->getOutput())->write('', TRUE);
                }
                // end price update service & record results:
                $result = $azSvc->endPriceUpdate();
                $job->end_time = new Carbon();
                $job->records_processed = $result->processedRows;
                $job->new_prices = $result->createdPrices;
                $job->updated_prices = $result->updatedPrices;
                $job->new_csp_meters = $result->createdCspMeters;
                $job->updated_csp_meters = $result->updatedCspMeters;
                $job->new_meters = $result->createdMeters;
                $job->new_region_lookups = $result->createdRegionLookups;
                $job->new_regions = $result->createdRegions;
                $job->error_message = $result->errorMessage;
                $job->save();
            }
        } catch(\Exception $e) {
            if($job instanceof CspPriceJob) {
                $job->end_time = new Carbon();
                $job->error_message = $e->getMessage();
                $job->save();
            } else {
                throw $e;
            }
        }
    }
}
