<?php

namespace App\Connectors\AzureDb\Jobs\CSP;


use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspUsageJob;
use App\Connectors\Common\Job;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class UpdateCspAzureSubscriptionUsageJob
 * This job ensures that all available usage data is downloaded for the specified subscription
 * @package App\Connectors\AzureDb\Jobs\CSP]
 */
class UpdateCspAzureSubscriptionUsageJob extends Job
{

    protected $_subscription_id;

    protected $_forceAll;

    protected $_rerunErrors;

    protected $_loadFromStore;

    /**
     * Create a new job instance.
     *
     * @param $subscription
     * @param bool $rerunErrors
     * @param bool $forceAll
     * @param bool $loadFromStore
     * @throws \Exception
     */
    public function __construct($subscription, $rerunErrors=FALSE, $forceAll=FALSE, $loadFromStore=FALSE)
    {
        if($subscription instanceof CspSubscription) {
            $this->_subscription_id = $subscription->id;
        } elseif(is_integer($subscription)) {
            $this->_subscription_id = $subscription;
        } else {
            throw new \Exception('Invalid parameter');
        }
        $this->_forceAll = (bool) $forceAll;
        $this->_rerunErrors = (bool) $rerunErrors;
        $this->_loadFromStore = (bool) $loadFromStore;
    }

    /**
     * @param CspSubscription $subscription
     * @return string[]|Collection
     */
    protected function getQueryDates(CspSubscription $subscription) {
        $start_date = $subscription->effective_start_date->copy()->timezone('UTC')->startOfDay();
        $end_date = (new Carbon())->timezone('UTC')->startOfDay()->subDay();
        if($this->_forceAll) {
            // we want to refresh all dates in the range
            return DB::connection('azure')
                ->table('dates')
                ->where('date', '>=', $start_date->toDateString())
                ->where('date', '<=', $end_date->toDateString())
                ->pluck('date');
        } else {
            $rerun = $this->_rerunErrors;
            // we want to refresh all dates in the range for which a job has not completed or not marked old
            $query = DB::connection('azure')
                ->table('dates AS d')
                //->leftJoin('csp_usage_jobs AS j', 'd.date', '=', 'j.query_date')
                ->leftJoin('csp_usage_jobs AS j', function ($join) use($subscription) {
                    $join->on('d.date', '=', 'j.query_date')->where('j.subscription_id', '=', $subscription->id);
                })
                ->where('d.date', '>=', $start_date->toDateString())
                ->where('d.date', '<=', $end_date->toDateString())
                ->where(function ($q1) use ($rerun) {
                    $q1->whereNull('j.id') // no job has been run
                        ->orWhere(function ($q2) use ($rerun) {
                            $q2->where('j.old', '=', 0) // job is not marked old (this should restrict results to a single job per date)
                                ->where(function ($q3) use ($rerun) {
                                    $q3->whereNull('j.end_time'); // job didn't complete
                                    if($rerun) {
                                        $q3->orWhereNotNull('j.error_message') // job encountered an error
                                            ->orWhere('j.error_count', '>', 0); // job encountered an error
                                    }
                            });
                        });
                })
                ->select(['d.date'])
                ->distinct();
//            dd( $query->toSql());
            return $query->pluck('d.date');
        }
    }


    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // get subscription from DB:
        /** @var CspSubscription $subscription */
        $subscription = CspSubscription::findOrFail($this->_subscription_id);
        /*
        if($this->_forceAll) {
            $query_date = $subscription->effective_start_date->copy();
            optional($this->cmd)->info('Refreshing Azure usage for subscription {'.$subscription->guid.'} for all dates');
        } else {
            $query_date = CspUsageJob::where('subscription_id', '=', $subscription->id)
                ->whereNull('error_message')
                ->whereNotNull('end_time')
                ->max('query_date');
            if(is_null($query_date)) {
                $query_date = $subscription->effective_start_date->copy();
                optional($this->cmd)->info('Getting Azure usage for subscription {'.$subscription->guid.'} since it was created ('.$query_date->toDateString().')');
            } else {
                // add one day to the max queried date, as the max was the last completed date (as end_time!=NULL && error_message==NULL):
                $query_date = (new Carbon($query_date, 'UTC'))->addDay();
                optional($this->cmd)->info('Getting Azure usage for subscription {'.$subscription->guid.'} since the last update ('.$query_date->toDateString().')');
            }
        }
        $max_date = (new Carbon())->setTimezone('UTC')->startOfDay()->subDay();
        while($query_date->lte($max_date)) {
            $job = new GetCspAzureSubscriptionUsageOnDateJob($subscription, $query_date);
            $job->dispatchChildJobs = $this->dispatchChildJobs;
            $job->cmd = $this->cmd;
            $this->dispatchNextJob($job);
            $query_date->addDay();
        }
        */
        $dates = $this->getQueryDates($subscription);
        if($this->_forceAll) {
            optional($this->cmd)->info('Refreshing CSP Azure usage for subscription {'.$subscription->guid.'} - '.$dates->count().' dates to query');
        } else {
            optional($this->cmd)->info('Updating CSP Azure usage for subscription {'.$subscription->guid.'} - '.$dates->count().' dates to query');
        }

        foreach($dates as $date) {
            $job = new GetCspAzureSubscriptionUsageOnDateJob($subscription, $date, $this->_loadFromStore);
            $job->cmd = $this->cmd;
            $this->dispatchNextJob($job);
        }
    }
}
