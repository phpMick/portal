<?php

namespace App\Connectors\AzureDb\Jobs\CSP;


use App\Connectors\AzureDb\Models\CspPriceJob;
use App\Connectors\AzureDb\Services\CspAzureService;
use App\Connectors\Common\Job;
use App\Connectors\MicrosoftPartnerCenterApi;
use Carbon\Carbon;

class UpdateCspAzurePricesJob extends Job
{

    protected $_currency;
    protected $_region;
    protected $_loadFromStoreDate;

    /**
     * Create a new job instance.
     *
     * @param string $currency
     * @param string $region
     * @param null $loadFromStoreDate
     */
    public function __construct($currency='GBP', $region='GB', $loadFromStoreDate=NULL)
    {
        $this->_currency = empty($currency) ? 'GBP' : $currency;
        $this->_region = empty($region) ? 'GB' : $region;
        $this->_loadFromStoreDate = $loadFromStoreDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // create a record of this call:
        $job = new CspPriceJob();
        $job->job_num = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->currency = $this->_currency;
        if(is_string($this->_loadFromStoreDate)) {
            $job->source_date =  new Carbon($this->_loadFromStoreDate);
        } elseif ($this->_loadFromStoreDate instanceof Carbon) {
            $job->source_date = $this->_loadFromStoreDate->copy()->startOfDay();
        } else {
            $job->source_date = Carbon::today();
        }
        $job->save();

        $msg = 'Updating CSP prices ('.$this->_currency.' - '.$this->_region.')';
        if(is_string($this->_loadFromStoreDate)) {
            if(strlen($this->_loadFromStoreDate)==8) {
                $msg .= ' for date: '.substr($this->_loadFromStoreDate, 0, 4).'-'.substr($this->_loadFromStoreDate, 4, 2).'-'.substr($this->_loadFromStoreDate, 6, 2);
            } else {
                $msg .= ' for date: '.$this->_loadFromStoreDate;
            }
        } elseif($this->_loadFromStoreDate instanceof Carbon) {
            $msg .= ' for date: '.$this->_loadFromStoreDate->format('Y-m-d');
        } else {
            $msg .= ' for today';
        }
        optional($this->cmd)->info($msg.'...');

        // get pricesheet from API:
        $api = new MicrosoftPartnerCenterApi();
//        $api->enableCaching(60*24*7);
        $info = array();
        $meters = $api->Billing()->azureRateCard($info, $this->_currency, $this->_region, $this->_loadFromStoreDate);
        foreach(['currency', 'locale', 'tax_included', 'offer_terms'] as $prop) {
            if (!array_key_exists($prop, $info)) {
                throw new \Exception('Failed to retrieve data');
            }
        }

        optional($this->cmd)->info(' > Meters to process: '.count($meters));
        $azSvc = new CspAzureService();
        $azSvc->beginPriceUpdate($info['currency'], $info['locale'], $info['tax_included'], $info['offer_terms']);
        if(count($meters)>0) {
            optional(optional($this->cmd)->getOutput())->write(' >> ');
            $i = 0;
            foreach ($meters as $meter) {
                if (!$azSvc->updatePrice($meter)) {
                    break;
                }
                if ($i % 500 == 0) {
                    optional(optional($this->cmd)->getOutput())->write($i);
                } elseif ($i % 50 == 0) {
                    optional(optional($this->cmd)->getOutput())->write('.');
                }
                $i++;
            }
            optional(optional($this->cmd)->getOutput())->write('', TRUE);
        }
        $result = $azSvc->endPriceUpdate();
        $job->end_time = new Carbon();
        $job->records_processed = $result->processedRows;
        $job->new_prices = $result->createdPrices;
        $job->updated_prices = $result->updatedPrices;
        $job->new_csp_meters = $result->createdCspMeters;
        $job->updated_csp_meters = $result->updatedCspMeters;
        $job->new_meters = $result->createdMeters;
        $job->new_region_lookups = $result->createdRegionLookups;
        $job->new_regions = $result->createdRegions;
        $job->error_message = $result->errorMessage;
        $job->save();
    }
}
