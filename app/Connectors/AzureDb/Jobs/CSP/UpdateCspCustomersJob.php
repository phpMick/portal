<?php

namespace App\Connectors\AzureDb\Jobs\CSP;

use App\Connectors\AzureDb\Models\CspCustomer;
use App\Connectors\AzureDb\Models\CspCustomerJob;
use App\Connectors\Common\Job;
use App\Connectors\MicrosoftPartnerCenterApi;
use Carbon\Carbon;

class UpdateCspCustomersJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle() {
        // create a record of this call:
        $job = new CspCustomerJob();
        $job->job_num = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->save();

        optional($this->cmd)->info('Updating CSP customer list...');

        $api = new MicrosoftPartnerCenterApi();
        $query = $api->Customers()->customers();
        $i = 0;
        $job->new = 0;
        $job->updated = 0;
        foreach($query as $customer) {
            /** @var CspCustomer $dbrow */
            $dbrow = CspCustomer::findByGuid($customer->guid);
            if(is_null($dbrow)) {
                $dbrow = CspCustomer::create($customer->toArray());
                $job->new++;
            } else {
                $dbrow->fill($customer->toArray());
                if($dbrow->isDirty()) {
                    $dbrow->save();
                    $job->updated++;
                }
            }
            // for each customer, we need to retrieve a list of all their subscriptions:
            $subjob = new UpdateCspCustomerSubscriptionsJob($dbrow);
            $subjob->cmd = $this->cmd;
            $this->dispatchNextJob($subjob);
            $i++;
        }
        $job->records_processed = $i;
        $job->end_time = new Carbon();
        $job->save();
    }
}
