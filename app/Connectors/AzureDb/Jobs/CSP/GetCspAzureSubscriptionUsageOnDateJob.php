<?php

namespace App\Connectors\AzureDb\Jobs\CSP;

use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspUsageJob;
use App\Connectors\AzureDb\Models\CspUsageJobError;
use App\Connectors\AzureDb\Services\CspAzureService;
use App\Connectors\Common\Job;
use App\Connectors\MicrosoftPartnerCenterApi;
use Carbon\Carbon;

class GetCspAzureSubscriptionUsageOnDateJob extends Job
{

    /**
     * @var integer
     */
    protected $_subscription_id;

    /**
     * @var string
     */
    protected $_date;

    /**
     * @var boolean
     */
    protected $loadFromStore;

    /**
     * Create a new job instance.
     *
     * @param $subscription
     * @param $date
     * @param bool $loadFromStore
     * @throws \Exception
     */
    public function __construct($subscription, $date, $loadFromStore=FALSE) {
        if($subscription instanceof CspSubscription) {
            $this->_subscription_id = $subscription->id;
        } elseif(is_int($subscription)) {
            $this->_subscription_id = $subscription;
        } elseif(is_string($subscription)) {
            $sub = CspSubscription::findByGuid($subscription);
            if($sub instanceof CspSubscription) {
                $this->_subscription_id = $sub->id;
            } else {
                throw new \Exception('Subscription not found');
            }
        } else {
            throw new \Exception('Invalid subscription parameter');
        }
        if($date instanceof Carbon) {
            $this->_date = $date->toDateString();
        } elseif(is_string($date)) {
            if(preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $date)) {
                $this->_date = $date;
            } else {
                throw new \Exception('Invalid date format');
            }
        } else {
            throw new \Exception('Invalid parameter');
        }
        $this->loadFromStore = $loadFromStore;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $queryDate = (new Carbon($this->_date, 'UTC'))->startOfDay();
        // locate any old records of job for this subscription & query date so we can retire them:
        $jobs = CspUsageJob::findBySubscriptionQueryDate($this->_subscription_id, $queryDate, FALSE);
        /** @var CspUsageJob $j */
        foreach ($jobs as $j) {
            $j->old = TRUE;
            $j->save();
        }
        // create a new record of this call:
        $job = new CspUsageJob();
        $job->subscription_id = $this->_subscription_id;
        $job->query_date = $queryDate;
        $job->job_num = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->save();

        /** @var CspSubscription $subscription */
        $subscription = CspSubscription::findOrFail($this->_subscription_id);
        optional($this->cmd)->info(' > Getting Azure usage for subscription {'.$subscription->guid.'} reported on '.$queryDate->toDateString().'...');
        $azSvc = new CspAzureService();

        try {
            if ($azSvc->beginUsageUpdate($subscription, $queryDate, $job)) {
                $api = new MicrosoftPartnerCenterApi();
                if($this->loadFromStore) {
                    $api->loadFromAuditStore(TRUE);
                }
//                $api->enableCaching(120);
                $query = $api->Billing()->azureUsage($subscription->customer->tenant_guid, $subscription->guid);
                $query->forDate($queryDate);

                foreach ($query as $row) {
                    $rowResult = $azSvc->addUsageRecord($row);
                    if($rowResult instanceof CspUsageJobError) {
                        $rowResult->save(); // transient error for a row
                    } elseif($rowResult===FALSE) {
                        break; // fatal error - no more rows can be processed (error held in service to be retrieved at end)
                    } // otherwise TRUE - continue normally
                }
            }
            $result = $azSvc->endUsageUpdate();
        } catch(\Exception $e) {
            $result = $azSvc->endUsageUpdate();
            if(is_null($result->errorMessage)) {
                $result->errorMessage = $this->getExceptionString($e);
            }
        }

        $job->end_time = new Carbon();
        $job->records_processed = $result->processedRows;
        $job->records_deleted = $result->deletedRows;
        $job->new_resources = $result->createdResources;
        $job->updated_resources = $result->updatedResources;
        $job->new_region_lookups = $result->createdRegionLookups;
        $job->new_regions = $result->createdRegions;
        $job->error_message = $result->errorMessage;
        if($result->errorCount==0 AND !empty($result->errorMessage)) {
            $job->error_count = 1;
        } else {
            $job->error_count = $result->errorCount;
        }
        $job->save();
    }
}
