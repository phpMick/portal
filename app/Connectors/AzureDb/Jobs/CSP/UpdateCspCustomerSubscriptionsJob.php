<?php

namespace App\Connectors\AzureDb\Jobs\CSP;

use App\CloudServices\Azure\Models\Subscription;
use App\Connectors\AzureDb\Models\CspCustomer;
use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspSubscriptionJob;
use App\Connectors\Common\Job;
use App\Connectors\MicrosoftPartnerCenterApi;
use Carbon\Carbon;

class UpdateCspCustomerSubscriptionsJob extends Job
{

    protected $_customer_id;

    /**
     * Create a new job instance.
     *
     * @param $customer
     * @throws \Exception
     */
    public function __construct($customer)
    {
        if($customer instanceof CspCustomer) {
            $this->_customer_id = $customer->id;
        } elseif(is_integer($customer)) {
            // don't bother checking if it exists - it'll fail when handling
            $this->_customer_id = $customer;
        } else {
            throw new \Exception('Invalid paramter');
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle() {
        // create a record of this call:
        $job = new CspSubscriptionJob();
        $job->customer_id = $this->_customer_id;
        $job->job_num = optional($this->job)->getJobId();
        $job->start_time = new Carbon();
        $job->save();

        try {

            /** @var CspCustomer $customer */
            $customer = CspCustomer::findOrFail($this->_customer_id);
            $api = new MicrosoftPartnerCenterApi();
            optional($this->cmd)->info('Getting subscriptions for customer {' . $customer->guid . '} - ' . $customer->name);

            $subs = $api->Customers()->getSubscriptions($customer->guid);
            $i = 0;
            $job->records_processed = 0;
            $job->new_azure = 0;
            $job->updated_azure = 0;
            $job->inactive_azure = 0;
            $job->active_azure = 0;
            foreach ($subs as $sub) {
                // we only care about azure subscriptions...
                if ($sub->isAzure()) {
                    /** @var CspSubscription $dbrow */
                    $dbrow = CspSubscription::findByGuid($sub->guid);
                    if (is_null($dbrow)) {
                        $dbrow = CspSubscription::make($sub->toArray());
                        $dbrow->customer()->associate($customer);
                        $dbrow->save();
                        $job->new_azure++;
                    } else {
                        $dbrow->fill($sub->toArray());
                        // check if anything has changed:
                        if ($dbrow->isDirty()) {
                            $dbrow->save();
                            $job->updated_azure++;
                        }
                    }
                    // update job record:
                    if ($sub->status == 'active') {
                        $job->active_azure++;
                    } else {
                        $job->inactive_azure++;
                    }
                    // we can dispatch each subscription usage update job as we go, so that this can be done in parallel (if queuing)
                    $subjob = new UpdateCspAzureSubscriptionUsageJob($dbrow);
                    $subjob->cmd = $this->cmd;
                    $this->dispatchNextJob($subjob);
                }
                $job->records_processed++;
            }
            $job->end_time = new Carbon();
            $job->save();
        } catch (\Exception $e) {
            // record exception in job record:
            $job->error_message = $this->getExceptionString($e);
            $job->end_time = new Carbon();
            $job->save();
        }
    }
}
