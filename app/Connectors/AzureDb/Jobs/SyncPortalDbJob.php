<?php

namespace App\Connectors\AzureDb\Jobs;

use App\Connectors\AzureDb\Models\CspCustomer;
use App\Connectors\AzureDb\Models\CspCustomerDetail;
use App\Connectors\AzureDb\Models\CspSubscription;
use App\Connectors\AzureDb\Models\CspUsage;
use App\Connectors\AzureDb\Models\SyncPortalJob;
use App\Connectors\Common\Job;
use App\Models\Agreement;
use App\Models\Customer;
use App\Models\Microsoft\AadTenant;
use App\Models\Microsoft\Azure\Service;
use App\Models\Microsoft\Azure\Subscription;
use App\Models\Microsoft\Azure\SubscriptionUsage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SyncPortalDbJob extends Job
{

    protected $_azureServices;

    /**
     * @var Carbon
     */
    protected $_lastSync;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CspCustomer $csp_customer
     * @return Customer
     */
    protected function syncPortalCustomer(CspCustomer $csp_customer) {
        /** @var Customer $customer */
        $customer = NULL;
        if(is_null($csp_customer->detail)) {
            $detail = new CspCustomerDetail();
            $detail->customer_id = $csp_customer->id;
            $detail->save();
            $csp_customer->detail = $detail;
        }
        if(!is_null($csp_customer->detail->portal_customer_id)) {
            $customer = Customer::find($csp_customer->detail->portal_customer_id);
        }
        if(is_null($customer)) {
            $customer = Customer::findByName($csp_customer->name);
        }
        if(is_null($customer)) {
            // need to create customer in portal DB:
            $customer = new Customer();
            $customer->name = $csp_customer->name;
            $customer->save();
            $csp_customer->detail->portal_customer_id = $customer->id;
            $csp_customer->detail->save();
        }
        return $customer;
    }

    /**
     * @param CspSubscription $csp_subscription
     * @return Agreement
     * @throws \Exception
     */
    protected function syncPortalAgreement(CspSubscription $csp_subscription) {
        // the portal agreement is roughly analogous with the CSP customer
        // there may be multiple CSP customer entries for a single Bytes customer
        // (e.g. if they require a test tenant)

        // first, see if there is an agreement that matches this tenant guid - that's the unique identifier
        $agreements = Agreement::query()
            ->where('type', '=', Agreement::TYPE_AZURE_CSP)
            ->where('identifier', '=', $csp_subscription->tenant_guid)
            ->get();
        switch($agreements->count()) {
            case 0:
                $agreement = new Agreement();
                $agreement->type = Agreement::TYPE_AZURE_CSP;
                $agreement->identifier = strtolower($csp_subscription->tenant_guid);
                // find the customer record by their name:
                $customer = $this->syncPortalCustomer($csp_subscription->customer);
                $agreement->customer()->associate($customer);
                $agreement->save();
                return $agreement;
            case 1:
                return $agreements->first();
            default:
                throw new \Exception('Multiple portal agreements found matching CSP Azure subscription {'.$csp_subscription->tenant_guid.'}');
        }
    }

    /**
     * @param CspSubscription $csp_subscription
     * @return AadTenant
     */
    protected function syncPortalTenant(CspSubscription $csp_subscription) {
        $tenant = AadTenant::findByGuid($csp_subscription->tenant_guid);
        if(is_null($tenant)) {
            $tenant = new AadTenant();
            $tenant->guid = strtolower($csp_subscription->tenant_guid);
            $tenant->save();
        }
        return $tenant;
    }

    /**
     * @param CspSubscription $csp_subscription
     * @param Agreement $agreement
     * @return Subscription
     */
    protected function syncPortalSubscription(CspSubscription $csp_subscription, Agreement $agreement) {
        $subscription = Subscription::findByGuid($csp_subscription->guid);
        if(is_null($subscription)) {
            $subscription = new Subscription();
            $subscription->customer_id = $agreement->customer_id;
            $subscription->guid = strtolower($csp_subscription->guid);
            $subscription->azure_name = $csp_subscription->friendly_name;
            $subscription->offer_code = $csp_subscription->offer_id;
            $subscription->agreement()->associate($agreement);
            $subscription->aadTenant()->associate($this->syncPortalTenant($csp_subscription));
            $subscription->save();
        }
        return $subscription;
    }

    protected function syncPortalCspUsageDate(CspSubscription $csp_subscription, Subscription $subscription, Carbon $date) {
        $dateStr = $date->toDateString();
        // run aggregation query
        $data = CspUsage::query()
            ->join('csp_meters', 'csp_usages.csp_meter_id', '=', 'csp_meters.id')
            ->join('meters', 'csp_meters.meter_id', '=', 'meters.id')
            ->selectRaw('meters.category AS service_name, SUM(csp_usages.estimate_price) AS cost')
            ->where('csp_usages.subscription_id', '=', $csp_subscription->id)
            ->where('csp_usages.usage_date', '=', $dateStr)
            ->where('csp_usages.deleted', '=', 0)
            ->groupBy(['meters.category'])
            ->pluck('cost', 'service_name')
            ->toArray();
        foreach($data as $serviceName=>$cost) {
            if(!array_key_exists($serviceName, $this->_azureServices)) {
                // check the portal db, just in case:
                $ps = Service::findByName($serviceName);
                if(is_null($ps)) {
                    $ps = new Service();
                    $ps->name = $serviceName;
                    $ps->save();
                }
                $this->_azureServices[$serviceName] = $ps->id;
            }
            $service_id = $this->_azureServices[$serviceName];
            // find or create the usage row in portal db:
            $pu = SubscriptionUsage::findByValues($subscription->id, $dateStr, $service_id);
            if(is_null($pu)) {
                $pu = new SubscriptionUsage();
                $pu->subscription_id = $subscription->id;
                $pu->usage_date = $date;
                $pu->usage_month = substr($dateStr,0,7);
                $pu->service_id = $service_id;
            }
            $pu->total_cost = $cost;
            $pu->save();
        }
    }

    protected function syncPortalCspUsage(CspSubscription $csp_subscription, Subscription $subscription) {
        // find which dates have had usage updated since the last sync:
        $dates = CspUsage::query()
            ->where('updated_at', '>=', $this->_lastSync)
            ->where('subscription_id', '=', $csp_subscription->id)
            ->distinct()
            ->pluck('usage_date');
        optional($this->cmd)->info(' > Syncing subscription usage ('.$dates->count().' days)...');
        if($dates->count()==0) {
            return 0;
        }
        optional($this->cmd)->getOutput()->write(' >> ');
        $i = 0;
        foreach($dates as $date) {
            $this->syncPortalCspUsageDate($csp_subscription, $subscription, $date);
            if($i%50==0) {
                optional($this->cmd)->getOutput()->write($i);
            } elseif($i%10==0) {
                optional($this->cmd)->getOutput()->write('.');
            }
            $i++;
        }
        optional($this->cmd)->getOutput()->write('', TRUE);
        return $dates->count();
    }



    protected function syncCspSubscriptions() {
        foreach(CspSubscription::with(['customer', 'customer.detail'])->get() as $csp_subscription) {
            optional($this->cmd)->info('Syncing CSP subscription {'.$csp_subscription->guid.'}');
            //$customer = $this->syncPortalCustomer($csp_subscription->customer);
            $agreement = $this->syncPortalAgreement($csp_subscription);
            $subscription = $this->syncPortalSubscription($csp_subscription, $agreement);
            $this->syncPortalCspUsage($csp_subscription, $subscription);
        }
    }

    protected function syncAzureServices() {
        $this->_azureServices = array();
        // run a query to summarise meter categories/services:
        $services = DB::connection('azure')
            ->table('meters')
            ->selectRaw('DISTINCT category')
            ->get();
        foreach($services as $service) {
            $ps = Service::findByName($service->category);
            if(is_null($ps)) {
                $ps = new Service();
                $ps->name = $service->category;
                $ps->save();
            }
            $this->_azureServices[$ps->name] = $ps->id;
        }
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $lastJobTime = SyncPortalJob::query()
            ->whereNotNull('end_time')
            ->whereNull('error_message')
            ->max('start_time');
        if(is_null($lastJobTime)) {
            $this->_lastSync = new Carbon('2000-01-01');
        } else {
            $this->_lastSync = new Carbon($lastJobTime);
        }
        $job = new SyncPortalJob();
        $job->start_time = new Carbon();
        $job->save();

        optional($this->cmd)->info('Starting database synchronisation.');
        $this->syncAzureServices();
        assert('is_array($this->_azureServices) AND count($this->_azureServices)>0', 'Azure Services list must be initialised (are meters populated?)');
        $this->syncCspSubscriptions();


        $job->end_time = new Carbon();
        $job->save();
    }
}
