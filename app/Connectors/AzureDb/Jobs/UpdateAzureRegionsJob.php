<?php

namespace App\Connectors\AzureDb\Jobs;

use App\Connectors\Common\Job;
use App\Connectors\AzureDb\Models\Region;
use App\Connectors\AzureDb\Models\RegionLookup;

class UpdateAzureRegionsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    protected function regionStatus($regionObject) {
        if($regionObject->restricted=='no') {
            if($regionObject->status=='available') {
                return Region::STATUS_AVAILABLE;
            }
            if($regionObject->status=='announced') {
                return Region::STATUS_ANNOUNCED;
            }
            if($regionObject->status=='virtual') {
                return Region::STATUS_VIRTUAL;
            }
        }
        if($regionObject->restricted=='legal-entity') {
            return Region::STATUS_LEGALENTITY;
        }
        if($regionObject->restricted=='us-gov') {
            return Region::STATUS_USGOV;
        }
        if($regionObject->restricted=='us-dod') {
            return Region::STATUS_USDOD;
        }
        return Region::STATUS_UNKNOWN;
    }

    protected function armName($regionObject) {
        return str_replace('-','', $regionObject->shortName);
    }

    protected function latlong($value) {
        if(empty($value)) {
            return NULL;
        }
        // get length of string after decimal:
        $deci = strpos($value, '.');
        if($deci===FALSE) {
            // not found - add decimal part:
            return $value.'.0000';
        }
        $decpart = substr($value, $deci);
        return substr($value,0, $deci) . str_pad(substr($value, $deci), 5, '0', STR_PAD_RIGHT);
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        optional($this->cmd)->info('Loading/updating Azure Regions...');
        // load data from file:
        $regions = json_decode(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'azure_region_data.json'));

        $regions_processed = 0;
        $regions_created = 0;
        $regions_updated = 0;
        $lookups_created = 0;
        $lookups_updated = 0;
        $regions_paired = 0;

        foreach($regions as $region) {
            // load it from DB if possible:
            $row = Region::findByShortName($region->shortName);
            if(is_null($row)) {
                $row = new Region();
            }
            $row->short_name = $region->shortName;
            $row->display_name = $region->displayName;
            $row->sort_name = $region->sortName;
            $row->status = $this->regionStatus($region);
            $row->continent = $region->continent;
            $row->country_code = empty($region->country) ? NULL : $region->country;
            $row->location = $region->location;
            $row->latitude = $this->latlong($region->latitude);
            $row->longitude = $this->latlong($region->longitude);
            $row->arm_name = $this->armName($region);
            if($row->isDirty()) {
                if($row->exists) {
                    $regions_updated++;
                } else {
                    $regions_created++;
                }
                $row->save();
            }
            $regions_processed++;

            // update/create lookups:
            $names = [$row->arm_name];
            if(strtolower($row->arm_name)!=strtolower($row->short_name)) {
                $names[] = $row->short_name;
            }
            if(strtolower($row->arm_name)!=strtolower($row->display_name)) {
                $names[] = $row->display_name;
            }
            foreach($region->altNames as $altname) {
                if(!in_array($altname, $names)) {
                    $names[] = $altname;
                }
            }

            foreach($names as $name) {
                $lookup = RegionLookup::findByName($name);
                if(is_null($lookup)) {
                    $lookup = new RegionLookup();
                    $lookup->name = $name;
                }
                if($lookup->region_id!==$row->id) {
                    $lookup->region()->associate($row);
                }
                if($lookup->isDirty()) {
                    $exists = $lookup->exists;
                    $lookup->save();
                    if($exists) {
                        $lookups_updated++;
                    } else {
                        $lookups_created++;
                    }
                }
            }
        }

        // now go back over all regions to associate the paired region:
        foreach($regions as $region) {
            if($region->status!='virtual') {
                $row = Region::findByShortName($region->shortName);
                $pair = Region::findByShortName($region->pairedName);
                if($row->paired_region_id!==$pair->id) {
                    $row->pairedRegion()->associate($pair);
                    $row->save();
                    $regions_paired++;
                }
            }
        }

        optional($this->cmd)->info(' > Regions processed = '.$regions_processed);
        optional($this->cmd)->info(' > Regions created = '.$regions_created);
        optional($this->cmd)->info(' > Regions updated = '.$regions_updated);
        optional($this->cmd)->info(' > Regions paired = '.$regions_paired);
        optional($this->cmd)->info(' > Region Lookups created = '.$lookups_created);
        optional($this->cmd)->info(' > Region Lookups updated = '.$lookups_updated);
    }
}
