<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\JobModel;

/**
 * Class CspSubscriptionJob
 * @package App\Connectors\AzureDb\Models
 * @property integer $customer_id
 * @property integer $new_azure
 * @property integer $updated_azure
 * @property integer $inactive_azure
 * @property integer $active_azure
 * @property CspCustomer $customer
 */
class CspSubscriptionJob extends JobModel
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(CspCustomer::class, 'customer_id');
    }

}
