<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $meter_id
 * @property string $guid
 * @property string $name
 * @property string $sub_category
 * @property string $category
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 */
class CspMeter extends Model
{

    /**
     * @param string $guid
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meter() {
        return $this->belongsTo(Meter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo(Region::class);
    }


}
