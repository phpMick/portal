<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $resource_id
 * @property Carbon $reported_date
 * @property Carbon $usage_date
 * @property Carbon $usage_start_time
 * @property Carbon $usage_end_time
 * @property integer $csp_meter_id
 * @property integer $csp_price_id
 * @property string $quantity
 * @property string $estimate_cost
 * @property string $estimate_price
 * @property boolean $deleted
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 * @property CspResource $resource
 * @property CspMeter $meter
 * @property CspPrice $price
 */
class CspUsage extends Model
{


    protected $dates = [
        'reported_date', 'usage_date', 'usage_start_time', 'usage_end_time'
    ];

    protected $casts = [
        'deleted' => 'boolean'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription() {
        return $this->belongsTo(CspSubscription::class, 'subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function resource() {
        return $this->belongsTo(CspResource::class, 'resource_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meter() {
        return $this->belongsTo(CspMeter::class, 'csp_meter_id');
    }

    public function price() {
        return $this->belongsTo(CspPrice::class, 'csp_price_id');
    }


}
