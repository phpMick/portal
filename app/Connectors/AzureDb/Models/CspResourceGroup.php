<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property string $name
 * @property boolean $mixed_case
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 *
 */
class CspResourceGroup extends Model
{

    protected $casts = [
        'mixed_case' => 'boolean'
    ];

    /**
     * @param integer|CspSubscription $subscription
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByName($subscription, $name) {
        if($subscription instanceof CspSubscription) {
            $subscription = $subscription->id;
        }
        if(!is_integer($subscription) OR empty($name) OR !is_string($name)) {
            return NULL;
        }
        return self::query()
            ->where('subscription_id', '=', $subscription)
            ->where('name', '=', $name)
            ->first();
    }

    static public function isMixedCase($value) {
        return !(strtolower($value)==$value OR strtoupper($value)==$value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription() {
        return $this->belongsTo(CspSubscription::class, 'subscription_id');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $this->attributes['mixed_case'] = self::isMixedCase($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function resources() {
        return $this->hasMany(CspResource::class, 'resource_group_id', 'id');
    }

}
