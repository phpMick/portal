<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $meter_id
 * @property string $guid
 * @property string $name
 * @property string $sub_category
 * @property string $category
 * @property string $product
 * @property string $service
 * @property integer $region_id
 * @property string $unit_of_measure
 * @property string $msdn_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 */
class Meter extends Model
{


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo(Region::class);
    }

    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }


}
