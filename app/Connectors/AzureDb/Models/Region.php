<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $short_name
 * @property string $display_name
 * @property string $sort_name
 * @property integer $status
 * @property string $continent
 * @property string $country_code
 * @property string $location
 * @property double $latitude
 * @property double $longitude
 * @property integer $paired_region_id
 * @property string $arm_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Region extends Model
{

    const STATUS_UNKNOWN = 0;
    const STATUS_VIRTUAL = 10;
    const STATUS_ANNOUNCED = 50;

    const STATUS_USDOD = 100;
    const STATUS_USGOV = 110;

    const STATUS_PREVIEW = 200;
    const STATUS_LEGALENTITY = 230;
    const STATUS_AVAILABLE = 250;



    /**
     * @param string $name
     * @return static
     */
    static public function findByArmName($name) {
        return self::where('arm_name', '=', $name)->first();
    }

    /**
     * @param string $name
     * @return static
     */
    static public function findByShortName($name) {
        return self::where('short_name', '=', $name)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pairedRegion() {
        return $this->belongsTo(self::class, 'paired_region_id');
    }


    static public function updateOrCreateFromRecord($object) {

    }

}
