<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;

/**
 * Class CspCustomer
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $guid
 * @property string $name
 * @property string $tenant_guid
 * @property string $default_domain
 * @property string $microsoft_domain
 * @property string $custom_domains
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomerDetail $detail
 */
class CspCustomer extends Model
{

    protected $casts = [
        'custom_domains'=>'array'
    ];


    /**
     * @param string $guid
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }

    /**
     * @param string $tenant_guid
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    static public function findByTenantGuid($tenant_guid) {
        return self::where('tenant_guid', '=', $tenant_guid)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detail() {
        return $this->hasOne(CspCustomerDetail::class, 'customer_id');
    }

}
