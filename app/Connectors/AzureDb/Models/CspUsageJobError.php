<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspUsageJob
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $job_id
 * @property integer $affected_row
 * @property string $error_type
 * @property string $error_detail
 * @property boolean $fixed
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class CspUsageJobError extends Model
{


    protected $casts = [
        'fixed' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job() {
        return $this->belongsTo(CspUsageJob::class, 'job_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affectedUsage() {
        return $this->belongsTo(CspUsage::class, 'affected_row');
    }
}
