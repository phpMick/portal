<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $csp_price_id
 * @property double $min_quantity
 * @property double $estimate_price
 * @property double $discount
 * @property double $estimate_cost_price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspPrice $price
 */
class CspPriceRate extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price() {
        return $this->belongsTo(CspPrice::class, 'csp_price_id');
    }


}
