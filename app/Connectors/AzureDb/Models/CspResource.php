<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $subscription_id
 * @property string $uri
 * @property string $name
 * @property integer $resource_group_id
 * @property integer $region_id
 * @property array $tags
 * @property array $info
 * @property array $marketplace
 * @property string $uri_hash
 * @property string $detail_hash
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspSubscription $subscription
 * @property Region $region
 * @property CspResourceGroup $resourceGroup
 *
 */
class CspResource extends Model
{

    protected $casts = [
        'tags' => 'array',
        'info' => 'array',
        'marketplace' => 'array'
    ];


    /**
     * @var AzureResourceUriParts
     */
    protected $uriParts = NULL;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription() {
        return $this->belongsTo(CspSubscription::class, 'subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo(Region::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function resourceGroup() {
        return $this->belongsTo(CspResourceGroup::class, 'resource_group_id');
    }

    protected function findOrCreateResourceGroup() {
        $rg = CspResourceGroup::findByName($this->subscription_id, $this->uriParts->resourceGroup);
        if(is_null($rg)) {
            // create a new one:
            $rg = new CspResourceGroup();
            $rg->subscription_id = $this->subscription_id;
            // if not mixed-case, use lowercase version:
            if(self::isMixedCase($this->uriParts->resourceGroup)) {
                $rg->name = $this->uriParts->resourceGroup; // mixed_case is updated automagically
            } else {
                $rg->name = strtolower($this->uriParts->resourceGroup); // mixed_case is updated automagically
            }
            $rg->save();
        } elseif(!$rg->mixed_case AND self::isMixedCase($this->uriParts->resourceGroup)) {
            // update existing RG with mixed-case name that we now have:
            $rg->name = $this->uriParts->resourceGroup; // mixed_case is updated automagically
            $rg->save();
        }
        return $rg;
    }

    public function setUriAttribute($value) {
        $this->attributes['uri'] = $value;
        $this->uriParts = new AzureResourceUriParts($value);
        // update resource name if we have a nicer format (i.e. not all upper/lower case)
        if(is_null($this->name) OR (!self::isMixedCase($this->name) AND self::isMixedCase($this->uriParts->name))) {
            $this->attributes['name'] = $this->uriParts->name;
        }
        // NOTE: resource group is not created if subscription ID is not set yet.
        // is there a resource group named in the URI, and do we have a subscription assigned?
        if(!empty($this->uriParts->resourceGroup) AND !is_null($this->subscription_id)) {
            // is a RG already assigned?
            if(is_null($this->resource_group_id)) {
                // try and find it from existing RGs:
                $rg = $this->findOrCreateResourceGroup();
                $this->resourceGroup()->associate($rg);
            } else {
                $rg = $this->resourceGroup;
                // is the name still valid?
                if(strtolower($rg->name)!=strtolower($this->uriParts->resourceGroup)) {
                    // name has changed - weird, but we can deal with it...
                    $rg2 = $this->findOrCreateResourceGroup();
                    $this->resourceGroup()->associate($rg2);
                }
                // check if we have a better (mixed_case) name now:
                elseif(!$rg->mixed_case AND self::isMixedCase($this->uriParts->resourceGroup)) {
                    $rg->name = $this->uriParts->resourceGroup;
                    $rg->save();
                }
            }
        }
    }

    static public function isMixedCase($str) {
        return !(strtolower($str)==$str OR strtoupper($str)==$str);
    }

    /**
     * @return AzureResourceUriParts
     */
    public function getUriParts() {
        if(is_null($this->uriParts)) {
            $this->uriParts = new AzureResourceUriParts($this->uri);
        }
        return $this->uriParts;
    }


    public function tagArray() {
        if(is_null($this->tags)) {
            return [];
        }
        if(is_array($this->tags)) {
            return $this->tags;
        }
        if(is_string($this->tags)) {
            $val = json_decode($this->tags, TRUE);
            if(!is_array($val)) {
                return [];
            }
            return $val;
        }
    }



}
