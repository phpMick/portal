<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\JobModel;
use Carbon\Carbon;

/**
 * Class CspCustomer
 * @package App\Connectors\AzureDb\Models
 * @property Carbon  $source_date
 * @property string  $currency
 * @property integer $new_prices
 * @property integer $updated_prices
 * @property integer $new_csp_meters
 * @property integer $updated_csp_meters
 * @property integer $new_meters
 * @property integer $new_region_lookups
 * @property integer $new_regions
 */
class CspPriceJob extends JobModel
{


}
