<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property string $name
 * @property integer $region_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Region $region
 */
class RegionLookup extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo(Region::class);
    }

    static public function findByName($name) {
        return self::where('name', '=', $name)->first();
    }

}
