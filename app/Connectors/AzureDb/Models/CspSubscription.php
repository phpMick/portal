<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $guid
 * @property string $tenant_guid
 * @property string $offer_id
 * @property string $offer_name
 * @property string $friendly_name
 * @property integer $quantity
 * @property string $unit_type
 * @property string $parent_subscription_guid
 * @property Carbon $creation_date
 * @property Carbon $effective_start_date
 * @property Carbon $commitment_end_date
 * @property string $status
 * @property boolean $is_trial
 * @property boolean $auto_renew
 * @property string $billing_type
 * @property string $billing_cycle
 * @property string[] $suspension_reasons
 * @property string $contract_type
 * @property string $order_guid
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomer $customer
 */
class CspSubscription extends Model
{

    protected $casts = [
        'suspension_reasons'=>'array',
        'is_trial'=>'boolean',
        'auto_renew'=>'boolean'
    ];

    protected $dates = [
        'creation_date', 'effective_start_date', 'commitment_end_date'
    ];

    /**
     * @param string $guid
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    static public function findByGuid($guid) {
        return self::where('guid', '=', $guid)->first();
    }

    /**
     * @param string $tenant_guid
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    static public function findByTenantGuid($tenant_guid) {
        return self::where('tenant_guid', '=', $tenant_guid)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(CspCustomer::class, 'customer_id');
    }


}
