<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;


/**
 * Class CspSubscription
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $csp_meter_id
 * @property string $currency
 * @property double $estimate_price
 * @property double $discount
 * @property double $estimate_cost_price
 * @property array $tags
 * @property array $prices
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspMeter $meter
 */
class CspPrice extends Model
{
    // TODO: move $tags property to CspMeter - in models, migration, and in DB

    protected $casts = [
        'tax_included' => 'boolean',
        'tags' => 'array',
        'prices' => 'array'
    ];

    protected $dates = [
        'start_date', 'end_date'
    ];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meter() {
        return $this->belongsTo(CspMeter::class, 'csp_meter_id');
    }


}
