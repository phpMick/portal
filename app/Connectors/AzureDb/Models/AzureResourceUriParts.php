<?php

namespace App\Connectors\AzureDb\Models;

/**
 * Class AzureResourceUriParts
 * @package App\Connectors\AzureDb\Models
 * @property string $subscription
 * @property string $resourceGroup
 * @property string $provider
 * @property string $providerContext
 * @property string $name
 */
class AzureResourceUriParts
{

    protected $subscription;
    protected $resourceGroup;
    protected $provider;
    protected $providerContext;
    protected $name;

    public function __construct($uri) {
        $regex = '/(^\/?subscriptions\/([^\/]+))?(\/resourcegroups\/([^\/]+))?.*?(\/providers\/([^\/]+))?(.*\/)?(.+)$/i';
        $matches = array();
        if(preg_match($regex, $uri, $matches)) {
            $this->subscription = empty($matches[2]) ? NULL : strtolower($matches[2]);
            $this->resourceGroup = empty($matches[4]) ? NULL : $matches[4];
            $this->provider = empty($matches[6]) ? NULL : $matches[6];
            $this->providerContext = empty($matches[7]) ? NULL : $matches[7];
            $this->name = empty($matches[8]) ? NULL : $matches[8];
        }
    }

    /**
     * @param string $name
     * @return string
     * @throws \Exception
     */
    public function __get($name)
    {
        switch($name) {
            case 'subscription': return $this->subscription;
            case 'resourceGroup': return $this->resourceGroup;
            case 'provider': return $this->provider;
            case 'providerContext': return $this->providerContext;
            case 'name': return $this->name;
        }
        throw new \Exception('Unknown property');
    }

    public function __isset($name)
    {
        switch($name) {
            case 'subscription': return isset($this->subscription);
            case 'resourceGroup': return isset($this->resourceGroup);
            case 'provider': return isset($this->provider);
            case 'providerContext': return isset($this->providerContext);
            case 'name': return isset($this->name);
        }
        throw new \Exception('Unknown property');
    }


}