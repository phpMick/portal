<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;


/**
 * Class CspSubscriptionJob
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property integer $job_num
 * @property string $error_message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class SyncPortalJob extends Model
{



}
