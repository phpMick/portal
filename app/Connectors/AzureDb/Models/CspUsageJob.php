<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\JobModel;
use Carbon\Carbon;


/**
 * Class CspUsageJob
 * @package App\Connectors\AzureDb\Models
 * @property integer $subscription_id
 * @property Carbon $query_date
 * @property boolean $old
 * @property integer $records_deleted
 * @property integer $new_resources
 * @property integer $updated_resources
 * @property integer $new_regions
 * @property integer $new_region_lookups
 * @property integer $error_count
 * @property CspSubscription $subscription
 */
class CspUsageJob extends JobModel
{

//    protected $dates = [
//        'start_time', 'end_time', 'query_date'
//    ];

    protected $casts = [
        'old' => 'boolean'
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->dates[] = 'query_date';
    }


    static public function findBySubscriptionQueryDate($subscription, $query_date, $old=NULL) {
        if($subscription instanceof CspSubscription) {
            assert('$subscription->exists', 'Subscription not saved to database');
            $subscription = $subscription->id;
        }
        if($query_date instanceof Carbon) {
            $query_date = $query_date->toDateString();
        }
        $query = self::where('subscription_id', '=', $subscription)
            ->where('query_date', '=', $query_date);
        if(!is_null($old)) {
            if($old) {
                $query = $query->where('old', '=', 1);
            } else {
                $query = $query->where('old', '=', 0);
            }
        }
        return $query->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription() {
        return $this->belongsTo(CspSubscription::class, 'subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function errors() {
        return $this->hasMany(CspUsageJobError::class, 'job_id');
    }

}
