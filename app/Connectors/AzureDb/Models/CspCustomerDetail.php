<?php

namespace App\Connectors\AzureDb\Models;

use App\Connectors\AzureDb\Model;
use Carbon\Carbon;

/**
 * Class CspCustomer
 * @package App\Connectors\AzureDb\Models
 * @property integer $id
 * @property integer $customer_id
 * @property string $currency
 * @property string $default_margin
 * @property integer $portal_customer_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CspCustomer $customer
 */
class CspCustomerDetail extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(CspCustomer::class, 'customer_id');
    }


}
