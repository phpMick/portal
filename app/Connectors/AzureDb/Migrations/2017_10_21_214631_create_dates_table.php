<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates', function (Blueprint $table) {
            $table->date('date');

            $table->primary('date');

        });

        // populate dates table:
        $d = new \Carbon\Carbon('2008-01-01', 'UTC');
        $endDate = new \Carbon\Carbon('2029-12-31', 'UTC');
        while($d->lte($endDate)) {
            \Illuminate\Support\Facades\DB::connection('azure')->insert('INSERT INTO dates VALUES(?)',[$d]);
            $d->addDay();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dates');
    }
}
