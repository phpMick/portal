<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // data sources: https://azure.microsoft.com/en-gb/regions/ + MicrosoftAzureApi()->ResourceManagement()->listLocations()
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_name');
            $table->string('display_name');
            $table->string('sort_name')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('continent')->nullable();
            $table->char('country_code', 2)->nullable();
            $table->string('location')->nullable();
            $table->decimal('latitude', 8, 4)->nullable();
            $table->decimal('longitude', 8, 4)->nullable();
            $table->unsignedInteger('paired_region_id')->nullable();
            $table->string('arm_name')->nullable();
            $table->timestamps();

            $table->foreign('paired_region_id')->references('id')->on('regions');
            $table->unique('short_name');
            $table->unique('arm_name');
            $table->unique('sort_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
