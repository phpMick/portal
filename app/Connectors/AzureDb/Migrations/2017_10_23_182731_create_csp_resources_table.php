<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('subscription_id');
            $table->string('uri', 2047)->nullable();
            $table->string('name')->nullable();
            $table->unsignedInteger('resource_group_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->text('tags')->nullable();
            $table->text('info')->nullable();
            $table->text('marketplace')->nullable();
            $table->char('uri_hash', 32);
            $table->char('detail_hash', 32)->nullable();
            $table->timestamps();

            $table->foreign('subscription_id')->references('id')->on('csp_subscriptions');
            $table->foreign('resource_group_id')->references('id')->on('csp_resource_groups');
            $table->foreign('region_id')->references('id')->on('regions');
            $table->index(['uri_hash', 'detail_hash']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_resources');
    }
}
