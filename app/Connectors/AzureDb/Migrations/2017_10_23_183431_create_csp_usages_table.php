<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_usages', function (Blueprint $table) {
            $prec = config('app.decimal_high.precision');
            $scale = config('app.decimal_high.scale');

            $table->bigIncrements('id');
            $table->unsignedInteger('subscription_id');
            $table->unsignedBigInteger('resource_id');
            $table->date('reported_date');
            $table->date('usage_date');
            //$table->dateTimeTz('usage_start_time');
            //$table->dateTimeTz('usage_end_time');
            $table->unsignedInteger('csp_meter_id');
            $table->unsignedInteger('csp_price_id')->nullable();
            $table->decimal('quantity', $prec, $scale);
            $table->decimal('estimate_cost', $prec, $scale)->nullable();
            $table->decimal('estimate_price', $prec, $scale)->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            /***************************************************
             * INDEXES REQUIRED:
             *
             * for update usage job (deleting old rows): [subscription_id, reported_date]
             * for portal sync: [subscription_id, usage_date]
             * for reports sync: [updated_at] also [resource_id, usage_date, csp_meter_id, deleted]
             */

            $table->foreign('subscription_id')->references('id')->on('csp_subscriptions');
            $table->foreign('resource_id')->references('id')->on('csp_resources');
            $table->foreign('csp_meter_id')->references('id')->on('csp_meters');
            $table->foreign('csp_price_id')->references('id')->on('csp_prices');
            $table->index('reported_date');
            $table->index('usage_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_usages');
    }
}
