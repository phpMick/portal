<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_meters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meter_id');
            $table->string('guid');
            $table->string('name');
            $table->string('sub_category');
            $table->string('category');
            $table->unsignedInteger('region_id');
            $table->string('unit_of_measure')->nullable();
            $table->timestamps();

            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('meter_id')->references('id')->on('meters');
            $table->unique(['guid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_meters');
    }
}
