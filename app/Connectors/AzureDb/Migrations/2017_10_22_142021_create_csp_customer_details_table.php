<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCspCustomers
 * @property integer $id
 * @property string $guid
 * @property string $tenantId
 * @property string $domain
 * @property string $name
 */
class CreateCspCustomerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_customer_details', function (Blueprint $table) {
            $lprec = config('app.decimal_low.precision');
            $lscale = config('app.decimal_low.scale');

            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->char('currency', 3)->default('GBP');
            $table->decimal('default_margin', $lprec, $lscale)->default('0.15');
            $table->unsignedInteger('portal_customer_id')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('csp_customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_customer_details');
    }
}
