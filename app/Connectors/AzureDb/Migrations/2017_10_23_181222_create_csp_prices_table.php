<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_prices', function (Blueprint $table) {
            $hprec = config('app.decimal_high.precision');
            $hscale = config('app.decimal_high.scale');
            $lprec = config('app.decimal_low.precision');
            $lscale = config('app.decimal_low.scale');

            $table->increments('id');
            $table->unsignedInteger('csp_meter_id');
            $table->char('currency', 3);
            //$table->string('locale', 10);
            //$table->boolean('tax_included');
            $table->decimal('estimate_price', $hprec, $hscale)->nullable();
            $table->decimal('discount', $lprec, $lscale)->nullable();
            $table->decimal('estimate_cost_price', $hprec, $hscale)->nullable();
            $table->text('tags')->nullable();
            $table->text('prices')->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('csp_meter_id')->references('id')->on('csp_meters');
            $table->index(['start_date', 'end_date', 'currency']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_prices');
    }
}
