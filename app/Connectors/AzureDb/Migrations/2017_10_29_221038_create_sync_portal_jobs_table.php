<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncPortalJobsTable extends Migration
{



    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('sync_portal_jobs', function (Blueprint $table)  {
            $table->bigIncrements('id');
            $table->dateTime('start_time');
            $table->dateTime('end_time')->nullable();
            $table->unsignedBigInteger('job_num')->nullable();
            $table->text('error_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_portal_jobs');

    }
}
