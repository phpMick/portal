<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('guid');
            $table->string('tenant_guid');
            $table->string('offer_id');
            $table->string('offer_name');
            $table->string('friendly_name')->nullable();
            $table->integer('quantity');
            $table->string('unit_type');
            $table->string('parent_subscription_guid')->nullable();
            $table->dateTime('creation_date');
            $table->dateTime('effective_start_date')->nullable();
            $table->dateTime('commitment_end_date')->nullable();
            $table->string('status');
            $table->boolean('is_trial')->nullable();
            $table->boolean('auto_renew')->nullable();
            $table->string('billing_type')->nullable();
            $table->string('billing_cycle')->nullable();
            $table->text('suspension_reasons')->nullable();
            $table->string('contract_type')->nullable();
            $table->string('order_guid')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('csp_customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_subscriptions');
    }
}
