<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspPriceRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_price_rates', function (Blueprint $table) {
            $hprec = config('app.decimal_high.precision');
            $hscale = config('app.decimal_high.scale');
            $lprec = config('app.decimal_low.precision');
            $lscale = config('app.decimal_low.scale');

            $table->bigIncrements('id');
            $table->unsignedInteger('csp_price_id');
            $table->decimal('min_quantity', $hprec, $hscale);
            $table->decimal('estimate_price', $hprec, $hscale);
            $table->decimal('discount', $lprec, $lscale);
            $table->decimal('estimate_cost_price', $hprec, $hscale);
            $table->timestamps();

            $table->foreign('csp_price_id')->references('id')->on('csp_prices');
            $table->index(['csp_price_id', 'min_quantity']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_price_rates');
    }
}
