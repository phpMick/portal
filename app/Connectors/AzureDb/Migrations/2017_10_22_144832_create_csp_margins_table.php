<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspMarginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('csp_margins', function (Blueprint $table) {
            $prec = config('app.decimal_low.precision');
            $scale = config('app.decimal_low.scale');

            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->decimal('margin', $prec, $scale);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('csp_customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_margins');
    }
}
