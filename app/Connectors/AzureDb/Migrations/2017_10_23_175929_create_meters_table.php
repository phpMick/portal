<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guid');
            $table->string('name');
            $table->string('sub_category');
            $table->string('category');
            $table->string('product')->nullable();
            $table->string('service')->nullable();
            $table->unsignedInteger('region_id');
            $table->string('unit_of_measure')->nullable();
            $table->boolean('msdn_pricing')->default(FALSE);
            $table->timestamps();

            $table->foreign('region_id')->references('id')->on('regions');
            $table->unique(['guid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meters');
    }
}
