<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCspCustomers
 * @property integer $id
 * @property string $guid
 * @property string $tenantId
 * @property string $domain
 * @property string $name
 */
class CreateCspCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csp_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guid');
            $table->string('name');
            $table->string('tenant_guid');
            $table->string('default_domain');
            $table->string('microsoft_domain')->nullable();
            $table->text('custom_domains')->nullable();
            $table->timestamps();

            $table->unique('guid');
            $table->index('name');
            $table->index('tenant_guid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_customers');
    }
}
