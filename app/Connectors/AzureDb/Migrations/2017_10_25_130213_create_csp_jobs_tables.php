<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCspJobsTables extends Migration
{

    protected function getTables() {
        return [
            'csp_customer_jobs' => ['new', 'updated'],
            'csp_subscription_jobs' => ['new_azure', 'updated_azure', 'inactive_azure', 'active_azure'],
            'csp_price_jobs' => ['new_prices', 'updated_prices', 'new_csp_meters', 'updated_csp_meters', 'new_meters', 'new_region_lookups', 'new_regions'],
            'csp_usage_jobs' => ['records_deleted', 'new_resources', 'updated_resources', 'new_region_lookups', 'new_regions']
        ];
    }



    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        foreach($this->getTables() as $tableName=>$extraColumns) {
            Schema::create($tableName, function (Blueprint $table) use ($tableName, $extraColumns) {
                $table->bigIncrements('id');
                $table->dateTime('start_time');
                $table->dateTime('end_time')->nullable();
                switch($tableName) {
                    case 'csp_usage_jobs':
                        $table->unsignedInteger('subscription_id');
                        $table->date('query_date');
                        $table->boolean('old')->default(0);
                        break;
                    case 'csp_subscription_jobs':
                        $table->unsignedInteger('customer_id');
                        break;
                }
                $table->unsignedBigInteger('job_num')->nullable();
                if($tableName=='csp_price_jobs') {
                    $table->date('source_date')->nullable();
                    $table->char('currency', 3)->nullable();
                }
                $table->integer('records_processed')->nullable();
                foreach($extraColumns as $column) {
                    $table->integer($column)->nullable();
                }
                if($tableName=='csp_usage_jobs') {
                    $table->unsignedInteger('error_count')->default(0);
                }
                $table->text('error_message')->nullable();
                $table->timestamps();

                switch($tableName) {
                    case 'csp_subscription_jobs':
                        $table->foreign('customer_id')->references('id')->on('csp_customers');
                        break;
                    case 'csp_usage_jobs':
                        $table->foreign('subscription_id')->references('id')->on('csp_subscriptions');
                        $table->index(['query_date', 'old', 'error_count']);
                        break;
                }

            });
        }

        Schema::create('csp_usage_job_errors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('job_id');
            $table->unsignedBigInteger('affected_row')->nullable();
            $table->string('error_type');
            $table->text('error_detail');
            $table->boolean('fixed')->default(0);
            $table->timestamps();

            $table->foreign('affected_row')->references('id')->on('csp_usages');
            $table->foreign('job_id')->references('id')->on('csp_usage_jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csp_usage_job_errors');

        foreach($this->getTables() as $tableName=>$extraColumns) {
            Schema::dropIfExists($tableName);
        }
    }
}
