<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 08/10/2017
 * Time: 14:20
 */

namespace App\Connectors\Common;


trait HasModules
{

    protected $_moduleNamespace = '';

    protected $_modules = array();

    public function __call($name, $arguments) {
        // magic method for loading modules:
        if(!array_key_exists($name, $this->_modules)) {
            if(!empty($this->_moduleNamespace)) {
                $className = $this->_moduleNamespace . '\\' . $name;
            } else {
                // assume module belong under a namespace equal to this api's class name
                $className = static::class . '\\' . $name;
            }
            if(class_exists($className)) {
                $this->_modules[$name] = new $className($this);
            } else {
                throw new \Exception('API module '.$name.' does not exist');
            }
        }
        return $this->_modules[$name];
    }

}