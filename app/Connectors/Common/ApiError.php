<?php


namespace App\Connectors\Common;

use GuzzleHttp\Psr7\Response;

class ApiError
{

    protected $_httpStatusCode;

    protected $_errorCode;

    protected $_errorMessage;


    public function __construct($response) {
        if($response instanceof Response) {
            $this->_httpStatusCode = $response->getStatusCode();
            if($response->hasHeader('Content-Type')) {
                if(preg_match('/application\/json/i', $response->getHeaderLine('Content-Type'))) {
                    $this->_readJsonError(json_decode($response->getBody()->getContents(), TRUE));
                    //$response->getBody()->rewind(); // DO NOT rewind - this ensures NULL is returned to the API calls, which means nothing weird will happen
                }
            } else {
                $this->_errorMessage = $response->getBody()->getContents();
                //$response->getBody()->rewind(); // DO NOT rewind - this ensures NULL is returned to the API calls, which means nothing weird will happen
            }
        } elseif(is_string($response)) {
            $this->_errorMessage = $response;
        }
    }

    protected function _readJsonError($data) {
        if(array_key_exists('error', $data)) {
            if(array_key_exists('code', $data['error'])) {
                $this->_errorCode = $data['error']['code'];
            }
            if(array_key_exists('message', $data['error'])) {
                $this->_errorMessage = $data['error']['message'];
            }
        } else {
            $this->_errorMessage = $data;
        }
    }

    public function httpStatusCode() {
        return $this->_httpStatusCode;
    }

    public function errorCode() {
        return $this->_errorCode;
    }

    public function errorMessage() {
        return $this->_errorMessage;
    }

}