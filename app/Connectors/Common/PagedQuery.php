<?php

namespace App\Connectors\Common;



trait PagedQuery
{

    /**
     * @var \stdClass|NULL
     */
    protected $_error = NULL;

    /**
     * @var ApiResponse
     */
    protected $_response = NULL;

    /**
     * @var mixed|NULL
     */
    protected $_data = NULL;

    /**
     * The row count in the currently loaded data set
     * @var integer|NULL
     */
    protected $_rowCount = NULL;

    /**
     * The index in the current data array for the next read. NULL = not loaded
     * @var integer|NULL
     */
    protected $_nextRow = NULL;

    /**
     * The total number of rows loaded so far for this query (may have more)
     * @var integer
     */
    protected $_totalRowsLoaded = 0;

    /**
     * Indicates if more data is available from the API for this query
     * @var boolean
     */
    protected $_moreRowsAvailable = TRUE;

    protected $_stringifyNumerics = FALSE;

    protected $_getDataAsObject = FALSE;

    abstract protected function getQueryUrl();
    abstract protected function getRowData($index); // depends on format of returned object
    abstract protected function decodeResponse(); // depends on format of returned object
    abstract public function getRowCount();

    public function load() {
        throw new \Exception('Deprecated');
        if($this->hasError()) {
            return FALSE;
        }
        $url = $this->getQueryUrl();
        if(empty($url)) {
            $this->_error = new \stdClass();
            $this->_error->code = 'NoQuery';
            $this->_error->message = 'No query was set';
            return FALSE;
        }

        $this->_response = $this->_get($url);
        if($this->_response->hasError()) {
            return FALSE;
        }
        if($this->decodeResponse()) {
            $this->_nextRow = 0;
            $this->_rowCount = $this->getRowCount();
            $this->_totalRowsLoaded += $this->_rowCount;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getNextRow() {
        if($this->hasError()) {
            return FALSE;
        }
        if(is_null($this->_nextRow)) {
            $this->load();
        }
        if($this->_nextRow >= $this->_rowCount) {
            if($this->_moreRowsAvailable) {
                $this->load();
            } else {
                return FALSE;
            }
        }
        return $this->getRowData($this->_nextRow++);
    }

    public function moreAvailable() {
        return $this->_moreRowsAvailable;
    }

    public function rowsLoaded() {
        return $this->_totalRowsLoaded;
    }

    public function hasError() {
        return !empty($this->_error);
    }

    public function getError() {
        return $this->_error;
    }


}