<?php

namespace App\Connectors\Common;


abstract class RestApiPagedQuery implements \Iterator
{

    const REGEX_UUID = '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}';

    protected $_rowObjectBaseClass = 'App\Connectors\Common\ApiObject';
    protected $_rowObjectNamespace = '';

    /**
     * @var RestApi
     */
    private $_api = NULL;

    protected $_auditPath = NULL;

    protected $_firstCallTime = NULL;

    /**
     * @var ApiResponse
     */
    protected $_response = NULL;

    /**
     * @var string|NULL Class name for the returned row objects; NULL = return as array
     */
    protected $_rowClass = NULL;

    /**
     * @var array
     */
    protected $_data = NULL;

    /**
     * @var integer|NULL The row count in the currently loaded data set
     */
    protected $_pageRowCount = NULL;

    /**
     * @var integer|NULL The index in the current data array for the next read. NULL = not loaded
     */
    protected $_currentRow = NULL;

    /**
     * @var integer The page number of the current set
     */
    protected $_currentPage = 0;

    /**
     * @var integer The total number of rows loaded so far for this query (may have more)
     */
    protected $_previousRowsLoaded = 0;

    /**
     * @var integer|NULL The total number of rows returned by this query if known
     */
    protected $_totalCount = NULL;

    /**
     * @var boolean Indicates if more data is available from the API for this query
     */
    protected $_moreRowsAvailable = TRUE;

    /**
     * @var bool Encode numbers as strings in all response object properties - useful to keep accuracy (to be overridden in child classes)
     */
    protected $_stringifyNumerics = FALSE;


//    abstract protected function decodeResponse();

    /**
     * @return ApiResponse
     */
    public function getResponse() {
        return $this->_response;
    }

    public function __construct(RestApi $api) {
        $this->_api = $api;
    }

    private function _tryClassNameVariants($classNameWithNamespace) {
        if(is_subclass_of($classNameWithNamespace, $this->_rowObjectBaseClass, TRUE)) {
            $this->_rowClass = $classNameWithNamespace;
            return TRUE;
        }
        // check from root namespace:
        if(substr($classNameWithNamespace, 0, 1)!='\\' AND is_subclass_of('\\'.$classNameWithNamespace, $this->_rowObjectBaseClass, TRUE)) {
            $this->_rowClass = '\\'.$classNameWithNamespace;
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Set the output row class name. If namespace is defined, it is added to the class name correctly.
     * If the namespace is not defined, it is determined by $this->_rowObjectNamespace or left empty.
     * Note that the specified class MUST be a sub-class of $this->_rowObjectBaseClass
     * @param string $className
     * @param string|NULL $namespace
     * @return boolean
     */
    public function setOutputClassName($className, $namespace=NULL) {
        assert('!empty($className)', 'Class name cannot be empty');
        // ignore namespace if the class is discoverable:
        if($this->_tryClassNameVariants($className)) {
            return TRUE;
        }
        $options = [$namespace, $this->_rowObjectNamespace, basename(get_class()), get_class()];
        foreach($options as $ns) {
            if(!empty($ns)) {
                if(substr($ns, -1)!='\\') {
                    $ns .= '\\';
                }
                if($this->_tryClassNameVariants($ns.$className)) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

//    protected function getQueryUrl() {
//        return $this->_nextPageLink;
//    }

    abstract protected function loadNextPage();

    /**
     * @return boolean
     */
    abstract protected function morePagesAvailable();

    protected function get($relativeUrl, $options=[]) {
        if(empty($this->_firstCallTime)) {
            $this->_firstCallTime = time();
        }
        if(array_key_exists('audit', $options)) {
            $options['audit'] = str_replace('{$time}', '{$time'.$this->_firstCallTime.'}', $options['audit']);
        } elseif(empty($this->_auditPath)) {
            $options['audit'] = '{$url}_'.'{$time='.$this->_firstCallTime.'}-{$page}';
        } else {
            $options['audit'] = str_replace('{$time}', '{$time='.$this->_firstCallTime.'}', $this->_auditPath);
        }
        $pageStr = str_pad($this->_currentPage, 4, '0', STR_PAD_LEFT);
        if(strpos($options['audit'], '{$page}')===FALSE) {
            $options['audit'] .= '-'.$pageStr;
        } else {
            $options['audit'] = str_replace('{$page}', $pageStr, $options['audit']);
        }
        $this->_response = $this->_api->get($relativeUrl, $options);
    }

    /**
     * @return bool
     */
    protected function getData() {
        $this->loadNextPage();
        if(!is_array($this->_data) OR !($this->_response instanceof ApiResponse) OR $this->_response->hasError()) {
            // no data loaded - make all array calls fail:
            $this->_currentRow = NULL;
            $this->_pageRowCount = 0;
            $this->_moreRowsAvailable = FALSE;
            return FALSE;
        }
        $this->_pageRowCount = count($this->_data);
        $this->_currentPage++;
        $this->_currentRow = 0;
        $this->_previousRowsLoaded += $this->_pageRowCount;
        $this->_moreRowsAvailable = $this->morePagesAvailable();
        return $this->_pageRowCount;
    }


    /**
     * @return integer|NULL
     */
    public function rowsLoaded() {
        return $this->_previousRowsLoaded + $this->_pageRowCount;
    }

    /**
     * @return boolean
     */
    public function hasError() {
        return ($this->_response instanceof ApiResponse) AND $this->_response->hasError();
    }

    public function getError() {
        return optional($this->_response)->getError();
    }

    public function current() {
        assert('is_array($this->_data)', 'No data loaded');
        $data = $this->_data[$this->_currentRow];
        // rowClass will have already been checked to be valid and directly instantiable
        if(!empty($this->_rowClass)) {
            $className = $this->_rowClass;
            return new $className($data, $this->_api);
        }
        return $data;
    }

    public function next() {
        $this->_currentRow++;
        // load next data page if out of bounds...
        if($this->_currentRow >= $this->_pageRowCount) {
            $this->getData();
            $this->_currentRow = 0;
        }
    }

    public function key() {
        return $this->_currentRow + $this->_previousRowsLoaded;
    }

    public function valid() {
        return (is_array($this->_data) AND ($this->_currentRow >= 0) AND ($this->_currentRow < $this->_pageRowCount));
    }

    /**
     * called at the beginning of each loop
     */
    public function rewind() {
        $this->reset();
        // perform first query to ascertain whether there are any results (required for valid() call that follows)
        $this->getData();
    }

    /**
     * Clear all data and start again - called in rewind() and also if query parameters are updated
     */
    public function reset() {
        $this->_response = NULL;
        $this->_data = NULL;
        $this->_currentRow = NULL;
        $this->_pageRowCount = NULL;
        $this->_moreRowsAvailable = TRUE;
        $this->_previousRowsLoaded = 0;
        $this->_totalCount = NULL;
    }

    /**
     * @return integer|NULL
     */
    public function count() {
        if(isset($this->_totalCount)) {
            return $this->_totalCount;
        }
        if(!$this->_moreRowsAvailable) {
            $this->_totalCount = $this->_previousRowsLoaded + $this->_pageRowCount;
        }
        // return freshly calculated value, or NULL (if there are more rows to load)
        return $this->_totalCount;
    }


}