<?php

namespace App\Connectors\Common;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\UriInterface;

class RestApi
{

    const AUDIT_LOAD_DISABLED = 0; // only load from API
    const AUDIT_LOAD_ENABLED = 1;  // load from audit store if available, otherwise from API
    const AUDIT_LOAD_STRICT = 10;  // only load from audit store

    /**
     * @var Client|NULL
     */
    protected $_client = NULL;

    /**
     * @var array
     */
    protected $_clientConfig = [
        'exceptions' => FALSE
    ];

    /**
     * @var integer The number of minutes to cache responses (0 = no caching)
     */
    protected $_cacheAge = 0;

    /**
     * @var \Illuminate\Filesystem\FilesystemAdapter|NULL
     */
    protected $_auditDisk = NULL;

    /**
     * @var string
     */
    protected $_auditDir = '';

    /**
     * @var boolean
     */
    protected $_auditAsSource = FALSE;


    // TODO: it would be really cool if we could add a read/write mode at the API layer and restrict REST operations to read/read+write operations respectively

    /**
     * @return Client|NULL
     */
    public function getClient() {
        if(!isset($this->_client)) {
            $this->_client = new Client($this->_clientConfig);
        }
        return $this->_client;
    }

    /**
     * @param string $relativeUrl
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function get($relativeUrl, $options=[]) {
        $request = new Request('GET', $relativeUrl);
        return $this->makeRequest($request, $options);
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function post($relativeUrl, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $body = json_encode($body);
        }
        $request = new Request('POST', $relativeUrl, [], $body);
        return $this->makeRequest($request, $options);
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function put($relativeUrl, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $body = json_encode($body);
        }
        $request = new Request('PUT', $relativeUrl, [], $body);
        return $this->makeRequest($request, $options);
    }

    /**
     * @param string $relativeUrl
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function delete($relativeUrl, $options=[]) {
        $request = new Request('DELETE', $relativeUrl);
        return $this->makeRequest($request, $options);
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function patch($relativeUrl, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $body = json_encode($body);
        }
        $request = new Request('PATCH', $relativeUrl, [], $body);
        return $this->makeRequest($request, $options);
    }

    /**
     * @param Request $request
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    public function makeRequest(Request $request, $options=[]) {
        $request_time = time();
        $auditFileName = array_key_exists('audit', $options) ? $options['audit'] : '{$url}_{$time}';
        // figure out true path:
        $auditFileName = $this->_auditDir . $auditFileName;
        $auditFileName = str_replace('{$url}', $request->getUri()->getPath(), $auditFileName);
        $auditFileName = str_replace('{$date}', date('Ymd'), $auditFileName);
        $auditFileName = str_replace('{$page}', '', $auditFileName) . '.log';
        // find the $time paramter, and check if it has a time specified - record and generalise if so:
        $matches = [];
        if(preg_match('/\{\$time(=(\d{10}))?\}/', $auditFileName, $matches)) {
            if(count($matches)==3) {
                $request_time = $matches[2]; // the time specified in the parameter
                $auditFileName = str_replace($matches[0], '{$time}', $auditFileName);
            }
        }

        unset($options['audit']);
        // add headers from $options array so we can keep a record in the request:
        if(array_key_exists('headers', $options)) {
            foreach($options['headers'] as $header=>$value) {
                if(!$request->hasHeader($header)) {
                    $request = $request->withHeader($header, $value);
                }
            }
            unset($options['headers']);
        }
        if($request->getMethod()=='GET' AND $this->_cacheAge>0) {
            $uri = $request->getUri();
            $params = [];
            foreach($request->getHeaders() as $header=>$value) {
                $name = strtolower($header);
                if(!in_array($name, ['authorization', 'accept', 'content-type'])) {
                    $params[] = $name.'='.$value;
                }
            }
            if(count($params)>0) {
                if(strpos($uri, '?')===FALSE) {
                    $uri .= '?' . implode('&', $params);
                } else {
                    $uri .= '&' . implode('&', $params);
                }
            }
            $uri = str_replace('/', '_', str_replace('?', '_', $uri));
            $cacheKey = basename(self::class) . '_'.$uri;

            if(Cache::has($cacheKey)) {
                $apiResponse = new ApiResponse();
                $apiResponse->setResponseBody(Cache::get($cacheKey));
                return $apiResponse;
            } else {
                $response = $this->getClient()->send($request, $options);
                // only store successful responses!
                if(!$response->hasError()) {
                    $body = (string) $response->response()->getBody()->getContents();
                    $response->response()->getBody()->rewind();
                    Cache::put($cacheKey, $body, $this->_cacheAge);
                }
            }
        } elseif($request->getMethod()=='GET' AND $this->_auditAsSource) {
            // see if audit file exists by scanning the directory part of the audit filename:
            $dirname = dirname($auditFileName);
            // TODO: come back in the year 2285 before time() returns a number longer than 10 digits
            $filename_regex = '/' . str_replace('{$time}', '\d{10}', basename($auditFileName)) .'/';
            $matches = [];
            if(!($this->_auditDisk instanceof \Illuminate\Filesystem\FilesystemAdapter)) {
                throw new \Exception('Invalid audit disk specified');
            }
            foreach($this->_auditDisk->listContents($dirname) as $fileMeta) {
                // check if basename matches our regex pattern:
                if(preg_match($filename_regex, $fileMeta['basename'])) {
                    $matches[] = $fileMeta['basename'];
                }
            }
            // sort matches so that the first one is the latest one:
            rsort($matches);
            if(count($matches)>0) {
                // we have a suitable source!
                try {
                    $data = $this->_auditDisk->read($dirname.'/'.$matches[0]);
                    if(strlen($data)>0) {
                        // we only want to populate the response with the response part of the audit file:
                        $pos = strpos($data, 'RESPONSE<<<<<<<<' . "\r\n");
                        if($pos>0) {
                            // now locate the end of the headers:
                            $pos2 = strpos($data, "\r\n\r\n", $pos);
                            if($pos2>0) {
                                $apiResponse = new ApiResponse();
                                $apiResponse->setResponseBody(substr($data, $pos2+4));
                                return $apiResponse;
                            }
                        }
                    }
                } catch (\Exception $e) {
                    // nothing to do - just fall out of if()
                }
            } elseif($this->_auditAsSource===self::AUDIT_LOAD_STRICT) {
                // file not found, and do not load from API - return error result
                $apiResponse = new ApiResponse();
                $apiResponse->setResponseBody('',404);
                return $apiResponse;
            }
            // no source - retrieve it form the API
            $response = $this->getClient()->send($request, $options);
        } else {
            $response = $this->getClient()->send($request, $options);
        }
        // store request & response in audit log if enabled:
        if(!is_null($this->_auditDisk)) {
            $auditFileName = str_replace('{$time}', $request_time, $auditFileName);
            $data  =  'REQUEST>>>>>>>>' . "\r\n" . \GuzzleHttp\Psr7\str($request);
            $data .= 'RESPONSE<<<<<<<<' . "\r\n" . \GuzzleHttp\Psr7\str($response);
            $this->_auditDisk->put($auditFileName, $data);
            $request->getBody()->rewind();
            $response->getBody()->rewind();
        }
        $apiResponse = new ApiResponse($response, $request);
        return $apiResponse;
    }

    /**
     * @param mixed $data
     * @param string|array $classNameOrPropertyMap
     * @return mixed
     * @throws \Exception
     */
    public function _normaliseObjectResponse($data, $classNameOrPropertyMap=NULL) {
        $result = new \stdClass();
        if(is_string($classNameOrPropertyMap)) {
            if(is_subclass_of($classNameOrPropertyMap, ApiObject::class, TRUE)) {
                return new $classNameOrPropertyMap($data, $this);
            } elseif(class_exists($classNameOrPropertyMap)) {
                return new $classNameOrPropertyMap($data);
            } else {
                throw new \Exception('Invalid class specified');
            }
        }
        if(is_array($classNameOrPropertyMap)) {
            foreach($classNameOrPropertyMap as $propName=>$arrayKey) {
                $arrayKeys = explode('.', $arrayKey);
                $currentArrayElement = $data;
                foreach($arrayKeys as $key) {
                    if(array_key_exists($key, $currentArrayElement)) {
                        $currentArrayElement = $currentArrayElement[$key];
                    } else {
                        $currentArrayElement = NULL;
                        break;
                    }
                }
                if(!is_string($propName)) {
                    if(count($arrayKeys)==0) {
                        continue;
                    }
                    $propName = array_pop($arrayKeys);
                }
                if(empty($currentArrayElement)) {
                    $result->$propName = NULL;
                } else {
                    $result->$propName = $currentArrayElement;
                }
            }
            return $result;
        } else {
            throw new \Exception('Invalid parameter');
        }
    }

    /**
     * @param array $data The JSON-decoded data in array format
     * @param string|array|NULL $classNameOrPropertyMap The fully-qualified class name for row data, or a property map array
     * @param string|NULL $arrayKeyProperty The object property to use as the returned array key (if we want an easy lookup)
     * @param string|NULL $listElement The array key for the JSON array we will be transforming into objects (default tries 'value','data','items')
     * @return array
     * @throws \Exception
     */
    public function _normaliseListResponse($data, $classNameOrPropertyMap=NULL, $arrayKeyProperty=NULL, $listElement=NULL) {
        if(empty($listElement)) {
            // if all array keys are numeric, then the base data array is our list:
            if(count(array_filter(array_keys($data), 'is_string')) == 0) {
                $listArray =& $data;
            } else {
                // try and figure out the property the list was stored against:
                foreach(['value', 'data', 'items'] as $v) {
                    if(array_key_exists($v, $data)) {
                        $listElement = $v;
                        break;
                    }
                }
                if(empty($listElement)) {
                    return []; // no list element identified
                }
                $listArray =& $data[$listElement];
            }
        } else {
            if(array_key_exists($listElement, $data)) {
                $listArray =& $data[$listElement];
            } else {
                return []; // list element is incorrect, or response is invalid
            }
        }
        $result = [];
        foreach($listArray as $element) {
            $object = $this->_normaliseObjectResponse($element, $classNameOrPropertyMap);
            if(isset($arrayKeyProperty) AND !empty($object->$arrayKeyProperty)) {
                $result[$object->$arrayKeyProperty] = $object;
            } else {
                $result[] = $object;
            }
        }
        return $result;
    }

    public function enableCaching($ageInMinutes=120) {
        if(is_integer($ageInMinutes)) {
            $this->_cacheAge = $ageInMinutes;
        } elseif(is_bool($ageInMinutes)) {
            $this->_cacheAge = $ageInMinutes ? 120 : 0;
        }
    }

    /**
     * @param string|\Illuminate\Filesystem\FilesystemAdapter|NULL $disk
     * @param string $basedir
     * @throws \Exception
     */
    public function enableAuditStore($disk, $basedir='') {
        if(is_null($disk)) {
            // disable auditing:
            $this->_auditDisk = NULL;
            $this->_auditDir = '';
//            if(array_key_exists('handler', $this->_clientConfig)) {
//                unset($this->_clientConfig['handler']);
//                // force recreation of Guzzle Client:
//                unset($this->_client);
//            }
        } else {
            if(is_string($disk)) {
                $this->_auditDisk = Storage::disk($disk);
            } elseif($disk instanceof \Illuminate\Filesystem\FilesystemAdapter) {
                $this->_auditDisk = $disk;
            } else {
                throw new \Exception('Invalid parameter');
            }
            $this->_auditDir = trim($basedir);
            if(substr($this->_auditDir, -1)!='/') {
                $this->_auditDir .= '/';
            }
            // add audit handler to client stack and force recreation of client:
//            $stack = HandlerStack::create();
//            $stack->push(Middleware::tap(NULL, function($request, $options, $promise) {
//                $promise->then(function($response) use($request, $options) {
//                    $this->storeRequest($request, $options, $response);
//                });
//            }));
//            $this->_clientConfig['handler'] = $stack;
//            unset($this->_client);
        }
    }

    /**
     * @param int $mode
     * @throws \Exception
     */
    public function loadFromAuditStore($mode=self::AUDIT_LOAD_ENABLED) {
        if($mode) {
            if(is_null($this->_auditDisk)) {
                throw new \Exception('Auditing must be enabled before attempting to load from audit store');
            }
        }
        $this->_auditAsSource = $mode;
    }





}