<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 08/10/2017
 * Time: 14:47
 */

namespace App\Connectors\Common;


use GuzzleHttp\Psr7\Request;

class RestApiModule
{

    /**
     * @var RestApi
     */
    protected $_api;

    /**
     * @var ApiResponse
     */
    protected $_lastResponse;

    public function __construct(RestApi $api) {
        $this->_api = $api;
    }

    /**
     * @return RestApi
     */
    protected function _getApi() {
        return $this->_api;
    }

    private function _objectClassName($className) {
        if(class_exists($className)) {
            return $className;
        } else {
            // get child class namespace:
            $c = get_class($this);
            $n = substr($c, 0, strrpos($c, '\\'));
            if(!empty($n) AND $n[0] != '\\') {
                $n = '\\' . $n;
            }
            // prepend child's namespace to specified class name:
            $className = $n.'\\'.$className;
            if(class_exists($className)) {
                return $className;
            }
        }
        return NULL;
    }

    /**
     * @param mixed $data
     * @param string|array $classNameOrPropertyMap
     * @param bool $skipClassCheck
     * @return mixed
     */
    protected function _normaliseObjectResponse($data, $classNameOrPropertyMap=NULL, $skipClassCheck=FALSE) {
        $result = new \stdClass();
        if(is_string($classNameOrPropertyMap)) {
            if($skipClassCheck) {
                return new $classNameOrPropertyMap($data);
            }
            $classNameOrPropertyMap = $this->_objectClassName($classNameOrPropertyMap);
            if(!empty($classNameOrPropertyMap)) {
                return new $classNameOrPropertyMap($data);
            }
        } elseif(is_array($classNameOrPropertyMap)) {
            foreach($classNameOrPropertyMap as $propName=>$arrayKey) {
                $arrayKeys = explode('.', $arrayKey);
                $currentArrayElement = $data;
                foreach($arrayKeys as $key) {
                    if(array_key_exists($key, $currentArrayElement)) {
                        $currentArrayElement = $currentArrayElement[$key];
                    } else {
                        $currentArrayElement = NULL;
                        break;
                    }
                }
                if(!is_string($propName)) {
                    if(count($arrayKeys)==0) {
                        continue;
                    }
                    $propName = array_pop($arrayKeys);
                }
                if(empty($currentArrayElement)) {
                    $result->$propName = NULL;
                } else {
                    $result->$propName = $currentArrayElement;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $data
     * @param string|array|NULL $classNameOrPropertyMap
     * @param string|NULL $arrayKeyProperty
     * @param string|NULL $listElement
     * @return array
     * @throws \Exception
     */
    protected function _normaliseListResponse($data, $classNameOrPropertyMap=NULL, $arrayKeyProperty=NULL, $listElement=NULL) {
        if(empty($listElement)) {
            // if all array keys are numeric, then the base data array is our list:
            if(count(array_filter(array_keys($data), 'is_string')) == 0) {
                $listArray =& $data;
            } else {
                // try and figure out the property the list was stored against:
                foreach(['value', 'data', 'items'] as $v) {
                    if(array_key_exists($v, $data)) {
                        $listElement = $v;
                        break;
                    }
                }
                if(empty($listElement)) {
                    return []; // no list element identified
                }
                $listArray =& $data[$listElement];
            }
        } else {
            if(array_key_exists($listElement, $data)) {
                $listArray =& $data[$listElement];
            } else {
                return []; // list element is incorrect, or response is invalid
            }
        }
        // optimisation: skip class checking in _normaliseObjectResponse():
        $skipClassCheck = FALSE;
        if(is_string($classNameOrPropertyMap)) {
            $classNameOrPropertyMap = $this->_objectClassName($classNameOrPropertyMap);
            if(empty($classNameOrPropertyMap)) {
                throw new \Exception('response class not found');
            }
            $skipClassCheck = TRUE;
        }
        $result = [];
        foreach($listArray as $element) {
            $object = $this->_normaliseObjectResponse($element, $classNameOrPropertyMap, $skipClassCheck);
            if(isset($arrayKeyProperty) AND !empty($object->$arrayKeyProperty)) {
                $result[$object->$arrayKeyProperty] = $object;
            } else {
                $result[] = $object;
            }
        }
        return $result;
    }

    /**
     * @return ApiResponse
     */
    public function lastResponse() {
        return $this->_lastResponse;
    }

    /**
     * @param Request $request
     * @param array $options
     * @return ApiResponse
     * @throws \Exception
     */
    protected function _makeRequest(Request $request, $options=[]) {
        $this->_lastResponse = $this->_api->makeRequest($request, $options);
        return $this->_lastResponse;
    }

    /**
     * @param string $relativeUrl
     * @param array $options
     * @return ApiResponse
     */
    protected function _get($relativeUrl, $options=[]) {
        $this->_lastResponse = $this->_api->get($relativeUrl, $options);
        return $this->_lastResponse;
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     */
    protected function _post($relativeUrl, $body=NULL, $options=[]) {
        $this->_lastResponse = $this->_api->post($relativeUrl, $body, $options);
        return $this->_lastResponse;
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     */
    protected function _put($relativeUrl, $body=NULL, $options=[]) {
        $this->_lastResponse = $this->_api->put($relativeUrl, $body, $options);
        return $this->_lastResponse;
    }

    /**
     * @param string $relativeUrl
     * @param array $options
     * @return ApiResponse
     */
    protected function _delete($relativeUrl, $options=[]) {
        $this->_lastResponse = $this->_api->delete($relativeUrl, $options);
        return $this->_lastResponse;
    }

    /**
     * @param string $relativeUrl
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiResponse
     */
    protected function _patch($relativeUrl, $body=NULL, $options=[]) {
        $this->_lastResponse = $this->_api->patch($relativeUrl, $body, $options);
        return $this->_lastResponse;
    }
}