<?php

namespace App\Connectors\Common;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * A wrapper for the Guzzle Response class to provide a couple of useful functions for API operations
 * @package App\Connectors\MicrosoftAzureApi
 *
 */
class ApiResponse
{

    /**
     * @var Response
     */
    protected $_response;

    /**
     * @var Request
     */
    protected $_request;

    /**
     * @var ApiError|NULL
     */
    protected $_error = NULL;

    /**
     * @var string Response body override - used for caching
     */
    protected $_response_body = NULL;

    protected $_chainErrorResponse = NULL;

    protected $_chainContinue = TRUE;

    protected $_httpStatusCode;


    public function __construct(Response $response=NULL, Request $request=NULL) {
        $this->_response = $response;
        $this->_request = $request;
        if($response instanceof Request) {
            $this->_httpStatusCode = $response->getStatusCode();
            if($this->_httpStatusCode<200 OR $this->_httpStatusCode>=300) {
                $this->_error = new ApiError($response);
            }
        }
    }

    /**
     * @return Response
     */
    public function response() {
        return $this->_response;
    }

    /**
     * @return Request
     */
    public function request() {
        return $this->_request;
    }

    public function getStatusCode() {
        return $this->_httpStatusCode ?: $this->_response->getStatusCode();
    }

    public function getResponseBody() {
        if(isset($this->_response_body)) {
            return $this->_response_body;
        } else {
            if(!$this->hasError()) {
                $this->_response_body = $this->_response->getBody()->getContents();
                $this->_response->getBody()->rewind();
                return $this->_response_body;
            } else {
                return NULL;
            }
        }
    }

    /**
     * @param boolean $numbersAsStrings Wrap object numeric properties as strings to ensure no rounding/overflow errors - fine with Elequent
     * @param boolean $asObject
     * @return array|object|null
     */
    public function decodeResponseBody($numbersAsStrings=TRUE, $asObject=FALSE) {
        if(!$this->_chainContinue) {
            return $this->_chainErrorResponse;
        }
        if(isset($this->_response_body)) {
            $body = $this->_response_body;
        } else {
            $body = $this->_response->getBody()->getContents();
        }
        // fixing up the UTF-8 Byte Order Mark if it's present:
        $utf8bom = implode('', [chr(0xEF), chr(0xBB), chr(0xBF)]);
        if(substr($body, 0, 3)==$utf8bom) {
            $body = substr($body, 3);
        }
        if($numbersAsStrings) {
            $body = preg_replace(
                /**
                 * REGEX PATTERN EXPLANATION:
                 * search for JSON object property names (will always end with [double-quote colon])
                 * then a numeric sequence (possibly with whitespace either side)
                 * followed by the end of the property value (denoted by comma)
                 * or the end of the object (denoted by right-curly-bracket)
                 *
                 * NOTE:
                 * ensures that the match is not in an encoded JSON string (by ensuring that the matching double-quote is not escaped with a backslash)
                 *
                 * LIMITATIONS:
                 * Only finds numeric values that are object properties:
                 *  - single numeric values are not encapsulated (e.g. JSON="123.987")
                 *  - arrays of numeric values are not encapsulated (e.g. JSON="[1,2,3,4,5]")
                 */
                '/([^\\\]"):\s*([0-9\.]+)\s*(,|})/',
                /**
                 * REPLACEMENT EXPLANATION:
                 * re-constitutes all elements with the numeric value encapsulated in double-quotes
                 */
                '$1:"$2"$3',
                $body
            );
        }
        return json_decode($body, !$asObject);
    }

    /**
     * @return boolean
     */
    public function hasError() {
        return !empty($this->_error);
    }

    /**
     * @return ApiError|NULL
     */
    public function getError() {
        return $this->_error;
    }

    /**
     * Ensures following chained functions give NULLed responses if the response was bad
     * @return $this
     */
    public function errorAsNull() {
        $this->_chainErrorResponse = NULL;
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->_response_body)) {
            return $this;
        }
        if(!$this->_response instanceof Response) {
            $this->_chainContinue = FALSE;
        } else {
            $status = $this->_response->getStatusCode();
            $this->_chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    /**
     * Ensures following chained functions give \stdClass() responses if the response was bad
     * @return $this
     */
    public function errorAsEmptyClass() {
        $this->_chainErrorResponse = new \stdClass();
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->_response_body)) {
            return $this;
        }
        if(!$this->_response instanceof Response) {
            $this->_chainContinue = FALSE;
        } else {
            $status = $this->_response->getStatusCode();
            $this->_chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    /**
     * Ensures following chained functions give empty array responses if the response was bad
     * @return $this
     */
    public function errorAsEmptyArray() {
        $this->_chainErrorResponse = [];
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->_response_body)) {
            return $this;
        }
        if(!$this->_response instanceof Response) {
            $this->_chainContinue = FALSE;
        } else {
            $status = $this->_response->getStatusCode();
            $this->_chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    public function setResponseBody($data, $statusCode=200) {
        $this->_response_body = $data;
        $this->_httpStatusCode = $statusCode;
    }

}