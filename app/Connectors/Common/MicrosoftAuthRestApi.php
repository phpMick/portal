<?php

namespace App\Connectors\Common;


use GuzzleHttp\Psr7\Request;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth as AadAuth;
use League\OAuth2\Client\Token\AccessToken;

class MicrosoftAuthRestApi extends RestApi
{

    const REGEX_UUID = '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}';

    /**
     * @var AadAuth
     */
    protected $_authProvider = NULL;

    protected $_authResource = NULL;

    public function __construct(AadAuth $authProvider=NULL) {
        // there may be occasions when this could be NULL
        // i.e. where the API call is to an unauthenticated endpoint (or we are trying to get an unauthenticated response error)
        $this->_authProvider = $authProvider;
        if($this->_authProvider instanceof AadAuth) {
            if(!$this->_authProvider->hasValidToken($this->_authResource)) {
                throw new \Exception('Unable to obtain access token for specified resource');
            }
        }
    }

    /**
     * @param Request $request
     * @param array $options
     * @param boolean $withAuthToken
     * @return ApiResponse
     * @throws \Exception
     */
    public function makeRequest(Request $request, $options=[], $withAuthToken=TRUE) {
        if(array_key_exists('withoutAuthToken', $options)) {
            if($options['withoutAuthToken']) {
                $withAuthToken = FALSE;
            }
            unset($options['withoutAuthToken']);
        }
        if(array_key_exists('withAuthToken', $options)) {
            if(is_bool($options['withAuthToken'])) {
                $withAuthToken = $options['withAuthToken'];
            } elseif(is_string($options['withAuthToken'])) {
                // use this strong as a token
                $request = $request->withHeader('Authorization', 'Bearer ' . $options['withAuthToken']);
                $withAuthToken = FALSE;
            } elseif($options['withAuthToken'] instanceof AccessToken) {
                $request = $request->withHeader('Authorization', 'Bearer ' . $options['withAuthToken']->getToken());
                $withAuthToken = FALSE;
            }
            unset($options['withAuthToken']);
        }
        if($withAuthToken) {
            $token = $this->_authProvider->getAccessToken($this->_authResource);
            $request = $request->withHeader('Authorization', 'Bearer ' . $token->getToken());
        }
        if($request->getBody()->getSize() AND !$request->hasHeader('Content-Type')) {
            $request = $request->withHeader('Content-Type', 'application/json');
        }
        if(!$request->hasHeader('Accept')) {
            $request = $request->withHeader('Accept', 'application/json');
        }
        return parent::makeRequest($request, $options);
    }

}