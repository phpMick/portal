<?php

namespace App\Connectors\Common;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $dispatchChildJobs = TRUE;

    /**
     * @var \Illuminate\Console\Command
     */
    public $cmd = NULL;

    protected function getExceptionString(\Exception $e) {
        $typeString = class_basename($e);
        return 'EXCEPTION on line ' . $e->getLine() . ' of '.$e->getFile() . ': ['.$typeString.'] "'.$e->getMessage().'"';
    }

    protected function dispatchNextJob(Job $job) {
        if($this->dispatchChildJobs) {
            // dispatch the next job on the same connection/queue as this job:
            if (isset($this->job)) {
                // was queued:
                dispatch($job->onConnection($this->connection)->onQueue($this->queue));
            } else {
                // was run synchronously:
                dispatch_now($job);
            }
        }
    }

    protected function runNextJob(Job $job) {
        if($this->dispatchChildJobs) {
            dispatch_now($job);
        }
    }


}