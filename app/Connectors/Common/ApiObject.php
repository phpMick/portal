<?php

namespace App\Connectors\Common;


class ApiObject
{

    const PROPERTY_MAP = [];

    const REGEX_UUID = '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}';

    /**
     * @var RestApi
     */
    protected $_api;

    /**
     * ApiObject constructor.
     * @param array|NULL $data
     * @param RestApi|NULL $api
     */
    public function __construct($data=NULL, RestApi $api=NULL) {
        $this->loadFromArray($data);
        $this->_api = $api;
    }

    /**
     * @param array $data
     * @return boolean
     */
    public function loadFromArray($data) {
        if(!is_array($data)) {
            return FALSE;
        }
        foreach(static::PROPERTY_MAP as $propName=>$arrayKey) {
            if(empty($arrayKey)) {
                continue;
            }
            $arrayKeys = explode('.', $arrayKey);
            $currentArrayElement = $data;
            foreach($arrayKeys as $key) {
                if(array_key_exists($key, $currentArrayElement)) {
                    $currentArrayElement = $currentArrayElement[$key];
                } else {
                    $currentArrayElement = NULL;
                    break;
                }
            }
            if(!is_string($propName)) {
                if(count($arrayKeys)==0) {
                    continue;
                }
                $propName = array_pop($arrayKeys);
            }
            if(!isset($currentArrayElement)) {
                $this->$propName = NULL;
            } else {
                $this->$propName = $currentArrayElement;
            }
        }
        return TRUE;
    }

    public function toArray() {
        // NEW VERSION which uses Reflection to get all properties:
        $result = [];
        $props = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach($props as $prop) {
            $propName = $prop->getName();
            if(isset($this->$propName)) {
                $result[$propName] = $this->$propName;
            }
        }
        /* OLD VERSION which uses PROPERTY_MAP, which doesn't include dynamically set properties
        $result = [];
        foreach(static::PROPERTY_MAP as $key=>$value) {
            // identify whether the property is the key or the value in the PROPERTY_MAP
            if(is_string($key)) {
                $propName = $key;
            } else {
                $propName = $value;
            }
            // skip over NULL properties so that we don't overwrite previously loaded values when loading into other models
            if(isset($this->$propName)) {
                $result[$propName] = $this->$propName;
            }
        }
        */
        return $result;
    }


}