<?php

$mysql_opts = [
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'strict' => true,
    'engine' => null,
];

$pgsql_opts = [
    'charset' => 'utf8',
    'schema' => 'public',
    'sslmode' => 'prefer',
];

$mssql_opts = [
    'charset' => 'utf8',
];

$sqlite_opts = [
    'prefix' => '',
];

$options = [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => 'portal',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
/*
        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],


        'mysql' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => env('DB_DRIVER', 'pgsql'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'reports' => [
            'driver' => env('REPORTS_DB_DRIVER', 'sqlsrv'),
            'host' => env('REPORTS_DB_HOST', 'localhost'),
            'port' => env('REPORTS_DB_PORT', '1433'),
            'database' => env('REPORTS_DB_DATABASE', 'bi'),
            'username' => env('REPORTS_DB_USERNAME', 'sa'),
            'password' => env('REPORTS_DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],
*/
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];

foreach(['PORTAL', 'AZURE', 'REPORTS'] as $conn) {
    $opts = [
        'driver' => env($conn.'_DB_DRIVER', ''),
        'host' => env($conn.'_DB_HOST', ''),
        'port' => env($conn.'_DB_PORT', ''),
        'database' => env($conn.'_DB_DATABASE', ''),
        'username' => env($conn.'_DB_USERNAME', ''),
        'password' => env($conn.'_DB_PASSWORD', ''),
    ];
    switch($opts['driver']) {
        case 'mysql': $opts = array_merge($opts, $mysql_opts); break;
        case 'pgsql': $opts = array_merge($opts, $pgsql_opts); break;
        case 'mssql': $opts = array_merge($opts, $mssql_opts); break;
        case 'sqlite': $opts = array_merge($opts, $sqlite_opts); break;
    }
    $options['connections'][strtolower($conn)] = $opts;
}

return $options;