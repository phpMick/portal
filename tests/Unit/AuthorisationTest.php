<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Customer;

class AuthorisationTest extends TestCase
{

    protected function checkAll($gateName, $allowedRoles, $user, $customer=NULL) {
        foreach(User::ROLE_ALL as $role) {
            $user->roles = [$role];
            if(isset($customer)) {
                $can = $user->can($gateName, $customer);
            } else {
                $can = $user->can($gateName);
            }
            if(in_array($role, $allowedRoles)) {
                $this->assertTrue($can);
            } else {
                $this->assertFalse($can);
            }
        }
    }


    /**
     * @test
     */
    public function restrict_partner_admin_access()
    {
        /* @var User $partner_user */
        $partner_user = factory(User::class, 'partner')->create();
        /* @var User $customer_user */
        $customer_user = factory(User::class, 'customer')->create();

        $allowed = [User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN];
        $this->checkAll('manage-partner', $allowed, $partner_user);
        $this->checkAll('manage-partner', [], $customer_user);
    }

    /**
     * @test
     */
    public function restrict_partner_access() {
        /* @var User $partner_user */
        $partner_user = factory(User::class, 'partner')->create();
        /* @var User $customer_user */
        $customer_user = factory(User::class, 'customer')->create();

        $allowed = [User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN, User::ROLE_PARTNER_VIEWALL, User::ROLE_PARTNER_USER];
        $this->checkAll('access-partner', $allowed, $partner_user);
        $this->checkAll('access-partner', [], $customer_user);
    }

    /**
     * @test
     */
    public function restrict_customer_admin_access() {
        /* @var Customer $customer */
        $customer = factory(Customer::class)->create();
        /* @var Customer $customer2 */
        $customer2 = factory(Customer::class)->create();
        /* @var User $partner_user */
        $partner_user = factory(User::class, 'partner')->create();
        /* @var User $customer_user */
        $customer_user = factory(User::class, 'customer')->create(['customer_id'=>$customer->id]);

        $allowed = [User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN];
        $this->checkAll('manage-customer', $allowed, $partner_user, $customer);
        $this->checkAll('manage-customer', $allowed, $partner_user, $customer2);

        $allowed = [User::ROLE_CUSTOMER_ADMIN];
        $this->checkAll('manage-customer', $allowed, $customer_user, $customer);
        $this->checkAll('manage-customer', [], $customer_user, $customer2);
    }

    /**
     * @test
     */
    public function restrict_customer_access() {
        /* @var Customer $customer */
        $customer = factory(Customer::class)->create();
        /* @var Customer $other_customer */
        $other_customer = factory(Customer::class)->create();
        /* @var User $partner_user */
        $partner_user = factory(User::class, 'partner')->create();
        /* @var User $customer_user */
        $customer_user = factory(User::class, 'customer')->create(['customer_id'=>$customer->id]);

        // priviledged partner users:
        $allowed = [User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN, User::ROLE_PARTNER_VIEWALL];
        $this->checkAll('access-customer', $allowed, $partner_user, $customer);
        $this->checkAll('access-customer', $allowed, $partner_user, $other_customer);

        // standard partner user
        $partner_user->customers()->save($customer);
        $allowed2 = [User::ROLE_GLOBAL_ADMIN, User::ROLE_PARTNER_ADMIN, User::ROLE_PARTNER_VIEWALL, User::ROLE_PARTNER_USER];
        $this->checkAll('access-customer', $allowed2, $partner_user, $customer);
        // don't allow standard user access to another customer:
        $this->checkAll('access-customer', $allowed, $partner_user, $other_customer);

        // customer users:
        $allowed = [User::ROLE_CUSTOMER_ADMIN, User::ROLE_CUSTOMER_USER];
        $this->checkAll('access-customer', $allowed, $customer_user, $customer);
        $this->checkAll('access-customer', [], $customer_user, $other_customer);

    }



}
