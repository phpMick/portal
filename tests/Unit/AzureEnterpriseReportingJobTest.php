<?php

namespace Tests\Unit;

use App\CloudServices\Azure\Jobs\AzureEnterpriseAgreement\Initialise;
use App\CloudServices\Azure\Models\Agreement;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Tests\TestCase;

class AzureEnterpriseReportingJobTest extends TestCase
{
    use DispatchesJobs;

    /**  @test */
    public function a_valid_ea_agreement_is_accessible()
    {
        /** @var Agreement $agreement */
        $agreement = factory(Agreement::class)->states('sce-valid')->create();
        $job = new Initialise($agreement);
        $this->dispatch($job);
        $this->assertEquals(Agreement::STATUS_OK, $agreement->fresh()->status);
    }

    /**  @test */
    public function an_agreement_with_no_credentials_is_marked_correctly()
    {
        /** @var Agreement $agreement */
        $agreement = factory(Agreement::class)->create(['type'=>Agreement::TYPE_SCE]);
        $job = new Initialise($agreement);
        $this->dispatch($job);
        $this->assertEquals(Agreement::STATUS_ERROR_NO_CREDENTIALS, $agreement->fresh()->status);
    }

    /**  @test */
    public function an_agreement_with_invalid_credentials_is_marked_correctly()
    {
        /** @var Agreement $agreement */
        $agreement = factory(Agreement::class)->states('sce-invalid-key')->create();
        $job = new Initialise($agreement);
        $this->dispatch($job);
        $this->assertEquals(Agreement::STATUS_ERROR_DENIED, $agreement->fresh()->status);


        /** @var Agreement $agreement */
        $agreement2 = factory(Agreement::class)->states('sce-invalid-secret')->create();
        $job = new Initialise($agreement2);
        $this->dispatch($job);
        $this->assertEquals(Agreement::STATUS_ERROR_DENIED, $agreement2->fresh()->status);
    }

    /** @test */
    public function an_agreement_job_updates_the_list_of_available_files() {
        /** @var Agreement $agreement */
        $agreement = factory(Agreement::class)->states('sce-valid')->create();
        $job = new Initialise($agreement);
        $this->dispatch($job);

    }


}
