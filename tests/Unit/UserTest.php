<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserGroup;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    private function makeUserWithUserGroup(&$user, &$user_group)
    {
        //make a user
        $user = create(User::class);

        //give it a role
        $user_group = create(UserGroup::class);

        //assign it
        $user->user_groups()->attach($user_group->id);

        //log them in so that session gets populated
        auth()->login($user);
    }

    /**
     * @test
     * @return void
     */
    public function success_test_for_hasUserGroupName()
    {
        $this->makeUserWithUserGroup($user, $user_group);

        //test the hasRoleName function
        $this->assertTrue($user->hasUserGroupName($user_group->name));

    }

    /**
     * @test
     * @return void
     */
    public function failure_test_for_hasUserGroupName()
    {
        //make a user
        $user = create(User::class);

        //make a group
        $user_group = create(UserGroup::class);

        auth()->login($user);

        //test the hasRoleName function
        $this->assertFalse($user->hasUserGroupName($user_group->name));
    }

    /**
     * test for hasAnyUserGroupName
     * @test
     * @return void
     */
    public function success_test_for_hasAnyUserGroupName()
    {
        $this->makeUserWithUserGroup($user, $user_group);

        //string
        $this->assertTrue($user->hasAnyUserGroupName($user_group->name));

        $otherGroup = create(UserGroup::class);
        //array
        $this->assertTrue($user->hasAnyUserGroupName([$otherGroup->name,$user_group->name]));
    }

    /**
     * test for hasAnyUserGroupName
     * @test
     * @return void
     */
    public function failure_test_for_hasAnyUserGroupName()
    {
        $this->makeUserWithUserGroup($user, $user_group);

        $otherGroup = create(UserGroup::class);

        //string
        $this->assertFalse($user->hasAnyUserGroupName($otherGroup->name));

        //array
        $this->assertFalse($user->hasAnyUserGroupName([$otherGroup->name]));
    }







}
