<?php

namespace Tests\Unit;

use App\Connectors\MicrosoftAzureActiveDirectoryAuth;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth\AuthState;
use App\Connectors\MicrosoftPartnerCenterApi;
use App\Connectors\MicrosoftPartnerCenterApi\Customers\CustomerQuery;
use App\Connectors\MicrosoftPartnerCenterApi\Customers\SimpleCustomer;
use Tests\TestCase;

class MicrosoftPartnerCenterApiTest extends TestCase
{

    /**
     * @return MicrosoftAzureActiveDirectoryAuth
     */
    protected function _getAad() {
        return MicrosoftAzureActiveDirectoryAuth::service(MicrosoftAzureActiveDirectoryAuth::APP_PARTNER, new AuthState());
    }

    /**
     * @return MicrosoftPartnerCenterApi
     */
    protected function _getApi() {
        return new MicrosoftPartnerCenterApi($this->_getAad());
    }


    /** @test */
    public function the_api_can_get_an_access_token()
    {
        $aad = $this->_getAad();
        $token = $aad->getAccessToken(MicrosoftAzureActiveDirectoryAuth::RESOURCE_PARTNER_CENTER);
        $this->assertNotNull($token, 'Access Token for Microsoft Partner Center not obtained from Authentication Service');
    }

    /** @test */
    public function the_api_can_retrieve_a_multipage_list_of_customers() {
        $api = $this->_getApi();
        $query = $api->Customers()->customers();
        $query->pageSize = 5;
        $rowCount = 0;
        foreach($query as $row) {
            $this->assertTrue($row instanceof SimpleCustomer, 'returned rows must be a SimpleCustomer object');
            if($row instanceof SimpleCustomer) {
                $rowCount++;
            }
        }
        $this->assertGreaterThan(6, $rowCount, 'Did not retrieve more than one page of customers');
        $this->assertEquals($rowCount, $query->count(), 'Count does not equal the number of customer rows retrieved');
    }

    /** @test */
    public function the_api_can_retrieve_a_filtered_list_of_customers() {
        $api = $this->_getApi();
        $query = $api->Customers()->customers();
        $query->whereStartsWith(CustomerQuery::FIELD_COMPANYNAME, 'BSS');
        $rowCount = 0;
        foreach($query as $row) {
            $this->assertTrue($row instanceof SimpleCustomer, 'returned rows must be a SimpleCustomer object');
            if($row instanceof SimpleCustomer) {
                $rowCount++;
            }
        }
        $this->assertGreaterThanOrEqual(1, $rowCount, 'Did not retrieve at least one matching customer');
        $this->assertEquals($rowCount, $query->count(), 'Count does not equal the number of customer rows retrieved');
    }



}
