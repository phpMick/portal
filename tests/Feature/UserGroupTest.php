<?php

namespace Tests\Feature;

use App\Connectors\MicrosoftPartnerCenterApi\Models\Customer;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//Models
use App\Models\User;
use App\Models\UserGroup;




class UserGroupTest extends TestCase
{

    //Testing the ability to select a user_group--------------------


    private function see_choose_user_group()
    {
        $this->get(route('home'))
            ->assertSee('Choose User Group');
    }

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_see_choose_user_group_menu()
    {
        $this->signInMasterAdmin();
        $this->see_choose_user_group();
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_see_choose_user_group_menu()
    {
        $this->signInAdmin();
        $this->see_choose_user_group();
    }


    /**
     * @test
     * @return void
     */
    public function an_user_cannot_see_choose_user_group_menu()
    {
        $user = $this->signInUser();

        $this->get(route('home'))
        ->assertDontSee('Choose User Group');
    }


//Ability to visit select user group -----------------------------------------------------------------------------------

    private function can_visit_select_user_group()
    {
        $this->get(route('user-groups.select-list'))
            ->assertSee('Select a User Group');
    }

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_visit_select_user_group()
    {
        $this->signInMasterAdmin();
        $this->can_visit_select_user_group();
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_visit_select_user_group()
    {
        $this->signInAdmin();
        $this->can_visit_select_user_group();
    }

    /**
     * @test
     * @return void
     */
    public function a_user_can_visit_select_user_group()
    {
        $user = $this->signInUser();

        $this->get(route('user-groups.select-list'))
            ->assertStatus(403);
    }



    private function select_a_user_group($customer = null)
    {

        if(!isset($customer)) {
            //create a customer
            $customer = factory(UserGroup::class, 'customer')->create();
        }

        //select it
        $this->get(route('user-groups.select',['user-group' => $customer->id]));

    }


    private function can_select_a_user_group($user = null)
    {
        //create a customer
        $customer = factory(UserGroup::class,'customer')->create();

        if(isset($user)) {
            //add this to the users groups
            $userGroup = UserGroup::findOrFail($user->user_group_id);

            $userGroup->appendNode($customer);
        }

        //this will redirect
        $this->post(route('user-groups.select',['user_group_id' => $customer->id]));

        //should be in session
        $selectedGroup = (session('selectedUserGroup'));

        $this->assertTrue($selectedGroup->id === $customer->id);
    }

    //Ability to select user group ---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_select_a_user_group()
    {
        $this->signInMasterAdmin();
        $this->can_select_a_user_group();
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_select_a_user_group()
    {
        $user = $this->signInAdmin();

        $this->can_select_a_user_group($user);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_user_can_select_a_user_group()
    {
        $user = $this->signInUser();

        $customer = factory(UserGroup::class, 'customer')->create();

        $this->post(route('user-groups.select',['user_group_id' => $customer->id]))
        ->assertStatus(403);
    }


    //Testing the ability to visit create a user_group------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_visit_create_a_user_group()
    {
        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.create'))
        ->assertSee('Add a User Group');

    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_visit_create_a_user_group()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.create'))
            ->assertSee('Add a User Group');

    }


    private function select_my_user_group($user)
    {

        $topGroup = UserGroup::findOrFail($user->user_group_id);

        $customer = create(UserGroup::class,['type' => UserGroup::CUSTOMER_TYPE]);

        //assign to hierarchy
        $topGroup->appendNode($customer);

        //must select user_group
        $this->select_a_user_group($customer);

    }


    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_visit_create_a_user_group()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.create'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_visit_create_a_user_group()
    {
        //must select user_group
        $this->select_a_user_group();

        $this->get(route('user-groups.create'))
            ->assertRedirect('/login');
    }


    //Ability to post a new user group ---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_post_a_new_user_group()
    {
        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1']);

        $this->assertDatabaseHas('user_groups',['name'=>'Test','type'=>'1']);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_post_a_new_user_group()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1']);

        $this->assertDatabaseHas('user_groups',['name'=>'Test','type'=>'1']);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_post_a_new_user_group()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1'])
        ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_post_a_new_user_group()
    {
        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1'])
            ->assertRedirect('/login');
    }


    //Cannot post without selecting a group ---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_cannot_post_a_new_user_group_without_selected_user_group()
    {
        $this->signInMasterAdmin();

        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1'])
            ->assertRedirect(route('user-groups.select-list'));
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_post_a_new_user_group_without_selected_user_group()
    {
        $user = $this->signInAdmin();

        $this->post(route('user-groups.store'),['name'=>'Test','type'=>'1'])
            ->assertRedirect(route('user-groups.select-list'));;
    }


    //Can view index with edit buttons---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_edit_index_user_groups()
    {
        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.index'))
            ->assertSee('Delete');

    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_edit_index_user_groups()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.index'))
            ->assertSee('Delete');

    }



    //view index without edit buttons---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_customer_user_can_edit_index_user_groups()
    {
        $this->signInUser();

        //must select user_group
        $this->select_a_user_group();

        $this->get(route('user-groups.index'))
            ->assertDontSee('Delete');

    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_user_cannot_visit_index_user_groups()
    {
        $test = $this->get(route('user-groups.index'))
            ->assertRedirect(route('login'));
    }



    //can delete user_group---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_delete_user_group()
    {
        $user = $this->signInMasterAdmin();

        $this->createAndDeleteCustomer();
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_delete_user_group()
    {
        $user = $this->signInAdmin();

        $parent = $user->user_group;

        $this->createAndDeleteCustomer($parent);
    }

    private function createAndDeleteCustomer($parent = null)
    {
        $customer = $this->createAndSelectCustomer();

        if(isset($parent)){
            $parent->appendNode($customer);
        }

       $response =  $this->DELETE(route("user-groups.destroy",[$customer->id]));
        $this->assertSoftDeleted('user_groups',['id' => $customer->id]);
    }


    //cannot delete user_group---------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_delete_user_group()
    {
        $user = $this->signInUser();

        $parent = $user->user_group;

        $customer = $this->createAndSelectCustomer();

        $parent->appendNode($customer);

        $this->DELETE(route("user-groups.destroy",[$customer->id]))
        ->assertStatus(403);
    }




    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_delete_user_group()
    {
        $customer = factory(UserGroup::class, 'customer')->create();

        $this->DELETE(route("user-groups.destroy",[$customer->id]))
            ->assertRedirect(route('login'));
    }

    //can edit user_group---------------------------------------------------------------------------------


    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_edit_user_group()
    {
        $this->signInMasterAdmin();

        $this->createAndEditCustomer();

    }

    private function createAndEditCustomer($user = null)
    {
        $customer = $this->createAndSelectCustomer();

        if(isset($user)){//if not master need access through the hierarchy
            $parent = $user->user_group;
            $parent->appendNode($customer);
        }

        $this->get(route("user-groups.edit",[$customer->id]))
        ->assertSee($customer->name);
    }




    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_edit_user_group()
    {
        $user = $this->signInAdmin();

        $this->createAndEditCustomer($user);

    }

    //cannot edit user_group---------------------------------------------------------------------------------
    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_edit_user_group()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectCustomer();

        $parent = $user->user_group;
        $parent->appendNode($customer);


        $this->get(route("user-groups.edit",[$customer->id]))
            ->assertStatus(403);
    }
    //unauth
    /**
     * @test
     * @return void
     */
    public function unathenticated_cannot_edit_user_group()
    {
        $customer = factory(UserGroup::class, 'customer')->create();

        $this->get(route("user-groups.edit",[$customer->id]))
            ->assertRedirect('login');
    }






}
