<?php

namespace Tests\Feature;



use Faker;

use Tests\TestCase;

//Models
use App\Models\Agreement;
use App\Models\AgreementFactory;
use App\Models\Customer;
use App\Models\User;




class AgreementTest extends TestCase
{

    const EA_EXAMPLE_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlE5WVpaUnA1UVRpMGVPMmNoV19aYmh1QlBpWSJ9.eyJFbnJvbGxtZW50TnVtYmVyIjoiNTMxMDkxODAiLCJJZCI6IjQzN2Y1MjVlLTI5ZWEtNGY5Zi05MzQzLWY0ZDMxN2NmNDExMCIsIlJlcG9ydFZpZXciOiJFbnRlcnByaXNlIiwiUGFydG5lcklkIjoiIiwiRGVwYXJ0bWVudElkIjoiIiwiQWNjb3VudElkIjoiIiwiaXNzIjoiZWEubWljcm9zb2Z0YXp1cmUuY29tIiwiYXVkIjoiY2xpZW50LmVhLm1pY3Jvc29mdGF6dXJlLmNvbSIsImV4cCI6MTUxODk0OTE1NSwibmJmIjoxNTAzMDUxNTU1fQ.maN-R0d74BYkIZbywuFcwGb1pibYwVXM_o9Wl45ZrKKghdkB4vxcmWh0VFCetN0arvW8vY9kbAbIw0WDsVBD2AFWk0gvDRIx6jd_l2Gz_gabLSmoS9hgwJ_nuy_UIhaz9LVFTCsc9PEICJ4lwcQ3Vv-tcwrZZyPSPYbgwTZmwIMmN4TzM07Sj84PDhBQLJp-7JgYJ_NX3xyfw3C7qsZ_RWhckM3Nm-nOFYudGWblMyoiYjvTJJ4UaU0iX0fMv1041wbBXMGMFK6D82Pd0mATUzvc6ThExEM029iA1vbxGbfr2i7zSxVTBW4feCMxvX0RoVfrTeZAKTznlcEEiG3-Kg";
    const EA_EXAMPLE_ENROLLMENT_NUMBER = "53109180";


    //make--------------------------------------------------------------------------------------------------------------make

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_make_agreements()
    {
        $user = $this->signInMasterAdmin();

        $this->createAndSelectCustomer();

        $this->get(route('agreements.make'))
        ->assertSee('Add an Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_make_agreements()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('agreements.make'))
            ->assertSee('Add an Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_make_agreements_for_other_customer()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectCustomer($user);

        $this->get(route('agreements.make'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function an_user_cannot_view_make_agreements()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectCustomer($user);

        $this->get(route('agreements.make'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_view_make_agreements()
    {
        $this->get(route('agreements.make'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------ view create


    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_create_agreements()
    {
        $user = $this->signInMasterAdmin();

        $this->createAndSelectCustomer();

        //we need a provider and type
        $provider = AgreementFactory::SERVICE_PROVIDERS[0]['provider'];
        $type  = AgreementFactory::TYPE_AZURE_EA;

        $this->get(route('agreements.create',['provider' => $provider, 'type' => $type]))
            ->assertSee('Add an Agreement');
    }


    /**
     * @test
     * @return void
     */
    public function admin_can_view_create_agreements()
    {
        $user = $this->signInAdmin();

        $this->createAndSelectChildCustomer($user);

        //we need a provider and type
        $provider = AgreementFactory::SERVICE_PROVIDERS[0]['provider'];
        $type  = AgreementFactory::TYPE_AZURE_EA;

        $this->get(route('agreements.create',['provider' => $provider, 'type' => $type]))
            ->assertSee('Add an Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function admin_can_view_create_agreements_for_another_customer()
    {
        $user = $this->signInAdmin();

        $this->createAndSelectCustomer($user);

        //we need a provider and type
        $provider = AgreementFactory::SERVICE_PROVIDERS[0]['provider'];
        $type  = AgreementFactory::TYPE_AZURE_EA;

        $this->get(route('agreements.create',['provider' => $provider, 'type' => $type]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_create_agreements()
    {
        $user = $this->signInAdmin();

        $this->createAndSelectCustomer($user);

        //we need a provider and type
        $provider = AgreementFactory::SERVICE_PROVIDERS[0]['provider'];
        $type  = AgreementFactory::TYPE_AZURE_EA;

        $this->get(route('agreements.create',['provider' => $provider, 'type' => $type]))
            ->assertStatus(403);
    }

    //index--------------------------------------------------------------------------------------------------------------index

    private function  makeAgreement($user)
    {
        $agreement = create(Agreement::class,['user_group_id' => $user->user_group_id]);

        $agreement->user_group->name;

        return $agreement;
    }

    /**
     * There needs to be a selected customer prior to visiting the create page.
     */
    private function visitIndexPage()
    {
        $customer = $this->createAndSelectCustomer();

        $this->get(route('agreements.index'))
            ->assertSee('Add Agreement')
            ->assertSee('Agreements');
    }

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_and_add_agreements_index()
    {
        $this->signInMasterAdmin();

        $customer = $this->createAndSelectCustomer();

        $agreement = create(Agreement::class,['user_group_id'=>$customer->id,'type'=>AgreementFactory::TYPE_AZURE_EA]);


        $this->get(route('agreements.index'))
            ->assertSee($agreement->name)
            ->assertSee('Add Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_and_add_agreements_index()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $agreement = create(Agreement::class,['user_group_id'=>$customer->id,'type'=>AgreementFactory::TYPE_AZURE_EA]);

        $this->get(route('agreements.index'))
            ->assertSee($agreement->name)
            ->assertSee('Add Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_can_view_not_add_agreements_index()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectChildCustomer($user);

        $agreement = create(Agreement::class,['user_group_id'=>$customer->id,'type'=>AgreementFactory::TYPE_AZURE_EA]);

        $this->get(route('agreements.index'))
            ->assertSee($agreement->name)
            ->assertDontSee('Add Agreement');
    }

    /**
     * @test
     * @return void
     */
    public function an_unauthenticated_user_cannot_view_agreements_index()
    {
        $this->get(route('agreements.index'))
            ->assertRedirect('/login');
    }


    //create------------------------------------------------------------------------------------------------------------create
    /**
     *
     * @test
     * @return void
     */
    public function a_master_admin_can_create_agreements()
    {
        $user = $this->signInMasterAdmin();
        $this->saveAndSeeAgreement($user);
    }


    private function saveAndSeeAgreement($user)
    {
        $customer = $this->createAndSelectChildCustomer($user);
        $agreement = $this->createAgreement($customer);

        $this->POST(route('agreements.store'),$agreement->toArray());
        $this->assertDatabaseHas('agreements',['name' => $agreement->name]);
    }

    private function createAgreement($customer)
    {
        return  make(Agreement::class,[
            'type'=>AgreementFactory::TYPE_AZURE_EA,
            'customer_id'=>$customer->id,
            'enrollment_number' => self::EA_EXAMPLE_ENROLLMENT_NUMBER,
            'access_key' => self::EA_EXAMPLE_KEY
        ]);
    }


    /**
     *
     * @test
     * @return void
     */
    public function an_admin_can_create_agreements()
    {
        $user = $this->signInAdmin();
        $this->saveAndSeeAgreement($user);
    }

    /**
     *
     * @test
     * @return void
     */
    public function a_user_cannot_create_agreements()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $agreement = $this->createAgreement($customer);

        $this->POST(route('agreements.store'),$agreement->toArray())
        ->assertStatus(403);

    }

    /**
     *
     * @test
     * @return void
     */
    public function authenticated_cannot_create_agreements()
    {

        $this->POST(route('agreements.store'))
            ->assertRedirect('login');
    }

    //edit------------------------------------------------------------------------------------------------------------edit
    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_edit_agreements()
    {
        $user = $this->signInMasterAdmin();

        $this->viewEditAgreements($user);
    }

    private function viewEditAgreements($user)
    {
        $customer = $this->createAndSelectChildCustomer($user);

        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_EA,'user_group_id' => $customer->id]);

        $this->get(route('agreements.edit',['id' => $agreement->id] ))
            ->assertSee($agreement->name);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_edit_their_agreements()
    {
        $user = $this->signInAdmin();

        $this->viewEditAgreements($user);

    }

    /**
 * @test
 * @return void
 */
    public function an_admin_cannot_edit_other_customers_agreements()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectCustomer($user);
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_EA,'user_group_id' => $customer->id]);

        $this->get(route('agreements.edit',['id' => $agreement->id] ))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_agreements()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_EA,'user_group_id' => $customer->id]);

        $this->get(route('agreements.edit',['id' => $agreement->id] ))
            ->assertStatus(403);
    }


    //update------------------------------------------------------------------------------------------------------------update
    /**
 * @test
 * @return void
 */
    public function a_master_admin_can_update_agreements()
    {
        $this->signInMasterAdmin();
        $customer = $this->createAndSelectCustomer();
        $this->updateAgreement($customer->id);
    }

    private function updateAgreement($customer_id)
    {
        $type = AgreementFactory::TYPE_AZURE_PAYG;
        $agreement = create(Agreement::class,['type'=>$type,'user_group_id'=>$customer_id]);
        //parameters
        $this->PATCH("agreements/$agreement->id",['type'=>$type,'user_group_id'=> $customer_id,'name'=> 'changed']);
        $this->assertDatabaseHas('agreements',['name' => 'changed']);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_update_their_customer_agreements()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->updateAgreement($customer->id);
    }


    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_update_other_customers_agreements()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectCustomer($user);

        $type = AgreementFactory::TYPE_AZURE_PAYG;
        $agreement = create(Agreement::class,['type'=>$type,'user_group_id'=>$customer->id]);
        //parameters
        $this->PATCH("agreements/$agreement->id",['type'=>$type,'user_group_id'=> $customer->id,'name'=> 'changed'])
        ->assertStatus(403);

    }


    /**
     * @test
     * @return void
     */
    public function unauthenicated_cannot_update_customers_agreements()
    {
        $agreement = create(Agreement::class);
        //parameters
        $this->PATCH("agreements/$agreement->id",[])
            ->assertRedirect(route('login'));
    }



    //Now the delete_____________________________________________________________________________________________________delete

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_delete_agreements(){

        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->createAndDeleteAgreement();
    }


    private function createAndDeleteAgreement($user_group_id = NULL)
    {

        if(isset($user_group_id)){
            $agreement = create(Agreement::class,['user_group_id'=>$user_group_id]);
        }else{
            $agreement = create(Agreement::class);
        }
        $response = $this->DELETE("/agreements/{$agreement->id}");

        $this->assertSoftDeleted('agreements',['id' => $agreement->id]);

    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_delete_their_own_agreements(){

        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $this->createAndDeleteAgreement($customer->id);

    }

    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_delete_other_customers_agreements(){

        $user = $this->signInAdmin();
        $customer = $this->createAndSelectCustomer($user);
        $agreement = create(Agreement::class,['user_group_id'=>$customer->id]);

        $response = $this->DELETE("/agreements/{$agreement->id}")
        ->assertStatus(403);
    }


    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_agreements()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectCustomer($user);
        $agreement = create(Agreement::class,['user_group_id'=>$customer->id]);

        $response = $this->DELETE("/agreements/{$agreement->id}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_delete_agreements()
    {
        $agreement = create(Agreement::class);

        $response = $this->DELETE("/agreements/{$agreement->id}")
            ->assertRedirect('login');
    }



    //Now the show_____________________________________________________________________________________________________show
    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_agreements()
    {
        $this->signInMasterAdmin();
        $this->createAndSelectCustomer();
        $this->viewAgreement();
    }

    private function viewAgreement()
    {
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_PAYG]);

        $this->get(route('agreements.show',['id' => $agreement->id] ))
            ->assertSee($agreement->name);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_own_agreements()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->viewOwnAgreement($customer);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_view_other_customers_agreements()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectCustomer($user);
        $this->viewOthersAgreement($customer);
    }

    private function viewOthersAgreement($customer)
    {
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_CSP,'user_group_id' => $customer->id]);

        $this->get(route('agreements.show',['id' => $agreement->id] ))
            ->assertStatus(403);
    }


    private function viewOwnAgreement($customer)
    {
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_CSP,'user_group_id' => $customer->id]);

        $this->get(route('agreements.show',['id' => $agreement->id] ))
            ->assertSee($agreement->name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_can_view_own_agreements()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->viewOwnAgreement($customer);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_other_customers_agreements()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectCustomer($user);
        $this->viewOthersAgreement($customer);
    }

    /**
     * @test
     * @return void
     */
    public function a_unauthenticated_user_cannot_view_agreements()
    {
        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_CSP]);

        $this->get(route('agreements.show',['id' => $agreement->id] ))
            ->assertSee('login');
    }

}
