<?php

namespace Tests\Feature;


use Tests\TestCase;

//Models
use App\Models\Customer;
use App\Models\Agreement;
use App\Models\AgreementFactory;


class AgreementMarginsTest extends TestCase
{

    //starting these again after Friday meeting 12/10/2018

    //Option to view margins -------------------------------------------------------------------------------------------
    /**
     * @test
     * @return void
     */
    public function a_global_admin_gets_view_margins_option()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->optionToViewMargins($customer);

    }

    private function  optionToViewMargins($customer)
    {
        $this->createCSPAgreement($customer);
        $this->get(route('agreements.index' ))
            ->assertSee('View Margins');
    }

    private function createCSPAgreement($customer)
    {
        //create a CSP agreement
        $agreement = create(Agreement::class,[
            'type'=>AgreementFactory::TYPE_AZURE_CSP,
            'user_group_id'=>$customer->id
        ]);

        return $agreement;

    }

    /**
     * @test
     * @return void
     */
    public function an_admin_gets_view_margins_option()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->optionToViewMargins($customer);

    }

    /**
     * @test
     * @return void
     */
    public function a_user_doesnt_get_view_margins_option()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->noOptionToViewMargins($customer);
    }

    private function noOptionToViewMargins($customer)
    {
        $this->createCSPAgreement($customer);

        $this->get(route('agreements.index' ))
            ->assertDontSee('View Margins');
    }


//Navigate to view margins page-----------------------------------------------------------------------------------------


    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_agreement_margins()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->canViewMargins($customer);
    }

    private function canViewMargins($customer)
    {
        $agreement = $this->createCSPAgreement($customer);

        $this->get(route('agreements.margins.show',['id' => $agreement->id] ))
            ->assertSee('Agreement Margins');
    }

    private function cannotViewMargins($customer)
    {
        $agreement = $this->createCSPAgreement($customer);

        $this->get(route('agreements.margins.show',['id' => $agreement->id] ))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_admin_can_view_agreement_margins()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->canViewMargins($customer);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_view_agreement_margins()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->cannotViewMargins($customer);
    }

    /**
     * @test
     * @return void
     */
    public function a_unauthenticated_user_cannot_view_edit_margins()
    {
        //create a CSP agreement
        $agreement = create(Agreement::class,['type'=>AgreementFactory::TYPE_AZURE_CSP]);

        $this->get(route('agreements.margins.show',['id' => $agreement->id] ))
            ->assertSee('login');
    }

    //Gets the edit button---------------------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_gets_edit_on_show_margins()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->getEditButtonOnViewMargins($customer);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_gets_edit_on_show_margins()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->getEditButtonOnViewMargins($customer);
    }


    private function getEditButtonOnViewMargins($customer)
    {
        //create a CSP agreement
        $agreement = $this->createCSPAgreement($customer);

        $this->get(route('agreements.margins.show',['id' => $agreement->id] ))
            ->assertSee('Edit Margins');
    }


    //GET Edit Margins------------------------------------------------------------------------------------------------------

    /**
     * @test
     * @return void
     */
    public function a_master_admin_gets_to_edit_margins()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->getToEditMargins($customer);
    }

    private function getToEditMargins($customer)
    {
        $agreement = $this->createCSPAgreement($customer);

        $this->get(route('agreements.margins.edit',['id' => $agreement->id] ))
            ->assertSee('Save Margins');
    }

    private function cantGetToEditMargins($customer)
    {
        //create a CSP agreement
        $agreement = $this->createCSPAgreement($customer);

        $this->get(route('agreements.margins.edit',['id' => $agreement->id] ))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_get_to_edit_margins()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->getToEditMargins($customer);

    }

    /**
     * @test
     * @return void
     */
    public function a_user_cant_get_to_edit_margins()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->cantGetToEditMargins($customer);

    }



    //storeMargins------------------------------------------------------------------------------------------------------
    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_store_margins()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->createValidMargins($start_date,$end_date,$margin);
        $this->postValidMargins($start_date,$end_date,$margin);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_store_margins()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->createValidMargins($start_date,$end_date,$margin);
        $this->postValidMargins($start_date,$end_date,$margin);
    }

    /**
     * @test
     * @return void
     */
    public function a_customer_user_cannot_store_margins()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->postAndRespond403();
    }


    private function postAndRespond403()
    {
        $agreement = create(Agreement::class);
        $this->createValidMargins($start_date,$end_date,$margin);

        $this->POST(route('agreements.margins.store',$agreement->id),['start_date' => $start_date,'end_date' => $end_date, 'margin' => $margin])
            ->assertStatus(403);

    }



    private function postValidMargins($start_date,$end_date,$margin,$agreement = NULL)
    {
        if(!isset($agreement)) {
            $agreement = create(Agreement::class);
        }

        //margins dates need to match agreement dates
        $start_date[0] = $agreement->start_date;
        $end_date[count($end_date)-1] = $agreement->end_date;


       $response = $this->POST(route('agreements.margins.store',$agreement->id),['start_date' => $start_date,'end_date' => $end_date, 'margin' => $margin,'agreement_id' => $agreement->id]);

        $this->assertDatabaseHas('agreement_margins',['agreement_id' => $agreement->id]);


    }

    private function createValidMargins(&$start_date,&$end_date,&$margin)
    {

        $start_date = array (
            0 => '2018-01-01',
            1 => '2018-01-07',
            2 => '2018-01-28',
        );

        $end_date =  array (
            0 => '2018-01-06',
            1 => '2018-01-27',
            2 => '2018-01-31',
        );

        $margin =  array (
            0 => '45.0000',
            1 => '22.0000',
            2 => '12',
        );

    }


    /**
     * @test
     * @return void
     */
    public function margins_with_invalid_dates_rejected()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '2018-01-06',
            1 => '2018-01-06',
            2 => '2018-01-27',
        );

        $end_date =  array (
            0 => '2018-01-01',
            1 => '2018-01-27',
            2 => '2018-01-31',
        );

        $margin =  array (
            0 => '45.0000',
            1 => '22.0000',
            2 => '12',
        );

        $this->postInvalidMargins($start_date,$end_date,$margin);

    }

    /**
     * @test
     * @return void
     */
    public function margins_with_non_concurrent_dates_rejected()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '2018-01-01',
            1 => '2018-01-07',//should be 06 to pass
            2 => '2018-01-27',
        );

        $end_date =  array (
            0 => '2018-01-06',
            1 => '2018-01-27',
            2 => '2018-01-31',
        );

        $margin =  array (
            0 => '45.0000',
            1 => '22.0000',
            2 => '12',
        );

        $this->postInvalidMargins($start_date,$end_date,$margin);

    }

    /**
     * @test
     * @return void
     */
    public function margins_with_invalid_margin_rejected()
    {

        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '2018-01-01',
            1 => '2018-01-06',
            2 => '2018-01-27',
        );

        $end_date =  array (
            0 => '2018-01-06',
            1 => '2018-01-27',
            2 => '2018-01-31',
        );

        $margin =  array (
            0 => '45.0000',
            1 => '22.0000',
            2 => '-5',
        );

        $this->postInvalidMargins($start_date,$end_date,$margin);


    }


    private function postInvalidMargins($start_date,$end_date,$margin)
    {
        $agreement = create(Agreement::class);

        $this->POST(route('agreements.margins.store',$agreement->id),['start_date' => $start_date,'end_date' => $end_date, 'margin' => $margin]);

        $this->assertDatabaseMissing('agreement_margins',['agreement_id' => $agreement->id]);
    }


    /**
     * @test
     * @return void
     */
    public function margins_with_null_start_date_accepted()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '',
        );

        $end_date =  array (
            0 => '2018-01-01',
        );

        $margin =  array (
            0 => '45.0000',
        );

        $this->postValidMargins($start_date,$end_date,$margin);

    }

    /**
     * @test
     * @return void
     */
    public function margins_with_null_dates_accepted()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '2018-01-01'
        );

        $end_date =  array (
            0 => '2018-01-06'
        );

        $margin =  array (
            0 => '45.0000'
        );

        $this->postValidMargins($start_date,$end_date,$margin);

    }

    /**
     * @test
     * @return void
     */
    public function margins_without_margin_rejected()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $start_date = array (
            0 => '',
        );

        $end_date =  array (
            0 => '',
        );

        $margin =  array (
            0 => '',
        );

        $this->postInvalidMargins($start_date,$end_date,$margin);

    }

    /**
     * @test
     * @return void
     */
    public function a_master_admin_cannot_store_margins_which_dont_match_agreement_dates()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->createValidMargins($start_date,$end_date,$margin);

        $agreement = create(Agreement::class);

        $this->POST(route('agreements.margins.store',$agreement->id),['start_date' => $start_date,'end_date' => $end_date, 'margin' => $margin,'agreement_id' => $agreement->id]);

        $this->assertDatabaseMissing('agreement_margins',['agreement_id' => $agreement->id]);
    }


    /**
     * @test
     * @return void
     */
    public function unauthenticated_user_cannot_post_margins()
    {
        $this->createValidMargins($start_date,$end_date,$margin);
        $agreement = create(Agreement::class);

        $this->POST(route('agreements.margins.store',$agreement->id),['start_date' => $start_date,'end_date' => $end_date, 'margin' => $margin,'agreement_id' => $agreement->id])
            ->assertSee('login');

    }



}
