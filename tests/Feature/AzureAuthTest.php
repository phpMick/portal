<?php

namespace Tests\Feature;



use Tests\TestCase;
use Mockery;

//Models
use App\Models\Microsoft\AadTenant;
use App\Models\Microsoft\AadUser;
use App\Models\User;


//Bytes
use App\CloudServices\Azure\AadLogin;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;

class AzureAuthTest extends TestCase
{
    //dummy data to retrieve from the MAADA
    private $userDetails = [
        'code' => '123456',
        'name' => 'joe',
        'username' => 'smith',
        'email' => 'joe@phpMick.co.uk',
        'domain' => 'phpMick.co.uk'
    ];

    private $authCode = '123456';

    private $tenant_id;

    public function tearDown()
    {
        Mockery::close();
    }

    //Helpers
    /**
     * Sets up the AadLogin
     * and attempts the login
     * @param $authCode
     * @param $MAADA
     * @throws \Exception
     */
    private function  simulateLogin( $MAADA){

        $requestArray = [];

        $aadLogin = new AadLogin($MAADA);

        $requestArray['code'] = $this->authCode;

        $aadLogin->attemptOAuthLogin($requestArray);

    }

    /**
     * Just does the assert for the session message
     * @param $expectedMessage
     */
    private function checkMessage($expectedMessage)
    {
        $sessionMessage = session('message');
        $this->assertEquals($expectedMessage,$sessionMessage);
    }




    //Start with the not aad_Tenant-------------------------------------------------------------------------------------


    /**
     * Mocks the MAADA
     *
     * @return Mockery\MockInterface
     */
    private function createMAADA($hasTenant,$tenantWithoutCustomer)
    {
        //mock the MAADA
        $MAADA = Mockery::mock('App\Connectors\MicrosoftAzureActiveDirectoryAuth');
        //add the functions
        $MAADA->shouldReceive('getUserDetails')->once()->with($this->authCode)->andReturn($this->userDetails);

        if ($hasTenant){
            //need an AadTenant
            if($tenantWithoutCustomer) {//not linked to customer
                $aadTenant = create(AadTenant::class,['user_group_id'=>null]);
            }else{//create linked to customer
                $aadTenant = create(AadTenant::class);
            }
        }else{//make, don't create
            $aadTenant = make(AadTenant::class);
        }

        $this->tenant_id = $aadTenant->id;

        $MAADA->shouldReceive('getTenantGuid')->once()->andReturn($aadTenant->guid);

        return $MAADA;
    }


    //Tests

    /**
     * @test
     * @return void
     * @throws \Exception
     */
    public function not_aad_tenant_or_standard_user()
    {
     //Arrange
        $MAADA = $this->createMAADA(false,false);
    //Act
        $this->simulateLogin($MAADA);
    //Assert
        $this->checkMessage("Your company is not currently signed up to this service.");
    }

    /**
     * When a standard user for a customer who is not an OAuth cus clicks the MS login
     * They should get an aad_user a record
     * and an aad_tenant record
     * @test
     * @return void
     * @throws \Exception
     */
    public function standard_user_no_aad_tenant_gets_records_created()
    {
        //Arrange
        $MAADA = $this->createMAADA(false,false);

        // and a standard user account
        $user = create(User::class,['email'=>$this->userDetails['email']]);

        //Act
        $this->simulateLogin($MAADA);
        //Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('aad_tenants',['user_group_id'=>$user->user_group_id]);
        $this->assertDatabaseHas('aad_users',['user_id'=>$user->id]);
    }

    //Now the tests with an aad_Tenant-------------------------------------------------------------------------------------

    /**
     * An aad_tenant record exists but without a customer associated
     * @test
     * @return void
     * @throws \Exception
     */
    public function aad_tenant_without_associated_customer()
    {
    //Arrange
        $MAADA = $this->createMAADA(true,true);

    //Act
        $this->simulateLogin($MAADA);
    //Assert
        $this->checkMessage("Your company is not currently signed up to this service.");
    }

    /**
     * An aad_tenant record exists and with a user_group_id
     * first one is ROLE_CUSTOMER_ADMIN
     * @test
     * @return void
     * @throws \Exception
     */
    public function first_user_for_OAuth_customer()
    {
    //Arrange
        $MAADA = $this->createMAADA(true,false);

    //Act
        $this->simulateLogin($MAADA);
    //Assert
        $this->assertDatabaseHas('users',['role' => User::ADMIN]);
    }

    /**
     * An aad_tenant record exists and with a user_group_id
     * first user will be ROLE_CUSTOMER_ADMIN
     * subsequent will be CUSTOMER_NOACCESS
     * @test
     * @return void
     * @throws \Exception
     */
    public function not_first_user_for_OAuth_customer()
    {
        //Arrange
        $MAADA = $this->createMAADA(true,false);
        //we need another aadUser
        create(AadUser::class,['tenant_id' => $this->tenant_id]);

        //Act
        $this->simulateLogin($MAADA);
        //Assert
        $this->assertDatabaseHas('users',['user_group_id' => NULL]);
        $this->checkMessage("New account created.");

    }


    /**
     * An aad_tenant record exists and with a user_group_id
     * first user will be ROLE_CUSTOMER_ADMIN
     * subsequent will be CUSTOMER_NOACCESS
     * @test
     * @return void
     * @throws \Exception
     */
    public function not_first_user_for_OAuth_customer_with_standard_user_account()
    {
        //Arrange
        $MAADA = $this->createMAADA(true,false);

        //we need another aadUser
        create(AadUser::class,['tenant_id' => $this->tenant_id]);
        // and a standard user account
        $user = create(User::class,['email'=>$this->userDetails['email']]);

        //Act
        $this->simulateLogin($MAADA);
        //Assert

        $this->assertAuthenticated();
    }


    // Customer admin can enable Azure login

    /**
     * @test
     * @throws \Exception
     */
    public function customer_admin_gets_admin_link()
    {
        $user = $this->signInAdmin();

        $this->createAndSelectChildCustomer($user);

        $this->get(route('home'))
            ->assertSee('Administration');
    }

    //doesnt

    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_doesnt_get_admin_link()
    {
        $this->signInMasterAdmin();
        $this->dontSeeAdministration();
    }


    /**
     * @test
     * @throws \Exception
     */
    public function customer_user_doesnt_get_admin_link()
    {
        $this->signInUser();
        $this->dontSeeAdministration();
    }



    private function dontSeeAdministration()
    {

        $this->get(route('user-groups.admin'))
            ->assertDontSee('Administration');
    }


    //can view the admin page ----------------------------------------------------------

    /**
     * @test
     * @throws \Exception
     */
    public function customer_admin_can_view_customer_admin()
    {
        $user = $this->signInAdmin();
        $this->createAndSelectChildCustomer($user);

        $this->get(route('user-groups.admin'))
            ->assertSee('User Group Administration');
    }

    //cannnot view the admin page ----------------------------------------------------------

    /**
     * @test
     * @throws \Exception
     */
    public function a_master_admin_cannot_view_customer_admin()
    {
        $user = $this->signInMasterAdmin();
        $this->createAndSelectCustomer($user);

        $this->view_customer_admin_gives_403();
    }


    /**
     * @test
     * @throws \Exception
     */
    public function a_customer_user_cannot_view_customer_admin()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->view_customer_admin_gives_403();
    }

    /**
     * @test
     * @throws \Exception
     */
    public function an_unauthenticated_user_cannot_view_customer_admin()
    {
        $this->get(route('user-groups.admin'))
            ->assertRedirect(route('login'));
    }

    private function view_customer_admin_gives_403()
    {
        $this->get(route('user-groups.admin'))
            ->assertStatus(403);
    }

    //can link to MS ----------------------------------------------------------

    /**
     * @test
     * @throws \Exception
     */
    public function customer_admin_can_link_to_MS()
    {
        //Arrange
        $MAADA = $this->createMAADA(false,false);

        //sign in
        $user = $this->signInAdmin();
        $this->createAndSelectChildCustomer($user);

        //Act
        $this->simulateLogin($MAADA);

        //Assert
        $this->assertDatabaseHas('aad_tenants',['user_group_id' => $user->user_group_id]);
    }

    //cannot link to MS ----------------------------------------------------------

    /**
     * Mocks the MAADA without function calls.
     * This is just so that we can catch an exception before
     * the function calls.
     *
     * @return Mockery\MockInterface
     */
    private function createMAADAWithoutFunctionCalls()
    {
        //mock the MAADA
        $MAADA = Mockery::mock('App\Connectors\MicrosoftAzureActiveDirectoryAuth');
        //add the functions
        $MAADA->shouldReceive('getUserDetails')->once()->with($this->authCode)->andReturn($this->userDetails);

        return $MAADA;
    }

    private function assert_access_denied()
    {
        $MAADA = $this->createMAADAWithoutFunctionCalls();

        //Act

        try {
            $test = $this->simulateLogin($MAADA);
        } catch (\Exception $e ){
            //Assert
            $this->assertContains ("Access denied",$e->getMessage());
        }
    }


    /**
     * @test
     * @throws \Exception
     */
    public function customer_user_cannot_link_to_MS()
    {
        //Arrange
        $user = $this->signInUser();
        $this->assert_access_denied();
    }

    /**
     * @test
     * @throws \Exception
     */
   /* public function master_admin_cannot_link_to_MS()
    {
        //Arrange
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);



        $this->assert_access_denied();
    }*/


    /**
     * Not sure how to test this, wouldnt get through the middleware
     */

    public function unauthenticated_user_cannot_link_to_MS()
    {

    }










}






