<?php

namespace Tests\Feature;

//Model
use App\Models\User;

use App\Mail\NewUser;


use Tests\TestCase;
use App\Events\UserCreated;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;


class AuthTest extends TestCase
{


    /**
     * @test
     * @throws \Exception
     */
    public function create_user_fires_event()
    {

        $newUser = $this->setupAndMakeNewUser();

        $this->expectsEvents(UserCreated::class);

        $this->POST(route('users.store'),$newUser->toArray());

    }

    private function setupAndMakeNewUser()
    {
        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $newUser = make(User::class,['role' => User::USER,'user_group_id' => $customer->id]);

        return $newUser;

    }

    private function setupAndCreateNewUser()
    {
        $user = $this->signInMasterAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $newUser = create(User::class,['role' => User::USER,'user_group_id' => $customer->id]);

        return $newUser;

    }

    /**
     * @test
     * @throws \Exception
     */
    public function create_normal_user_sends_email()
    {
        Mail::fake();

        $newUser = $this->setupAndMakeNewUser();

        $this->POST(route('users.store'),$newUser->toArray());

        Mail::assertSent(NewUser::class, function ($mail) use ($newUser) {
            return $mail->hasTo($newUser['email']);
        });

    }


    /**
     * @test
     * @throws \Exception
     */
    public function create_aad_user_sends_email()
    {
        Mail::fake();

        $newUser = $this->setupAndMakeNewUser();
        //make aadTenant
        //mbtodo mbhere

        $this->POST(route('users.store'),$newUser->toArray());

        Mail::assertSent(NewUser::class, function ($mail) use ($newUser) {
            return $mail->hasTo($newUser['email']);
        });

    }








    /**
     * @test
     * @throws \Exception
     */
    public function new_user_can_view_reset_password_page()
    {
        //need a token
        $user = $this->setupAndCreateNewUser();

        //just make token?
        $token = Password::getRepository()->create($user);

        $this->GET(route('new-user-reset',['token ' => $token,'id' => $user->id]))
        ->assertSee('Confirm Password');

    }


    /**
     * @test
     * @throws \Exception
     */
    public function new_user_can_change_their_password()
    {
        $user = $this->setupAndCreateNewUser();

        $oldPassword = $user->password;

        $token = Password::getRepository()->create($user);

        $parameters = [
            'token' => $token,
            'email' => $user->email,
            'password' => 'changedpassword',
            'password_confirmation' => 'changedpassword'
        ];

        auth()->logout();

       $response =  $this->POST('password/reset',$parameters);

        //check password in the db
        $user = $user->refresh();

        $this->assertTrue(Hash::check('changedpassword',$user->password));

    }

}






