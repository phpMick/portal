<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserGroup;
use Tests\TestCase;

use Faker;


class UserTest extends TestCase
{

    /**
     * @test
     * @throws \Exception
     */
    public function master_admin_must_select_user_group_before_list_users()
    {
        $this->signInMasterAdmin();

        $this->get(route('users.index'))
            ->assertRedirect(route('user-groups.select-list'));
    }

    /**
     * @test
     * @throws \Exception
     */
    public function admin_must_select_user_group_before_list_users()
    {
        $this->signInAdmin();

        $this->get(route('users.index'))
            ->assertRedirect(route('user-groups.select-list'));
    }

    /**
     * @test
     * @throws \Exception
     */
    public function master_admin_can_see_users_list()
    {
        $this->signInMasterAdmin();
        $this->createAndSelectCustomer();

        $this->get(route('users.index'))
            ->assertSee('Add User');
    }

    /**
     * @test
     * @throws \Exception
     */
    public function admin_can_see_users_list()
    {
        $user = $this->signInAdmin();
        $this->createAndSelectChildCustomer($user);

        $this->get(route('users.index'))
            ->assertSee('Add User');
    }


    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_see_users_list()
    {
        $user = $this->signInUser();
        $this->createAndSelectChildCustomer($user);

        $this->get(route('users.index'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function unauthenticated_cannot_see_users_list()
    {
        $this->get(route('users.index'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------view create

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_create_users()
    {
        $user = $this->signInMasterAdmin();
        $this->createAndSelectCustomer();

        $this->get(route('users.create'))
            ->assertSee('Add a User');
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_create_users()
    {
        $user = $this->signInAdmin();
        $this->createAndSelectChildCustomer($user);

        $this->get(route('users.create'))
            ->assertSee('Add a User');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_create_users()
    {
        $user = $this->signInUser();
        $this->createAndSelectChildCustomer($user);

        $this->get(route('users.create'))
            ->assertStatus(403);
    }
    //store------------------------------------------------------------------------------------------------------------store

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_create_users()
    {
        $user = $this->signInMasterAdmin();
        $this->saveAndSeeUser($user);
    }

    private function saveAndSeeUser($user)
    {
        $customer = $this->createAndSelectChildCustomer($user);
        $newUser = $this->createUser($customer);

        $this->POST(route('users.store'),$newUser);
        $this->assertDatabaseHas('users',['name' => $newUser['name']]);
    }

    private function createUser($customer)
    {
        $faker = Faker\Factory::create();

        $user['name'] = $faker->name;
        $user['email'] = $faker->email;
        $user['role'] = User::USER;
        $user['user_group_id'] = User::USER;

        return  $user;
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_create_users()
    {
        $user = $this->signInAdmin();
        $this->saveAndSeeUser($user);
    }


    /**
     * @test
     * @return void
     */
    public function a_user_cannot_create_users()
    {
        $user = $this->signInUser();

        $customer = $this->createAndSelectChildCustomer($user);
        $newUser = $this->createUser($customer);

        $this->POST(route('users.store'),$newUser)
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_create_users()
    {

        $this->POST(route('users.store'),[])
            ->assertRedirect(route('login'));
    }

    //show--------------------------------------------------------------------------------------------------------------show

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_users()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectCustomer($user);
        $this->viewUser($customer);
    }

    private function viewUser($customer)
    {
        $user = create(User::class,['user_group_id'=>$customer->id]);

        $this->get(route('users.show',['id' => $user->id] ))
            ->assertSee($user->name);
    }


    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_own_users()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);
        $this->viewUser($customer);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_view_other_customers_users()
    {
        $user = $this->signInAdmin();
        $this->createAndSelectCustomer($user);
        $user = create(User::class);

        $this->get(route('users.show',['id' => $user->id] ))
            ->assertStatus(403);
    }


    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_other_users()
    {
        $user = $this->signInUser();
        $this->createAndSelectCustomer($user);
        $user = create(User::class);

        $this->get(route('users.show',['id' => $user->id] ))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_users()
    {
        $user = create(User::class);

        $this->get(route('users.show',['id' => $user->id] ))
            ->assertRedirect(route('login'));
    }

    //edit--------------------------------------------------------------------------------------------------------------edit

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_view_edit_users()
    {
        $user = $this->signInMasterAdmin();

        $this->viewEditUsers($user);
    }

    private function viewEditUsers($user)
    {
        $customer = $this->createAndSelectChildCustomer($user);

        $newUser = create(User::class,['user_group_id' => $customer->id]);

        $this->get(route('users.edit',['id' => $newUser->id] ))
            ->assertSee($newUser->name);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_view_edit_users()
    {
        $user = $this->signInAdmin();
        $this->viewEditUsers($user);
    }

    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_edit_other_customers_users()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectCustomer($user);

        $user = create(User::class,[]);

        $this->get(route('users.edit',['id' => $user->id] ))
            ->assertStatus(403);
    }


    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_users()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);
        $user = create(User::class,[]);

        $this->get(route('users.edit',['id' => $user->id] ))
            ->assertStatus(403);
    }


    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_edit_users()
    {
        $user = create(User::class,[]);

        $this->get(route('users.edit',['id' => $user->id] ))
            ->assertRedirect(route('login'));
    }

    //update------------------------------------------------------------------------------------------------------------update

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_update_users()
    {
        $this->signInMasterAdmin();
        $customer = $this->createAndSelectCustomer();
        $this->updateUser($customer->id);
    }

    private function updateUser($customer_id)
    {
        $user = create(User::class,['user_group_id'=>$customer_id]);
        //parameters
        $test = $this->PATCH("users/$user->id",['user_group_id' => $customer_id,'name'=> 'changed','email'=> 'changed@yahoo.com','role'=> User::USER]);
        $this->assertDatabaseHas('users',['name' => 'changed']);
    }


    /**
     * @test
     * @return void
     */
    public function an_admin_can_update_their_users()
    {
        $this->signInMasterAdmin();
        $customer = $this->createAndSelectCustomer();
        $this->updateUser($customer->id);
    }

    /**
    * @test
    * @return void
    */
    public function an_admin_cannot_update_other_customers_users()
    {
        $user = $this->signInAdmin();

        $customer = $this->createAndSelectCustomer($user);

        $user = create(User::class,['user_group_id'=>$customer->id]);
        //parameters
        $test = $this->PATCH("users/$user->id",['name'=> 'changed','email'=> 'changed@yahoo.com','role'=> User::USER])
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenicated_cannot_update_customers_agreements()
    {
        $user = create(User::class);
        //parameters
        $this->PATCH("users/$user->id",[])
            ->assertRedirect(route('login'));
    }

    //destroy-----------------------------------------------------------------------------------------------------------destroy


    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_delete_users(){

        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->createAndDeleteUser($customer);
    }

    private function createAndDeleteUser($customer)
    {

        $user = create(User::class,['user_group_id'=>$customer->id]);

        $response = $this->DELETE("/users/{$user->id}");

        $this->assertSoftDeleted('users',['id' => $user->id]);

    }

    /**
     * @test
     * @return void
     */
    public function an_admin_can_delete_their_own_users(){

        $user = $this->signInAdmin();

        $customer = $this->createAndSelectChildCustomer($user);

        $user = create(User::class,['user_group_id'=>$user->user_group_id]);

        $response = $this->DELETE("/users/{$user->id}");

        $this->assertSoftDeleted('users',['id' => $user->id]);

    }

    /**
     * @test
     * @return void
     */
    public function an_admin_cannot_delete_other_customers_users(){

        $user = $this->signInAdmin();

        $customer = $this->createAndSelectCustomer($user);

        $user = create(User::class,['user_group_id'=>$user->user_group_id]);

        $response = $this->DELETE("/users/{$user->id}")
            ->assertStatus(403);;
    }


    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_users()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectCustomer($user);
        $user = create(User::class,['user_group_id'=>$customer->id]);

        $response = $this->DELETE("/users/{$user->id}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_delete_users()
    {
        $user = create(User::class);

        $response = $this->DELETE("/users/{$user->id}")
            ->assertRedirect('login');
    }



    //admin functions----------------------------------------------------------------------------------------------------admin

    /**
     * @test
     * @return void
     */
    public function master_admin_gets_admin_options_with_selected_customer()
    {
        $user = $this->signInMasterAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('home'))
            ->assertSee('Administration Functions');
    }

    /**
     * @test
     * @return void
     */
    public function admin_gets_admin_options_with_selected_customer()
    {
        $user = $this->signInAdmin();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('home'))
            ->assertSee('Administration Functions');
    }

    /**
     * @test
     * @return void
     */
    public function user_doesnt_get_admin_options_with_selected_customer()
    {
        $user = $this->signInUser();
        $customer = $this->createAndSelectChildCustomer($user);

        $this->get(route('home'))
            ->assertDontSee('Administration Functions');
    }

    /**
     * @test
     * @return void
     */
    public function master_admin_gets_admin_options_without_selected_customer()
    {
        $user = $this->signInMasterAdmin();

        $this->get(route('home'))
            ->assertSee('Administration Functions');
    }

    /**
     * @test
     * @return void
     */
    public function admin_gets_admin_options_without_selected_customer()
    {
        $user = $this->signInAdmin();

        $this->get(route('home'))
            ->assertSee('Administration Functions');
    }

    /**
     * @test
     * @return void
     */
    public function user_doesnt_get_admin_options_without_selected_customer()
    {
        $user = $this->signInUser();

        $this->get(route('home'))
            ->assertDontSee('Administration Functions');
    }










}






