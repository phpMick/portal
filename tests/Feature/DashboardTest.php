<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//Models
use App\Models\User;
use App\Models\UserGroup;




class DashboardTest extends TestCase
{

    /**
     * @test
     * @return void
     */
    public function a_master_admin_can_see_dashboard()
    {
        $this->signInMasterAdmin();

        $this->get(route('home'))
            ->assertSee('Cloud Insights Dashboard');
    }


}
