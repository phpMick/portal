<?php

namespace Tests;

use App\Models\User;

use App\Models\UserGroup;
use App\Models\AgreementFactory;
use App\Models\Agreement;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected function signInMasterAdmin()
    {
        //create Master
        $masterGroup = factory(UserGroup::class)->create([
            'name' => UserGroup::MASTER_NAME,
            'type' => UserGroup::MASTER_TYPE
        ]);

        $user = create(User::class,['user_group_id' => $masterGroup->id, 'role' => User::ADMIN]);
        $this->actingAs($user);

        return $user;
    }


    protected function signInAdmin()
    {
        //create customer
        $customer = factory(UserGroup::class,'customer')->create();

        $user = create(User::class,['user_group_id' => $customer->id, 'role' => User::ADMIN]);
        $this->actingAs($user);

        return $user;
    }


    protected function signInUser()
    {
        //create customer
        $customer = factory(UserGroup::class,'customer')->create();

        $user = create(User::class,['user_group_id' => $customer->id, 'role' => User::USER]);
        $this->actingAs($user);

        return $user;
    }









    //This all old------------------------------------------------------------------------------------------------------

    /*protected function signInGlobalAdmin()
    {

        $role = User::ROLE_GLOBAL_ADMIN;

        $user = factory(User::class, 'partner')->create(['roles'=>[$role]]);

        $this->actingAs($user);

        return $user;


    }

    protected function signInPartnerAdmin()
    {

        $role = User::ROLE_PARTNER_ADMIN;

        $user = factory(User::class, 'partner')->create(['roles'=>[$role]]);

        $this->actingAs($user);

        return $user;


    }

    protected function signInPartnerUser()
    {

        $role = user::ROLE_PARTNER_USER;

        $user = factory(user::class, 'partner')->create(['roles'=>[$role]]);

        $this->actingas($user);

        return $user;


    }

    protected function signInPartnerViewAll()
    {

        $role = User::ROLE_PARTNER_VIEWALL;

        $user = factory(User::class, 'partner')->create(['roles'=>[$role]]);

        $this->actingAs($user);

        return $user;

    }




    protected function signInCustomerUser()
    {

        $role = User::ROLE_CUSTOMER_USER;


        $user = factory(User::class, 'customer')->create(['roles'=>[$role]]);

        $this->actingAs($user);

        return $user;

    }


    protected function signInCustomerAdmin()
    {

        $role = User::ROLE_CUSTOMER_ADMIN;


        $user = factory(User::class, 'customer')->create(['roles'=>[$role]]);

        $this->actingAs($user);

        return $user;

    }


    protected function createPartnerCustomerAgreement($user)
    {

        //must be entry in customer_user
        $customer = create(Customer::class);
        $customer->partner_users()->save($user);

        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_CSP,'customer_id' => $customer->id]);

        return $agreement;

    }


    protected function createPartnerOtherCustomerAgreement($user)
    {

        //must be entry in customer_user
        $customer = create(Customer::class);
        $customer->partner_users()->save($user);

        $agreement = create(Agreement::class,['type' => AgreementFactory::TYPE_AZURE_CSP,'customer_id' => $customer->id+1]);

        return $agreement;

    }
*/
    protected function createAndSelectCustomer(){

        $customer = factory(UserGroup::class, 'customer')->create();

        session()->put('selectedUserGroup', $customer);

        return $customer;
    }


    protected function createAndSelectChildCustomer($user){

        $parent = $user->user_group;

        $customer = factory(UserGroup::class, 'customer')->create();

        $parent->appendNode($customer);

        session()->put('selectedUserGroup', $customer);

        return $customer;
    }








}
