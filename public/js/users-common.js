

/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this user?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyUser(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyUser(id){

    axios.delete('/users/' + id, {

    })
        .then(function (response) {

            window.location.replace('/users')

        })
        .catch(function (error) {
            console.log(error);
        });


}


