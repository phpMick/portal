

/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this user group?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyUserGroup(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyUserGroup(id){

    axios.delete('/user-groups/' + id, {

    })
        .then(function (response) {

            window.location.replace('/user-groups')

        })
        .catch(function (error) {
            console.log(error);
        });


}


