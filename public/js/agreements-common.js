

/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this agreement?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyAgreement(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyAgreement(id){

    axios.delete('/agreements/' + id, {

    })
        .then(function (response) {

            window.location.replace('/agreements')

        })
        .catch(function (error) {
            console.log(error);
        });


}


