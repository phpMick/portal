<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', function() {
    return redirect()->route('home');
});


//mbtodo auth?
Route::get('/new-user-reset/{token}/{id}', 'UserController@newUserReset')->name('new-user-reset');
Route::post('/new-user-reset', 'UserController@savePassword')->name('new-user-save-password');




Route::get('/', 'DashboardController@index')->name('home');

Route::get('/message-page', 'DashboardController@messagePage')->name('message-page');


//Route::get('/login/with-aad/{context?}', 'Auth\External\AzureActiveDirectoryController@login')->name('login.aad');
Route::get('/login/with-aad', 'Auth\External\AzureActiveDirectoryController@loginStart')->name('login.aad.start');
Route::get('/login/with-aad/{application?}', 'Auth\External\AzureActiveDirectoryController@loginResponse')->name('login.aad.response');
Route::get('/logout/with-aad/{application?}', 'Auth\External\AzureActiveDirectoryController@logout');

//user groups

Route::group(['middleware'=>'auth'], function() {

    Route::get('user-groups/select', 'UserGroupController@selectList')->name('user-groups.select-list');
    Route::post('user-groups/select', 'UserGroupController@select')->name('user-groups.select');



    Route::get('user-groups/admin', 'UserGroupController@admin')->name('user-groups.admin');
    Route::get('user-groups/link', 'UserGroupController@initiateLink')->name('user-groups.link');
    Route::get('user-groups/select/none', 'UserGroupController@selectNone')->name('user-groups.select-none');


    Route::get('user-groups/deselect/{user_group}', 'UserGroupController@deselect')->name('user-groups.deselect');
    Route::get('user-groups/request-access/{user_group}', 'UserGroupController@requestAccess')->name('user-groups.requestaccess');
    Route::get('user-groups/autocomplete', 'UserGroupController@autocomplete')->name('user-groups.autocomplete');
    Route::resource('user-groups', 'UserGroupController');


    Route::resource('users','UserController');


});



//Created a new group, without the azure prefix
Route::group(['middleware'=>'auth', 'namespace'=>'CloudService'], function() {

    //Agreements
    Route::get('agreements/make', 'AgreementController@make')->name('agreements.make');
    Route::resource('agreements', 'AgreementController');
    Route::get('agreements/{agreement}/delete', 'AgreementController@delete')->name('agreements.delete');
    Route::get('agreements/{agreement}/discover', 'AgreementController@discover')->name('agreements.discover');
    Route::get('agreements/{agreement}/add-subscription', 'AgreementController@addSubscription')->name('agreements.add-subscription');

    //Margins
    Route::get('agreements/{agreement}/margins', 'AgreementController@showMargins')->name('agreements.margins.show');
    Route::get('agreements/{agreement}/margins/edit', 'AgreementController@editMargins')->name('agreements.margins.edit');
    Route::post('agreements/{agreement}/margins', 'AgreementController@storeMargins')->name('agreements.margins.store');


});


Route::group(['middleware'=>'auth', 'namespace'=>'CloudService', 'prefix'=>'/services/azure'], function() {
    Route::get('', 'AzureController@index')->name('azure.index');
    //Route::get('discover', 'AzureController@discover')->name('azure.discover');
    Route::get('subscriptions/{subscription}/usage', 'AzureSubscriptionController@usage')->name('azure.subscriptions.usage');

    Route::resource('subscriptions', 'AzureSubscriptionController', ['as'=>'azure']);
    Route::get('subscriptions/{subscription}/delete', 'AzureSubscriptionController@delete')->name('azure.subscriptions.delete');
    Route::get('subscriptions/{subscription}/connect', 'AzureSubscriptionController@connect')->name('azure.subscriptions.connect');
    Route::post('subscriptions/{subscription}/attach', 'AzureSubscriptionController@attach')->name('azure.subscriptions.attach');
    Route::get('subscriptions/{subscription}/check', 'AzureSubscriptionController@check')->name('azure.subscriptions.check');
});

Route::group(['middleware'=>'auth', 'namespace'=>'CloudService', 'prefix'=>'/services/aad'], function() {
    Route::get('tenants/{aadTenant}/connect/{applicationName?}', 'MicrosoftAadTenantController@connect')->name('aad.tenants.connect');
    Route::post('tenants/{aadTenant}/attach/{applicationName?}', 'MicrosoftAadTenantController@attach')->name('aad.tenants.attach');
});



// TESTS! ---------------------------------------------------------------------------------------------------------------
Route::get('/azure-test', 'CloudService\AzureController@test')->name('azure.test');
Route::get('/tests', 'TestController@index');
Route::get('/tests/ea', 'TestController@eaApi');
Route::get('/tests/partner', 'TestController@partnerApi');
Route::get('/tests/reports', 'TestController@reports');
Route::get('/tests/add-azure-agreement/{which}', 'TestController@addAzureAgreement');

Route::group(['middleware'=>'auth', 'prefix'=>'/data'], function() {
    Route::get('/azure/agreements/count', 'DataController@azureAgreementCount');
    Route::get('/azure/subscriptions/count', 'DataController@azureSubscriptionCount');
    //Route::get('/azure/spend/{month?}', 'DataController@azureSpend');
    //Route::get('/azure/spend2', 'DataController@azureSpend2');
    Route::get('/azure/spend', 'DataController@azureSpend');
});


